export const emptyUser = {
  _id: null,
  email: null,
  name: null,
  language: null,
  last_access: null,
  isPro: false,
}