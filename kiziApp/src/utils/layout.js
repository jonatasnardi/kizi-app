import { Platform } from "react-native";
import Colors from "./colors";
import { getStatusBarHeight } from "react-native-iphone-x-helper";

export const Type = {
  body: {
    fontFamily: Platform.OS === 'ios' ? 'AvenirNextCondensed-Medium' : 'avenir-medium',
    backgroundColor: 'transparent',
    fontSize: 18,
    color: Colors.primary
  },
  wrapper: {
    paddingTop: getStatusBarHeight(),
    paddingLeft: 15,
    paddingRight: 15,
  },
  paddingLateral: {
    paddingLeft: 15,
    paddingRight: 15,
  },
  buttonStyle: {
    width: '100%',
    backgroundColor: Colors.primary,
    alignItems: 'center',
    padding: 20
  },
  buttonText: {
    fontSize: 20,
    color: Colors.white
  },
};