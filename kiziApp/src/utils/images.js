

const images = {
  '1': require('../assets/avatar1.jpg'),
  '2': require('../assets/avatar2.jpg'),
  '3': require('../assets/avatar3.jpg'),
  '4': require('../assets/avatar4.jpg'),
  '5': require('../assets/avatar5.jpg'),
  '6': require('../assets/avatar6.jpg'),
  '7': require('../assets/avatar7.jpg'),
  '8': require('../assets/avatar8.jpg'),
  '9': require('../assets/avatar9.jpg'),
  '10': require('../assets/avatar10.jpg'),
  '11': require('../assets/avatar11.jpg'),
};

export default images;
