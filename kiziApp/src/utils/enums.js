export const REWARDS_ENUM = Object.freeze({
  playedFirstMatch: `Primeira partida jogada`,
  won1WeeklyTournament: `1 torneio semanal conquistado`,
  won1OpenTournament: `1 torneio aberto conquistado`,
  won10OpenTournament: `10 torneios abertos conquistados`,
  joined1OpenTournament: `Entrou em 1 torneio aberto`,
  joined5OpenTournament: `Entrou em 5 torneios abertos`,
  joined50OpenTournament: `Entrou em 50 torneios abertos`,
  joined100OpenTournament: `Entrou em 100 torneios abertos`,
  reached500RankingPoints: `Alcançou 500 pontos ranking`,
  reached1000RankingPoints: `Alcançou 1000 pontos ranking`,
  reached5000RankingPoints: `Alcançou 5000 pontos ranking`,
  reached10000RankingPoints: `Alcançou 10000 pontos ranking`,
  invited1Friend: `Convidou 1 amigo`,
  invited5Friends: `Convidou 5 amigos`,
  added1Question: `Adicionou uma pergunta`,
  hits10Mathematics: `10 acertos em matemática`,
  hits100Mathematics: `100 acertos em matemática`,
  hits500Mathematics: `500 acertos em matemática`,
  hits10Portuguese: `10 acertos em português`,
  hits100Portuguese: `100 acertos em português`,
  hits500Portuguese: `500 acertos em português`,
  hits10Arts: `10 acertos em artes`,
  hits100Arts: `100 acertos em artes`,
  hits500Arts: `500 acertos em artes`,
  hits10Science: `10 acertos em ciências`,
  hits100Science: `100 acertos em ciências`,
  hits500Science: `500 acertos em ciências`,
  hits10History: `10 acertos em história`,
  hits100History: `100 acertos em história`,
  hits500History: `500 acertos em história`,
  hits10English: `10 acertos em inglês`,
  hits100English: `100 acertos em inglês`,
  hits500English: `500 acertos em inglês`,
  hits10Sports: `10 acertos em esportes`,
  hits100Sports: `100 acertos em esportes`,
  hits500Sports: `500 acertos em esportes`,
  hits10Entertainment: `10 acertos em entretenimento`,
  hits100Entertainment: `100 acertos em entretenimento`,
  hits500Entertainment: `500 acertos em entretenimento`,
  hits10General: `10 acertos em geral`,
  hits100General: `100 acertos em geral`,
  hits500General: `500 acertos em geral`,
  hits10Geography: `10 acertos em geografia`,
  hits100Geography: `100 acertos em geografia`,
  hits500Geography: `500 acertos em geografia`,
});

export const CURRENT_ROUND_NAME_ENUM = Object.freeze({
  '0': `Fase de grupos`,
  '1': `Rodada de 64`,
  '2': `Rodada de 32`,
  '3': `Rodada de 16`,
  '4': `Oitavas de final`,
  '5': `Quartas de final`,
  '6': `Semi-final`,
  '7': `Final`,
});



export const CURRENT_ROUND_DAY_NAME_ENUM = Object.freeze({
  '0': 'Sábado',
  '1': 'Domingo',
  '2': 'Segunda-feira',
  '3': 'Terça-feira',
  '4': 'Quarta-feira',
  '5': 'Quarta-feira',
  '6': 'Quinta-feira',
  '7': 'Quinta-feira',
});

export const CURRENT_ROUND_DAY_ENUM = Object.freeze({
  '0': 6,
  '1': 0,
  '2': 1,
  '3': 2,
  '4': 3,
  '5': 3,
  '6': 4,
  '7': 4,
});

export const CURRENT_ROUND_HOUR_ENDS_ENUM = Object.freeze({
  '0': 22,
  '1': 22,
  '2': 22,
  '3': 22,
  '4': 14,
  '5': 22,
  '6': 14,
  '7': 22,
});

export const CURRENT_DIVISION_NAME = Object.freeze({
  '0': 'classificação',
  '1': '1ª divisão',
  '2': '2ª divisão',
  '3': '3ª divisão',
  '4': '4ª divisão',
});

// export const CURRENT_ROUND_QTD_CONFRONTATIONS_ENUM = Object.freeze({
//   '0': null,
//   '1': 64,
//   '2': 32,
//   '3': 16,
//   '4': 8,
//   '5': 4,
//   '6': 2,
//   '7': 1,
// });