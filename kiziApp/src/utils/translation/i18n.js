import I18n from 'react-native-i18n';
import en from '../translation/en';
import es from '../translation/es';
import pt from '../translation/pt';

I18n.fallbacks = true;

I18n.translations = {
  en,
  es,
  pt,
};

export default I18n;
