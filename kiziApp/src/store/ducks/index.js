import { combineReducers } from 'redux';

import { reducer as offline } from 'redux-offline-queue';
import { reducer as userInfo } from './userInfo';

export default combineReducers({
  offline,
  userInfo,
});