import { createActions, createReducer } from 'reduxsauce';
import Immutable from 'seamless-immutable';
import { markActionsOffline } from 'redux-offline-queue';

export const { Types, Creators } = createActions({
  updateItems: ['data'],
  updateUserInfo: ['data'],
  addListRequest: ['lists', 'id'],
  addListSuccess: ['data'],
  updateDeviceLanguage: ['language'],
  updateListsOrder: ['data'],
  updateListsTitleRequest: ['lists'],
  updateListsTitleSuccess: ['data'],
  updateIsPro: ['data'],
  updateIsLogged: ['data'],
  updateToken: ['data'],
  updateNotificationsCount: ['data'],
  updateTkn: ['data'],
  updateChatsCount: ['data'],
  updateChallengesCount: ['data'],
  updateLeaguesCount: ['data'],
  updateFollowersCount: ['data'],
  updateFollowingCount: ['data'],
  updateNotAnsweredQuestionCount: ['data'], 
  updateAwardWeekly: ['data'],
  updateAwardDaily: ['data'],
  updateCurrentRound: ['data'],
  updatePs: ['data'],
  updatePlayCount: ['data'],
  updateRewardPrize: ['data'],
  updateShowRatingModal: ['data'],
  updateShowReward: ['data'],
  updateActiveSound: ['data'],
  updateShowDaily: ['data'],
  updateAlreadyShowImportantWarn: ['data'],
  updateAlreadyRated: ['data'],
  updateBankInfo: ['data'],
  updateLateTasks: ['data'],
  updateDailyTasks: ['data'],
});

// configure actions that needs to work online 
markActionsOffline(Creators, ['addListRequest']);

export const UserInfoTypes = Types;
export default Creators;

// It was created the Types and Creators
// console.log(Types);
// console.log(Creators);

const INITIAL_STATE = {
  currentCash: 7500,
  user: {
    _id: null,
    email: null,
    name: null,
    phone: null,
    isPro: false,
  },
  auth: {
    isLogged: false,
  },
  lists: [],
  tkn: null,
  notificationsCount: 0,
  chatsCount: 0,
  challengesCount: 0,
  leaguesCount: 0,
  followersCount: 0,
  followingCount: 0,
  notAnsweredQuestionCount: 0,
  awardWeekly: 100,
  awardDaily: 10,
  currentRound: 0,
  ps: null,
  playCount: 0,
  rewardPrize: 0,
  showReward: false,
  showRatingModal: false,
  activeSound: true,
  showDaily: true,
  alreadyRated: false,
  alreadyShowImportantWarn: false,
  bankInfo: {
    user: '',
    fullName: '',
    bank: '',
    agency: '',
    account: '',
    cpf: '',
    type: '',
  },
  lateTasks: [],
  dailyTasks: [],
  items: [],
};

// like as reducers

const updateItems = (state = INITIAL_STATE, action) => {
  return { 
    ...state,
    items: action.data
  }
}

const updateUserInfo = (state = INITIAL_STATE, action) => {
  return { 
    ...state,
    user: {
      ...state.user,
      ...action.data,
    }
  }
}

const addListRequest = (state = INITIAL_STATE, action) => {
  return { 
    ...state,
    lists: action.lists
  }
}

const addListSuccess = (state = INITIAL_STATE, action) => {
  return { 
    ...state
  }
}

const updateListsOrder = (state = INITIAL_STATE, action) => {
  return { 
    ...state,
    lists: action.data
  }
}

const updateListsTitleRequest = (state = INITIAL_STATE, action) => {
  

  return { 
    ...state,
    lists: action.lists
  }
}

const updateListsTitleSuccess = (state = INITIAL_STATE, action) => {
  return { 
    ...state
  }
}

const updateDeviceLanguage = (state = INITIAL_STATE, action) => {
  return { 
    ...state,
    user: {
      ...state.user,
      language: action.data
    }
  }
}

const updateIsPro = (state = INITIAL_STATE, action) => {
  return { 
    ...state,
    user: {
      ...state.user,
      isPro: action.data
    }
  }
}

const updateIsLogged = (state = INITIAL_STATE, action) => {
  return { 
    ...state,
    auth: {
      ...state.auth,
      isLogged: action.data,
    }
  }
}

 const updateToken = (state = INITIAL_STATE, action) => {
  return { 
    ...state,
    auth: {
      ...state.auth,
      token: action.data,
    }
  }
}

const updateNotificationsCount = (state = INITIAL_STATE, action) => {
  return { 
    ...state,
    notificationsCount: action.data
  }
}

const updateTkn = (state = INITIAL_STATE, action) => {
  return { 
    ...state,
    tkn: action.data
  }
}

const updateChatsCount = (state = INITIAL_STATE, action) => {
  return { 
    ...state,
    chatsCount: action.data
  }
}

const updateChallengesCount = (state = INITIAL_STATE, action) => {
  return { 
    ...state,
    challengesCount: action.data
  }
}

const updateLeaguesCount = (state = INITIAL_STATE, action) => {
  return { 
    ...state,
    leaguesCount: action.data
  }
}

const updateFollowersCount = (state = INITIAL_STATE, action) => {
  return { 
    ...state,
    followersCount: action.data
  }
}

const updateFollowingCount = (state = INITIAL_STATE, action) => {
  return {
    ...state,
    followingCount: action.data
  }
}

const updateNotAnsweredQuestionCount = (state = INITIAL_STATE, action) => {
  return {
    ...state,
    notAnsweredQuestionCount: action.data
  }
}

const updateAwardWeekly = (state = INITIAL_STATE, action) => {
  return {
    ...state,
    awardWeekly: action.data
  }
}

const updateAwardDaily = (state = INITIAL_STATE, action) => {
  return {
    ...state,
    awardDaily: action.data
  }
}

const updateCurrentRound = (state = INITIAL_STATE, action) => {
  return {
    ...state,
    currentRound: action.data
  }
}

const updatePs = (state = INITIAL_STATE, action) => {
  return {
    ...state,
    ps: action.data
  }
}

const updatePlayCount = (state = INITIAL_STATE, action) => {
  return {
    ...state,
    playCount: action.data
  }
}

const updateRewardPrize = (state = INITIAL_STATE, action) => {
  return {
    ...state,
    rewardPrize: action.data
  }
}

const updateShowRatingModal = (state = INITIAL_STATE, action) => {
  return { 
    ...state,
    showRatingModal: action.data
  }
}

const updateShowReward = (state = INITIAL_STATE, action) => {
  return { 
    ...state,
    showReward: action.data
  }
}



const updateActiveSound = (state = INITIAL_STATE, action) => {
  return { 
    ...state,
    activeSound: action.data
  }
}

const updateShowDaily = (state = INITIAL_STATE, action) => {
  return { 
    ...state,
    showDaily: action.data
  }
}

const updateAlreadyShowImportantWarn = (state = INITIAL_STATE, action) => {
  return { 
    ...state,
    alreadyShowImportantWarn: action.data
  }
}



const updateAlreadyRated = (state = INITIAL_STATE, action) => {
  return { 
    ...state,
    alreadyRated: action.data
  }
}

const updateBankInfo = (state = INITIAL_STATE, action) => {
  return { 
    ...state,
    bankInfo: action.data
  }
}

const updateLateTasks = (state = INITIAL_STATE, action) => {
  return { 
    ...state,
    lateTasks: action.data
  }
}

const updateDailyTasks = (state = INITIAL_STATE, action) => {
  return { 
    ...state,
    dailyTasks: action.data
  }
}

export const reducer = createReducer(INITIAL_STATE, {
  [Types.UPDATE_ITEMS]: updateItems,
  [Types.UPDATE_USER_INFO]: updateUserInfo,
  [Types.ADD_LIST_REQUEST]: addListRequest,
  [Types.ADD_LIST_SUCCESS]: addListSuccess,
  [Types.UPDATE_LISTS_ORDER]: updateListsOrder,
  [Types.UPDATE_LISTS_TITLE_REQUEST]: updateListsTitleRequest,
  [Types.UPDATE_LISTS_TITLE_SUCCESS]: updateListsTitleSuccess,
  [Types.UPDATE_DEVICE_LANGUAGE]: updateDeviceLanguage,
  [Types.UPDATE_IS_PRO]: updateIsPro,
  [Types.UPDATE_IS_LOGGED]: updateIsLogged,
  [Types.UPDATE_TOKEN]: updateToken,
  [Types.UPDATE_NOTIFICATIONS_COUNT]: updateNotificationsCount,
  [Types.UPDATE_TKN]: updateTkn,
  [Types.UPDATE_CHATS_COUNT]: updateChatsCount,
  [Types.UPDATE_CHALLENGES_COUNT]: updateChallengesCount,
  [Types.UPDATE_LEAGUES_COUNT]: updateLeaguesCount,
  [Types.UPDATE_FOLLOWERS_COUNT]: updateFollowersCount,
  [Types.UPDATE_FOLLOWING_COUNT]: updateFollowingCount,
  [Types.UPDATE_NOT_ANSWERED_QUESTION_COUNT]: updateNotAnsweredQuestionCount,
  [Types.UPDATE_AWARD_WEEKLY]: updateAwardWeekly,
  [Types.UPDATE_AWARD_DAILY]: updateAwardDaily,
  [Types.UPDATE_CURRENT_ROUND]: updateCurrentRound,
  [Types.UPDATE_PS]: updatePs,
  [Types.UPDATE_PLAY_COUNT]: updatePlayCount,
  [Types.UPDATE_REWARD_PRIZE]: updateRewardPrize,
  [Types.UPDATE_SHOW_RATING_MODAL]: updateShowRatingModal,
  [Types.UPDATE_SHOW_REWARD]: updateShowReward,
  [Types.UPDATE_ACTIVE_SOUND]: updateActiveSound,
  [Types.UPDATE_SHOW_DAILY]: updateShowDaily,
  [Types.UPDATE_ALREADY_SHOW_IMPORTANT_WARN]: updateAlreadyShowImportantWarn,
  [Types.UPDATE_ALREADY_RATED]: updateAlreadyRated,
  [Types.UPDATE_BANK_INFO]: updateBankInfo,
  [Types.UPDATE_LATE_TASKS]: updateLateTasks,
  [Types.UPDATE_DAILY_TASKS]: updateDailyTasks,
});