import { createStore, applyMiddleware, compose } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import createSagaMiddleware from 'redux-saga';
import {
  offlineMiddleware,
  suspendSaga,
  consumeActionMiddleware,
} from 'redux-offline-queue';

import rootReducer from './ducks';
import rootSaga from './sagas';
import tron from '../config/ReactotronConfig';

const middlewares = [];
const enhancers = [];

const sagaMiddleware = createSagaMiddleware();

middlewares.push(offlineMiddleware());
middlewares.push(suspendSaga(sagaMiddleware));
middlewares.push(consumeActionMiddleware());

enhancers.push(applyMiddleware(...middlewares));

const createAppropriateStore = createStore;

const persistConfig = {
  key: 'organizer',
  storage,
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

const store = createAppropriateStore(
  persistedReducer,
  compose(...enhancers, tron.createEnhancer()),
);

sagaMiddleware.run(rootSaga);

const persistor = persistStore(store);

export { store, persistor };