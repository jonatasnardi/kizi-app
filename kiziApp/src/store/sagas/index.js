import { all, spawn, takeEvery } from 'redux-saga/effects';

import { UserInfoTypes } from '../ducks/userInfo';
import { addList } from './userInfo'
import { startWatchingNetworkConnectivity } from './offline';


export default function* rootSaga() {
  yield all([
    spawn(startWatchingNetworkConnectivity),
    
    takeEvery(UserInfoTypes.ADD_LIST_REQUEST, addList)
  ])
};