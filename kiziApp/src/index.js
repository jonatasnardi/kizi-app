import React from 'react';
import { YellowBox, StatusBar, Text } from 'react-native';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';

import NavigationService from './services/navigator';

import './config/ReactotronConfig';

import Routes from './routes';
import { store, persistor } from './store';
import Colors from './utils/colors';

const App = () => {
  YellowBox.ignoreWarnings([
    'Warning: componentWillMount is deprecated',
    'Warning: componentWillUpdate is deprecated',
    'Warning: componentWillReceiveProps is deprecated',
  ]);

  Text.defaultProps = Text.defaultProps || {};
  Text.defaultProps.allowFontScaling = false;

  return (
  <>
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <StatusBar barStyle='light-content' backgroundColor={Colors.primary} />
        <Routes ref={navigatorRef => {
          NavigationService.setTopLevelNavigator(navigatorRef);
        }}/>
      </PersistGate>
    </Provider>
  </>
  )
};

export default App;