import React from 'react'
import { StyleSheet, View } from 'react-native'
import { isIphoneX } from 'react-native-iphone-x-helper'

import Icon from 'react-native-vector-icons/Ionicons';

export default class MenuButton extends React.Component {
	render() {
		return(
			<View style={{ width: 80 }}>
				<Icon
					name="md-menu"
					color={Colors.white}
					size={32}
					style={styles.menuIcon}
					onPress={() => {this.props.navigation.toggleDrawer()}}
				/>
			</View>
		)
	}
}

const styles = StyleSheet.create({
	menuIcon: {
		zIndex: 9,
		marginTop: isIphoneX() ? 20 : 0,
		marginBottom: 10,
		paddingLeft: 15,
	}
})