import React from 'react';
import Colors from '../../utils/colors';
import { LoaderOverlay } from './styles';

import { DotIndicator } from 'react-native-indicators';

const FullLoader = () => (
  <LoaderOverlay>
    <DotIndicator color={Colors.secondary} count={3} size={16} />
  </LoaderOverlay>   
);

export default FullLoader;