import React from 'react';
import {
	View, 
	Text,
	ScrollView,
	StyleSheet,
	TouchableOpacity,
	Platform,
} from 'react-native';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Icon from 'react-native-vector-icons/FontAwesome5';
import I18n from '../../utils/translation/i18n';

import { Creators as UserInfoActions } from '../../store/ducks/userInfo';

import Colors from '../../utils/colors';
import { Type } from '../../utils/layout';
import { emptyUser } from '../../helpers/shared';
import Images from '../../utils/images';
import { ImageIconUser, NotificationCount, NotificationValue } from './styles';

class Menu extends React.Component {
	appVersion = Platform.select({
    android: '1.4.18',
    ios: '1.4.18',
  });

	navLink(nav, text, logout = false) {
		const { notificationsCount } = this.props;

		return(
			<TouchableOpacity style={{height: 50, flexDirection: 'row'}} onPress={() => this.navigateTo(nav, text, logout)}>
				<Text style={styles.link}>{text}</Text>
				
				{
					nav === 'Notifications' && notificationsCount > 0 && (
						<View style={{ position: 'absolute', right: 15, top: 10 }}>
							<NotificationCount style={{ zIndex: 999999}}>
								<NotificationValue>{notificationsCount}</NotificationValue>
							</NotificationCount>
						</View>
					)
				}

				{
					nav === 'Pro' && (
						<View style={{ position: 'absolute', right: 15, top: 14 }}>
							<Icon name="coins" size={20} color={Colors.yellow} />
						</View>
					)
				}

				{
					nav === 'InviteFriends' && (
						<View style={{ position: 'absolute', right: 15, top: 14 }}>
							<Icon name="user-plus" size={20} color={Colors.secondary} />
						</View>
					)
				}

				{
					nav === 'SearchList' && (
						<View style={{ position: 'absolute', right: 15, top: 14 }}>
							<Icon name="search" size={20} color={Colors.secondary} />
						</View>
					)
				}

				{
					nav === 'UserQuestionDashboard' && (
						<View style={{ position: 'absolute', right: 15, top: 14 }}>
							<Icon name="fire" size={20} color={Colors.secondary} />
						</View>
					)
				}
			</TouchableOpacity>
		)
  }
  
  navigateTo(nav, text, logout = false) {
    if (logout) {
      this.props.updateUserInfo(emptyUser);
    }

    this.props.navigation.navigate(nav);
  }

	render() {
		const { user } = this.props;
		return(
			<View style={styles.container}>
				<ScrollView style={styles.scroller}>
					<View style={styles.topLinks}>
						<View style={styles.profile}>
							<View style={styles.profileText}>
								{
									!user.image ? (
										<ImageIconUser source={Images['1']} />
									) : (
										<ImageIconUser source={{uri: user.image}} />
									)
								}
								<Text style={styles.name}>{this.props.user.name}</Text>
							</View>
						</View>
					</View>
					<View style={styles.bottomLinks}>
						{this.navLink('Main', I18n.t('menuHome'), false)}
						{this.navLink('Profile', I18n.t('menuProfile'), false)}
						{this.navLink('SearchList', 'Procurar usuários', false)}
						{this.navLink('UserQuestionDashboard', 'Perguntas para mim', false)}
						{this.navLink('Notifications', 'Notificações', false)}
						
						{this.navLink('ChatList', 'Chat', false)}
						{this.navLink('InviteFriends', 'Convidar amigos', false)}
						
						{this.navLink('MyLeagues', 'Meus torneios', false)}
						{this.navLink('GetMyMoney', 'Solicitar pagamento', false)}
						{this.navLink('GeneralRanking', 'Ranking geral', false)}
						{this.navLink('Rewards', 'Recompensas', false)}
						{this.navLink('AddQuestion', 'Adicionar perguntas', false)}
						{
							this.props.user.buyCount <= 0 ? (
								this.navLink('Pro', 'Comprar KizoTokens', false)
							) : null
							
						}
						{/* {this.navLink('Profile', 'Recompensas', false)} */}
						{/* {this.navLink('Profile', 'Ganhar KTs', false)} */}
            {this.navLink('Contact', I18n.t('menuContact'), false)}
						{/* { !this.props.user.isPro ? this.navLink('Pro', I18n.t('menuRemoveAds'), false) : null }
            { !this.props.user.isPro ? this.navLink('Pro', I18n.t('menuBecomePro'), false) : null } */}
						{this.navLink('Settings', 'Configurações', false)}
						{this.navLink('Terms', 'Política e Termos', false)}
						{this.navLink('Login', I18n.t('menuQuit'), true)}
						{/* {this.navLink('Contact', 'Versão Pro')} */}
					</View>
				</ScrollView>
				<View style={styles.footer}>
					<Text style={styles.description}>Kizo</Text>
					<Text style={styles.version}>v{ this.appVersion }</Text>
				</View>
			</View>
		)
	}
}


const mapStateToProps = state => ({
  user: state.userInfo.user,
	updateUserInfo: state.userInfo.updateUserInfo,
	notificationsCount: state.userInfo.notificationsCount,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(UserInfoActions, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Menu);

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: Colors.primary,
	},
	scroller: {
		flex: 1,
	},
	profile: {
		flex: 1,
		flexDirection: 'row',
		alignItems: 'center',
		paddingTop: 25,
		borderBottomWidth: 1,
		borderBottomColor: '#777777',
	},
	profileText: {
		flex: 3,
		flexDirection: 'row',
    justifyContent: 'flex-start',
    paddingLeft: 20
	},
	name: {
    ...Type.body,
    fontSize: 20,
    flex: 1,
		paddingBottom: 5,
		color: Colors.white,
		textAlign: 'left',
  },
  flagPro: {
    ...Type.body,
    fontSize: 18,
    fontWeight: 'bold',
		paddingBottom: 3,
		paddingTop: 3,
		paddingLeft: 10,
		paddingRight: 10,
    color: Colors.white,
    backgroundColor: '#FFCC33',
    textAlign: 'center',
    color: Colors.black,
    marginRight: 10,
    borderRadius: 5,
  },

  flagFree: {
    ...Type.body,
    fontSize: 18,
    fontWeight: 'bold',
		paddingBottom: 3,
		paddingTop: 3,
		paddingLeft: 10,
		paddingRight: 10,
    color: Colors.white,
    backgroundColor: Colors.white,
    textAlign: 'center',
    color: Colors.black,
    marginRight: 10,
    borderRadius: 5,
  },
	imgView: {
		flex: 1,
		paddingLeft: 20,
		paddingRight: 20,
	},
	img: {
		height: 70,
		width: 70,
		borderRadius: 50,
	},
	topLinks:{
		height: 160,
		backgroundColor: Colors.primary,
	},
	bottomLinks: {
		flex: 1,
		backgroundColor: 'white',
		paddingTop: 10,
		paddingBottom: 450,
	},
	link: {
    ...Type.body,
		flex: 1,
		fontSize: 20,
		padding: 6,
		paddingLeft: 14,
		margin: 5,
    textAlign: 'left',
    color: Colors.primary
	},
	footer: {
		height: 50,
		flexDirection: 'row',
		alignItems: 'center',
		backgroundColor: 'white',
		borderTopWidth: 1,
		borderTopColor: 'lightgray'
	},
	version: {
		flex: 1, 
		textAlign: 'right',
		marginRight: 20,
		color: 'gray'
	},
	description: {
		flex: 1, 
		marginLeft: 20,
    fontSize: 18,
    color: Colors.primary,
	}
})