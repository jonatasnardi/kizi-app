import { StyleSheet, Animated } from 'react-native';
import styled from 'styled-components/native';
import { Type } from '../../utils/layout';

export const Container = styled(Animated.ScrollView)`
  margin: 0 30px;
`;

export const Code = styled.View`
  background: #FFF;
  padding: 10px;
  align-self: center;
`;

export const Nav = styled.View`
  margin-top: 30px;
  border-top-width: ${StyleSheet.hairlineWidth}px;
  border-top-color: rgba(255, 255, 255, 0.8);
  align-self: stretch;
`;

export const NavItem = styled.View`
  flex-direction: row;
  align-items: center;
  padding: 12px 0;
  border-bottom-width: ${StyleSheet.hairlineWidth}px;
  border-bottom-color: rgba(255, 255, 255, 0.8);
`;

export const NavText = styled.Text`
  font-size: 17px;
  color: #FFF;
  margin-left: 20px;
`;

export const SignOutButton = styled.TouchableOpacity`
  border-width: ${StyleSheet.hairlineWidth}px;
  border-color: rgba(255, 255, 255, 0.8);
  border-radius: 4px;
  justify-content: center;
  align-items: center;
  padding: 12px;
  margin-top: 15px;
  width: 100%;
`;

export const SignOutButtonText = styled.Text`
  font-weight: bold;
  font-size: 15px;
  color: #FFF;
`;

export const ImageIconUser = styled.Image`
  width: 30px;
  height: 30px;
  top: -3px;
  border-radius: 30px;
  border: 2px solid ${Colors.secondary};
  margin-right: 10px;
`;

export const NotificationCount = styled.View`
  width: 30px;
  height: 30px;
  position: relative;
  border-radius: 30px;
  background: ${Colors.red};
  z-index: 9999999;
  align-items: center;
  justify-content: center;
`;

export const NotificationValue = styled.Text`
  ${Type.body};
  color: ${Colors.white};
  font-size: 14px;
  text-align: center;
`;