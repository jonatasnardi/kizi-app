import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { ButtonContainer, ButtonText } from './styles';
import Colors from '../../../utils/colors';

const ButtonSecondary = ({ onPress, title, backgroundColor, textColor, icon }) => (
  <ButtonContainer 
    style={{ backgroundColor: backgroundColor ? backgroundColor : Colors.white}} 
    onPress={onPress}>
    <ButtonText 
      style={{ color: textColor ? textColor : Colors.darkGreen}}>
      {title}
    </ButtonText>

    {
      icon && (
        <Icon style={{ position: 'absolute', right: 15 }} name={icon} size={20} color={textColor ? textColor : Colors.darkGreen} />
      )
    }
  </ButtonContainer>
);

export default ButtonSecondary;