import styled from 'styled-components/native';
import { Type } from '../../../utils/layout';
import Colors from '../../../utils/colors';

export const ButtonContainer = styled.TouchableOpacity`
  padding: 10px;
  align-items: center;
  justify-content: center;
  border-radius: 5px;
  background: ${Colors.white};
`;

export const ButtonText = styled.Text`
  ${Type.body};
  font-size: 20px;
  color: ${Colors.primary};
`;