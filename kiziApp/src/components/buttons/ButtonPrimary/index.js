import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { ButtonContainer, ButtonText } from './styles';
import Colors from '../../../utils/colors';

const ButtonPrimary = ({ onPress, title, backgroundColor, textColor, icon, fontSize, disabled = false }) => (
  <ButtonContainer 
    style={{ backgroundColor: backgroundColor ? backgroundColor : Colors.secondary}} 
    onPress={onPress}
    disabled={disabled}
    opacity={0.5}>
    <ButtonText 
      style={{ color: textColor ? textColor : Colors.white, fontSize: fontSize ? fontSize : 20, textAlign: 'center'}}
      allowFontScaling={false}>
      {title}
    </ButtonText>

    {
      icon && (
        <Icon style={{ position: 'absolute', right: 15 }} name={icon} size={20} color={textColor ? textColor : Colors.white} />
      )
    }
    
  </ButtonContainer>
);
export default ButtonPrimary;