import styled from 'styled-components/native';
import { Type } from '../../../utils/layout';
import Colors from '../../../utils/colors';

export const MinTextComp = styled.Text`
  ${Type.body};
  font-size: 14px;
  color: ${Colors.primary};
`;