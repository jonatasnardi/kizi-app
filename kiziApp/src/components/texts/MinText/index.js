import React from 'react';
import { MinTextComp } from './styles';
import Colors from '../../../utils/colors';

const MinText = ({ text, color }) => (
  <MinTextComp
    style={{ color: color ? color : Colors.primary}}>
    {text}
  </MinTextComp>
);

export default MinText;