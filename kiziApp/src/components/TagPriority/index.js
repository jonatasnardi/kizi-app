import React from 'react';
import Colors from '../../utils/colors';
import { Container } from './styles';

const TagPriority = ({ color }) => (
  <Container style={{ backgroundColor: color ? color : Colors.blue }}>
  </Container>   
);

export default TagPriority;