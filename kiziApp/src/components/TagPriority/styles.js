import styled from 'styled-components/native';

export const Container = styled.View`
  width: 4px;
  height: 42px;
  margin-top: 4px;
  background: ${Colors.red};
  position: absolute;
  top: 0;
  right: 0;
  elevation: 3;
`;