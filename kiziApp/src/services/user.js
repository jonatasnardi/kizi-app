import axios from 'axios';
import { BASE_URL_API } from '../helpers/variables';

import { store } from '../store';


// const state = store.getState();
// let authToken = state.userInfo.auth.token;

// store.subscribe(listener);

// function select(state) {
//   return state.userInfo.auth.token;
// }

// function listener() {
//   let token = select(store.getState());
//   authToken = token;
// }

export const registerUser = (body, deviceId) => {
  return axios(`${BASE_URL_API}/user/register`, {
    method: 'POST',
    headers: {
      // // authorization: `Bearer ${authToken}`,
    },
    data: {
      ...body,
      deviceId,
    },
  })
    .then(response => response.data)
    .catch(error => {
      throw error.response;
    });
};

export const createRewards = (userId) => {
  return axios(`${BASE_URL_API}/reward`, {
    method: 'POST',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
    data: {
      user: userId,
    },
  })
    .then(response => response.data)
    .catch(error => {
      throw error.response;
    });
};

export const updateRewards = (userId, payload, ktsToEarn) => {
  return axios(`${BASE_URL_API}/reward/update-by-user-id/${userId}`, {
    method: 'PUT',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
    data: {
      payload,
      ktsToEarn,
    },
  })
    .then(response => response.data)
    .catch(error => {
      throw error.response;
    });
};

export const getUserRewards = (id) => {
  return axios(`${BASE_URL_API}/reward/get-by-user-id/${id}`, {
    method: 'GET',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
  })
    .then(response => response.data)
    .catch(error => {
      throw error.response;
    });
};

export const searchUsers = (text) => {
  return axios(`${BASE_URL_API}/user/search-users/${text}`, {
    method: 'GET',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
  })
    .then(response => response.data)
    .catch(error => {
      throw error.response;
    });
};

export const updateUserRewardId = (userId, rewardId) => {
  return axios(`${BASE_URL_API}/reward/update-user-reward/${userId}/${rewardId}`, {
    method: 'PUT',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
    data: {
      rewardId
    },
  })
    .then(response => response.data)
    .catch(error => {
      throw error.response;
    });
};


export const editProfile = (body) => {
  return axios(`${BASE_URL_API}/user/${body._id}`, {
    method: 'PUT',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
    data: {
      name: body.name,
      email: body.email,
      phone: body.phone,
      username: body.username,
    }
  })
    .then(response => response.data)
    .catch(error => {
      throw error.response;
    });
};

export const deleteAccount = (id) => {
  return axios(`${BASE_URL_API}/user/${id}`, {
    method: 'DELETE',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
  })
    .then(response => response.data)
    .catch(error => {
      throw error.response;
    });
};

export const updateAvatar = (id, avatar) => {
  return axios(`${BASE_URL_API}/user/${id}/avatar`, {
    method: 'PUT',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
    data: {
      avatar,
    }
  })
    .then(response => response.data)
    .catch(error => {
      throw error.response;
    });
};

export const updateImageProfile = (id, image) => {
  return axios(`${BASE_URL_API}/user/${id}/profile-image`, {
    method: 'PUT',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
    data: {
      image,
    }
  })
    .then(response => response.data)
    .catch(error => {
      throw error.response;
    });
};

export const toPro = (id) => {
  return axios(`${BASE_URL_API}/user/toPro/${id}`, {
    method: 'PUT',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
    data: {}
  })
    .then(response => response.data)
    .catch(error => {
      throw error;
    });
};

export const doLogin = (email, password = '') => {
  return axios(`${BASE_URL_API}/user/authenticate`, {
    method: 'POST',
    headers: {
      accept: 'application/json',
      // // authorization: `Bearer ${authToken}`,
    },
    data: {
      email,
      password
    },
  })
    .then(response => response.data)
    .catch(error => {
      throw error.response;
    });
};

export const userContact = (message, id) => {
  return axios(`${BASE_URL_API}/contact`, {
    method: 'POST',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
    data: {
      user: id,
      message
    },
  })
    .then(response => response.data)
    .catch(error => {
      throw error;
    });
};

export const forgotPassword = (email) => {
  return axios(`${BASE_URL_API}/user/forgot-password`, {
    method: 'POST',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
    data: {
      email,
    },
  })
    .then(response => response.data)
    .catch(error => {
      throw error.response;
    });
};

export const changePassword = (email, password) => {
  return axios(`${BASE_URL_API}/user/change-password`, {
    method: 'POST',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
    data: {
      email,
      password,
    },
  })
    .then(response => response.data)
    .catch(error => {
      throw error.response;
    });
};

export const getUserInfo = (id) => {
  return axios(`${BASE_URL_API}/user/${id}`, {
    method: 'GET',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
  })
    .then(response => response.data)
    .catch(error => {
      throw error.response;
    });
};

export const getUserInfoProfile = (id, myId) => {
  return axios(`${BASE_URL_API}/user/info-profile/${id}/${myId}`, {
    method: 'GET',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
  })
    .then(response => response.data)
    .catch(error => {
      throw error;
    });
};

