import axios from 'axios';
import { BASE_URL_API } from '../helpers/variables';

import { store } from '../store';


// const state = store.getState();
// let authToken = state.userInfo.auth.token;

// store.subscribe(listener);

// function select(state) {
//   return state.userInfo.auth.token;
// }

// function listener() {
//   let token = select(store.getState());
//   authToken = token;
// }

export const addUserQuestion = (from, to, question, anonymous) => {
  return axios(`${BASE_URL_API}/userQuestion`, {
    method: 'POST',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
    data: {
      from,
      to,
      question,
      anonymous,
    }
  })
    .then(response => response.data)
    .catch(error => {
      throw error.response;
    });
};

export const answerUserQuestion = (id, answer) => {
  return axios(`${BASE_URL_API}/userQuestion/${id}`, {
    method: 'PUT',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
    data: {
      answer,
    }
  })
    .then(response => response.data)
    .catch(error => {
      throw error.response;
    });
};

export const getUserQuestionById = (id) => {
  return axios(`${BASE_URL_API}/userQuestion/${id}`, {
    method: 'GET',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
  })
    .then(response => response.data)
    .catch(error => {
      throw error.response;
    });
};

export const getUserMyQuestionNotAnswered = (id) => {
  return axios(`${BASE_URL_API}/userQuestion/questions-my-list/not-answered/${id}`, {
    method: 'GET',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
  })
    .then(response => response.data)
    .catch(error => {
      throw error.response;
    });
};

export const getUserQuestionBeMeNotAnswered = (id) => {
  return axios(`${BASE_URL_API}/userQuestion/question-list-by-me/not-answered/${id}`, {
    method: 'GET',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
  })
    .then(response => response.data)
    .catch(error => {
      throw error.response;
    });
};

export const getUserQuestionBeMeAnswered = (id) => {
  return axios(`${BASE_URL_API}/userQuestion/question-list-by-me/answered/${id}`, {
    method: 'GET',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
  })
    .then(response => response.data)
    .catch(error => {
      throw error.response;
    });
};

export const getUserQuestionUser = (id) => {
  return axios(`${BASE_URL_API}/userQuestion/question-list-by-user/${id}`, {
    method: 'GET',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
  })
    .then(response => response.data)
    .catch(error => {
      throw error.response;
    });
};

export const deleteUserQuestion = (id) => {
  return axios(`${BASE_URL_API}/userQuestion/${id}`, {
    method: 'DELETE',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
    data: {
    },
  })
    .then(response => response.data)
    .catch(error => {
      throw error.response;
    });
};