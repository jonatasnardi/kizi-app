import axios from 'axios';
import { BASE_URL_API } from '../helpers/variables';

import { store } from '../store';


// const state = store.getState();
// let authToken = state.userInfo.auth.token;

// store.subscribe(listener);

// function select(state) {
//   return state.userInfo.auth.token;
// }

// function listener() {
//   let token = select(store.getState());
//   authToken = token;
// }

export const addSuggestionQuestion = (userId, payload) => {
  return axios(`${BASE_URL_API}/question/suggestion`, {
    method: 'POST',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
    data: {
      ...payload,
      addedBy: userId,
    }
  })
    .then(response => response.data)
    .catch(error => {
      throw error.response;
    });
};

export const get6RandomQuestions = () => {
  return axios(`${BASE_URL_API}/question/random/6`, {
    method: 'GET',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
  })
    .then(response => response.data)
    .catch(error => {
      throw error.response;
    });
};