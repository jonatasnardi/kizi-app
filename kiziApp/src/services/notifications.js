import axios from 'axios';
import { BASE_URL_API } from '../helpers/variables';

import { store } from '../store';


// const state = store.getState();
// let authToken = state.userInfo.auth.token;

// store.subscribe(listener);

// function select(state) {
//   return state.userInfo.auth.token;
// }

// function listener() {
//   let token = select(store.getState());
//   authToken = token;
// }

export const getNotifications = (userId) => {
  return axios(`${BASE_URL_API}/notification/by-user/${userId}`, {
    method: 'GET',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
  })
    .then(response => response.data)
    .catch(error => {
      throw error.response;
    });
};

export const setAllNotificationsRead = (userId) => {
  return axios(`${BASE_URL_API}/notification/set-all-read-by-user/${userId}`, {
    method: 'PUT',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
    data: {
      
    }
  })
    .then(response => response.data)
    .catch(error => {
      throw error.response;
    });
};