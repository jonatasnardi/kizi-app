import axios from 'axios';
import NavigatorService from './navigator';
import { BASE_URL_API } from '../helpers/variables';
import { doLogin } from './user';
import { store } from '../store';
import { showAlert, showToast } from '../utils/functions';

// const state = store.getState();
// let ps = store.getState().userInfo.ps;
// let email = store.getState().userInfo.user.email;

// store.subscribe(listener);

// function select(state) {
//   return state.userInfo.ps;
// }
// function selectEmail(state) {
//   return state.userInfo.user.email;
// }

// function listener() {
//   let userEmail = selectEmail(store.getState());
//   let userPs = select(store.getState());
//   email = userEmail;
//   ps = userPs;
// }
const api = axios.create({
  baseURL: BASE_URL_API,
});

const onRequestSuccess = config => {
  if (store) {
    const state = store.getState();
    let authToken = state.userInfo.auth.token;
    if (authToken) {
      config.headers.Authorization = `Bearer ${authToken}`;
    }
  }
  
  return config;
};
const onRequestFail = error => {
  console.log("request error", error);
  return Promise.reject(error);
};
axios.interceptors.request.use(onRequestSuccess, onRequestFail);

const onResponseSuccess = response => {
  return response;
};
const onResponseFail = error => {
  // console.tron.log('1', error);
  // console.tron.log('2', error.response);
  
  const status = error.status || error.response.status;
  const message = error.response.request._response.toLowerCase();
  const url = error.response.request._url;
  if (status === 401 && message.indexOf('token') > -1) {
    try {
      // tentar logar de novo
      // doLogin(email, ps)
      //   .then((data) => { 
      //     // store.dispatch(updateToken('StackOverflow)
      //   })
      //   .catch((error) => {
      //     console.log(error);
      //     NavigatorService.navigate('Login', { invalidToken: true });
          
      //     showAlert('Erro', 'Ê necessário logar novamente');
      //   }); 
      if ((url.indexOf('http://localhost:3000/user/') > -1 && url.length <= 53) || (url.indexOf('https://kizoapp.com.br/user/') > -1 && url.length <= 53)) {

      } else {
        NavigatorService.navigateRoot('Main');
        showToast('Erro', 'É necessário logar novamente');
      }
      

      
      
    } catch (err) {
    }
    
  }
  return Promise.reject(error);
};
axios.interceptors.response.use(onResponseSuccess, onResponseFail);

export default api;