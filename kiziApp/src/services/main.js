import axios from 'axios';
import { BASE_URL_API } from '../helpers/variables';

export const getLists = (_id) => {
  return axios(`${BASE_URL_API}/list/byUser/${_id}`, {
    method: 'GET',
    headers: {
    },
    // data: payload,
  })
    .then(response => response.data)
    .catch(error => {
      throw error;
    });
};

export const updateListsOrder = (lists) => {
  console.log(lists)
  return axios(`${BASE_URL_API}/list/update/order`, {
    method: 'PUT',
    headers: {
    },
    data: {
      lists: lists
    },
  })
    .then(response => response.data)
    .catch(error => {
      throw error;
    });
};

export const addListPost = (title, userId) => {
  return axios(`${BASE_URL_API}/list`, {
    method: 'POST',
    headers: {
    },
    data: {
      id_user: userId,
      icon: '',
      title: title
    },
  })
    .then(response => response.data)
    .catch(error => {
      throw error;
    });
};

export const updateListTitle = (id, title) => {
  return axios(`${BASE_URL_API}/list/${id}`, {
    method: 'PUT',
    headers: {
    },
    data: {
      icon: '',
      title: title
    },
  })
    .then(response => response.data)
    .catch(error => {
      throw error;
    });
};

export const deleteList = (id) => {
  return axios(`${BASE_URL_API}/list/${id}`, {
    method: 'DELETE',
    headers: {
    },
  })
    .then(response => response.data)
    .catch(error => {
      throw error;
    });
};


