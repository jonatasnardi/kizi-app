import axios from 'axios';
import { BASE_URL_API } from '../helpers/variables';

import { store } from '../store';


// const state = store.getState();
// let authToken = state.userInfo.auth.token;

// store.subscribe(listener);

// function select(state) {
//   return state.userInfo.auth.token;
// }

// function listener() {
//   let token = select(store.getState());
//   authToken = token;
// }

export const requestPayment = (id, payload) => {
  return axios(`${BASE_URL_API}/requestPayment`, {
    method: 'POST',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
    data: {
      user: id,
      fullName: payload.fullName,
      account: payload.account,
      cpf: payload.cpf,
      bank: payload.bank,
      agency: payload.agency,
      type: payload.type,
    },
  })
    .then(response => response.data)
    .catch(error => {
      throw error.response;
    });
};