import axios from 'axios';
import { BASE_URL_API } from '../helpers/variables';

import { store } from '../store';


// const state = store.getState();
// let authToken = state.userInfo.auth.token;

// store.subscribe(listener);

// function select(state) {
//   return state.userInfo.auth.token;
// }

// function listener() {
//   let token = select(store.getState());
//   authToken = token;
// }

export const sendMessage = (user, to, message) => {
  return axios(`${BASE_URL_API}/chat`, {
    method: 'POST',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
    data: {
      user,
      to,
      message,
    },
  })
    .then(response => response.data)
    .catch(error => {
      throw error.response;
    });
};

export const getMessages = (userId, toId) => {
  return axios(`${BASE_URL_API}/chat/messages-list/${userId}/${toId}`, {
    method: 'GET',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
  })
    .then(response => response.data)
    .catch(error => {
      throw error;
    });
};


export const getChats = (userId) => {
  return axios(`${BASE_URL_API}/chatList/by-user/${userId}`, {
    method: 'GET',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
  })
    .then(response => response.data)
    .catch(error => {
      throw error;
    });
};

export const setAsRead = (userId, withId) => {
  return axios(`${BASE_URL_API}/chatList/set-as-read/${userId}/${withId}`, {
    method: 'PUT',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
    data: {
    },
  })
    .then(response => response.data)
    .catch(error => {
      throw error.response;
    });
};