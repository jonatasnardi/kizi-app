import axios from 'axios';
import { BASE_URL_API } from '../helpers/variables';

import { store } from '../store';


// const state = store.getState();
// let authToken = state.userInfo.auth.token;

// store.subscribe(listener);

// function select(state) {
//   return state.userInfo.auth.token;
// }

// function listener() {
//   let token = select(store.getState());
//   authToken = token;
// }

export const getUserRankingList = (group) => {
  return axios(`${BASE_URL_API}/league/list/weekly-ranking-mata-mata/${group}`, {
    method: 'GET',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
  })
    .then(response => response.data)
    .catch(error => {
      throw error.response;
    });
};

export const getActiveMataMataRanking = (group) => {
  return axios(`${BASE_URL_API}/mata/ranking/mata-mata`, {
    method: 'GET',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
  })
    .then(response => response.data)
    .catch(error => {
      throw error.response;
    });
};

export const getUserDailyRankingList = () => {
  return axios(`${BASE_URL_API}/league/list/daily-ranking`, {
    method: 'GET',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
  })
    .then(response => response.data)
    .catch(error => {
      throw error.response;
    });
};

export const getGeneralRanking = () => {
  return axios(`${BASE_URL_API}/league/list/general-ranking`, {
    method: 'GET',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
  })
    .then(response => response.data)
    .catch(error => {
      throw error.response;
    });
};

export const getRankingWinnersWeekly = () => {
  return axios(`${BASE_URL_API}/league/list/general-ranking-titles`, {
    method: 'GET',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
  })
    .then(response => response.data)
    .catch(error => {
      throw error.response;
    });
};



export const getLeagues = () => {
  return axios(`${BASE_URL_API}/league/`, {
    method: 'GET',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
  })
    .then(response => response.data)
    .catch(error => {
      throw error.response;
    });
};

export const getLeague = (id) => {
  return axios(`${BASE_URL_API}/league/${id}`, {
    method: 'GET',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
  })
    .then(response => response.data)
    .catch(error => {
      throw error.response;
    });
};

export const createLeague = (payload) => {
  return axios(`${BASE_URL_API}/league`, {
    method: 'POST',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
    data: {
      title: payload.title,
      award: payload.award,
      userQtd: payload.userQtd,
      createdBy: payload.createdBy,
      priceToJoin: payload.priceToJoin,
    },
  })
    .then(response => response.data)
    .catch(error => {
      throw error.response;
    });
};

export const listUsersOfLeague = (idLeague) => {
  return axios(`${BASE_URL_API}/userLeague/list-users-of-league/${idLeague}`, {
    method: 'GET',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
  })
    .then(response => response.data)
    .catch(error => {
      throw error.response;
    });
};

export const updateUserPointsLeague = (idLeague, idUser, payload) => {
  return axios(`${BASE_URL_API}/userLeague/${idLeague}/${idUser}/user-points`, {
    method: 'PUT',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
    data: {
      hits: payload.hits,
      fails: payload.fails,
      points: payload.points,
      hitsMathematics: payload.hitsMathematics,
      failsMathematics: payload.failsMathematics,
      hitsPortuguese: payload.hitsPortuguese,
      failsPortuguese: payload.failsPortuguese,
      hitsArts: payload.hitsArts,
      failsArts: payload.failsArts,
      hitsScience: payload.hitsScience,
      failsScience: payload.failsScience,
      hitsHistory: payload.hitsHistory,
      failsHistory: payload.failsHistory,
      hitsEnglish: payload.hitsEnglish,
      failsEnglish: payload.failsEnglish,
      hitsSports: payload.hitsSports,
      failsSports: payload.failsSports,
      hitsEntertainment: payload.hitsEntertainment,
      failsEntertainment: payload.failsEntertainment,
      hitsGeography: payload.hitsGeography,
      failsGeography: payload.failsGeography,
      hitsGeneral: payload.hitsGeneral,
      failsGeneral: payload.failsGeneral,
    },
  })
    .then(response => response.data)
    .catch(error => {
      throw error.response;
    });
};


export const vinculeUserLeague = (idLeague, idUser, deviceId) => {
  return axios(`${BASE_URL_API}/userLeague`, {
    method: 'POST',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
    data: {
      idUser: idUser,
      idLeague: idLeague,
      deviceId,
    },
  })
    .then(response => response.data)
    .catch(error => {
      throw error.response;
    });
};