import axios from 'axios';

import { BASE_URL_API } from '../helpers/variables';

import { store } from '../store';

// const state = store.getState();
// let authToken = state.userInfo.auth.token;

// store.subscribe(listener);

// function select(state) {
//   return state.userInfo.auth.token;
// }

// function listener() {
//   let token = select(store.getState());
//   authToken = token;
// }

export const removeUserKTs = (userId, qtd) => {
  return axios(`${BASE_URL_API}/general/remove-kts/${userId}`, {
    method: 'PUT',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
    data: {
      qtd,
    }
  })
    .then(response => response.data)
    .catch(error => {
      throw error.response;
    });
};

export const addUserKTs = (userId, qtd) => {
  return axios(`${BASE_URL_API}/general/add-kts/${userId}`, {
    method: 'PUT',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
    data: {
      qtd,
    }
  })
    .then(response => response.data)
    .catch(error => {
      throw error.response;
    });
};

export const addUserMoney = (userId) => {
  return axios(`${BASE_URL_API}/general/add-mny/${userId}`, {
    method: 'PUT',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
    data: {
    }
  })
    .then(response => response.data)
    .catch(error => {
      throw error.response;
    });
};

export const setPlayerId = (userId, playerId) => {
  return axios(`${BASE_URL_API}/general/set-player-id/${userId}`, {
    method: 'PUT',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
    data: {
      playerId,
    }
  })
    .then(response => response.data)
    .catch(error => {
      throw error.response;
    });
};

export const setDeviceId = (userId, deviceId) => {
  return axios(`${BASE_URL_API}/general/set-device-id/new/${userId}`, {
    method: 'PUT',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
    data: {
      deviceId,
    }
  })
    .then(response => response.data)
    .catch(error => {
      throw error.response;
    });
};

export const setLastAccess = (userId, lastAccess) => {
  return axios(`${BASE_URL_API}/general/set-last-access/${userId}`, {
    method: 'PUT',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
    data: {
      lastAccess,
    }
  })
    .then(response => response.data)
    .catch(error => {
      throw error.response;
    });
};

export const updateValuesAfterWeeklyRound = (userId, payload) => {
  return axios(`${BASE_URL_API}/general/finish-round/weekly-tournament/${userId}`, {
    method: 'PUT',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
    data: {
      hits: payload.hits,
      fails: payload.fails,
      points: payload.points,
      hitsMathematics: payload.hitsMathematics,
      failsMathematics: payload.failsMathematics,
      hitsPortuguese: payload.hitsPortuguese,
      failsPortuguese: payload.failsPortuguese,
      hitsArts: payload.hitsArts,
      failsArts: payload.failsArts,
      hitsScience: payload.hitsScience,
      failsScience: payload.failsScience,
      hitsHistory: payload.hitsHistory,
      failsHistory: payload.failsHistory,
      hitsEnglish: payload.hitsEnglish,
      failsEnglish: payload.failsEnglish,
      hitsSports: payload.hitsSports,
      failsSports: payload.failsSports,
      hitsEntertainment: payload.hitsEntertainment,
      failsEntertainment: payload.failsEntertainment,
      hitsGeography: payload.hitsGeography,
      failsGeography: payload.failsGeography,
      hitsGeneral: payload.hitsGeneral,
      failsGeneral: payload.failsGeneral,
    }
  })
    .then(response => response.data)
    .catch(error => {
      throw error.response;
    });
};

export const updateValuesAfterDailyRound = (userId, payload) => {
  return axios(`${BASE_URL_API}/general/finish-round/daily-tournament/${userId}`, {
    method: 'PUT',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
    data: {
      hits: payload.hits,
      fails: payload.fails,
      points: payload.points,
      hitsMathematics: payload.hitsMathematics,
      failsMathematics: payload.failsMathematics,
      hitsPortuguese: payload.hitsPortuguese,
      failsPortuguese: payload.failsPortuguese,
      hitsArts: payload.hitsArts,
      failsArts: payload.failsArts,
      hitsScience: payload.hitsScience,
      failsScience: payload.failsScience,
      hitsHistory: payload.hitsHistory,
      failsHistory: payload.failsHistory,
      hitsEnglish: payload.hitsEnglish,
      failsEnglish: payload.failsEnglish,
      hitsSports: payload.hitsSports,
      failsSports: payload.failsSports,
      hitsEntertainment: payload.hitsEntertainment,
      failsEntertainment: payload.failsEntertainment,
      hitsGeography: payload.hitsGeography,
      failsGeography: payload.failsGeography,
      hitsGeneral: payload.hitsGeneral,
      failsGeneral: payload.failsGeneral,
    }
  })
    .then(response => response.data)
    .catch(error => {
      throw error.response;
    });
};

export const listLeaguesFromUser = (idUser) => {
  return axios(`${BASE_URL_API}/general/user-leagues/${idUser}`, {
    method: 'GET',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
  })
    .then(response => response.data)
    .catch(error => {
      throw error.response;
    });
};

export const getCurrentRoundInfo = () => {
  return axios(`${BASE_URL_API}/config/current/info`, {
    method: 'GET',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
  })
    .then(response => response.data)
    .catch(error => {
      throw error.response;
    });
};



export const lastWeekWinner = () => {
  return axios(`${BASE_URL_API}/weekWinner/last/list`, {
    method: 'GET',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
  })
    .then(response => response.data)
    .catch(error => {
      throw error.response;
    });
};

export const addInvitationCode = (indicator) => {
  return axios(`${BASE_URL_API}/indication`, {
    method: 'POST',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
    data: {
      indicator,
    },
  })
    .then(response => response.data)
    .catch(error => {
      throw error.response;
    });
};