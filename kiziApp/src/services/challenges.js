import axios from 'axios';
import { BASE_URL_API } from '../helpers/variables';

import { store } from '../store';


// const state = store.getState();
// let authToken = state.userInfo.auth.token;

// store.subscribe(listener);

// function select(state) {
//   return state.userInfo.auth.token;
// }

// function listener() {
//   let token = select(store.getState());
//   authToken = token;
// }

export const challengeUser = (user, challenged) => {
  return axios(`${BASE_URL_API}/challenge`, {
    method: 'POST',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
    data: {
      user,
      challenged
    },
  })
    .then(response => response.data)
    .catch(error => {
      throw error.response;
    });
};

export const getChallengeQuestions = (idChallenge) => {
  return axios(`${BASE_URL_API}/challenge/${idChallenge}`, {
    method: 'GET',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
  })
    .then(response => response.data)
    .catch(error => {
      throw error.response;
    });
};

export const getSendChallenges = (userId) => {
  return axios(`${BASE_URL_API}/challenge/challenge-send-list-by-user/${userId}`, {
    method: 'GET',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
  })
    .then(response => response.data)
    .catch(error => {
      throw error.response;
    });
};


export const getWaitingYouChallenges = (userId) => {
  return axios(`${BASE_URL_API}/challenge/challenge-waiting-you-list-by-user/${userId}`, {
    method: 'GET',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
  })
    .then(response => response.data)
    .catch(error => {
      throw error.response;
    });
};

export const getOpponentInfo = (opponentId, userId) => {
  return axios(`${BASE_URL_API}/user/info/challenges/${opponentId}/${userId}`, {
    method: 'GET',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
  })
    .then(response => response.data)
    .catch(error => {
      throw error.response;
    });
};

export const updateValuesChallenge = (userId, payload, challengeId, messageToOpponent) => {
  return axios(`${BASE_URL_API}/challenge/${challengeId}/${userId}/user-points`, {
    method: 'PUT',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
    data: {
      hits: payload.hits,
      fails: payload.fails,
      points: payload.points,
      hitsMathematics: payload.hitsMathematics,
      failsMathematics: payload.failsMathematics,
      hitsPortuguese: payload.hitsPortuguese,
      failsPortuguese: payload.failsPortuguese,
      hitsArts: payload.hitsArts,
      failsArts: payload.failsArts,
      hitsScience: payload.hitsScience,
      failsScience: payload.failsScience,
      hitsHistory: payload.hitsHistory,
      failsHistory: payload.failsHistory,
      hitsEnglish: payload.hitsEnglish,
      failsEnglish: payload.failsEnglish,
      hitsSports: payload.hitsSports,
      failsSports: payload.failsSports,
      hitsEntertainment: payload.hitsEntertainment,
      failsEntertainment: payload.failsEntertainment,
      hitsGeography: payload.hitsGeography,
      failsGeography: payload.failsGeography,
      hitsGeneral: payload.hitsGeneral,
      failsGeneral: payload.failsGeneral,
      messageToOpponent,
    }
  })
    .then(response => response.data)
    .catch(error => {
      throw error.response;
    });
};


export const createChallenge = (user, challenged) => {
  return axios(`${BASE_URL_API}/challenge`, {
    method: 'POST',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
    data: {
      user,
      challenged,
    },
  })
    .then(response => response.data)
    .catch(error => {
      throw error.response;
    });
};