
import { NavigationActions, StackActions } from 'react-navigation';

let _navigator;

function setTopLevelNavigator(navigatorRef) {
  _navigator = navigatorRef;
}

function navigate(routeName, params) {
  _navigator.dispatch(
    NavigationActions.navigate({
      type: NavigationActions.NAVIGATE,
      routeName,
      params,
    })
  );
}

function navigateRoot(routeName, params) {
  _navigator.dispatch(StackActions.reset({
    index: 0,
    actions: [
      NavigationActions.navigate({
        routeName: 'Main',
        params: { someParams: 'parameters goes here...' },
      }),
    ],
  }))
  
}

// add other navigation functions that you need and export them

export default {
  navigate,
  navigateRoot,
  setTopLevelNavigator,
};