import axios from 'axios';
import { BASE_URL_API } from '../helpers/variables';

import { store } from '../store';


// const state = store.getState();
// let authToken = state.userInfo.auth.token;

// store.subscribe(listener);

// function select(state) {
//   return state.userInfo.auth.token;
// }

// function listener() {
//   let token = select(store.getState());
//   authToken = token;
// }

export const followUser = (user, follows) => {
  return axios(`${BASE_URL_API}/follower`, {
    method: 'POST',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
    data: {
      user,
      follows
    },
  })
    .then(response => response.data)
    .catch(error => {
      throw error.response;
    });
};

export const unfollowUser = (user, follows) => {
  return axios(`${BASE_URL_API}/follower/${user}/${follows}`, {
    method: 'DELETE',
    headers: {
      // authorization: `Bearer ${authToken}`,
    },
    data: {
    },
  })
    .then(response => response.data)
    .catch(error => {
      throw error.response;
    });
};

export const getFollowing = (userId) => {
  return axios(`${BASE_URL_API}/follower/follow-list-by-user/${userId}`, {
    method: 'GET',
    headers: {
      // // authorization: `Bearer ${authToken}`,
    },
  })
    .then(response => response.data)
    .catch(error => {
      throw error;
    });
};


export const getFollowers = (userId) => {
  return axios(`${BASE_URL_API}/follower/followers-list-by-user/${userId}`, {
    method: 'GET',
    headers: {
      // // authorization: `Bearer ${authToken}`,
    },
  })
    .then(response => response.data)
    .catch(error => {
      throw error;
    });
};

