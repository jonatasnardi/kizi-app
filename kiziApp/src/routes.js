import { createAppContainer, createSwitchNavigator, createBottomTabNavigator, createStackNavigator, createDrawerNavigator } from 'react-navigation';
import React from 'react';
import { Button, Dimensions } from 'react-native';
import I18n from './utils/translation/i18n';


import Main from './pages/Main';
import Contact from './pages/Contact';
import Settings from './pages/Settings';
import Colors from './utils/colors';
import Menu from './components/Menu';
import Login from './pages/Login';
import Profile from './pages/Profile';
import EditProfile from './pages/Profile/Edit';
import Pro from './pages/Pro';
import GetMyMoney from './pages/GetMyMoney';
import WeeklyTournament from './pages/WeeklyTournament';
import WeeklyRanking from './pages/WeeklyRanking';
import AddQuestion from './pages/AddQuestion';
import Question from './pages/Question';
import Leagues from './pages/Leagues';
import LeagueDetails from './pages/LeagueDetails';
import AddTournament from './pages/AddTournament';
import MyLeagues from './pages/MyLeagues';
import AddPayment from './pages/AddPayment';
import PaymentRules from './pages/PaymentRules';
import Rewards from './pages/Rewards';
import GeneralRanking from './pages/GeneralRanking';
import Notifications from './pages/Notifications';
import NotificationItem from './pages/NotificationItem';
import FriendProfile from './pages/FriendProfile';
import FollowerList from './pages/FollowerList';
import Chat from './pages/Chat';
import ChatList from './pages/ChatList';
import DailyTournament from './pages/DailyTournament';
import DailyRanking from './pages/DailyRanking';
import InviteFriends from './pages/InviteFriends';
import Challenges from './pages/Challenges';
import SearchList from './pages/SearchList';
import AddUserQuestion from './pages/AddUserQuestion';
import UserQuestionWaiting from './pages/UserQuestionWaiting';
import UserQuestionDashboard from './pages/UserQuestionDashboard';
import UserQuestionWaitingTheir from './pages/UserQuestionWaitingTheir';
import UserQuestionAnsweredTheir from './pages/UserQuestionAnsweredTheir';
import UserQuestionProfile from './pages/UserQuestionProfile';
import QuestionOpen from './pages/QuestionOpen';
import QuestionToAnswer from './pages/QuestionToAnswer';
import ShareStories from './pages/ShareStories';
import AdsMoney from './pages/AdsMoney';
import Terms from './pages/Terms';

const WIDTH = Dimensions.get('window').width;

const DrawerConfig = {
	drawerWidth: WIDTH * 0.83,
	contentComponent: ({ navigation }) => {
		return(<Menu navigation={navigation} />)
	}
}

const mainScreenStack = createStackNavigator(
  {
    Main: {
      screen: Main,
      navigationOptions: {
        header: null,
        headerBackTitle: null,
        headerTintColor: 'white',
        gesturesEnabled: false,
      }
    },
    Login: {
      screen: Login,
      navigationOptions: {
        header: null,
        headerBackTitle: null,
        headerTintColor: 'white'
      }
    },
    Profile: {
      screen: Profile,
      navigationOptions: {
        title: 'Perfil',
        headerStyle: { backgroundColor: Colors.primary },
        headerTitleStyle: { color: Colors.white },
        headerBackTitle: null,
        headerTintColor: 'white'
      }
    },
    EditProfile: {
      screen: EditProfile,
      navigationOptions: {
        title: 'Editar perfil',
        headerStyle: { backgroundColor: Colors.primary },
        headerTitleStyle: { color: Colors.white },
        headerBackTitle: null,
        headerTintColor: 'white'
      }
    },
    FriendProfile: {
      screen: FriendProfile,
      navigationOptions: {
        title: 'Perfil do usuário',
        headerStyle: { backgroundColor: Colors.primary },
        headerTitleStyle: { color: Colors.white },
        headerBackTitle: null,
        headerTintColor: 'white'
      }
    },
    Contact: { 
      screen: Contact,
      navigationOptions: {
        title: 'Contato',
        headerStyle: { backgroundColor: Colors.primary },
        headerTitleStyle: { color: Colors.white },
        headerBackTitle: null,
        headerTintColor: 'white'
      }
    },
    Settings: { 
      screen: Settings,
      navigationOptions: {
        title: 'Configurações',
        headerStyle: { backgroundColor: Colors.primary },
        headerTitleStyle: { color: Colors.white },
        headerBackTitle: null,
        headerTintColor: 'white'
      }
    },
    Chat: { 
      screen: Chat,
      navigationOptions: ({navigation}) => ({
        title: navigation.state.params.userName,
        headerStyle: { backgroundColor: Colors.secondary },
        headerTitleStyle: { color: Colors.primary },
        headerBackTitle: null,
        headerTintColor: 'white'
      })
    },
    PaymentRules: { 
      screen: PaymentRules,
      navigationOptions: {
        title: 'Regras oficiais',
        headerStyle: { backgroundColor: Colors.primary },
        headerTitleStyle: { color: Colors.white },
        headerBackTitle: null,
        headerTintColor: 'white'
      }
    },
    Terms: { 
      screen: Terms,
      navigationOptions: {
        title: 'Política de privacidade e termos de uso',
        headerStyle: { backgroundColor: Colors.primary },
        headerTitleStyle: { color: Colors.white },
        headerBackTitle: null,
        headerTintColor: 'white'
      }
    },
    Pro: {
      screen: Pro,
      navigationOptions: {
        title: 'Comprar KizoTokens',
        headerStyle: { backgroundColor: Colors.primary },
        headerTitleStyle: { color: Colors.white },
        headerBackTitle: null,
        headerTintColor: 'white'
      }
    },
    GetMyMoney: {
      screen: GetMyMoney,
      navigationOptions: {
        title: 'Solicitar pagamento',
        headerStyle: { backgroundColor: Colors.primary },
        headerTitleStyle: { color: Colors.white },
        headerBackTitle: null,
        headerTintColor: 'white'
      }
    },
    WeeklyTournament: {
      screen: WeeklyTournament,
      navigationOptions: {
        title: 'Torneio semanal',
        headerStyle: { backgroundColor: Colors.primary },
        headerTitleStyle: { color: Colors.white },
        headerBackTitle: null,
        headerTintColor: 'white'
      }
    },
    DailyTournament: {
      screen: DailyTournament,
      navigationOptions: {
        title: 'Torneio do dia',
        headerStyle: { backgroundColor: Colors.primary },
        headerTitleStyle: { color: Colors.white },
        headerBackTitle: null,
        headerTintColor: 'white'
      }
    },
    WeeklyRanking: {
      screen: WeeklyRanking,
      navigationOptions: {
        title: 'Classificação',
        headerStyle: { backgroundColor: Colors.primary },
        headerTitleStyle: { color: Colors.white },
        headerBackTitle: null,
        headerTintColor: 'white'
      }
    },
    DailyRanking: {
      screen: DailyRanking,
      navigationOptions: {
        title: 'Ranking do dia',
        headerStyle: { backgroundColor: Colors.primary },
        headerTitleStyle: { color: Colors.white },
        headerBackTitle: null,
        headerTintColor: 'white'
      }
    },
    InviteFriends: {
      screen: InviteFriends,
      navigationOptions: {
        title: 'Convidar amigos',
        headerStyle: { backgroundColor: Colors.primary },
        headerTitleStyle: { color: Colors.white },
        headerBackTitle: null,
        headerTintColor: 'white'
      }
    },
    GeneralRanking: {
      screen: GeneralRanking,
      navigationOptions: {
        title: 'Ranking geral',
        headerStyle: { backgroundColor: Colors.primary },
        headerTitleStyle: { color: Colors.white },
        headerBackTitle: null,
        headerTintColor: 'white'
      }
    },
    FollowerList: {
      screen: FollowerList,
      navigationOptions: {
        title: 'Lista',
        headerStyle: { backgroundColor: Colors.primary },
        headerTitleStyle: { color: Colors.white },
        headerBackTitle: null,
        headerTintColor: 'white'
      }
    },
    SearchList: {
      screen: SearchList,
      navigationOptions: {
        title: 'Procurar usuários',
        headerStyle: { backgroundColor: Colors.secondary },
        headerTitleStyle: { color: Colors.primary },
        headerBackTitle: null,
        headerTintColor: 'white'
      }
    },
    AdsMoney: {
      screen: AdsMoney,
      navigationOptions: {
        title: 'Ganhar dinheiro',
        headerStyle: { backgroundColor: Colors.secondary },
        headerTitleStyle: { color: Colors.primary },
        headerBackTitle: null,
        headerTintColor: 'white'
      }
    },
    ShareStories: {
      screen: ShareStories,
      navigationOptions: {
        title: 'Me faça uma pergunta',
        headerStyle: { backgroundColor: Colors.secondary },
        headerTitleStyle: { color: Colors.primary },
        headerBackTitle: null,
        headerTintColor: 'white'
      }
    },
    
    Challenges: {
      screen: Challenges,
      navigationOptions: {
        title: 'Desafios X1',
        headerStyle: { backgroundColor: Colors.secondary },
        headerTitleStyle: { color: Colors.white },
        headerBackTitle: null,
        headerTintColor: 'white'
      }
    },
    ChatList: {
      screen: ChatList,
      navigationOptions: ({navigation}) => ({
        title: 'Lista de conversas',
        headerStyle: { backgroundColor: Colors.secondary },
        headerTitleStyle: { color: Colors.primary },
        headerBackTitle: null,
        headerTintColor: 'white',
        headerLeft: null,
      })
    },
    AddUserQuestion: {
      screen: AddUserQuestion,
      navigationOptions: ({navigation}) => ({
        title: 'Fazer pergunta',
        headerStyle: { backgroundColor: Colors.secondary },
        headerTitleStyle: { color: Colors.primary },
        headerBackTitle: null,
        headerTintColor: 'white',
      })
    },
    UserQuestionDashboard: {
      screen: UserQuestionDashboard,
      navigationOptions: ({navigation}) => ({
        title: 'Minhas perguntas',
        headerStyle: { backgroundColor: Colors.secondary },
        headerTitleStyle: { color: Colors.primary },
        headerBackTitle: null,
        headerTintColor: 'white',
      })
    },
    UserQuestionWaiting: {
      screen: UserQuestionWaiting,
      navigationOptions: ({navigation}) => ({
        title: 'Perguntas para mim',
        headerStyle: { backgroundColor: Colors.secondary },
        headerTitleStyle: { color: Colors.primary },
        headerBackTitle: null,
        headerTintColor: 'white',
      })
    },
    UserQuestionWaitingTheir: {
      screen: UserQuestionWaitingTheir,
      navigationOptions: ({navigation}) => ({
        title: 'Perguntas que eu fiz',
        headerStyle: { backgroundColor: Colors.secondary },
        headerTitleStyle: { color: Colors.primary },
        headerBackTitle: null,
        headerTintColor: 'white',
      })
    },
    UserQuestionAnsweredTheir: {
      screen: UserQuestionAnsweredTheir,
      navigationOptions: ({navigation}) => ({
        title: 'Perguntas que eu fiz',
        headerStyle: { backgroundColor: Colors.secondary },
        headerTitleStyle: { color: Colors.primary },
        headerBackTitle: null,
        headerTintColor: 'white',
      })
    },
    UserQuestionProfile: {
      screen: UserQuestionProfile,
      navigationOptions: ({navigation}) => ({
        title: 'Perguntas e respostas',
        headerStyle: { backgroundColor: Colors.secondary },
        headerTitleStyle: { color: Colors.primary },
        headerBackTitle: null,
        headerTintColor: 'white',
      })
    },
    QuestionOpen: {
      screen: QuestionOpen,
      navigationOptions: ({navigation}) => ({
        title: 'Pergunta aberta',
        headerStyle: { backgroundColor: Colors.secondary },
        headerTitleStyle: { color: Colors.primary },
        headerBackTitle: null,
        headerTintColor: 'white',
      })
    },
    QuestionToAnswer: {
      screen: QuestionToAnswer,
      navigationOptions: ({navigation}) => ({
        title: 'Responder pergunta',
        headerStyle: { backgroundColor: Colors.secondary },
        headerTitleStyle: { color: Colors.primary },
        headerBackTitle: null,
        headerTintColor: 'white',
      })
    },
    Rewards: {
      screen: Rewards,
      navigationOptions: {
        title: 'Recompensas',
        headerStyle: { backgroundColor: Colors.primary },
        headerTitleStyle: { color: Colors.white },
        headerBackTitle: null,
        headerTintColor: 'white'
      }
    },
    AddQuestion: {
      screen: AddQuestion,
      navigationOptions: {
        title: 'Adicionar pergunta',
        headerStyle: { backgroundColor: Colors.primary },
        headerTitleStyle: { color: Colors.white },
        headerBackTitle: null,
        headerTintColor: 'white'
      }
    },
    AddTournament: {
      screen: AddTournament,
      navigationOptions: {
        title: 'Adicionar torneio',
        headerStyle: { backgroundColor: Colors.primary },
        headerTitleStyle: { color: Colors.white },
        headerBackTitle: null,
        headerTintColor: 'white'
      }
    },
    AddPayment: {
      screen: AddPayment,
      navigationOptions: {
        title: 'Solicitar pagamento',
        headerStyle: { backgroundColor: Colors.primary },
        headerTitleStyle: { color: Colors.white },
        headerBackTitle: null,
        headerTintColor: 'white'
      }
    },
    Question: {
      screen: Question,
      navigationOptions: {
        title: 'Pergunta',
        headerStyle: { backgroundColor: Colors.secondary },
        headerTitleStyle: { color: Colors.primary },
        headerBackTitle: null,
        headerTintColor: 'white',
        headerLeft: null,
        gesturesEnabled: false,
      }
    },
    Leagues: {
      screen: Leagues,
      navigationOptions: {
        title: 'Torneios abertos',
        headerStyle: { backgroundColor: Colors.secondary },
        headerTitleStyle: { color: Colors.primary },
        headerBackTitle: null,
        headerTintColor: 'white'
      }
    },
    Notifications: {
      screen: Notifications,
      navigationOptions: {
        title: 'Notificações',
        headerStyle: { backgroundColor: Colors.primary },
        headerTitleStyle: { color: Colors.white },
        headerBackTitle: null,
        headerTintColor: 'white'
      }
    },
    NotificationItem: {
      screen: NotificationItem,
      navigationOptions: {
        title: 'Mensagem',
        headerStyle: { backgroundColor: Colors.primary },
        headerTitleStyle: { color: Colors.white },
        headerBackTitle: null,
        headerTintColor: 'white'
      }
    },
    LeagueDetails: {
      screen: LeagueDetails,
      navigationOptions: {
        title: 'Detalhes do torneio',
        headerStyle: { backgroundColor: Colors.secondary },
        headerTitleStyle: { color: Colors.primary },
        headerBackTitle: null,
        headerTintColor: 'white'
      }
    },
    MyLeagues: {
      screen: MyLeagues,
      navigationOptions: {
        title: 'Meus torneios',
        headerStyle: { backgroundColor: Colors.secondary },
        headerTitleStyle: { color: Colors.primary },
        headerBackTitle: null,
        headerTintColor: 'white'
      }
    },
  },
  {
    navigationOptions: ({ navigation }) => ({
      initialRouteName: 'Main',
      headerMode: 'screen',
      headerTitle: 'Main Screen Header',
      drawerLabel: 'Main Screen',
    }),
  }
);

const AppNavigator = createDrawerNavigator(
  {
    Main: {
      name: 'MainScreenStack',
      screen: mainScreenStack,
    }
  },
  DrawerConfig
);

const Routes = createAppContainer(AppNavigator);

export default Routes;