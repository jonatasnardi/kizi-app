import React, {Component} from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import Icon from 'react-native-vector-icons/FontAwesome5';

import { Creators as UserInfoActions } from '../../../store/ducks/userInfo';

import RadioGroup from 'react-native-radio-buttons-group';

import I18n from '../../../utils/translation/i18n';

import { Alert, ActivityIndicator, NativeModules, Text, TouchableOpacity, View } from 'react-native';

import { 
  Wrapper,
  InputName,
  InputEmail,
  LabelForm,
  Fieldset,
  LabelValue,
  ModalWrapper,
  ModalHeader,
  ModalPassword,
  ButtonCloseModal,
  LabelFormDark,
  InputPassword,
  InputConfirmPassword,
} from './styles';
import Colors from '../../../utils/colors';
import { showAlert, showToast, isValidEmail } from '../../../utils/functions';
import ButtonPrimary from '../../../components/buttons/ButtonPrimary';
import FullLoader from '../../../components/FullLoader';
import { userContact, editProfile, deleteAccount, changePassword } from '../../../services/user';
import TextInputMask from 'react-native-text-input-mask';

class EditProfile extends Component {
  state = {
    user: this.props.user,
    username: this.props.username,
    textSuggestion: '',
    isLoading: false,
    modalPasswordVisible: false,
    loginInfo: {
      password: '',
    },
    confirmPassword: '',
  };

  refInputName;
  refInputUsername;
  refInputEmail;
  refInputPhone;

  constructor(props) {
    super(props);
   
  }

  componentDidMount() {
    
  }

  saveUser = () => {
    const { user } = this.state;
    
    if (user.phone.length < 15) {
      showAlert('Editar perfil', 'Informe um número de telefone válido.');
    } else if (user.name.length < 2) {
      showAlert('Editar perfil', 'Informe um nome com mais de 2 caracteres.');
    } else {
      this.setState({
        isLoading: true
      });
  
      editProfile(user)
        .then((data) => { 
          this.setState({
            isLoading: false,
          });
  
          this.props.updateUserInfo(user);
  
          showToast(I18n.t('profileUpdated'));

          this.props.navigation.goBack(null);
        })
        .catch((error) => {
          console.log(error);
          this.setState({
            isLoading: false
          });
          if (error.data) {
            showToast(error.data.message);
          } else {
            showToast(I18n.t('errorGenericMessage'));
          }
          
          
        }); 
    }
  }

  deleteAccount = () => {
    const { user } = this.state;

    Alert.alert(
      'Excluir a conta',
      'Tem certeza que deseja excluir a sua conta? Você perderá todos os dados e progresso dentro do aplicativo imediatamente.',
      [
        { text: "Cancelar", onPress: () => {}},
        {text: 'Sim, excluir', onPress: () => {
          this.setState({
            isLoading: true
          });
      
          deleteAccount(user._id)
            .then((data) => { 
              this.setState({
                isLoading: false,
              });
              this.props.navigation.replace('Login');
              this.navigate.push('Login');
            })
            .catch((error) => {
              console.log(error);
              this.setState({
                isLoading: false
              });
              if (error.data) {
                showToast(error.data.message);
              } else {
                showToast(I18n.t('errorGenericMessage'));
              }
              
              
            }); 
        }},
      ],
      {cancelable: true},
    );
    
      
  }

  setModalPasswordVisible(visible) {
    this.setState({modalPasswordVisible: visible});
  }

  nextStepPassword = () => {
    if (this.state.loginInfo.password != this.state.confirmPassword) {
      return showAlert('As senhas não conferem', 'As senhas estão diferentes!');
    }

    this.setState({
      isLoading: true,
    });

    changePassword(this.props.user.email, this.state.loginInfo.password)
      .then((data) => { 
        this.setState({
          isLoading: false,
        });
        showAlert('Senha alterada', 'Senha alterada com sucesso! Faça login novamente.');
        this.props.navigation.replace('Login');
        this.props.navigation.push('Login');

          
      })
      .catch((error) => {
        this.setState({
          isLoading: false,
        });
        showAlert(I18n.t('errorTitle'), I18n.t('errorGenericMessage'));
      }); 
  }

  render() {
    const { user } = this.state;

    return (
      <>
        <Wrapper>
          
          <Fieldset>
            <LabelForm>Nome:</LabelForm>

            <InputName
              ref={ref => (this.refInputName = ref)}
              value={this.state.user.name}
              onChangeText={(text) => this.setState({user: { ...this.state.user, name: text}})}
              placeholder={I18n.t('yourNamePlaceholder')}
              placeholderTextColor={Colors.primary}>
            </InputName>
          </Fieldset>

          <Fieldset>
            <LabelForm>Usuário:</LabelForm>

            <InputName
              ref={ref => (this.refInputUsername = ref)}
              value={this.state.user.username}
              onChangeText={(text) => this.setState({user: { ...this.state.user, username: text.toLowerCase()}})}
              placeholder={'@seu.usuario'}
              placeholderTextColor={Colors.primary}>
            </InputName>
          </Fieldset>

          <Fieldset>
            <LabelForm>Telefone:</LabelForm>

            <TextInputMask
              ref={ref => (this.refInputPhone = ref)}
              style={{
                paddingBottom: 5,
                fontSize: 20,
                color: Colors.primary,
                borderBottomColor: Colors.primary,
                borderBottomWidth: 2,
              }}
              value={this.state.user.phone}
              onChangeText={(text) => this.setState({user: { ...this.state.user, phone: text}})}
              placeholder="(  ) _____-____"
              placeholderTextColor={Colors.primary}
              mask={"([00]) [00000]-[0000]"}
              maxLength={15}>
            </TextInputMask>
          </Fieldset>

          <Fieldset>
            <ButtonPrimary
              onPress={() => this.saveUser()}
              title={I18n.t('save')}>
            </ButtonPrimary>
          </Fieldset>

          <Fieldset>
            <ButtonPrimary
              onPress={() => this.setModalPasswordVisible(true)}
              backgroundColor={Colors.primary}
              icon={'key'}
              title={'Trocar senha'}>
            </ButtonPrimary>
          </Fieldset>

          <Fieldset>
            <ButtonPrimary
              onPress={() => this.deleteAccount()}
              backgroundColor={Colors.red}
              icon={'trash'}
              title={'Excluir a conta'}>
            </ButtonPrimary>
          </Fieldset>
        </Wrapper>

        {
          this.state.isLoading && (
            <FullLoader></FullLoader>
          )
        }

        <ModalPassword
          animationType="slide"
          transparent={false}
          visible={this.state.modalPasswordVisible}>

          {
            this.state.isLoading && (
              <FullLoader></FullLoader>
            )
          }

          <ModalWrapper>
            <ModalHeader>
              <ButtonCloseModal onPress={() => {
                this.setModalPasswordVisible(!this.state.modalPasswordVisible);
              }}>
                <Icon name="times" size={40} color={Colors.primary} />
              </ButtonCloseModal>
            </ModalHeader>

            <Fieldset>
              <LabelFormDark>Informe a sua nova senha:</LabelFormDark>
              <InputPassword
                ref={ref => (this.refInputPassword = ref)}
                value={this.state.loginInfo.password}
                onChangeText={(text) => this.setState({loginInfo: { ...this.state.loginInfo, password: text}})}
                placeholder={'Nova senha'}
                placeholderTextColor={'#17242A'}
                secureTextEntry={true}
                autoCapitalize="none">
              </InputPassword>
            </Fieldset>

            <Fieldset>
              <InputConfirmPassword
                ref={ref => (this.refInputConfirmPassword = ref)}
                value={this.state.confirmPassword}
                onChangeText={(text) => this.setState({confirmPassword: text})}
                placeholder={'Confirme a sua nova senha'}
                placeholderTextColor={'#17242A'}
                secureTextEntry={true}
                autoCapitalize="none">
              </InputConfirmPassword>
            </Fieldset>

            <ButtonPrimary
              onPress={this.nextStepPassword}
              title={'Confirmar'}
              icon={'check'}>
            </ButtonPrimary>
           
          </ModalWrapper>
        </ModalPassword>

       </>
    )
  }
}

const mapStateToProps = state => ({
  updateUserInfo: state.userInfo.updateUserInfo,
  addListRequest: state.userInfo.addListRequest,
  updateListsTitleRequest: state.userInfo.updateListsTitleRequest,
  lists: state.userInfo.lists,
  user: state.userInfo.user,
  language: state.userInfo.user.language,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(UserInfoActions, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(EditProfile);