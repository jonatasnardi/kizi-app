import styled from 'styled-components/native';
import Colors from '../../../utils/colors';
import { Type } from '../../../utils/layout';


export const Wrapper = styled.ScrollView`
  ${Type.wrapper};
  flex: 1;
`;

export const Fieldset = styled.View`
  flex-direction: column;
  margin-bottom: 15px;
  justify-content: flex-start;
`;

export const LabelForm = styled.Text`
  ${Type.body};
  color: ${Colors.black};
  font-size: 18px;
  margin-bottom: 5px;
  margin-top: 10px;
`;

export const LabelFormDark = styled.Text`
  ${Type.body};
  color: ${Colors.primary};
  font-size: 18px;
  margin-bottom: 10px;
`;

export const LabelValue = styled.Text`
  ${Type.body};
  color: ${Colors.primary};
  font-size: 18px;
`;

export const InputName = styled.TextInput`
  ${Type.body};
  font-size: 20px;
  border-bottom-color: ${Colors.primary};
  border-bottom-width: 2px;
`;

export const InputConfirmPassword = styled.TextInput`
  ${Type.body};
  padding-bottom: 5px;
  font-size: 20px;
  color: ${Colors.primary};
  border-bottom-color: ${Colors.primary};
  border-bottom-width: 2px;
`;

export const InputPassword = styled.TextInput`
  ${Type.body};
  padding-bottom: 5px;
  font-size: 20px;
  color: ${Colors.primary};
  border-bottom-color: ${Colors.primary};
  border-bottom-width: 2px;
`;

export const InputEmail = styled.TextInput`
  ${Type.body};
  font-size: 18px;
  border-bottom-color: ${Colors.primary};
  border-bottom-width: 2px;
`;

export const ModalPassword = styled.Modal`
  ${Type.wrapper};
`;

export const ModalWrapper = styled.View`
  ${Type.wrapper};
`;

export const ModalHeader = styled.View`
  margin-top: 20px;
  flex-direction: row;
  justify-content: flex-end;
  align-items: center;
  margin-bottom: 30px;
  padding-bottom: 10px;
`;

export const ButtonCloseModal = styled.TouchableOpacity`

`;

export const ButtonGeneric = styled.TouchableOpacity`

`;
