import React, {Component} from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ImagePicker from 'react-native-image-crop-picker';

import { Creators as UserInfoActions } from '../../store/ducks/userInfo';

import I18n from '../../utils/translation/i18n';

import Icon from 'react-native-vector-icons/FontAwesome5';

import { 
  Wrapper,
  ImageUser,
  LabelForm,
  Fieldset,
  LabelValue,
  ModalAvatar,
  ModalWrapper,
  ModalTitle,
  ModalHeader,
  ButtonCloseModal,
  ContainerButtonsAvatar,
  WrapperButtonAvatar,
  ButtonQuickAccess,
  ContainerButtonsHome,
  WrapperButtonHome,
  ButtonHomeText,
  ContainerFollowers,
  ButtonHomeTextFollowers,
  ButtonHomeText2,
} from './styles';
import Colors from '../../utils/colors';
import { showAlert, showToast, calculatePercentage } from '../../utils/functions';
import ButtonPrimary from '../../components/buttons/ButtonPrimary';
import FullLoader from '../../components/FullLoader';
import { userContact, updateAvatar, getUserInfo, updateImageProfile } from '../../services/user';
import Images from '../../utils/images';
import { Alert, View, TouchableOpacity, ScrollView, Platform, Image } from 'react-native';

class Profile extends Component {
  state = {
    textSuggestion: '',
    isLoadingAdd: false,
    modalAvatarVisible: false,
    followsCount: 0,
    followersCount: 0,
  };

  constructor(props) {
    super(props);

    this.navigate = this.props.navigation;
  }

  componentDidMount() {
    const { user } = this.props;
    getUserInfo(user._id)
      .then((data) => { 
        let userInfo = {
          ...data.user,
        }

        this.setState({
          followsCount: data.followsCount,
          followersCount: data.followersCount,
        });

        this.props.updateFollowersCount(data.followersCount);
        this.props.updateFollowingCount(data.followsCount);
        
        
        this.props.updateUserInfo(userInfo);
      })
      .catch((error) => {
        console.log(error)
      });
  }

  openImagePicker() {
    ImagePicker.openPicker({
      width: 65,
      height: 65,
      mediaType: 'photo',
      cropperChooseText: 'Selecionar',
      cropperCancelText: 'Cancelar',
      loadingLabelText: 'Carregando...',
      cropperToolbarTitle: 'Cortar foto',
      compressImageMaxWidth: 65,
      compressImageMaxHeight: 65,
      compressImageQuality: (Platform.OS === 'ios') ? 1 : 1,
      cropping: true,
      useFrontCamera: true,
      includeBase64: true,
      cropperActiveWidgetColor: Colors.secondary,
      cropperStatusBarColor: Colors.secondary,
      cropperToolbarColor: Colors.secondary,
      
    }).then(image => {
      const { user } = this.props;

      let mime = image.mime.split('/')[1];

      const imageFormats = Object.freeze({
        jpeg: 'jpeg',
        jpg:  'jpeg',
        png: 'png',
      });

      image = `data:image/${imageFormats[mime]};base64,${image.data}`;

      Alert.alert(
        'Atualizar foto',
        'Tem certeza que deseja enviar esta foto?',
        [
          { text: "Cancelar", onPress: () => {}},
          {text: 'Sim', onPress: () => {
            this.setState({
              isLoadingAdd: true,
            });
        
            updateImageProfile(user._id, image)
            .then((data) => { 
              this.setState({
                isLoadingAdd: false,
              });
  
              user.image = image;
      
              this.props.updateUserInfo(user);
      
              showToast('Imagem de perfil atualizada com sucesso!');
            })
            .catch((error) => {
              console.log(error);
              this.setState({
                isLoadingAdd: false
              });
              showToast(I18n.t('errorGenericMessage'));
            }); 
          }},
        ],
        {cancelable: true},
      );

        
      
    });
  }

  render() {
    const { modalAvatarVisible, followsCount, followersCount } = this.state;
    const { user } = this.props;

    return (
      <>
        <Wrapper>
          <Fieldset style={{ flexDirection: 'row', justifyContent: 'center' }}>
              <TouchableOpacity onPress={() => this.openImagePicker()}>
                {
                  !user.image ? (
                    <ImageUser source={Images['1']} />
                  ) : (
                    <ImageUser source={{uri: user.image}} />
                  )
                }
                <View style={{ paddingTop: 8, width: 42, paddingRight: 8, paddingBottom: 8, paddingLeft: 8, borderRadius: 50, backgroundColor: Colors.lightGrey, position: 'relative', top: -23, left: 48 }}>
                  <Icon name="camera" size={25} color={Colors.secondary} />
                </View>
              </TouchableOpacity>
          </Fieldset>
          
          <Fieldset style={{ alignItems: 'center', paddingTop: 0, paddingBottom: 25, marginTop: -15}}>
            <View style={{ flexDirection: 'row' }}>
              <LabelValue style={{ color: Colors.secondary }}>{user.name}</LabelValue>
              {
                user.verified && (
                  <Image style={{ width: 18, height: 18, position: 'relative', top: 3 }} source={require('../../assets/verified.png')} />
                )
              }
            </View>
            
            <LabelValue style={{ color: Colors.primary }}>@{user.username}</LabelValue>
            <LabelValue style={{ fontSize: 22, color: Colors.primary, fontWeight: 'normal' }}>{user.points} pontos</LabelValue>
            {
              user.division >= 1 && (
                <LabelValue style={{ fontSize: 18, color: Colors.secondary, fontWeight: 'bold' }}>{user.division}ª divisão</LabelValue>
              )
              
            }
            
          </Fieldset>

          <ContainerButtonsHome>
            <WrapperButtonHome style={{ paddingRight: 10 }}>
              <ContainerFollowers style={{ backgroundColor: 'transparent', }} onPress={() => this.navigate.push('FollowerList', {
                    isFollowers: true,
                    userId: user._id
                  })}>
                <ButtonHomeText2>Seguidores</ButtonHomeText2>
                <ButtonHomeTextFollowers>{ this.props.followersCount }</ButtonHomeTextFollowers>
              </ContainerFollowers>
            </WrapperButtonHome>

            <WrapperButtonHome style={{ paddingLeft: 10 }}>
              <ContainerFollowers style={{ backgroundColor: 'transparent', }} onPress={() => this.navigate.push('FollowerList', {
                    isFollowers: false,
                    userId: user._id
                  })}>
                <ButtonHomeText2>Seguindo</ButtonHomeText2>
                <ButtonHomeTextFollowers>{ this.props.followingCount }</ButtonHomeTextFollowers>
              </ContainerFollowers>
            </WrapperButtonHome>
          </ContainerButtonsHome>
          

          {/* <Fieldset>
            <LabelForm>{I18n.t('labelAccountType')}:</LabelForm>
            <LabelValue>{ user.isPro ? 'Pro' : I18n.t('freeAccount')}</LabelValue>
          </Fieldset> */}

          <Fieldset>
            <ButtonPrimary
              onPress={() => this.navigate.push('UserQuestionProfile', {
                userObj: user,
                followersCount: this.props.followersCount,
                followsCount: this.props.followsCount,
              })}
              title={'Ver perguntas'}
              backgroundColor={Colors.primary}
              icon={'fire'}>
            </ButtonPrimary>
          </Fieldset>

          <Fieldset>
            <ButtonPrimary
              onPress={() => this.navigate.push('EditProfile')}
              icon={'pen'}
              title={I18n.t('editProfile')}>
            </ButtonPrimary>
          </Fieldset>

          <Fieldset>
            <ButtonPrimary
              onPress={() => this.navigate.push('MyLeagues')}
              icon={'trophy'}
              title={'Meus torneios'}>
            </ButtonPrimary>
          </Fieldset>

          <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
            <LabelForm style={{ fontSize: 20, color: Colors.primary }}>Seus títulos:</LabelForm>
          </View>

          <ContainerButtonsHome>
            <WrapperButtonHome style={{ paddingRight: 10 }}>
              <ButtonQuickAccess style={{ backgroundColor: 'transparent' }} onPress={() => {}}>
                <Icon name="trophy" size={40} color={Colors.yellow} />
                <ButtonHomeText>TORNEIOS{'\n'}SEMANAIS</ButtonHomeText>
                <View style={{ flexDirection: 'row' }}>
                  <ButtonHomeText style={{ fontSize: 26 }}>{ user.weekLeagueTitles }</ButtonHomeText>
                </View>
              </ButtonQuickAccess>
            </WrapperButtonHome>

            <WrapperButtonHome style={{ paddingLeft: 10 }}>
              <ButtonQuickAccess style={{ backgroundColor: 'transparent' }} onPress={() => {}}>
                <Icon name="trophy" size={40} color={Colors.blue} />
                <ButtonHomeText>TORNEIOS{'\n'}DO DIA</ButtonHomeText>
                <View style={{ flexDirection: 'row' }}>
                  <ButtonHomeText style={{ fontSize: 26 }}>{ user.dailyLeagueTitles }</ButtonHomeText>
                </View>
              </ButtonQuickAccess>
            </WrapperButtonHome>

            <WrapperButtonHome style={{ paddingRight: 10, justifyContent: 'center' }}>
              <ButtonQuickAccess style={{ backgroundColor: 'transparent' }} onPress={() => {}}>
                <Icon name="medal" size={40} color={Colors.yellow} />
                <ButtonHomeText>TORNEIOS{'\n'}ABERTOS</ButtonHomeText>
                <View style={{ flexDirection: 'row' }}>
                  <ButtonHomeText style={{ fontSize: 26 }}>{ user.leagueTitles }</ButtonHomeText>
                </View>
              </ButtonQuickAccess>
            </WrapperButtonHome>

            <WrapperButtonHome style={{ paddingRight: 10, justifyContent: 'center' }}>
              <ButtonQuickAccess style={{ backgroundColor: 'transparent' }} onPress={() => {}}>
                <Icon name="medal" size={40} color={Colors.red} />
                <ButtonHomeText>DESAFIOS X1{'\n'}GANHOS</ButtonHomeText>
                <View style={{ flexDirection: 'row' }}>
                  <ButtonHomeText style={{ fontSize: 26 }}>{ user.challengesWon }</ButtonHomeText>
                </View>
              </ButtonQuickAccess>
            </WrapperButtonHome>
          </ContainerButtonsHome>

          <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
            <LabelForm style={{ fontSize: 20, color: Colors.primary }}>Seu desempenho:</LabelForm>
          </View>

          <ContainerButtonsHome>
            <WrapperButtonHome style={{ paddingRight: 10 }}>
              <ButtonQuickAccess style={{ backgroundColor: 'transparent' }} onPress={() => {}}>
                <Icon name="square-root-alt" size={40} color={Colors.blue} />
                <ButtonHomeText>Matemática</ButtonHomeText>
                <View style={{ flexDirection: 'row' }}>
                  <Icon name="check" size={18} color={Colors.green} />
                  <ButtonHomeText style={{ marginTop: 0, marginLeft: 4 }}>{ user.hitsMathematics }</ButtonHomeText>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Icon name="times" size={18} color={Colors.red} />
                  <ButtonHomeText style={{ marginTop: 0, marginLeft: 4 }}>{ user.failsMathematics }</ButtonHomeText>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <ButtonHomeText style={{ marginTop: 0, marginLeft: 4, fontSize: 26 }}>{ calculatePercentage(user.hitsMathematics, user.hitsMathematics + user.failsMathematics) }</ButtonHomeText>
                  <Icon style={{ position: 'relative', top: 5 }} name="percentage" size={26} color={Colors.black} />
                </View>
              </ButtonQuickAccess>
            </WrapperButtonHome>

            <WrapperButtonHome style={{ paddingLeft: 10 }}>
              <ButtonQuickAccess style={{ backgroundColor: 'transparent' }} onPress={() => {}}>
                <Icon name="book" size={40} color={Colors.yellow} />
                <ButtonHomeText>Português</ButtonHomeText>
                <View style={{ flexDirection: 'row' }}>
                  <Icon name="check" size={18} color={Colors.green} />
                  <ButtonHomeText style={{ marginTop: 0, marginLeft: 4 }}>{ user.hitsPortuguese }</ButtonHomeText>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Icon name="times" size={18} color={Colors.red} />
                  <ButtonHomeText style={{ marginTop: 0, marginLeft: 4 }}>{ user.failsPortuguese }</ButtonHomeText>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <ButtonHomeText style={{ marginTop: 0, marginLeft: 4, fontSize: 26 }}>{ calculatePercentage(user.hitsPortuguese, user.hitsPortuguese + user.failsPortuguese) }</ButtonHomeText>
                  <Icon style={{ position: 'relative', top: 5 }} name="percentage" size={26} color={Colors.black} />
                </View>
              </ButtonQuickAccess>
            </WrapperButtonHome>

            <WrapperButtonHome style={{ paddingRight: 10 }}>
              <ButtonQuickAccess style={{ backgroundColor: 'transparent' }} onPress={() => {}}>
                <Icon name="palette" size={40} color={Colors.pink} />
                <ButtonHomeText>Artes</ButtonHomeText>
                <View style={{ flexDirection: 'row' }}>
                  <Icon name="check" size={18} color={Colors.green} />
                  <ButtonHomeText style={{ marginTop: 0, marginLeft: 4 }}>{ user.hitsArts }</ButtonHomeText>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Icon name="times" size={18} color={Colors.red} />
                  <ButtonHomeText style={{ marginTop: 0, marginLeft: 4 }}>{ user.failsArts }</ButtonHomeText>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <ButtonHomeText style={{ marginTop: 0, marginLeft: 4, fontSize: 26 }}>{ calculatePercentage(user.hitsArts, user.hitsArts + user.failsArts) }</ButtonHomeText>
                  <Icon style={{ position: 'relative', top: 5 }} name="percentage" size={26} color={Colors.black} />
                </View>
              </ButtonQuickAccess>
            </WrapperButtonHome>

            <WrapperButtonHome style={{ paddingLeft: 10 }}>
              <ButtonQuickAccess style={{ backgroundColor: 'transparent' }} onPress={() => {}}>
                <Icon name="flask" size={40} color={Colors.green} />
                <ButtonHomeText>Ciências</ButtonHomeText>
                <View style={{ flexDirection: 'row' }}>
                  <Icon name="check" size={18} color={Colors.green} />
                  <ButtonHomeText style={{ marginTop: 0, marginLeft: 4 }}>{ user.hitsScience }</ButtonHomeText>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Icon name="times" size={18} color={Colors.red} />
                  <ButtonHomeText style={{ marginTop: 0, marginLeft: 4 }}>{ user.failsScience }</ButtonHomeText>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <ButtonHomeText style={{ marginTop: 0, marginLeft: 4, fontSize: 26 }}>{ calculatePercentage(user.hitsScience, user.hitsScience + user.failsScience) }</ButtonHomeText>
                  <Icon style={{ position: 'relative', top: 5 }} name="percentage" size={26} color={Colors.black} />
                </View>
              </ButtonQuickAccess>
            </WrapperButtonHome>

            <WrapperButtonHome style={{ paddingRight: 10 }}>
              <ButtonQuickAccess style={{ backgroundColor: 'transparent' }} onPress={() => {}}>
                <Icon name="chess-rook" size={40} color={Colors.brown} />
                <ButtonHomeText>História</ButtonHomeText>
                <View style={{ flexDirection: 'row' }}>
                  <Icon name="check" size={18} color={Colors.green} />
                  <ButtonHomeText style={{ marginTop: 0, marginLeft: 4 }}>{ user.hitsHistory }</ButtonHomeText>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Icon name="times" size={18} color={Colors.red} />
                  <ButtonHomeText style={{ marginTop: 0, marginLeft: 4 }}>{ user.failsHistory }</ButtonHomeText>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <ButtonHomeText style={{ marginTop: 0, marginLeft: 4, fontSize: 26 }}>{ calculatePercentage(user.hitsHistory, user.hitsHistory + user.failsHistory) }</ButtonHomeText>
                  <Icon style={{ position: 'relative', top: 5 }} name="percentage" size={26} color={Colors.black} />
                </View>
              </ButtonQuickAccess>
            </WrapperButtonHome>

            <WrapperButtonHome style={{ paddingLeft: 10 }}>
              <ButtonQuickAccess style={{ backgroundColor: 'transparent' }} onPress={() => {}}>
                <Icon name="globe-americas" size={40} color={Colors.purple} />
                <ButtonHomeText>Geografia</ButtonHomeText>
                <View style={{ flexDirection: 'row' }}>
                  <Icon name="check" size={18} color={Colors.green} />
                  <ButtonHomeText style={{ marginTop: 0, marginLeft: 4 }}>{ user.hitsGeography }</ButtonHomeText>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Icon name="times" size={18} color={Colors.red} />
                  <ButtonHomeText style={{ marginTop: 0, marginLeft: 4 }}>{ user.failsGeography }</ButtonHomeText>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <ButtonHomeText style={{ marginTop: 0, marginLeft: 4, fontSize: 26 }}>{ calculatePercentage(user.hitsGeography, user.hitsGeography + user.failsGeography) }</ButtonHomeText>
                  <Icon style={{ position: 'relative', top: 5 }} name="percentage" size={26} color={Colors.black} />
                </View>
              </ButtonQuickAccess>
            </WrapperButtonHome>

            <WrapperButtonHome style={{ paddingRight: 10 }}>
              <ButtonQuickAccess style={{ backgroundColor: 'transparent' }} onPress={() => {}}>
                <Icon name="futbol" size={40} color={Colors.orange} />
                <ButtonHomeText>Esportes</ButtonHomeText>
                <View style={{ flexDirection: 'row' }}>
                  <Icon name="check" size={18} color={Colors.green} />
                  <ButtonHomeText style={{ marginTop: 0, marginLeft: 4 }}>{ user.hitsSports }</ButtonHomeText>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Icon name="times" size={18} color={Colors.red} />
                  <ButtonHomeText style={{ marginTop: 0, marginLeft: 4 }}>{ user.failsSports }</ButtonHomeText>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <ButtonHomeText style={{ marginTop: 0, marginLeft: 4, fontSize: 26 }}>{ calculatePercentage(user.hitsSports, user.hitsSports + user.failsSports) }</ButtonHomeText>
                  <Icon style={{ position: 'relative', top: 5 }} name="percentage" size={26} color={Colors.black} />
                </View>
              </ButtonQuickAccess>
            </WrapperButtonHome>

            <WrapperButtonHome style={{ paddingLeft: 10 }}>
              <ButtonQuickAccess style={{ backgroundColor: 'transparent' }} onPress={() => {}}>
                <Icon name="tv" size={40} color={Colors.black} />
                <ButtonHomeText>Entretenimento</ButtonHomeText>
                <View style={{ flexDirection: 'row' }}>
                  <Icon name="check" size={18} color={Colors.green} />
                  <ButtonHomeText style={{ marginTop: 0, marginLeft: 4 }}>{ user.hitsEntertainment }</ButtonHomeText>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Icon name="times" size={18} color={Colors.red} />
                  <ButtonHomeText style={{ marginTop: 0, marginLeft: 4 }}>{ user.failsEntertainment }</ButtonHomeText>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <ButtonHomeText style={{ marginTop: 0, marginLeft: 4, fontSize: 26 }}>{ calculatePercentage(user.hitsEntertainment, user.hitsEntertainment + user.failsEntertainment) }</ButtonHomeText>
                  <Icon style={{ position: 'relative', top: 5 }} name="percentage" size={26} color={Colors.black} />
                </View>
              </ButtonQuickAccess>
            </WrapperButtonHome>

            <WrapperButtonHome style={{ paddingRight: 10 }}>
              <ButtonQuickAccess style={{ backgroundColor: 'transparent' }} onPress={() => {}}>
                <Icon name="flag-usa" size={40} color={Colors.red} />
                <ButtonHomeText>Inglês</ButtonHomeText>
                <View style={{ flexDirection: 'row' }}>
                  <Icon name="check" size={18} color={Colors.green} />
                  <ButtonHomeText style={{ marginTop: 0, marginLeft: 4 }}>{ user.hitsEnglish }</ButtonHomeText>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Icon name="times" size={18} color={Colors.red} />
                  <ButtonHomeText style={{ marginTop: 0, marginLeft: 4 }}>{ user.failsEnglish }</ButtonHomeText>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <ButtonHomeText style={{ marginTop: 0, marginLeft: 4, fontSize: 26 }}>{ calculatePercentage(user.hitsEnglish, user.hitsEnglish + user.failsEnglish) }</ButtonHomeText>
                  <Icon style={{ position: 'relative', top: 5 }} name="percentage" size={26} color={Colors.black} />
                </View>
              </ButtonQuickAccess>
            </WrapperButtonHome>

            <WrapperButtonHome style={{ paddingLeft: 10 }}>
              <ButtonQuickAccess style={{ backgroundColor: 'transparent' }} onPress={() => {}}>
                <Icon name="table" size={40} color={Colors.wine} />
                <ButtonHomeText>Geral</ButtonHomeText>
                <View style={{ flexDirection: 'row' }}>
                  <Icon name="check" size={18} color={Colors.green} />
                  <ButtonHomeText style={{ marginTop: 0, marginLeft: 4 }}>{ user.hitsGeneral }</ButtonHomeText>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Icon name="times" size={18} color={Colors.red} />
                  <ButtonHomeText style={{ marginTop: 0, marginLeft: 4 }}>{ user.failsGeneral }</ButtonHomeText>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <ButtonHomeText style={{ marginTop: 0, marginLeft: 4, fontSize: 26 }}>{ calculatePercentage(user.hitsGeneral, user.hitsGeneral + user.failsGeneral) }</ButtonHomeText>
                  <Icon style={{ position: 'relative', top: 5 }} name="percentage" size={26} color={Colors.black} />
                </View>
              </ButtonQuickAccess>
            </WrapperButtonHome>

          </ContainerButtonsHome>

          {/* {
            !user.isPro && (
              <Fieldset>
                <ButtonPrimary
                  onPress={() => this.navigate.push('Pro')}
                  title={I18n.t('goToProVersion')}
                  backgroundColor={'#FFCC33'}
                  textColor={Colors.darkGrey}>
                </ButtonPrimary>
              </Fieldset>
            )
          } */}
          <View style={{ marginBottom: 200 }} />
    
        </Wrapper>

        {
          this.state.isLoadingAdd && (
            <FullLoader></FullLoader>
          )
        }
        

       </>
    )
  }
}

const mapStateToProps = state => ({
  addListRequest: state.userInfo.addListRequest,
  updateListsTitleRequest: state.userInfo.updateListsTitleRequest,
  updateFollowersCount: state.userInfo.updateFollowersCount,
  updateFollowingCount: state.userInfo.updateFollowingCount,
  followersCount: state.userInfo.followersCount,
  followingCount: state.userInfo.followingCount,
  lists: state.userInfo.lists,
  user: state.userInfo.user,
  language: state.userInfo.user.language,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(UserInfoActions, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Profile);