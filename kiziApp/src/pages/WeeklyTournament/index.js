import React, {Component} from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Icon from 'react-native-vector-icons/FontAwesome5';

import { Creators as UserInfoActions } from '../../store/ducks/userInfo';

import { 
  Wrapper,
  BigBox,
  SubtitleText,
  TextMoney,
  LabelForm,
  ContainerMainBanner,
  LabelFormMin,
  WrapperInner,
  LabelTitleTopic,
  LabelDescriptionTopic,
  LabelAwards,
} from './styles';
import { numberToReal, getFirstWord, showAlert } from '../../utils/functions';
import ButtonPrimary from '../../components/buttons/ButtonPrimary';
import FullLoader from '../../components/FullLoader';
import Colors from '../../utils/colors';
import { Alert, View, ScrollView } from 'react-native';
import { requestPayment } from '../../services/payments';
import ButtonSecondary from '../../components/buttons/ButtonSecondary';
import { getCurrentRoundInfo } from '../../services/general';
import { CURRENT_ROUND_NAME_ENUM, CURRENT_ROUND_DAY_NAME_ENUM, CURRENT_ROUND_HOUR_ENDS_ENUM } from '../../utils/enums';

class WeeklyTournament extends Component {
  state = {
    textSuggestion: '',
    isLoadingAdd: false,
    switchValue: true,
  }

  refInputSuggestion;

  constructor(props) {
    super(props);

    this.navigate = this.props.navigation;

    // this.getConfigInfo();
  }

  getConfigInfo = () => {

    getCurrentRoundInfo()
      .then((res) => { 
        this.props.updateCurrentRound(res.currentRound);
      })
      .catch((error) => {
      });
  }

  async componentDidMount() {
    
  }


  render() {
    const { user } = this.props;

    return (
      <>
        <Wrapper>
          {/* <ContainerMainBanner>
            <LabelForm>Só depende de você :){'\n'}Toda semana tem um vencedor!</LabelForm>
            <BigBox>
              <TextMoney>VALENDO { numberToReal(this.props.awardWeekly)}</TextMoney>
            </BigBox>
            <LabelFormMin>Você pode jogar todos os dias{'\n'}e quantas vezes quiser</LabelFormMin>
          </ContainerMainBanner> */}

          <ScrollView>
            <WrapperInner>
              <LabelTitleTopic>Premiação</LabelTitleTopic>
              <View style={{ flexDirection: 'row', marginTop: 10 }}>
                <Icon style={{marginRight: 4}} name="trophy" size={24} color={'#f8bc44'} />
                <LabelAwards>Campeão - { numberToReal(this.props.awardWeekly)} + 50</LabelAwards>
                <Icon style={{marginLeft: 4}} name="coins" size={20} color={Colors.yellow} />
              </View>
              <View style={{ flexDirection: 'row', marginTop: 10 }}>
                <Icon style={{marginRight: 4}} name="trophy" size={24} color={'#d6d6d6'} />
                <LabelAwards>Vice-campeão - R$ 10,00 + 25</LabelAwards>
                <Icon style={{marginLeft: 4}} name="coins" size={20} color={Colors.yellow} />
              </View>
              <View style={{ flexDirection: 'row', marginTop: 10 }}>
                <Icon style={{marginRight: 4}} name="trophy" size={24} color={'#e9a051'} />
                <LabelAwards>3º Lugar - R$ 4,00 + 20</LabelAwards>
                <Icon style={{marginLeft: 4}} name="coins" size={20} color={Colors.yellow} />
              </View>
              <View style={{ flexDirection: 'row', marginTop: 10 }}>
                <Icon style={{marginRight: 4}} name="trophy" size={24} color={Colors.secondary} />
                <LabelAwards>4º Lugar - R$ 4,00 + 20</LabelAwards>
                <Icon style={{marginLeft: 4}} name="coins" size={20} color={Colors.yellow} />
              </View>

              <LabelTitleTopic>Fases do torneio</LabelTitleTopic>
              <View style={{ flexDirection: 'row', marginTop: 10 }}>
                <Icon style={{marginRight: 4}} name="calendar-day" size={24} color={Colors.primary} />
                <LabelAwards>{CURRENT_ROUND_NAME_ENUM[0]} - Finaliza { CURRENT_ROUND_DAY_NAME_ENUM[0].toLowerCase() } às {CURRENT_ROUND_HOUR_ENDS_ENUM[0]}:00h</LabelAwards>
                
              </View>
              <View style={{ flexDirection: 'row', marginTop: 10 }}>
                <Icon style={{marginRight: 4}} name="calendar-day" size={24} color={Colors.primary} />
                <LabelAwards>{CURRENT_ROUND_NAME_ENUM[1]} - Finaliza { CURRENT_ROUND_DAY_NAME_ENUM[1].toLowerCase() } às {CURRENT_ROUND_HOUR_ENDS_ENUM[1]}:00h</LabelAwards>
                
              </View>
              <View style={{ flexDirection: 'row', marginTop: 10 }}>
                <Icon style={{marginRight: 4}} name="calendar-day" size={24} color={Colors.primary} />
                <LabelAwards>{CURRENT_ROUND_NAME_ENUM[2]} - Finaliza { CURRENT_ROUND_DAY_NAME_ENUM[2].toLowerCase() } às {CURRENT_ROUND_HOUR_ENDS_ENUM[2]}:00h</LabelAwards>
                
              </View>
              <View style={{ flexDirection: 'row', marginTop: 10 }}>
                <Icon style={{marginRight: 4}} name="calendar-day" size={24} color={Colors.primary} />
                <LabelAwards>{CURRENT_ROUND_NAME_ENUM[3]} - Finaliza { CURRENT_ROUND_DAY_NAME_ENUM[3].toLowerCase() } às {CURRENT_ROUND_HOUR_ENDS_ENUM[3]}:00h</LabelAwards>
                
              </View>
              <View style={{ flexDirection: 'row', marginTop: 10 }}>
                <Icon style={{marginRight: 4}} name="calendar-day" size={24} color={Colors.primary} />
                <LabelAwards>{CURRENT_ROUND_NAME_ENUM[4]} - Finaliza { CURRENT_ROUND_DAY_NAME_ENUM[4].toLowerCase() } às {CURRENT_ROUND_HOUR_ENDS_ENUM[4]}:00h</LabelAwards>
                
              </View>
              <View style={{ flexDirection: 'row', marginTop: 10 }}>
                <Icon style={{marginRight: 4}} name="calendar-day" size={24} color={Colors.primary} />
                <LabelAwards>{CURRENT_ROUND_NAME_ENUM[5]} - Finaliza { CURRENT_ROUND_DAY_NAME_ENUM[5].toLowerCase() } às {CURRENT_ROUND_HOUR_ENDS_ENUM[5]}:00h</LabelAwards>
                
              </View>
              <View style={{ flexDirection: 'row', marginTop: 10 }}>
                <Icon style={{marginRight: 4}} name="calendar-day" size={24} color={Colors.primary} />
                <LabelAwards>{CURRENT_ROUND_NAME_ENUM[6]} - Finaliza { CURRENT_ROUND_DAY_NAME_ENUM[6].toLowerCase() } às {CURRENT_ROUND_HOUR_ENDS_ENUM[6]}:00h</LabelAwards>
                
              </View>
              <View style={{ flexDirection: 'row', marginTop: 10 }}>
                <Icon style={{marginRight: 4}} name="calendar-day" size={24} color={Colors.primary} />
                <LabelAwards>{CURRENT_ROUND_NAME_ENUM[7]} - Finaliza { CURRENT_ROUND_DAY_NAME_ENUM[7].toLowerCase() } às {CURRENT_ROUND_HOUR_ENDS_ENUM[7]}:00h</LabelAwards>
                
              </View>

              <LabelTitleTopic>Como funciona?</LabelTitleTopic>
              
              <LabelDescriptionTopic>
                Na fase de grupos, classificam-se 4 equipes de cada grupo para a fase de mata-mata (eliminatórias).{'\n'}
                Cada pergunta vale de 1 a 3 pontos, de acordo com sua respectiva dificuldade.{'\n'}O torneio se inicia às quintas-feiras após as 22:00h e finaliza na quinta-feira seguinte às 22:00h. Acompanhe o ganhador toda semana nas redes sociais do Kizo.{'\n'}{'\n'}
                O valor da premiação será adicionado no jogo e você poderá soliticar o pagamento quando quiser.
              </LabelDescriptionTopic>

              <LabelTitleTopic>Como será o pagamento?</LabelTitleTopic>
              <View style={{ marginTop: 10 }}/>

              <ButtonPrimary
                onPress={() => this.navigate.push('PaymentRules')}
                title={'Regras oficiais'}
                // disabled={user.money < 30}
                backgroundColor={Colors.primary}
                icon={'align-right'}>
              </ButtonPrimary>
              <View style={{ marginBottom: 100 }}/>
            </WrapperInner>
          </ScrollView>
          
        </Wrapper>

        {
          this.state.isLoadingAdd && (
            <FullLoader></FullLoader>
          )
        }
       </>
    )
  }
}

const mapStateToProps = state => ({
  addListRequest: state.userInfo.addListRequest,
  updateListsTitleRequest: state.userInfo.updateListsTitleRequest,
  updateCurrentRound: state.userInfo.updateCurrentRound,
  currentRound: state.userInfo.currentRound,
  lists: state.userInfo.lists,
  awardWeekly: state.userInfo.awardWeekly,
  user: state.userInfo.user,
  language: state.userInfo.user.language,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(UserInfoActions, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(WeeklyTournament);