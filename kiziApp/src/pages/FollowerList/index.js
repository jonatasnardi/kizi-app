import React, {Component} from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Icon from 'react-native-vector-icons/FontAwesome5';

import { Creators as UserInfoActions } from '../../store/ducks/userInfo';
import {fetch} from 'react-native-ssl-pinning';

import { 
  Wrapper,
  WrapperInner,
  ListContainer,
  Row,
  SmallTitle,
  LabelPoints,
  LabelPosition,
  ImageIconUser,
  ContainerBanner,
  Fieldset,
  InputName,
} from './styles';
import { numberToReal, getFirstWord, showAlert, showToast, isIntoMinutes, normalizarString } from '../../utils/functions';
import I18n from '../../utils/translation/i18n';
import ButtonPrimary from '../../components/buttons/ButtonPrimary';
import FullLoader from '../../components/FullLoader';
import Colors from '../../utils/colors';
import { Alert, View, ScrollView, FlatList, Platform, TouchableOpacity, Image } from 'react-native';
import { requestPayment } from '../../services/payments';
import ButtonSecondary from '../../components/buttons/ButtonSecondary';
import { getUserRankingList, getGeneralRanking, getRankingWinnersWeekly } from '../../services/leagues';
import Images from '../../utils/images';
import { getFollowers, getFollowing } from '../../services/follower';
import { BASE_URL_API } from '../../helpers/variables';

class FollowerList extends Component {
  state = {
    bkpUsers: [],
    users: [],
    isLoadingAdd: false,
    seeGeneralRanking: true,
    rankingSemanalData: [],
    isFollowers: true,
    searchText: '',
  }

  navigate;

  refInputSuggestion;
  refInputSearch

  constructor(props) {
    super(props);

    this.navigate = this.props.navigation;
  }

  componentDidMount() {
    let isFollowers = true;
    if (this.navigate.state.params.isFollowers) {
      isFollowers = true;
    } else {
      isFollowers = false;
    }

    this.setState({
      isFollowers,
    }, () => {
      this.loadItems();
    });

    

    
  }

  loadItems = () => {
    const { isFollowers } = this.state;
    
    this.setState({
      isLoadingAdd: true,
    });

    if (isFollowers) {
      fetch(`${BASE_URL_API}/follower/followers-list-by-user/${this.navigate.state.params.userId}`, {
        method: "GET" ,
        timeoutInterval: 30000,
        sslPinning: {
            certs: ["mycert"],
        },
        headers:{
          Authorization: `Bearer ${this.props.auth.token}`,
        }
      })
      .then(res => {
        
        res = JSON.parse(res.bodyString);
        this.setState({
          bkpUsers: res.followers,
          users: res.followers,
          isLoadingAdd: false,
        });
      })
      .catch(err => {
        console.log(error);
          showToast(I18n.t('errorGenericMessage'));
          this.setState({
            isLoadingAdd: false,
          });
      })

      // getFollowers(this.navigate.state.params.userId)
      //   .then((res) => { 
      //     this.setState({
      //       bkpUsers: res.followers,
      //       users: res.followers,
      //       isLoadingAdd: false,
      //     });
      //   })
      //   .catch((error) => {
      //     console.log(error);
      //     showToast(I18n.t('errorGenericMessage'));
      //     this.setState({
      //       isLoadingAdd: false,
      //     });
      //   });
    } else {
      fetch(`${BASE_URL_API}/follower/follow-list-by-user/${this.navigate.state.params.userId}`, {
        method: "GET" ,
        timeoutInterval: 30000,
        sslPinning: {
            certs: ["mycert"],
        },
        headers:{
          Authorization: `Bearer ${this.props.auth.token}`,
        }
      })
      .then(res => {
        
        res = JSON.parse(res.bodyString);
        this.setState({
          bkpUsers: res.followers,
          users: res.followers,
          isLoadingAdd: false,
        });
      })
      .catch(err => {
        console.log(error);
          showToast(I18n.t('errorGenericMessage'));
          this.setState({
            isLoadingAdd: false,
          });
      })

      // getFollowing(this.navigate.state.params.userId)
      //   .then((res) => { 
      //     this.setState({
      //       bkpUsers: res.followers,
      //       users: res.followers,
      //       isLoadingAdd: false,
      //     });
      //   })
      //   .catch((error) => {
      //     console.log(error);
      //     showToast(I18n.t('errorGenericMessage'));
      //     this.setState({
      //       isLoadingAdd: false,
      //     });
      //   });
    }
  }

  filterUsers = (text) => {
    let users = this.state.bkpUsers;
    if (text.length > 0) {
      users = this.state.bkpUsers.filter(item => {
        const baseItem = this.state.isFollowers ? item.user : item.follows;
  
        return normalizarString(baseItem.name.toLowerCase()).indexOf(normalizarString(text.toLowerCase())) > -1;
      });
    }
    

    this.setState({
      searchText: text,
      users,
    });
  }

  render() {
    const { users, seeGeneralRanking, rankingSemanalData, isFollowers } = this.state;
    const { user } = this.props;

    let listToLoad = users;

    return (
      <>
        <Wrapper>
          <ScrollView>
            <WrapperInner>

              <Fieldset>
                <InputName
                  ref={ref => (this.refInputSearch = ref)}
                  value={this.state.searchText}
                  onChangeText={(text) => this.filterUsers(text)}
                  placeholder={'Pesquisar...'}
                  placeholderTextColor={'#566b89'}
                  maxLength={60}>
                </InputName>
              </Fieldset>

              <Row style={{ justifyContent: 'center', marginTop: 0 }}>
                <LabelPosition style={{ color: Colors.secondary, fontSize: 22, marginBottom: 15 }}>{ isFollowers ? 'Seguidores' : 'Seguindo' }</LabelPosition>
              </Row>

              {
                (users.length > 0) ? (
                  <>
                    <Row>
                      <SmallTitle style={{ top: 1 }}>Usuário</SmallTitle>
                    </Row>
                    <FlatList
                      data={listToLoad}
                      renderItem={({ item, index }) => {
                        let baseItem = isFollowers ? item.user : item.follows;
                      
                        if (baseItem) {
                          return (
                            <TouchableOpacity onPress={() => this.navigate.push('FriendProfile', {
                              idUser: baseItem._id,
                            })}>
                              <ListContainer>
                                <Row>
                                  {
                                    !baseItem.image ? (
                                      <ImageIconUser source={Images['1']} />
                                    ) : (
                                      <ImageIconUser source={{uri: baseItem.image}} />
                                    )
                                  }
                                  {
                                    (baseItem.lastAccess && isIntoMinutes(30, baseItem.lastAccess)) && (
                                      <View style={{ backgroundColor: Colors.green, position: 'absolute', left: 40, bottom: 2, width: 14, height: 14, borderRadius: 14 }} />
                                    )
                                  }
                                  <View style={{ flexDirection: 'row' }}>
                                    <SmallTitle>{ baseItem.name }</SmallTitle>
                                    {
                                      baseItem.verified && (
                                        <Image style={{ width: 18, height: 18, position: 'relative', top: 8 }} source={require('../../assets/verified.png')} />
                                      )
                                    }
                                  </View>
                                  
                                  <View style={{ position: 'absolute', right: 15, top: 12 }}>
                                    <Icon name="chevron-right" size={20} color={Colors.secondary} />
                                  </View>
                                </Row>
                              </ListContainer>
                            </TouchableOpacity>
                          )
                        } else {
                          return null
                        }
                      }
                      }
                      
                    />
                  </>
                ) : (
                  <SmallTitle style={{ textAlign: 'center' }}>{ isFollowers ? 'Você não segue ninguém' : 'Nenhum seguidor'}</SmallTitle>
                )
              }
              
              <View style={{ marginBottom: 200 }} />
            </WrapperInner>
          </ScrollView>
          
        </Wrapper>

        {
          this.state.isLoadingAdd && (
            <FullLoader></FullLoader>
          )
        }
       </>
    )
  }
}

const mapStateToProps = state => ({
  addListRequest: state.userInfo.addListRequest,
  updateListsTitleRequest: state.userInfo.updateListsTitleRequest,
  lists: state.userInfo.lists,
  user: state.userInfo.user,
  auth: state.userInfo.auth,
  language: state.userInfo.user.language,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(UserInfoActions, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(FollowerList);