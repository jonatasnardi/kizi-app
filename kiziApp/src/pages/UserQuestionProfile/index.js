import React, {Component} from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ImagePicker from 'react-native-image-crop-picker';

import { Creators as UserInfoActions } from '../../store/ducks/userInfo';

import I18n from '../../utils/translation/i18n';

import Icon from 'react-native-vector-icons/FontAwesome5';

import { 
  Wrapper,
  ImageUser,
  LabelForm,
  Fieldset,
  LabelValue,
  ModalAvatar,
  ModalWrapper,
  ModalTitle,
  ModalHeader,
  ButtonCloseModal,
  ContainerButtonsAvatar,
  WrapperButtonAvatar,
  ButtonQuickAccess,
  ContainerButtonsHome,
  WrapperButtonHome,
  ButtonHomeText,
  ContainerFollowers,
  ButtonHomeTextFollowers,
  ButtonHomeText2,
  Row,
  SmallTitle,
  ListContainer,
  LabelPosition,
  ImageIconUser,
} from './styles';
import Colors from '../../utils/colors';
import { showAlert, showToast, calculatePercentage, isIntoMinutes, cutString } from '../../utils/functions';
import ButtonPrimary from '../../components/buttons/ButtonPrimary';
import FullLoader from '../../components/FullLoader';
import { userContact, updateAvatar, getUserInfo, updateImageProfile } from '../../services/user';
import Images from '../../utils/images';
import { Alert, View, TouchableOpacity, FlatList, ScrollView, Platform } from 'react-native';
import { getUserQuestionUser } from '../../services/userQuestion';

class UserQuestionProfile extends Component {
  state = {
    userQuestions: [],
    isLoadingAdd: false,
    modalAvatarVisible: false,
    followsCount: 0,
    followersCount: 0,
  };

  constructor(props) {
    super(props);

    this.navigate = this.props.navigation;
  }

  componentDidMount() {
    const { user } = this.props;

    this.setState({
      isLoadingAdd: true,
    });
    getUserQuestionUser(this.navigate.state.params.userObj ? this.navigate.state.params.userObj._id : user._id)
      .then((data) => {
        this.setState({
          userQuestions: data.userQuestions,
          isLoadingAdd: false,
        });
      })
      .catch((error) => {
        console.log(error);
        this.setState({
          isLoadingAdd: false,
        });
      });
  }

  openImagePicker() {
    ImagePicker.openPicker({
      width: 65,
      height: 65,
      mediaType: 'photo',
      cropperChooseText: 'Selecionar',
      cropperCancelText: 'Cancelar',
      loadingLabelText: 'Carregando...',
      cropperToolbarTitle: 'Cortar foto',
      compressImageMaxWidth: 65,
      compressImageMaxHeight: 65,
      compressImageQuality: (Platform.OS === 'ios') ? 1 : 1,
      cropping: true,
      useFrontCamera: true,
      includeBase64: true,
      cropperActiveWidgetColor: Colors.secondary,
      cropperStatusBarColor: Colors.secondary,
      cropperToolbarColor: Colors.secondary,
      
    }).then(image => {
      const { user } = this.props;

      let mime = image.mime.split('/')[1];

      const imageFormats = Object.freeze({
        jpeg: 'jpeg',
        jpg:  'jpeg',
        png: 'png',
      });

      image = `data:image/${imageFormats[mime]};base64,${image.data}`;

      Alert.alert(
        'Atualizar foto',
        'Tem certeza que deseja enviar esta foto?',
        [
          { text: "Cancelar", onPress: () => {}},
          {text: 'Sim', onPress: () => {
            this.setState({
              isLoadingAdd: true,
            });
        
            updateImageProfile(user._id, image)
            .then((data) => { 
              this.setState({
                isLoadingAdd: false,
              });
  
              user.image = image;
      
              this.props.updateUserInfo(user);
      
              showToast('Imagem de perfil atualizada com sucesso!');
            })
            .catch((error) => {
              console.log(error);
              this.setState({
                isLoadingAdd: false
              });
              showToast(I18n.t('errorGenericMessage'));
            }); 
          }},
        ],
        {cancelable: true},
      );

        
      
    });
  }

  render() {
    const { userQuestions, followsCount, followersCount } = this.state;
    let { user } = this.props;

    user = this.navigate.state.params.userObj ? this.navigate.state.params.userObj : user;

    return (
      <>
        <Wrapper>
          <Fieldset style={{ flexDirection: 'row', justifyContent: 'center', marginBottom: 35 }}>
              <TouchableOpacity onPress={() => this.openImagePicker()}>
                {
                  !user.image ? (
                    <ImageUser source={Images['1']} />
                  ) : (
                    <ImageUser source={{uri: user.image}} />
                  )
                }
              </TouchableOpacity>
          </Fieldset>
          
          <Fieldset style={{ alignItems: 'center', paddingTop: 0, paddingBottom: 25, marginTop: -15}}>
            <LabelValue style={{ color: Colors.secondary }}>{user.name}</LabelValue>
            <LabelValue style={{ color: Colors.primary, }}>@{user.username}</LabelValue>
          </Fieldset>

          {
            (user._id === this.props.user._id) ? (
              <Fieldset style={{ paddingTop: 0, paddingBottom: 25, marginTop: -15, marginBottom: 15}}>
                <ButtonPrimary
                  onPress={() => this.navigate.push('ShareStories')}
                  title={'Compartilhar perfil\nnos stories'}
                  backgroundColor={Colors.secondary}
                  icon={'share'}>
                </ButtonPrimary>
              </Fieldset>
            ) : (
              <Fieldset style={{ paddingTop: 0, paddingBottom: 25, marginTop: -15, marginBottom: 15}}>
                <ButtonPrimary
                  onPress={() => this.navigate.push('AddUserQuestion', {
                    to: user._id,
                  })}
                  title={'Fazer pergunta\nanonimamente'}
                  backgroundColor={Colors.secondary}
                  icon={'fire'}>
                </ButtonPrimary>
              </Fieldset>
            )
          }
          

          <ContainerButtonsHome>
            <WrapperButtonHome style={{ paddingRight: 10 }}>
              <ContainerFollowers style={{ backgroundColor: 'transparent', }} onPress={() => this.navigate.push('FollowerList', {
                    isFollowers: true,
                    userId: user._id
                  })}>
                <ButtonHomeText2>Seguidores</ButtonHomeText2>
                <ButtonHomeTextFollowers>{ (this.navigate.state.params.followersCount >= 0) ? this.navigate.state.params.followersCount : this.props.followersCount }</ButtonHomeTextFollowers>
              </ContainerFollowers>
            </WrapperButtonHome>

            <WrapperButtonHome style={{ paddingLeft: 10 }}>
              <ContainerFollowers style={{ backgroundColor: 'transparent', }} onPress={() => this.navigate.push('FollowerList', {
                    isFollowers: false,
                    userId: user._id
                  })}>
                <ButtonHomeText2>Seguindo</ButtonHomeText2>
                <ButtonHomeTextFollowers>{ (this.navigate.state.params.followsCount >= 0) ? this.navigate.state.params.followsCount : this.props.followingCount }</ButtonHomeTextFollowers>
              </ContainerFollowers>
            </WrapperButtonHome>
          </ContainerButtonsHome>

          <Row style={{ justifyContent: 'center', marginTop: 0 }}>
            <LabelPosition style={{ color: Colors.secondary, fontSize: 22, marginBottom: 15 }}>Perguntas</LabelPosition>
          </Row>

          {
            (userQuestions.length > 0) ? (
              <>
                <FlatList
                  data={userQuestions}
                  renderItem={({ item, index }) => {
                    let baseItem = item.from;
                  
                    if (baseItem) {
                      return (
                        <ListContainer>
                          <Row style={{ flexDirection: 'column' }}>
                            <SmallTitle style={{ position: 'absolute', top: -6, left: 0, color: Colors.secondary, fontSize: 14 }}>{ cutString((item.anonymous ? 'Anônimo' : baseItem.name), 58) }</SmallTitle>
                            <SmallTitle style={{ fontWeight: 'bold' }}>Pergunta:</SmallTitle>
                            <SmallTitle>{ item.question }</SmallTitle>
                            <SmallTitle style={{ color: Colors.secondary, fontWeight: 'bold', marginTop: 20 }}>Resposta:</SmallTitle>
                            <SmallTitle style={{ marginBottom: 10 }}>{ item.answer ? item.answer : 'Ainda não foi respondido' }</SmallTitle>
                            
                            <View style={{ position: 'absolute', right: 0, top: 0 }}>
                              <TouchableOpacity onPress={() => item.anonymous ? null : this.navigate.push('FriendProfile', {
                                idUser: baseItem._id,
                              })}>
                                {
                                  (!baseItem.image || item.anonymous) ? (
                                    <ImageIconUser source={Images['1']} />
                                  ) : (
                                    <ImageIconUser source={{uri: baseItem.image}} />
                                  )
                                }
                              </TouchableOpacity>
                            </View>
                            
                          </Row>
                        </ListContainer>
                      )
                    } else {
                      return null
                    }
                  }
                  }
                  
                />
              </>
            ) : (
              <>
                <SmallTitle style={{ textAlign: 'center', marginBottom: 20 }}>Nenhuma pergunta até aqui</SmallTitle>
              </>
            )
          }
          
          <View style={{ marginBottom: 200 }} />
    
        </Wrapper>

        {
          this.state.isLoadingAdd && (
            <FullLoader></FullLoader>
          )
        }
        

       </>
    )
  }
}

const mapStateToProps = state => ({
  addListRequest: state.userInfo.addListRequest,
  updateListsTitleRequest: state.userInfo.updateListsTitleRequest,
  updateFollowersCount: state.userInfo.updateFollowersCount,
  updateFollowingCount: state.userInfo.updateFollowingCount,
  followersCount: state.userInfo.followersCount,
  followingCount: state.userInfo.followingCount,
  lists: state.userInfo.lists,
  user: state.userInfo.user,
  language: state.userInfo.user.language,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(UserInfoActions, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(UserQuestionProfile);