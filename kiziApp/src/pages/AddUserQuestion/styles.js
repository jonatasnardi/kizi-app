import styled from 'styled-components/native';
import Colors from '../../utils/colors';
import { Type } from '../../utils/layout';
import { Dimensions } from 'react-native';


export const Wrapper = styled.View`
  ${Type.wrapper};
  padding: 0;
  flex: 1;
`;

export const WrapperInner = styled.View`
  ${Type.wrapper};
  flex: 1;
`;

export const Fieldset = styled.View`
  flex-direction: column;
  margin-bottom: 20px;
`;

export const LabelForm = styled.Text`
  ${Type.body};
  color: ${Colors.secondary};
  font-size: 20px;
  margin-top: 10px;
  margin-bottom: 10px;
`;

export const LabelTitleTopic = styled.Text`
  ${Type.body};
  color: ${Colors.primary};
  font-size: 20px;
  text-align: left;
`;

export const LabelDescriptionTopic = styled.Text`
  ${Type.body};
  color: ${Colors.primary};
  font-size: 17px;
`;

export const LabelAwards = styled.Text`
  ${Type.body};
  color: ${Colors.primary};
  font-size: 20px;
`;

export const LabelFormMin = styled.Text`
  ${Type.body};
  color: ${Colors.secondary};
  font-size: 18px;
  margin-bottom: 10px;
  text-align: center;
  width: 100%;
`;

export const BigBox = styled.View`
  width: 100%;
  height: 58px;
`;

export const SubtitleText = styled.Text`
  ${Type.body};
  padding-top: 15px;
  color: ${Colors.primary};
`;

export const TextMoney = styled.Text`
  ${Type.body};
  color: ${Colors.secondary};
  font-size: 32px;
  text-align: center;
`;

export const ContainerMainBanner = styled.View`
  padding: 0px 15px 20px 15px;
  width: 100%;
  height: 180px;
  background: ${Colors.primary};
`;

export const ListContainer = styled.View`
  margin-bottom: 0;
  background: ${Colors.white};
  padding: 10px;
  margin: 5px 0;
  border-width: 1px;
  border-color: ${Colors.primary};
  border-radius: 5px;
  min-height: 30px;
  flex-direction: column;
`;

export const Row = styled.View`
  width: 100%;
  flex-direction: row;
`;

export const SmallTitle = styled.Text`
  ${Type.body};
  color: ${Colors.black};
  font-size: 18px;
  margin-left: 10px;
`;

export const CenteredText = styled.Text`
  ${Type.body};
  color: ${Colors.primary};
  font-size: 18px;
  text-align: center;
`;

export const LabelPosition = styled.Text`
  ${Type.body};
  color: ${Colors.secondary};
  font-size: 18px;
  text-align: center;
`;

export const LabelPoints = styled.Text`
  ${Type.body};
  color: ${Colors.primary};
  font-size: 20px;
  position: absolute;
  right: 10px;
  top: 1px;
`;
export const InputName = styled.TextInput`
  ${Type.body};
  font-size: 20px;
  border-bottom-color: ${Colors.primary};
  border-bottom-width: 2px;
`;

export const ImageAction = styled.TouchableOpacity`
  width: ${(Dimensions.get('window').width - 30)};
  height: ${(Dimensions.get('window').width - 30)};

`;


export const ImageContainer = styled.View`
  width: 100%;
  height: 100%;
  justify-content: center;
  align-items: center;
`;

export const ImageOverlay = styled.View`
  width: 100%;
  height: 100%;
  position: absolute;
  top: 0;
  left: 0;
`;

export const ImageWithContent = styled.Image`
  width: 100%;
  height: 100%;
  position: absolute;
  top: 0;
  left: 0;
  justify-content: center;
  align-items: center;
`;

export const ImageBlock = styled.View`
  width: 100%;
  height: 100%;
  background: ${Colors.primary};
  justify-content: center;
  align-items: center;
`;