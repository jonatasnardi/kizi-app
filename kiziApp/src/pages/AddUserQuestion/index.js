import React, {Component} from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Icon from 'react-native-vector-icons/FontAwesome5';
import RadioGroup from 'react-native-radio-buttons-group';
import ImagePicker from 'react-native-image-picker';

import { Creators as UserInfoActions } from '../../store/ducks/userInfo';

import { 
  Wrapper,
  WrapperInner,
  LabelTitleTopic,
  Row,
  SmallTitle,
  LabelPoints,
  LabelPosition,
  Fieldset,
  LabelForm,
  InputName,
  ImageWithContent,
  ImageContainer,
  ImageOverlay,
  ImageAction,
  ImageBlock,
} from './styles';
import { numberToReal, getFirstWord, showAlert, showToast } from '../../utils/functions';
import I18n from '../../utils/translation/i18n';
import ButtonPrimary from '../../components/buttons/ButtonPrimary';
import FullLoader from '../../components/FullLoader';
import Colors from '../../utils/colors';
import { ScrollView, Alert, View, Switch } from 'react-native';
import { requestPayment } from '../../services/payments';
import ButtonSecondary from '../../components/buttons/ButtonSecondary';
import { getUserRankingList } from '../../services/leagues';
import Images from '../../utils/images';
import { addSuggestionQuestion } from '../../services/questions';
import { addUserQuestion } from '../../services/userQuestion';

class AddUserQuestion extends Component {
  state = {
    question: {
      question: '',
    },
    switchValueAnonymous: true,
    isLoadingAdd: false,
    isLoadingImage: false,
  }

  refInputName;

  constructor(props) {
    super(props);

    this.navigate = this.props.navigation;
  }

  componentDidMount() {
  }

  confirmAdd = () => {
    const { question } = this.state;
    const { user } = this.props;

    this.setState({
      isLoadingAdd: true,
    });

    addUserQuestion(user._id, this.navigate.state.params.to, question.question, this.state.switchValueAnonymous)
      .then((res) => { 
        this.setState({
          isLoadingAdd: false,
        });

        showAlert('Pergunta enviada', 'Você será notificado quando ela respondida 😎');

        this.props.navigation.goBack(null);
      })
      .catch((error) => {
        console.log(error);
        this.setState({
          isLoadingAdd: false
        });
        showToast(I18n.t('errorGenericMessage'));
      }); 
  }

  selectPhoto() {
    const options = {
      title: I18n.t('selectImage'),
      customButtons: [],
      quality: 0.3,
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    this.setState({
      isLoadingImage: true,
    })
    
    /**
     * The first arg is the options object for customization (it can also be null or omitted for default options),
     * The second arg is the callback which sends object: response (more info in the API Reference)
     */
    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);
    
      if (response.didCancel) {
        console.log('User cancelled image picker');
        this.setState({
          isLoadingImage: false,
        })
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
        this.setState({
          isLoadingImage: false,
        })
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
        this.setState({
          isLoadingImage: false,
        })
      } else {
        // const source = { uri: response.uri };
    
        // You can also display the image using data:
        const source = 'data:image/jpeg;base64,' + response.data;

        console.log('b64', source);

        this.setState({
          question: {
            ...this.state.question,
            image: source,
          },
          isLoadingImage: false,
        });
      }
    });
  }

  toggleSwitchAnonymous = (value) => {
    this.setState({
      switchValueAnonymous: value,
      question: {
        ...this.state.question,
        image: '',
      },
    });
  }

  onPressRadio = data => {
    this.setState({ alternatives: data });

    setTimeout(() => {
      let selectedButton = this.state.alternatives.find(e => e.selected == true);
        selectedButton = selectedButton ? selectedButton.label : this.state.alternatives[0].label;

      this.setState({
        question: {
          ...this.state.question,
          correctOption: selectedButton,
        }
      });
    }, 800);
  };

  onPressRadioWeight = data => {
    this.setState({ weights: data });

    setTimeout(() => {
      let selectedButton = this.state.weights.find(e => e.selected == true);
        selectedButton = selectedButton ? selectedButton.value + 1 : this.state.weights[0].value + 1;

      this.setState({
        question: {
          ...this.state.question,
          weight: selectedButton,
        }
      });
    }, 800);
  };

  isFormValid = () => {
    const { question } = this.state;
    let valid = false;

    if (
      question.question.length >= 2 
      ) {
        valid = true;
      }
    
    return valid;
  }

  render() {
    const { switchValueAnonymous } = this.state;

    return (
      <>
        <Wrapper>
          <ScrollView>
            <WrapperInner>
              <Fieldset>
                <LabelForm>Pergunta:</LabelForm>

                <InputName
                  ref={ref => (this.refInputName = ref)}
                  value={this.state.question.question}
                  onChangeText={(text) => this.setState({question: { ...this.state.question, question: text}})}
                  placeholder={'Ex: De quem você gosta?'}
                  placeholderTextColor={'#778aa5'}
                  multiline={true}>
                </InputName>
              </Fieldset>

              <Fieldset>
                <LabelForm>Perguntar anonimamente?</LabelForm>
                <View style={{ justifyContent: 'center', alignItems: 'flex-start' }}>
                  <Switch
                    onValueChange={this.toggleSwitchAnonymous}
                    value={switchValueAnonymous}/>
                </View>
                
                {/* { switchValueAnonymous && (
                  <View style={{ flexDirection: 'row', marginTop: 10 }}>
                    <ImageAction onPress={() => this.selectPhoto()} style={{paddingRight: 7.5}}>
                      {
                        this.state.question.image.length > 0 && (
                          <ImageContainer>
                            <ImageWithContent source={{uri: this.state.question.image}}/>
                            <ImageOverlay />
                            <Icon name="camera" size={45} color={Colors.white} />
                          </ImageContainer>
                        )
                      }

                      {
                        this.state.question.image.length == 0 && (
                          <ImageBlock>
                            <Icon name="camera" size={45} color={Colors.white} />
                          </ImageBlock>
                        )
                      }
                    </ImageAction>
                  </View>
                )} */}
                
              </Fieldset>

              <ButtonPrimary
                onPress={() => this.confirmAdd()}
                title={'Enviar pergunta'}
                disabled={!this.isFormValid()}
                backgroundColor={!(this.isFormValid()) ? Colors.lightSecondary : Colors.secondary}
                icon={'chevron-right'}>
              </ButtonPrimary>

              <View style={{ marginBottom: 200 }}/>
            </WrapperInner>
          </ScrollView>
          
        </Wrapper>

        {
          (this.state.isLoadingImage || this.state.isLoadingAdd) && (
            <FullLoader></FullLoader>
          )
        }
       </>
    )
  }
}

const mapStateToProps = state => ({
  addListRequest: state.userInfo.addListRequest,
  updateListsTitleRequest: state.userInfo.updateListsTitleRequest,
  lists: state.userInfo.lists,
  user: state.userInfo.user,
  language: state.userInfo.user.language,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(UserInfoActions, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(AddUserQuestion);