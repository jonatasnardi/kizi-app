import React, {Component} from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Icon from 'react-native-vector-icons/FontAwesome5';
import CountDown from 'react-native-countdown-component';
import { DotIndicator } from 'react-native-indicators';
import {fetch} from 'react-native-ssl-pinning';
import debounce from 'lodash.debounce';

import {
  AdMobBanner,
  AdMobRewarded,
} from 'react-native-admob';

import { Creators as UserInfoActions } from '../../store/ducks/userInfo';

import { 
  Wrapper,
  WrapperInner,
  ListContainer,
  Row,
  SmallTitle,
  LabelPoints,
  LabelPosition,
  LabelForm,
  TextMoney,
  BigBox,
  ImageIconUser,
  ContainerMainBanner,
  ContainerBanner,
} from './styles';
import { numberToReal, getFirstWord, showAlert, showToast, cutString, isIntoMinutes } from '../../utils/functions';
import I18n from '../../utils/translation/i18n';
import ButtonPrimary from '../../components/buttons/ButtonPrimary';
import FullLoader from '../../components/FullLoader';
import Colors from '../../utils/colors';
import { Alert, View, ScrollView, ActionSheetIOS, FlatList, Platform, TouchableOpacity, Image, Picker, Button } from 'react-native';
import { requestPayment } from '../../services/payments';
import ButtonSecondary from '../../components/buttons/ButtonSecondary';
import { getUserRankingList, getActiveMataMataRanking } from '../../services/leagues';
import { getCurrentRoundInfo } from '../../services/general';

import Images from '../../utils/images';
import { CURRENT_ROUND_NAME_ENUM, CURRENT_ROUND_DAY_ENUM, CURRENT_ROUND_HOUR_ENDS_ENUM, CURRENT_ROUND_DAY_NAME_ENUM } from '../../utils/enums';
import { BASE_URL_API } from '../../helpers/variables';

const withPreventDoubleClick = (WrappedComponent) => {

  class PreventDoubleClick extends React.PureComponent {

    debouncedOnPress = () => {
      this.props.onPress && this.props.onPress();
    }

    onPress = debounce(this.debouncedOnPress, 300, { leading: true, trailing: false });

    render() {
      return <WrappedComponent {...this.props} onPress={this.onPress} />;
    }
  }

  PreventDoubleClick.displayName = `withPreventDoubleClick(${WrappedComponent.displayName ||WrappedComponent.name})`
  return PreventDoubleClick;
}

const ButtonPrimaryEx = withPreventDoubleClick(ButtonPrimary);

class WeeklyRanking extends Component {
  state = {
    users: [],
    isLoadingAdd: false,
    iAmInGame: false,
    currentRound: 0,
    disabled: false,
    selectedValueGroup: this.props.user.group || '1',
  }

  rewardedBanner = Platform.select({
    android: 'ca-app-pub-4982456847713514/9290095372',
    ios: 'ca-app-pub-4982456847713514/4229340388',
  });

  refInputSuggestion;
  _scrollView;

  bottomBannerId = Platform.select({
    android: 'ca-app-pub-4982456847713514/5350850363',
    ios: 'ca-app-pub-4982456847713514/5542422057',
  }); 

  navigate;

  constructor(props) {
    super(props);

    this.navigate = this.props.navigation;
  }

  async componentDidMount() {
    this.loadItems();

    this.setState({
      currentRound: this.props.currentRound,
    });

    AdMobRewarded.addEventListener('rewarded', reward => {
      this.setState({
        isLoadingAdd: false,
      });
      this.props.updatePlayCount(0);
      this.navigate.push('Question', { tournamentType: 'weekly', onGoBack: () => this.refreshFunction() });
      AdMobRewarded.removeAllListeners();
    });
  }


  loadItems(group = this.props.user.group || '1') {
    this.setState({
      isLoadingAdd: true,
      users: [],
    });

    if (this.props.currentRound === 0) {

      fetch(`${BASE_URL_API}/league/list/weekly-ranking-mata-mata/Kidnc129a0kad/${group}`, {
        method: "GET" ,
        timeoutInterval: 30000,
        sslPinning: {
            certs: ["mycert"],
        },
        headers:{
          Authorization: `Bearer ${this.props.auth.token}`,
        }
      })
      .then(res => {
        // console.tron.log('res', res);
        res = JSON.parse(res.bodyString);
        // console.tron.log('res2', res);
        this.setState({
          users: res.users,
          isLoadingAdd: false,
        });
      })
      .catch(err => {
        // console.tron.log('err', err);
        console.log(error);
          showToast(I18n.t('errorGenericMessage'));
          this.setState({
            isLoadingAdd: false,
          });
      })


      // getUserRankingList(group)
      //   .then((res) => {
      //     this.setState({
      //       users: res.users,
      //       isLoadingAdd: false,
      //     });
      //   })
      //   .catch((error) => {
      //     console.log(error);
      //     showToast(I18n.t('errorGenericMessage'));
      //     this.setState({
      //       isLoadingAdd: false,
      //     });
      //   });
    } else {
      getActiveMataMataRanking()
        .then((res) => { 
          const index1 = res.mata.findIndex(x => x.home._id === this.props.user._id);
          const index2 = res.mata.findIndex(x => x.away._id === this.props.user._id);

          let iAmInGame = false;
          let cloneItem;

          if (index1 > -1) {
            cloneItem = res.mata[index1];
            res.mata.splice(index1, 1);
            res.mata.unshift(cloneItem);
            iAmInGame = true;
          }
          if (index2 > -1) {
            cloneItem = res.mata[index2];
            res.mata.splice(index2, 1);
            res.mata.unshift(cloneItem);
            iAmInGame = true;
          }
          

          this.setState({
            users: res.mata,
            isLoadingAdd: false,
            iAmInGame,
          });

        })
        .catch((error) => {
          console.log(error);
          showToast(I18n.t('errorGenericMessage'));
          this.setState({
            isLoadingAdd: false,
          });
        });
    }
  }

  getSecondsToFinish = () => {
    let t1 = new Date();
    let t2 = this.nextDayOfWeek(CURRENT_ROUND_DAY_ENUM[this.state.currentRound]);
    t2.setHours(CURRENT_ROUND_HOUR_ENDS_ENUM[this.state.currentRound], 0, 0, 0);
    
    let dif = t1.getTime() - t2.getTime();

    let Seconds_from_T1_to_T2 = dif / 1000;
    let Seconds_Between_Dates = Math.abs(Seconds_from_T1_to_T2);

    if (Math.sign(Seconds_from_T1_to_T2) === 1) {
      return 0;
    } else {
      return Seconds_Between_Dates;
    }
  }

  nextDayOfWeek = (x) => {
    let now = new Date();    
    now.setDate(now.getDate() + (x+(7-now.getDay())) % 7);
    return now;
  }

  getBgColor = (index) => {
    if (index < 4) {
      return '#56bcb5';
    }

    return Colors.white;
  }

  refreshFunction = () => {
    this.loadItems();
  }

  playnow = () => {
    if (this.props.playCount >= 30) {
      this.setState({
        isLoadingAdd: true,
        disabled: true,
      });
      
      // enable after 5 second
      setTimeout(()=>{
         this.setState({
          disabled: false,
        });
      }, 10000)
      
      AdMobRewarded.setAdUnitID(this.rewardedBanner);
      AdMobRewarded.requestAd().then(() => {
        this.setState({
          isLoadingAdd: false,
        });
        AdMobRewarded.showAd();
      }).catch(error => {
        this.setState({
          isLoadingAdd: false,
        });
        this.props.updatePlayCount(5);
        // this.navigate.push('Question', { tournamentType: 'weekly', onGoBack: () => this.refreshFunction() });
      });
  
      
    } else {
      this.props.updatePlayCount((this.props.playCount || 0) + 1);
      this.navigate.push('Question', { tournamentType: 'weekly', onGoBack: () => this.refreshFunction() });
    }
    
  }

  onPressAction = () => {
    let options = ['Cancelar'];

    for (let i = 0; i < 32; i++) {
      options.push(`Grupo ${i + 1}`);
    }

    ActionSheetIOS.showActionSheetWithOptions(
      {
        options: options,
        destructiveButtonIndex: 0,
        cancelButtonIndex: 0
      },
      buttonIndex => {
        if (buttonIndex === 0) {
          // cancel action
        } else {
          this.setSelectedGroup(buttonIndex.toString())
        }
      }
    );
  }
    

  setSelectedGroup = (group) => {
    this.loadItems(group);

    this.setState({
      selectedValueGroup: group
    });
  }

  render() {
    const { users } = this.state;
    const { user } = this.props;

    return (
      <>
        <Wrapper>
          
          <ScrollView ref={(scrollView) => { this._scrollView = scrollView; }}>
          <ContainerMainBanner>
                <LabelForm>Só depende de você :){'\n'}Toda semana tem um vencedor!</LabelForm>
                <BigBox>
                  <TextMoney>VALENDO { numberToReal(this.props.awardWeekly)}</TextMoney>
                </BigBox>
                {/* <LabelFormMin>Você pode jogar todos os dias{'\n'}e quantas vezes quiser</LabelFormMin> */}
              </ContainerMainBanner>
            <WrapperInner>
              
              {/* <Row style={{ justifyContent: 'center' }}>
                <Icon name="trophy" size={60} color={'#f8bc44'} />
              </Row> */}

              {
                (this.state.iAmInGame || this.state.currentRound === 0) ? (
                  <ButtonPrimaryEx
                    onPress={() => this.playnow()}
                    title={'JOGAR AGORA'}
                    icon={'gamepad'}>
                  </ButtonPrimaryEx>
                ) : (
                  <LabelPosition style={{ color: Colors.primary, fontWeight: 'bold', marginTop: -10, marginBottom: 10 }}>Você foi eliminado 😢{'\n'}Aguarde o novo torneio, que iniciará na quinta-feira às 22:00h. Enquanto isso, dispute o torneio do dia 😄</LabelPosition>
                )
              }

              <View style={{ marginTop: 10 }} />

              <ButtonPrimary
                onPress={() => this._scrollView.scrollTo({y: 300})}
                title={'Ver ranking'}
                backgroundColor={Colors.primary}
                icon={'medal'}>
              </ButtonPrimary>
              

              <View style={{ marginTop: 10 }} />

              <ButtonPrimary
                onPress={() => this.navigate.push('WeeklyTournament')}
                backgroundColor={Colors.primary}
                title={'Premiações e horários'}
                icon={'dollar-sign'}>
              </ButtonPrimary>

              <Row style={{ justifyContent: 'center', marginTop: 10 }}>
                <LabelPosition style={{ color: Colors.primary }}>Se encerra em:</LabelPosition>
              </Row>
              <Row style={{ justifyContent: 'center', marginTop: 10 }}>
                <CountDown
                  until={this.getSecondsToFinish()}
                  onFinish={() => {}}
                  onPress={() => {}}
                  size={20}
                  digitStyle={{backgroundColor: Colors.primary}}
                  digitTxtStyle={{color: Colors.secondary}}
                  timeLabels={{d: 'Dia(s)', h: 'Hora(s)', m: 'Min.', s: 'Seg.'}}
                />
              </Row>

              <Row style={{ justifyContent: 'center', marginTop: 0 }}>
                <LabelPosition style={{ color: Colors.secondary }}>Esta rodada se encerra { CURRENT_ROUND_DAY_NAME_ENUM[this.state.currentRound] } às { CURRENT_ROUND_HOUR_ENDS_ENUM[this.state.currentRound] }:00h</LabelPosition>
              </Row>

              <Row style={{ justifyContent: 'center', marginTop: 10 }}>
                {
                  (this.state.iAmInGame || this.state.currentRound === 0 || this.state.isLoadingAdd) && (
                    <LabelPosition style={{ color: Colors.primary, fontWeight: 'bold' }}>Você tem { user.weekPointsMataMata } pontos</LabelPosition>
                  )
                }
                
              </Row>

              <Row style={{ flexDirection: 'column', justifyContent: 'center', marginTop: 10, marginBottom: 10 }}>
                <LabelPosition style={{ fontSize: 20, color: Colors.secondary, fontWeight: 'bold'}}>{CURRENT_ROUND_NAME_ENUM[this.state.currentRound]}</LabelPosition>
                {
                  (this.state.currentRound === 0) && (
                    <>
                        {
                          Platform.OS === 'android' ? (
                            <Row style={{flexDirection: 'column', height: 50, justifyContent: 'center', position: 'relative', top: 10 }}>
                            <Picker
                              selectedValue={this.state.selectedValueGroup.toString()}
                              style={{ height: 30, width: 150 }}
                              itemStyle={{height: 30}}
                              onValueChange={(itemValue, itemIndex) => this.setSelectedGroup(itemValue)}
                            >
                              <Picker.Item label="Grupo 1" value="1" />
                              <Picker.Item label="Grupo 2" value="2" />
                              <Picker.Item label="Grupo 3" value="3" />
                              <Picker.Item label="Grupo 4" value="4" />
                              <Picker.Item label="Grupo 5" value="5" />
                              <Picker.Item label="Grupo 6" value="6" />
                              <Picker.Item label="Grupo 7" value="7" />
                              <Picker.Item label="Grupo 8" value="8" />
                              <Picker.Item label="Grupo 9" value="9" />
                              <Picker.Item label="Grupo 10" value="10" />
                              <Picker.Item label="Grupo 11" value="11" />
                              <Picker.Item label="Grupo 12" value="12" />
                              <Picker.Item label="Grupo 13" value="13" />
                              <Picker.Item label="Grupo 14" value="14" />
                              <Picker.Item label="Grupo 15" value="15" />
                              <Picker.Item label="Grupo 16" value="16" />
                              <Picker.Item label="Grupo 17" value="17" />
                              <Picker.Item label="Grupo 18" value="18" />
                              <Picker.Item label="Grupo 19" value="19" />
                              <Picker.Item label="Grupo 20" value="20" />
                              <Picker.Item label="Grupo 21" value="21" />
                              <Picker.Item label="Grupo 22" value="22" />
                              <Picker.Item label="Grupo 23" value="23" />
                              <Picker.Item label="Grupo 24" value="24" />
                              <Picker.Item label="Grupo 25" value="25" />
                              <Picker.Item label="Grupo 26" value="26" />
                              <Picker.Item label="Grupo 27" value="27" />
                              <Picker.Item label="Grupo 28" value="28" />
                              <Picker.Item label="Grupo 29" value="29" />
                              <Picker.Item label="Grupo 30" value="30" />
                              <Picker.Item label="Grupo 31" value="31" />
                              <Picker.Item label="Grupo 32" value="32" />
                            </Picker>
                            </Row>
                          ) : (
                            <Row style={{ flexDirection: 'column', height: 70, justifyContent: 'center', position: 'relative', top: 0 }}>
                              <LabelPosition style={{ fontSize: 20, color: Colors.secondary, fontWeight: 'bold', marginBottom: 15}}>GRUPO {this.state.selectedValueGroup.toString()}</LabelPosition>
                              <Button onPress={() => this.onPressAction()} title="Ver outro grupo" />
                            </Row>
                          )
                        }
                        
                        
                      {/* <LabelPosition style={{ fontSize: 20, color: Colors.secondary, fontWeight: 'bold'}}>GRUPO {user.group}</LabelPosition> */}
                    </>
                  )
                }
                
              </Row>

              

              {
                (this.state.currentRound === 0) && (
                  <>
                    <Row style={{ flexDirection: 'row',  marginTop: 10, marginBottom: 10 }}>
                      <View style={{ width: 45, height: 25, backgroundColor: '#56bcb5' }} />
                      <SmallTitle style={{ top: 1 }}>Zona de classificação</SmallTitle>
                    </Row>
                    <Row>
                      <LabelPosition style={{ top: 1 }}>Pos.</LabelPosition>
                      <SmallTitle style={{ top: 1 }}>Usuário</SmallTitle>
                      <LabelPoints style={{ top: 1 }}>Pontos</LabelPoints>
                    </Row>
                  </>
                )
              }

              {
                this.state.isLoadingAdd && (
                  <View style={{ marginTop: 10}}>
                    <DotIndicator color={Colors.secondary} count={3} size={16} />
                  </View>
                  
                )
              }
              
              {
                (this.state.currentRound === 0) ? (
                  <FlatList
                    data={users}
                    renderItem={({ item, index }) => (
                      <TouchableOpacity onPress={() => {
                        if (item._id !== user._id) {
                          this.navigate.push('FriendProfile', {
                            idUser: item._id,
                          })
                        }
                      }}>
                        <ListContainer style={{ backgroundColor: this.getBgColor(index) }}>
                          <Row>
                            <LabelPosition style={{ color: Colors.primary }}>{ index + 1 }º</LabelPosition>
                            {
                              !item.image ? (
                                <ImageIconUser source={Images['1']} />
                              ) : (
                                <ImageIconUser source={{uri: item.image}} />
                              )
                            }
                            {
                              (item.lastAccess && isIntoMinutes(30, item.lastAccess)) && (
                                <View style={{ backgroundColor: Colors.green, position: 'absolute', left: (index > 9) ? 60 : 55, bottom: 2, width: 14, height: 14, borderRadius: 14 }} />
                              )
                            }
                            
                            <View style={{ flexDirection: 'row' }}>
                              <SmallTitle style={{ fontWeight: this.props.user._id === item._id ? 'bold' : 'normal' }}>{ cutString(item.name) }</SmallTitle>
                              {
                                item.verified && (
                                  <Image style={{ width: 18, height: 18, position: 'relative', top: 8 }} source={require('../../assets/verified.png')} />
                                )
                              }
                            </View>
                            
                            <LabelPoints>{ item.weekPointsMataMata }</LabelPoints>
                          </Row>
                        </ListContainer>
                      </TouchableOpacity>
                    )}
                  />
                ) : (
                  <FlatList
                    data={users}
                    renderItem={({ item, index }) => (
                      <>
                        <Row>
                          {
                            this.state.currentRound === 7 ? (
                              <LabelPosition style={{ color: Colors.primary, top: 1 }}>Decisão 🏆</LabelPosition>
                            ) : (
                              <LabelPosition style={{ color: Colors.primary, top: 1 }}>Confronto { index + 1 }</LabelPosition>
                            )
                          }

                          <LabelPoints style={{ top: 1 }}>Pontos</LabelPoints>
                        </Row>
                        <ListContainer style={{ backgroundColor: this.getBgColor(1) }}>
                          <TouchableOpacity onPress={() => {
                              if (item.home._id !== user._id) {
                                this.navigate.push('FriendProfile', {
                                  idUser: item.home._id,
                                })
                              }
                            }}>
                            <Row style={{ marginBottom: 10 }}>
                              {/* <LabelPosition style={{ color: Colors.primary }}>{ index + 1 }º</LabelPosition> */}
                              {
                                !item.home.image ? (
                                  <ImageIconUser source={Images['1']} />
                                ) : (
                                  <ImageIconUser source={{uri: item.home.image}} />
                                )
                              }
                              {
                                (item.home.lastAccess && isIntoMinutes(30, item.home.lastAccess)) && (
                                  <View style={{ backgroundColor: Colors.green, position: 'absolute', left: 37, bottom: 2, width: 14, height: 14, borderRadius: 14 }} />
                                )
                              }
                              
                              <SmallTitle style={{ fontWeight: this.props.user._id === item.home._id ? 'bold' : 'normal' }}>{ cutString(item.home.name) }</SmallTitle>
                              <LabelPoints>{ item.home.weekPointsMataMata }</LabelPoints>
                            </Row>
                          </TouchableOpacity>
                          <TouchableOpacity onPress={() => {
                              if (item.away._id !== user._id) {
                                this.navigate.push('FriendProfile', {
                                  idUser: item.away._id,
                                })
                              }
                            }}>
                            <Row>
                              {/* <LabelPosition style={{ color: Colors.primary }}>{ index + 1 }º</LabelPosition> */}
                              {
                                !item.away.image ? (
                                  <ImageIconUser source={Images['1']} />
                                ) : (
                                  <ImageIconUser source={{uri: item.away.image}} />
                                )
                              }
                              {
                                (item.away.lastAccess && isIntoMinutes(30, item.away.lastAccess)) && (
                                  <View style={{ backgroundColor: Colors.green, position: 'absolute', left: 37, bottom: 2, width: 14, height: 14, borderRadius: 14 }} />
                                )
                              }
                              
                              <SmallTitle style={{ fontWeight: this.props.user._id === item.away._id ? 'bold' : 'normal' }}>{ cutString(item.away.name) }</SmallTitle>
                              <LabelPoints>{ item.away.weekPointsMataMata }</LabelPoints>
                            </Row>
                          </TouchableOpacity>
                        </ListContainer>
                      </>
                    )}
                  />
                )
              }
              <View style={{ marginBottom: 200 }} />
            </WrapperInner>
          </ScrollView>
          
        </Wrapper>

        

        <ContainerBanner>
          <AdMobBanner
            adSize="fullBanner"
            adUnitID={this.bottomBannerId}
            height={90}
          />
        </ContainerBanner>
       </>
    )
  }
}

const mapStateToProps = state => ({
  addListRequest: state.userInfo.addListRequest,
  updateListsTitleRequest: state.userInfo.updateListsTitleRequest,
  currentRound: state.userInfo.currentRound,
  updatePlayCount: state.userInfo.updatePlayCount,
  playCount: state.userInfo.playCount,
  awardWeekly: state.userInfo.awardWeekly,
  lists: state.userInfo.lists,
  user: state.userInfo.user,
  auth: state.userInfo.auth,
  language: state.userInfo.user.language,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(UserInfoActions, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(WeeklyRanking);