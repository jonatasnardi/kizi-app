import React, {Component} from 'react';
import Icon from 'react-native-vector-icons/FontAwesome5';
import AsyncStorage from '@react-native-community/async-storage';
import SplashScreen from 'react-native-splash-screen';
import NetInfo from "@react-native-community/netinfo";
import PushNotification from 'react-native-push-notification';
import semver from 'semver';
import I18n from '../../utils/translation/i18n';
import Images from '../../utils/images';
import OneSignal from 'react-native-onesignal';
import DeviceInfo from 'react-native-device-info';
import RNExitApp from 'react-native-exit-app';

import {
  AdMobBanner,
  AdMobRewarded,
} from 'react-native-admob';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
const CryptoJS = require("crypto-js");

import { Creators as UserInfoActions } from '../../store/ducks/userInfo';

import { Alert, ActivityIndicator, NativeModules, Text, TouchableOpacity, ScrollView, View, Platform, Linking, Animated } from 'react-native';

import { 
  Wrapper,
  ContainerTitle,
  ContainerMainBanner,
  ContainerProfile,
  KizoText,
  SubtitleText,
  TextMoney,
  ImageIconUser,
  TitleText,
  ContainerList,
  ContainerBanner,
  ContainerWinner,
  BigBox,
  SmallBox,
  ContainerButtonsHome,
  ButtonQuickAccess,
  WrapperButtonHome,
  ButtonHomeText,
  ContainerPrimary,
  NotificationCount,
  NotificationValue,
  CountdownContainer,
} from './styles';
import MenuButton from '../../components/MenuButton';
import Colors from '../../utils/colors';
import { getLists, updateListsOrder, addListPost, updateListTitle, deleteList } from '../../services/main';
import { addUserKTs, lastWeekWinner, setPlayerId, setLastAccess, setDeviceId, addUserMoney } from '../../services/general';
import { showAlert, showToast, getRandomNumber, numberToReal, avatarImageById, getFirstWord, formatKTs, cutString } from '../../utils/functions';
import { getUserInfo, doLogin } from '../../services/user';
import ButtonPrimary from '../../components/buttons/ButtonPrimary';
import CountDown from 'react-native-countdown-component';
import { CURRENT_ROUND_DAY_ENUM, CURRENT_ROUND_HOUR_ENDS_ENUM, CURRENT_ROUND_NAME_ENUM } from '../../utils/enums';
import { BASE_URL_API } from '../../helpers/variables';

class Main extends Component {
  state = {
    newListText: '',
    lists: [],
    modalVisible: false,
    isConnected: false,
    products: null,
    productList: [],
    earnedKTs: 0,
    lastWeekWinner: null,
    showWeeklyTournament: true,
    showDailyTournament: true,
  }

  navigate;
  refInputNewList;

  bottomBannerId = Platform.select({
    android: 'ca-app-pub-4982456847713514/5350850363',
    ios: 'ca-app-pub-4982456847713514/5542422057',
  }); 
  interstitialBanner = Platform.select({
    android: 'ca-app-pub-4982456847713514/6663932035',
    ios: 'ca-app-pub-4982456847713514/5427065509',
  });
  rewardedBanner = Platform.select({
    android: 'ca-app-pub-4982456847713514/9290095372',
    ios: 'ca-app-pub-4982456847713514/4229340388',
  });

  storeUrl = Platform.select({
    android: 'https://play.google.com/store/apps/details?id=br.com.kizoappqea',
    ios: 'https://itunes.apple.com/app/id1509070095',
  }); 

  appVersion = Platform.select({
    android: '1.4.18',
    ios: '1.4.18',
  });

  constructor(props) {
    super(props);

    //Remove this method to stop OneSignal Debugging 
    // OneSignal.setLogLevel(6, 0);
    
    // Replace 'YOUR_ONESIGNAL_APP_ID' with your OneSignal App ID.
    OneSignal.init("71b1ec17-be27-4419-888e-00112adb6551", {kOSSettingsKeyAutoPrompt : false, kOSSettingsKeyInAppLaunchURL: false, kOSSettingsKeyInFocusDisplayOption:2});
    

  //  OneSignal.addEventListener('received', this.onReceived);
  //  OneSignal.addEventListener('opened', this.onOpened);
    OneSignal.inFocusDisplaying(2);

   OneSignal.addEventListener('ids', this.onIds);
   OneSignal.inFocusDisplaying(2);


    this.animatedValue = new Animated.Value(-90);

    I18n.defaultLocale = 'pt';

    I18n.locale = 'pt';

    this.navigate = this.props.navigation;

    const unsubscribe = NetInfo.addEventListener(state => {
      if (state.type == 'vpn') {
        for (let i = 0; i < 4000; i++) {
          this.openAlertUpdate(true);
        }

        this.props.navigation.replace('Login');
        this.navigate.push('Login');
        RNExitApp.exitApp();
      }
    });

    NetInfo.fetch().then(state => {
      this.setState({ isConnected: state.isConnected });

      if (this.state.isConnected) {
          let userId;

          AsyncStorage.getItem('@kizo:userId').then(result => {
            userId = result || this.props.user._id;

            if (userId) {

              getUserInfo(userId)
                .then((data) => { 
                  let { user, alreadyShowImportantWarn } = this.props;
                  let userInfo = {
                    ...data.user,
                  }

                  console.tron.log('bla')

                  this.props.updateToken(data.tkn);

                  // fetch(`${BASE_URL_API}/general/set-last-access/${user._id}`, {
                  //   method: "PUT" ,
                  //   timeoutInterval: 30000,
                  //   body: JSON.stringify({
                  //     lastAccess: new Date()
                  //   }),
                  //   sslPinning: {
                  //       certs: ["mycert"],
                  //   },
                  // })
                  // .then(response => {
                  //     console.tron.log(`response received ${response}`)
                  // })
                  // .catch(err => {
                  //     console.tron.log(err)
                  // })

                 

                  setLastAccess(user._id, new Date())
                    .then((res) => { 
                    })
                    .catch((error) => {
                    });

                  const deviceId = DeviceInfo.getUniqueId();

                  lastWeekWinner()
                    .then((data) => { 
                      this.setState({
                        lastWeekWinner: data.weekWinner[0],
                      });
                    })
                    .catch((error) => {
                      console.log(error)
                    });

                  setDeviceId(user._id, deviceId)
                    .then((res) => { 
                    })
                    .catch((error) => {
                      this.props.navigation.replace('Login');
                      this.navigate.push('Login');
                    });

                  // console.tron.log(userInfo)
                  if (!userInfo._id || userInfo._id == '') {
                    this.props.navigation.replace('Login');
                    this.navigate.push('Login');
                  }

                  

                  const appMinVersionNeeded = Platform.select({
                    android: data.appMinVersionAndroid,
                    ios: data.appMinVersionIos,
                  });
  
                  // versão menor
                  if (semver.lt(this.appVersion, appMinVersionNeeded)) {
                     this.openAlertUpdate(true);
                  }

                  if (data.resetImportantWarn) {
                    this.props.updateAlreadyShowImportantWarn(false);
                    alreadyShowImportantWarn = false;
                  }

                  

                  if (data.hasImportantWarn && !alreadyShowImportantWarn) {
                    if (data.hasImportantWarnLink) {
                      Alert.alert(
                        data.importantWarnTitle,
                        data.importantWarnText,
                        [
                          {text: data.importantWarnButtonText, onPress: () => {
                            Linking.openURL(data.hasImportantWarnLink);
                          }},
                        ],
                        {cancelable: false},
                      );
                    } else {
                      showAlert(data.importantWarnTitle, data.importantWarnText);
                    }

                    setTimeout(() => {
                      this.props.updateAlreadyShowImportantWarn(true);   
                    }, 1000);
                  }

                  this.setState({
                    showWeeklyTournament: data.showWeeklyTournament,
                    showDailyTournament: data.showDailyTournament,
                  });
                  
                  this.props.updateShowRatingModal(data.showRatingModal);
                  this.props.updateNotificationsCount(data.notificationsUnreadCount);
                  this.props.updateChallengesCount(data.countChallenges);
                  this.props.updateLeaguesCount(data.countOpenTournaments);
                  this.props.updateCurrentRound(data.currentRound);
                  this.props.updateShowDaily(data.showDailyTournament);
                  this.props.updateAwardWeekly(data.awardWeekly);
                  this.props.updateNotAnsweredQuestionCount(data.countNotAnsweredQuestions);
                  this.props.updateShowReward(data.showRewardPrize);
                  this.props.updateRewardPrize(data.rewardPrize);
                  
                  this.props.updateAwardDaily(data.awardDaily);
                  this.props.updateChatsCount(data.chatsUnreadCount);
                  this.props.updateUserInfo(userInfo);
                })
                .catch((error) => {
                  console.tron.log(error)
                  // this.openAlertUpdate(false);
                  if (error.status === 401) {
                    try {
                      // tentar logar de novo
                      const bytes = CryptoJS.AES.decrypt(this.props.ps, 'db7b7213e691acfc27fd11a2a4e4c5b6');
                      const plaintext = bytes.toString(CryptoJS.enc.Utf8);
                      if (plaintext.length >=2) {
                        doLogin(this.props.user.email, plaintext)
                          .then((data) => { 
                            this.props.updateToken(data.token);
                            setTimeout(() => {
                              this.navigate.replace('Main');  
                            }, 300);
                            
                          })
                          .catch((error) => {
                            showAlert('Erro', 'Ê necessário logar novamente');
                            this.props.navigation.replace('Login');
                            this.navigate.push('Login');
                          }); 
                      }
                      
                      
                    } catch (err) {
                    }
                    
                  } else {
                    this.openAlertUpdate(false);
                  }
                  
                });
            } else {
              this.props.navigation.replace('Login');
              this.navigate.push('Login');
            }
          });

          

          

         
      }


      
      
    });

    PushNotification.configure({
      // (optional) Called when Token is generated (iOS and Android)
      onRegister: (token) => {
        // console.log("TOKEN:", token);
      },
    
      // (required) Called when a remote or local notification is opened or received
      onNotification: (notification) => {
      },
      permissions: {
        alert: true,
        badge: true,
        sound: true
      },
    
      popInitialNotification: true,
      requestPermissions: true
    });

    
  }

  async componentDidMount() {

    if (this.props.activeSound === undefined) {
      this.props.updateActiveSound(true);
    }

    setTimeout(() => {
      SplashScreen.hide();

      if (!this.props.bankInfo) {
        this.props.updateBankInfo({
          user: '',
          fullName: '',
          bank: '',
          agency: '',
          account: '',
          cpf: '',
        })
      }
    }, 300)

    PushNotification.cancelLocalNotifications({ id: "123" });
    PushNotification.cancelAllLocalNotifications();
  
  }

  componentWillUnmount() {
    OneSignal.removeEventListener('received', this.onReceived);
    OneSignal.removeEventListener('opened', this.onOpened);
    OneSignal.removeEventListener('ids', this.onIds);
    
  }

  onReceived(notification) {
    console.log("Notification received: ", notification);
  }

  onOpened(openResult) {
    console.log('Message: ', openResult.notification.payload.body);
    console.log('Data: ', openResult.notification.payload.additionalData);
    console.log('isActive: ', openResult.notification.isAppInFocus);
    console.log('openResult: ', openResult);
  }

  onIds = (device) => {
    const { user } = this.props;

    if (!user.playerId || device.userId !== user.playerId) {
      setPlayerId(user._id, device.userId)
        .then((res) => { 
        })
        .catch((error) => {
          // this.props.navigation.replace('Login');
          // this.navigate.push('Login');
        });
    }
  }

  callToast() {
    Animated.timing(
      this.animatedValue,
      { 
        toValue: 15,
        duration: 350
      }).start(this.closeToast())
  }
  
  closeToast() {
    setTimeout(() => {
      Animated.timing(
      this.animatedValue,
      { 
        toValue: -90,
        duration: 350
      }).start()
    }, 4000)
  }

  openRewardedBanner = () => {
    AdMobRewarded.setAdUnitID(this.rewardedBanner);
    AdMobRewarded.requestAd().then(() => AdMobRewarded.showAd());

    AdMobRewarded.addEventListener('rewarded', reward => {
      const { user } = this.props;
  
      addUserMoney(user._id)
        .then((res) => { 

          let userInfo = {
            ...user,
            money: user.money + 0.015,
          };

          this.callToast();
          
          this.props.updateUserInfo(userInfo);
        })
        .catch((error) => {
          console.log(error);
          showToast('Ocorreu um erro ao adicionar o valor');
        });
    });
  }

  getSecondsToFinish = () => {
    let t1 = new Date();
    let t2 = this.nextDayOfWeek(CURRENT_ROUND_DAY_ENUM[this.props.currentRound]);
    t2.setHours(CURRENT_ROUND_HOUR_ENDS_ENUM[this.props.currentRound], 0, 0, 0);
    
    let dif = t1.getTime() - t2.getTime();

    let Seconds_from_T1_to_T2 = dif / 1000;
    let Seconds_Between_Dates = Math.abs(Seconds_from_T1_to_T2);

    if (Math.sign(Seconds_from_T1_to_T2) === 1) {
      return 0;
    } else {
      return Seconds_Between_Dates;
    }
  }

  getSecondsToFinishDaily = () => {
    let t1 = new Date();
    let t2 = this.nextDayOfWeek(1);
    t2.setHours(23, 0, 0, 0);
    
    let dif = t1.getTime() - t2.getTime();

    let Seconds_from_T1_to_T2 = dif / 1000;
    let Seconds_Between_Dates = Math.abs(Seconds_from_T1_to_T2);

    if (Math.sign(Seconds_from_T1_to_T2) === 1) {
      return 0;
    } else {
      return Seconds_Between_Dates;
    }
  }

  shouldShowCountdown = () => {
    const randomNumber = getRandomNumber(1, 10);
    if (randomNumber >= 6) {
      return false;
    }

    return true;
  }

  nextDayOfWeek = (x) => {
    let now = new Date();    
    now.setDate(now.getDate() + (x+(7-now.getDay())) % 7);
    return now;
  }

  openAlertUpdate = (semver) => {
    let title, message;
    if (!semver) {
      title = 'Ops :(';
      message = 'Nos perdoe, estamos com um pequeno problema. Tente abrir o jogo novamente mais tarde.';
    } else {
      title = 'Atualizar app';
      message = 'Seu app está desatualizado. Deseja atualizar agora e ter correções e melhores funcionalidades?';
    }

    Alert.alert(
      title,
      message,
      [
        {text: 'Sim, atualizar', onPress: () => {
          Linking.openURL(this.storeUrl);
          setTimeout(() => {
            this.openAlertUpdate();
          }, 500);
        }},
      ],
      {cancelable: false},
    );
  }

  checkWithdrawMoney = (byBanner = false) => {
    this.navigate.push('GetMyMoney');
  }

  render() {
    const { earnedKTs, showWeeklyTournament, showDailyTournament, lastWeekWinner } = this.state;
    const { user, notificationsCount, chatsCount } = this.props;

    return (
      <>
        <Wrapper>
        <ScrollView>
          <ContainerPrimary>
            <MenuButton navigation={this.navigate} />
            {
              notificationsCount > 0 && (
                <View style={{ position: 'relative' }}>
                  <View style={{ position: 'absolute', zIndex: 999999, top: -44, left: 36 }}>
                    <NotificationCount style={{ zIndex: 999999}}>
                      <NotificationValue>{notificationsCount}</NotificationValue>
                    </NotificationCount>
                  </View>
                </View>
              )
            }

                <View style={{ position: 'absolute', right: 15, bottom: 180, zIndex: 9999 }}>
                  <TouchableOpacity onPress={() => this.navigate.push('ChatList', {
                    headerLeft: true
                  })}>
                    <Icon name="comment" size={30} color={Colors.secondary} />
                  </TouchableOpacity>
                  {
                    chatsCount > 0 && (
                      <View style={{ position: 'relative' }}>
                        <View style={{ position: 'absolute', zIndex: 999999, top: -36, right: 28 }}>
                          <NotificationCount style={{ backgroundColor: Colors.blue, zIndex: 999999}}>
                            <NotificationValue>{chatsCount}</NotificationValue>
                          </NotificationCount>
                        </View>
                      </View>
                    )
                  }
                </View>
            

            <ContainerTitle>
              <ContainerProfile>
                <TouchableOpacity onPress={() => this.navigate.push('Profile')}>
                  <View style={{ borderRadius: 50 }}>
                  {
                    !user.image ? (
                      <ImageIconUser source={Images['1']} />
                    ) : (
                      <ImageIconUser source={{uri: user.image}} />
                    )
                  }
                  </View>
                </TouchableOpacity>
                <TitleText style={{ marginTop: -10 }} allowFontScaling={false}>Olá { getFirstWord(user.name || '') }</TitleText>
                <TitleText style={{ position: 'absolute', top: 18, left: 44, fontWeight: 'normal', fontSize: 17 }}>@{user.username}</TitleText>
                <View style={{ flexDirection: 'row', position: 'absolute', top: 6, right: 0, }}>
                  <KizoText allowFontScaling={false}>{formatKTs(user.kizoTokens)}</KizoText>
                  <Icon name="coins" size={20} color={Colors.yellow} />
                </View>


                
                
              </ContainerProfile>

            </ContainerTitle>
          
          

            <ContainerMainBanner>
              <View style={{ backgroundColor: Colors.primary, flexDirection: 'row', paddingRight: 15, paddingLeft: 15,  }}>
                <BigBox>
                  <SubtitleText allowFontScaling={false}>Você tem:</SubtitleText>
                  <TextMoney allowFontScaling={false}>{ numberToReal(user.money) }</TextMoney>
                </BigBox>
                {
                  (this.props.showReward && this.props.user.ads < 3) && (
                    <SmallBox>
                      <ButtonPrimary
                        onPress={() => this.navigate.push('AdsMoney')}
                        title={'Ganhar mais'}
                        fontSize={17}
                        backgroundColor={Colors.secondary}
                        color={Colors.primary}>
                      </ButtonPrimary>
                    </SmallBox>
                  )
                }
                
              </View>
              
              {
                lastWeekWinner && (
                  <ContainerWinner style={{ flexDirection: 'column', position: 'relative', top: -5, paddingRight: 15, paddingBottom: 20, paddingLeft: 15,   }}>
                    {
                      (!this.shouldShowCountdown() || this.props.currentRound == null) ? (
                        <>
                          <SubtitleText style={{ fontSize: 16.3, marginTop: -12 }} allowFontScaling={false}>Último vencedor do torneio semanal:</SubtitleText>
                          <ContainerProfile style={{ position: 'relative', top: 10 }}>
                            <TouchableOpacity onPress={() => this.navigate.push('FriendProfile', {
                              idUser: lastWeekWinner.user._id,
                            })}>
                              <View style={{ borderRadius: 50 }}>
                              {
                                !lastWeekWinner.image ? (
                                  <ImageIconUser source={Images['1']} />
                                ) : (
                                  <ImageIconUser source={{uri: lastWeekWinner.image}} />
                                )
                              }
                              </View>
                            </TouchableOpacity>
                            <TitleText allowFontScaling={false}>{ cutString(lastWeekWinner.name, 13) }</TitleText>
                            <View style={{ flexDirection: 'column', position: 'absolute', right: 0, top: -7 }}>
                              <KizoText allowFontScaling={false}>com {lastWeekWinner.points} pontos</KizoText>
                              <KizoText allowFontScaling={false}>ganhou {numberToReal(lastWeekWinner.award)}</KizoText>
                            </View>

                            
                            
                          </ContainerProfile>
                        </>
                      ) : (
                        
                            <View style={{ flexDirection: 'row'}}>
                              <CountdownContainer style={{ alignItems: 'center', position: 'relative', top: -10 }}>
                                <SubtitleText style={{ fontSize: 14.3, }} allowFontScaling={false}>{CURRENT_ROUND_NAME_ENUM[this.props.currentRound]} finaliza em:</SubtitleText>
                                <CountDown
                                  until={this.getSecondsToFinish()}
                                  onFinish={() => {}}
                                  onPress={() => {}}
                                  size={15}
                                  digitStyle={{backgroundColor: Colors.primary}}
                                  digitTxtStyle={{color: Colors.yellow}}
                                  timeLabelStyle={{color: Colors.yellow}}
                                  
                                  timeLabels={{d: 'Dia(s)', h: 'Hora(s)', m: 'Min.', s: 'Seg.'}}
                                />
                              </CountdownContainer>
                              {
                                showDailyTournament && (
                                  <CountdownContainer style={{ alignItems: 'center', position: 'relative', top: -10 }}>
                                    <SubtitleText style={{ fontSize: 14.3, }} allowFontScaling={false}>Torneio do dia finaliza em:</SubtitleText>
                                    <CountDown
                                      until={this.getSecondsToFinishDaily()}
                                      onFinish={() => {}}
                                      onPress={() => {}}
                                      size={15}
                                      digitStyle={{backgroundColor: Colors.primary}}
                                      digitTxtStyle={{color: Colors.yellow}}
                                      timeLabelStyle={{color: Colors.yellow}}
                                      timeToShow={['H', 'M', 'S']}
                                      timeLabels={{h: 'Hora(s)', m: 'Min.', s: 'Seg.'}}
                                    />
                                  </CountdownContainer>
                                )
                              }
                              
                            </View>
                        
                      )
                    }

                    

                    
                    
                  </ContainerWinner>
                )
              }
              
             
            </ContainerMainBanner>
          </ContainerPrimary>
          
          
            <ContainerTitle style={{ marginTop: 10 }}>
            
              <ContainerButtonsHome>
                <WrapperButtonHome style={{ backgroundColor: Colors.secondary}}>
                  <ButtonQuickAccess onPress={() => this.navigate.push('SearchList')}>
                    <View style={{ width: 60, }}>
                      <Icon name="search" size={40} color={Colors.primary} />  
                    </View>
                    
                    <ButtonHomeText style={{ color: Colors.primary }}>PROCURAR USUÁRIOS</ButtonHomeText>
                  </ButtonQuickAccess>
                </WrapperButtonHome>

                <WrapperButtonHome style={{ backgroundColor: Colors.secondary}}>
                  <ButtonQuickAccess onPress={() => this.navigate.push('UserQuestionDashboard')}>
                    <View style={{ width: 60, }}>
                      <Icon name="comments" size={40} color={Colors.primary} />
                    </View>
                    
                    <ButtonHomeText style={{ color: Colors.primary }}>PERGUNTAS{'\n'}QUE ME FIZERAM</ButtonHomeText>
                    {
                      this.props.notAnsweredQuestionCount > 0 && (
                          <View style={{ position: 'absolute', zIndex: 999999, top: 5, left: 45 }}>
                            <NotificationCount style={{ backgroundColor: Colors.red, zIndex: 999999}}>
                              <NotificationValue style={{ color: Colors.white }}>{this.props.notAnsweredQuestionCount}</NotificationValue>
                            </NotificationCount>
                          </View>
                      )
                    }
                  </ButtonQuickAccess>
                </WrapperButtonHome>
                <View style={{ justifyContent: 'center', marginBottom: 5 }}>
                  <SubtitleText style={{ color: Colors.primary, textAlign: 'center', fontSize: 24}} allowFontScaling={false}>JOGAR</SubtitleText>
                </View>
                {
                  (showWeeklyTournament) && (
                    <WrapperButtonHome>
                      <ButtonQuickAccess onPress={() => this.navigate.push('WeeklyRanking')}>
                        <View style={{ width: 60, }}>
                          <Icon name="gamepad" size={40} color={Colors.secondary} />
                        </View>
                        
                        <ButtonHomeText allowFontScaling={false}>JOGAR{'\n'}TORNEIO SEMANAL</ButtonHomeText>
                        {
                          (this.props.currentRound >= 0) && (
                              <View style={{ position: 'absolute', zIndex: 999999, top: 15, left: 130 }}>
                                <NotificationValue style={{ color: Colors.yellow }}>({CURRENT_ROUND_NAME_ENUM[this.props.currentRound]})</NotificationValue>
                              </View>
                          )
                        }
                        <View style={{ backgroundColor: Colors.yellow, width: 80, paddingTop: 3, paddingRight: 3, paddingLeft: 3, paddingBottom: 3, borderRadius: 8, justifyContent: 'center', position: 'absolute', right: 15 }}>
                          <Text style={{ color: Colors.primary, fontWeight: 'bold', textAlign: 'center' }}>Prêmio{'\n'}{ numberToReal(this.props.awardWeekly)}</Text>
                        </View>
                      </ButtonQuickAccess>
                    </WrapperButtonHome>
                  )
                }
                
                {
                  (showDailyTournament) && (
                    <WrapperButtonHome>
                      <ButtonQuickAccess onPress={() => this.navigate.push('DailyTournament')}>
                        <View style={{ width: 60, }}>
                          <Icon name="gamepad" size={40} color={Colors.blue} />
                        </View>
                        
                        <ButtonHomeText allowFontScaling={false}>JOGAR{'\n'}TORNEIO DO DIA</ButtonHomeText>
                        <View style={{ backgroundColor: Colors.yellow, width: 80, paddingTop: 3, paddingRight: 3, paddingLeft: 3, paddingBottom: 3, borderRadius: 8, justifyContent: 'center', position: 'absolute', right: 15 }}>
                          <Text style={{ color: Colors.primary, fontWeight: 'bold', textAlign: 'center' }}>Prêmio{'\n'}{ numberToReal(this.props.awardDaily) }</Text>
                        </View>
                      </ButtonQuickAccess>
                    </WrapperButtonHome>
                  )
                }
                

                <WrapperButtonHome>
                  <ButtonQuickAccess onPress={() => this.navigate.push('Challenges')}>
                    <View style={{ width: 60, }}>
                      <Icon name="gamepad" size={40} color={Colors.red} />
                    </View>
                    
                    <ButtonHomeText allowFontScaling={false}>JOGAR{'\n'}DESAFIOS X1</ButtonHomeText>
                    {
                      this.props.challengesCount > 0 && (
                          <View style={{ position: 'absolute', zIndex: 999999, top: 5, left: 45 }}>
                            <NotificationCount style={{ backgroundColor: Colors.yellow, zIndex: 999999}}>
                              <NotificationValue style={{ color: Colors.black }}>{this.props.challengesCount}</NotificationValue>
                            </NotificationCount>
                          </View>
                      )
                    }
                    <Text style={{ position: 'absolute', left: 60, top: 20  }}>{}</Text>
                    <View style={{ backgroundColor: Colors.yellow, width: 80, paddingTop: 3, paddingRight: 3, paddingLeft: 3, paddingBottom: 3, borderRadius: 8, justifyContent: 'center', position: 'absolute', right: 15 }}>
                      <Text style={{ color: Colors.primary, fontWeight: 'bold', textAlign: 'center' }}>Premiado</Text>
                    </View>
                  </ButtonQuickAccess>
                </WrapperButtonHome>

                <WrapperButtonHome>
                  <ButtonQuickAccess onPress={() => this.navigate.push('Leagues')}>
                    <View style={{ width: 60, }}>
                      <Icon name="globe-americas" size={40} color={Colors.secondary} />
                    </View>
                    
                    <ButtonHomeText allowFontScaling={false}>JOGAR{'\n'}TORNEIOS ABERTOS</ButtonHomeText>
                    {
                      this.props.leaguesCount > 0 && (
                          <View style={{ position: 'absolute', zIndex: 999999, top: 5, left: 45 }}>
                            <NotificationCount style={{ backgroundColor: Colors.yellow, zIndex: 999999}}>
                              <NotificationValue style={{ color: Colors.black }}>{this.props.leaguesCount}</NotificationValue>
                            </NotificationCount>
                          </View>
                      )
                    }
                    <View style={{ backgroundColor: Colors.yellow, width: 80, paddingTop: 3, paddingRight: 3, paddingLeft: 3, paddingBottom: 3, borderRadius: 8, justifyContent: 'center', position: 'absolute', right: 15 }}>
                      <Text style={{ color: Colors.primary, fontWeight: 'bold', textAlign: 'center' }}>Premiado</Text>
                    </View>
                  </ButtonQuickAccess>
                </WrapperButtonHome>

                <View style={{ justifyContent: 'center', marginBottom: 5 }}>
                  <SubtitleText style={{ color: Colors.primary, textAlign: 'center', fontSize: 24}} allowFontScaling={false}>MAIS</SubtitleText>
                </View>
                

                <WrapperButtonHome>
                  <ButtonQuickAccess onPress={() => this.navigate.push('InviteFriends')}>
                    <View style={{ width: 60, }}>
                      <Icon name="user-plus" size={40} color={Colors.secondary} />  
                    </View>
                    
                    <ButtonHomeText allowFontScaling={false}>CONVIDAR AMIGOS</ButtonHomeText>
                    <View style={{ backgroundColor: Colors.yellow, width: 80, paddingTop: 3, paddingRight: 3, paddingLeft: 3, paddingBottom: 3, borderRadius: 8, justifyContent: 'center', position: 'absolute', right: 15 }}>
                      <Text style={{ color: Colors.primary, fontWeight: 'bold', textAlign: 'center' }}>Ganhe 10%</Text>
                    </View>
                  </ButtonQuickAccess>
                </WrapperButtonHome>

                
                {/* <WrapperButtonHome>
                  <ButtonQuickAccess onPress={() => this.navigate.push('Profile')}>
                    <View style={{ width: 60, }}>
                      <Icon name="user" size={40} color={Colors.secondary} />  
                    </View>
                    
                    <ButtonHomeText allowFontScaling={false}>MEU PERFIL</ButtonHomeText>
                  </ButtonQuickAccess>
                </WrapperButtonHome> */}

                {
                  this.props.showReward && (
                    <WrapperButtonHome>
                      <ButtonQuickAccess onPress={() => this.navigate.push('AdsMoney')}>
                        <View style={{ width: 60, }}>
                          <Icon name="dollar-sign" size={40} color={Colors.yellow} />  
                        </View>
                        
                      <ButtonHomeText allowFontScaling={false}>GANHAR DINHEIRO{'\n'}NA HORA</ButtonHomeText>
                      </ButtonQuickAccess>
                    </WrapperButtonHome>
                  )
                }

                
                {
                  this.props.user.buyCount <= 0 && (
                    <WrapperButtonHome>
                      <ButtonQuickAccess onPress={() => this.navigate.push('Pro')}>
                        <View style={{ width: 60, }}>
                          <Icon name="shopping-cart" size={40} color={Colors.secondary} />  
                        </View>
                        
                        <ButtonHomeText allowFontScaling={false}>COMPRAR KizoTokens (KTs)</ButtonHomeText>
                      </ButtonQuickAccess>
                    </WrapperButtonHome>
                  )
                }
                

                <WrapperButtonHome>
                  <ButtonQuickAccess onPress={() => this.navigate.push('AddQuestion')}>
                    <View style={{ width: 60, }}>
                      <Icon name="plus" size={40} color={Colors.secondary} />  
                    </View>
                    
                    <ButtonHomeText allowFontScaling={false}>ADICIONAR PERGUNTAS</ButtonHomeText>
                  </ButtonQuickAccess>
                </WrapperButtonHome>

                {/* <WrapperButtonHome>
                  <ButtonQuickAccess onPress={() => this.checkWithdrawMoney()}>
                    <View style={{ width: 60, }}>
                      <Icon name="dollar-sign" size={40} color={Colors.secondary} />  
                    </View>
                    
                    <ButtonHomeText allowFontScaling={false}>SOLICITAR PAGAMENTO</ButtonHomeText>
                  </ButtonQuickAccess>
                </WrapperButtonHome> */}

                {/* <WrapperButtonHome>
                  <ButtonQuickAccess onPress={() => this.navigate.push('GeneralRanking')}>
                    <View style={{ width: 60, }}>
                      <Icon name="medal" size={40} color={Colors.secondary} />  
                    </View>
                    
                    <ButtonHomeText allowFontScaling={false}>RANKING GERAL</ButtonHomeText>
                  </ButtonQuickAccess>
                </WrapperButtonHome>

                <WrapperButtonHome>
                  <ButtonQuickAccess onPress={() => this.navigate.push('Rewards')}>
                    <View style={{ width: 60, }}>
                      <Icon name="hand-holding-usd" size={40} color={Colors.secondary} />  
                    </View>
                    
                    <ButtonHomeText allowFontScaling={false}>RECOMPENSAS</ButtonHomeText>
                  </ButtonQuickAccess>
                </WrapperButtonHome> */}
              </ContainerButtonsHome>

            </ContainerTitle>

            <View style={{ marginBottom: 200 }} />
      
          </ScrollView>
        </Wrapper>

        
        <ContainerBanner>
          <AdMobBanner
            adSize="fullBanner"
            adUnitID={this.bottomBannerId}
            height={90}
          />
        </ContainerBanner>

        <Animated.View 
          style={{ 
            transform: [{ translateY: this.animatedValue }], 
            height: 90, 
            marginRight: 15,
            marginLeft: 15,
            borderRadius: 8,
            backgroundColor: Colors.lightGrey, 
            position: 'absolute',
            left: 0, 
            top: 0, 
            right: 0, 
            justifyContent: 'center',
          }}>
          <KizoText style={{ color: Colors.primary, marginLeft: 15, fontSize: 20 }}>Você ganhou</KizoText>

          <View style={{ flexDirection: 'row', position: 'absolute', right: 15, top: 35  }}>
            <KizoText style={{ color: Colors.primary, fontSize: 22 }}>+{formatKTs(earnedKTs)}</KizoText>
            <Icon name="coins" size={20} color={Colors.yellow} />
          </View>
        </Animated.View>
        
      </>
    )
  }

  
}

const mapStateToProps = state => ({
  updateUserInfo: state.userInfo.updateUserInfo,
  updateNotificationsCount: state.userInfo.updateNotificationsCount,
  updateChatsCount: state.userInfo.updateChatsCount,
  updateShowRatingModal: state.userInfo.updateShowRatingModal,
  updateChallengesCount: state.userInfo.updateChallengesCount,
  updateLeaguesCount: state.userInfo.updateLeaguesCount,
  updateShowDaily: state.userInfo.updateShowDaily,
  updateAwardWeekly: state.userInfo.updateAwardWeekly,
  updateAwardDaily: state.userInfo.updateAwardDaily,
  updateActiveSound: state.userInfo.updateActiveSound,
  activeSound: state.userInfo.activeSound,
  updateAlreadyShowImportantWarn: state.userInfo.updateAlreadyShowImportantWarn,
  updateCurrentRound: state.userInfo.updateCurrentRound,
  currentRound: state.userInfo.currentRound,
  awardWeekly: state.userInfo.awardWeekly,
  awardDaily: state.userInfo.awardDaily,
  alreadyShowImportantWarn: state.userInfo.alreadyShowImportantWarn,
  notificationsCount: state.userInfo.notificationsCount,
  chatsCount: state.userInfo.chatsCount,
  challengesCount: state.userInfo.challengesCount,
  leaguesCount: state.userInfo.leaguesCount,
  showDaily: state.userInfo.showDaily,
  updateNotAnsweredQuestionCount: state.userInfo.updateNotAnsweredQuestionCount,
  notAnsweredQuestionCount: state.userInfo.notAnsweredQuestionCount,
  updateShowReward: state.userInfo.updateShowReward,
  showReward: state.userInfo.showReward,
  updateRewardPrize: state.userInfo.updateRewardPrize,
  rewardPrize: state.userInfo.rewardPrize,
  updateToken: state.userInfo.updateToken,
  updatePs: state.userInfo.updatePs,
  ps: state.userInfo.ps,
  addListRequest: state.userInfo.addListRequest,
  updateListsTitleRequest: state.userInfo.updateListsTitleRequest,
  lists: state.userInfo.lists,
  user: state.userInfo.user,
  bankInfo: state.userInfo.bankInfo,
  updateBankInfo: state.userInfo.updateBankInfo,
  auth: state.userInfo.auth,
  language: state.userInfo.user.language,
  updateDeviceLanguage: state.userInfo.updateDeviceLanguage,
  updateItems: state.userInfo.updateItems,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(UserInfoActions, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Main);