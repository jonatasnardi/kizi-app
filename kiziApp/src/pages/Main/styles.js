import styled from 'styled-components/native';
import Colors from '../../utils/colors';
import { Type } from '../../utils/layout';
import { Dimensions } from 'react-native';
import { isIphoneX } from '../../utils/functions';

export const Wrapper = styled.View`
  ${Type.wrapper};
  flex: 1;
  background: ${Colors.lightGrey};
  padding-top: 0;
  padding-left: 0;
  padding-right: 0;
`;

export const ContainerPrimary = styled.View`
  ${Type.wrapper};
  background-color: ${Colors.primary};
  padding-left: 0;
  padding-right: 0;
`;


export const ButtonNavigation = styled.TouchableOpacity`
  ${Type.buttonStyle};
  background-color: ${Colors.white};
`;

export const ButtonText = styled.Text`
  ${Type.body};
  ${Type.buttonText};
  color: ${Colors.primary};
`;

export const ContainerBanner = styled.View`
  width: 100%;
  height: 60px;
  position: absolute;
  bottom: ${isIphoneX() ? 22 : 0};
  left: 0;
`;

export const ContainerTitle = styled.View`
  padding: 0px 15px 0px 15px;
`;

export const ContainerMainBanner = styled.View`
  width: 100%;
  height: 128px;
  background: ${Colors.primary};
  border-bottom-width: 3px;
  border-bottom-color: ${Colors.secondary};
`;

export const ContainerWinner = styled.View`
  border-top-width: 3px;
  border-top-color: ${Colors.secondary};
`;

export const ContainerProfile = styled.View`
  flex-direction: row;
  background: ${Colors.primary};
`;

export const TitleText = styled.Text`
  ${Type.body};
  color: ${Colors.white};
  margin-bottom: 0px;
  margin-left: 10px;
  font-weight: bold;
  flex: 1;
  position: relative;
  top: 5px;
`;

export const KizoText = styled.Text`
  ${Type.body};
  font-size: 17;
  color: ${Colors.yellow};
  margin-right: 5px;
`;

export const SubtitleText = styled.Text`
  ${Type.body};
  padding-top: 15px;
  color: ${Colors.white};
`;


export const TextMoney = styled.Text`
  ${Type.body};
  color: ${Colors.secondary};
  font-size: 30px;
  position: relative;
  top: 5px;
  left: 5px;
`;

export const ContainerList = styled.View`
  flex: 1;
  margin: 20px 0;
`;

export const BigBox = styled.View`
  flex-direction: row;
  margin-top: -10px;
  width: 70%;
  height: 55px;
`;

export const SmallBox = styled.View`
  width: 30%;
  height: 100px;
  padding-top: 15px;
  position: absolute;
  right: 15px;
  top: -24px;
`;

export const TitleListGroup = styled.Text`
  ${Type.body};
  color: ${Colors.white};
  margin-bottom: 10px;
  font-weight: bold;
`;

export const ContainerItemContent = styled.View`
  flex-direction: row;
`;

export const ButtonEditList = styled.TouchableOpacity`
`;

export const SubtitleListGroup = styled.Text`
  ${Type.body};
  color: ${Colors.white};
  margin-bottom: 20px;
  font-size: 14px;
`;

export const ListGroup = styled.View`
  flex: 1;
  margin: 0;
`;

export const ListItemContainer = styled.TouchableOpacity`
`;

export const ListItem = styled.View`
  background: ${Colors.white};
  padding: 10px;
  margin: 7px 0;
  border-width: 1px;
  border-color: ${Colors.white};
  border-radius: 5px;
  min-height: 40px;
  shadow-color: ${Colors.white};
  shadow-offset: 0px 2px;
  shadow-opacity: 0.6;
  shadow-radius: 2;
  elevation: 1;
`;

export const ItemTitle = styled.Text`
  ${Type.body};
  color: ${Colors.primary};
  flex: 1;
`;

export const ButtonAddList = styled.TouchableOpacity`
  position: absolute;
  top: 0;
  right: 0;
  width: 30px;
  height: 30px;
  align-items: center;
  justify-content: center;
  border-radius: 50;
  border-color: ${Colors.white};
  border-width: 1px;
`;


export const BoxAddItem = styled.View`
  background: ${Colors.white};
  padding: 10px;
  margin: 7px 0;
  border-width: 1px;
  border-color: ${Colors.white};
  border-radius: 5px;
  min-height: 40px;
  shadow-color: ${Colors.white};
  shadow-offset: 0px 2px;
  shadow-opacity: 0.6;
  shadow-radius: 2;
  elevation: 1;
  flex-direction: row;
`;

export const InputNewList = styled.TextInput`
  ${Type.body};
  flex: 1;
`;

export const ButtonConfirmAdd = styled.TouchableOpacity`
  height: 30px;
  padding: 0 10px;
  align-items: center;
  justify-content: center;
  border-radius: 5px;
  background: ${Colors.green};
  margin-right: 5px;
`;

export const ButtonCancelAdd = styled.TouchableOpacity`
  height: 30px;
  padding: 0 10px;
  align-items: center;
  justify-content: center;
  border-radius: 5px;
  background: ${Colors.red};
`;

export const ButtonAddText = styled.Text`
  ${Type.body};
  color: ${Colors.white};
`;

export const ModalEditList = styled.Modal`
  ${Type.wrapper};
`;

export const ModalWrapper = styled.ScrollView`
  ${Type.wrapper};
`;

export const ModalHeader = styled.View`
  margin-top: 20px;
  flex-direction: row;
  align-items: center;
  margin-bottom: 30px;
  padding-bottom: 10px;
  border-bottom-color: ${Colors.black50};
  border-bottom-width: 1px;
`;

export const ModalTitle = styled.Text`
  ${Type.body};
  color: ${Colors.black};
  flex: 1;
  font-size: 22px;
`;

export const ButtonCloseModal = styled.TouchableOpacity`

`;

export const Fieldset = styled.View`
  flex-direction: column;
  margin-bottom: 20px;
`;

export const LabelForm = styled.Text`
  ${Type.body};
  color: ${Colors.black};
  font-size: 14px;
`;

export const InputEditListTitle = styled.TextInput`
  ${Type.body};
  font-size: 24px;
  border-bottom-color: ${Colors.primary};
  border-bottom-width: 2px;
`;

export const ButtonDeleteList = styled.TouchableOpacity`

`;

export const ButtonSave = styled.TouchableOpacity`
  padding: 0 10px;
  align-items: center;
  justify-content: center;
  border-radius: 5px;
  background: ${Colors.green};
`;

export const ContainerButtons = styled.View`
  flex-direction: row;
  justify-content: space-between;
`;

export const CenteredText = styled.Text`
  ${Type.body};
  color: ${Colors.white};
  font-size: 18px;
  text-align: center;
`;

export const DarkButtonText = styled.Text`
  ${Type.body};
  color: ${Colors.white};
`;

export const RemoveItemButton = styled.View`
  width: 70px;
  background: ${Colors.red};
  align-items: center;
  justify-content: center;
  border-radius: 5px;
  margin: 7px 0 0 0;
  min-height: 45px;
`;

export const TextRemoveItem = styled.Text`
  ${Type.body};
  color: ${Colors.white};
  font-size: 15px;
`;

export const ImageIconUser = styled.Image`
  width: 45px;
  height: 45px;
  top: -5px;
  border-radius: 45px;
  border: 2px solid ${Colors.secondary};
`;

export const ImageLogo = styled.Image`
  width: 30px;
  height: 30px;
  top: -3px;
`;

export const ContainerButtonsHome = styled.View`
  flex-direction: row;
  flex-wrap: wrap;
`;

export const ButtonQuickAccess = styled.TouchableOpacity`
  ${Type.buttonStyle};
  background-color: transparent;
  padding: 12px 10px;
  position: relative;
  flex-direction: row;
`;

export const WrapperButtonHome = styled.View`
  width: 100%;
  margin-bottom: 10px;
  shadow-opacity: 0.4;
  shadow-radius: 6px;
  shadow-color: ${Colors.secondary};
  shadow-offset: 0px 0px;
  background: ${Colors.primary};
  border-radius: 8px;
`;

export const ButtonHomeText = styled.Text`
  ${Type.body};
  font-family: 'Futura-Medium';
  font-weight: bold;
  color: ${Colors.lightGrey};
  font-size: 14px;
  text-align: left;
  margin-top: 3px;
`;

export const NotificationCount = styled.View`
  width: 25px;
  height: 25px;
  position: relative;
  border-radius: 25px;
  background: ${Colors.secondary};
  z-index: 9999999;
  align-items: center;
  justify-content: center;
`;

export const NotificationValue = styled.Text`
  ${Type.body};
  color: ${Colors.white};
  font-size: 14px;
  text-align: center;
`;


export const CountdownContainer = styled.View`
  width: 50%;
`;

