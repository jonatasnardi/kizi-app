import React, {Component} from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Icon from 'react-native-vector-icons/FontAwesome5';
import CountDown from 'react-native-countdown-component';

import { Creators as UserInfoActions } from '../../store/ducks/userInfo';

import { 
  Wrapper,
  WrapperInner,
  ListContainer,
  Row,
  SmallTitle,
  LabelPoints,
  LabelPosition,
  KizoText,
  KizoTextFeatured,
} from './styles';
import { numberToReal, getFirstWord, showAlert, showToast, cutString } from '../../utils/functions';
import I18n from '../../utils/translation/i18n';
import ButtonPrimary from '../../components/buttons/ButtonPrimary';
import FullLoader from '../../components/FullLoader';
import Colors from '../../utils/colors';
import { Alert, View, ScrollView, FlatList, TouchableOpacity } from 'react-native';
import { requestPayment } from '../../services/payments';
import ButtonSecondary from '../../components/buttons/ButtonSecondary';
import { getUserRankingList, getLeagues } from '../../services/leagues';
import Images from '../../utils/images';
import { listLeaguesFromUser } from '../../services/general';

class MyLeagues extends Component {
  state = {
    leagues: [],
    isLoadingAdd: false,
  }

  navigate;

  refInputSuggestion;

  constructor(props) {
    super(props);

    this.navigate = this.props.navigation;
  }

  componentDidMount() {
    this.loadLeagues();
  }

  loadLeagues = () => {
    const { user } = this.props;

    this.setState({
      isLoadingAdd: true,
    });
    listLeaguesFromUser(user._id)
      .then((res) => { 
        this.setState({
          leagues: res.userLeagues,
          isLoadingAdd: false,
        });
      })
      .catch((error) => {
        console.log(error);
        this.setState({
          isLoadingAdd: false,
        });
        showToast(I18n.t('errorGenericMessage'));
      });
  }

  refreshFunction = () => {
    this.loadLeagues();
  }

  nextDayOfWeek = (x) => {
    let now = new Date();    
    now.setDate(now.getDate() + (x+(7-now.getDay())) % 7);
    return now;
  }

  getLabelColor = (index) => {
    if (index <= 2) {
      return Colors.primary;
    } else {
      return Colors.secondary;
    }
  }

  render() {
    const { leagues } = this.state;

    return (
      <>
        <Wrapper>
          <ScrollView>
            <WrapperInner>
            
              <Row>
                <LabelPosition>Nome do torneio</LabelPosition>
                <LabelPoints>Prêmio</LabelPoints>
              </Row>
              <FlatList
                data={leagues}
                renderItem={({ item, index }) => (
                  <TouchableOpacity onPress={() => this.navigate.push('LeagueDetails', {
                    idLeague: item.idLeague._id,
                  })}>
                    <ListContainer>
                      <Row>
                        <Icon name="trophy" size={20} color={Colors.yellow} />
                        <LabelPosition style={{ marginLeft: 4 }}>{ cutString(item.idLeague.title) }</LabelPosition>
                        <LabelPoints>{ numberToReal(item.idLeague.award) }</LabelPoints>
                        <View style={{ flexDirection: 'row', position: 'absolute', left: 0, top: 30, }}>
                          <KizoText>Máximo de participantes: { item.idLeague.userQtd }</KizoText>
                        </View>
                        <View style={{ flexDirection: 'row', position: 'absolute', left: 0, top: 55, }}>
                          {
                            item.idLeague.priceToJoin ? (
                              <>
                                <KizoTextFeatured>KTs para entrar: { item.idLeague.priceToJoin }</KizoTextFeatured>
                                <Icon name="coins" size={20} color={Colors.yellow} />
                              </>
                            ) : (
                              <KizoTextFeatured style={{ color: Colors.green }}>Entrada gratuita</KizoTextFeatured>
                            )
                          }
                        </View>
                        <View style={{ flexDirection: 'row', position: 'absolute', left: 0, top: 80, }}>
                          {
                            item.idLeague.active ? (
                              <KizoTextFeatured style={{ color: Colors.blue }}>Ativo</KizoTextFeatured>
                            ) : (
                              <KizoTextFeatured style={{ color: Colors.red }}>Finalizado</KizoTextFeatured>
                            )
                          }
                          
                        </View>
                        <View style={{ flexDirection: 'row', position: 'absolute', right: 10, top: 50, }}>
                          <LabelPoints style={{ top: -3, right: 20 }}>Detalhes</LabelPoints>
                          <Icon name="chevron-right" size={20} color={Colors.secondary} />
                        </View>
                      </Row>
                    </ListContainer>
                  </TouchableOpacity>
                )}
              />

              {
                (leagues.length === 0 && !this.state.isLoadingAdd) && (
                  <Row style={{ marginTop: 30, justifyContent: 'center' }}>
                    <LabelPosition>Nenhum torneio disponível</LabelPosition>
                  </Row>
                )
              }
            </WrapperInner>
          </ScrollView>
          
        </Wrapper>

        {
          this.state.isLoadingAdd && (
            <FullLoader></FullLoader>
          )
        }
       </>
    )
  }
}

const mapStateToProps = state => ({
  addListRequest: state.userInfo.addListRequest,
  updateListsTitleRequest: state.userInfo.updateListsTitleRequest,
  lists: state.userInfo.lists,
  user: state.userInfo.user,
  language: state.userInfo.user.language,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(UserInfoActions, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(MyLeagues);