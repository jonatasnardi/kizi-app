import React, {Component} from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { GiftedChat } from 'react-native-gifted-chat';

import { Creators as UserInfoActions } from '../../store/ducks/userInfo';

import I18n from '../../utils/translation/i18n';

import { Alert, ActivityIndicator, NativeModules, Text, TouchableOpacity, View } from 'react-native';

import { 
  Wrapper,
  InputSuggestion,
  LabelForm,
  Fieldset,
} from './styles';
import Colors from '../../utils/colors';
import { showAlert, showToast } from '../../utils/functions';
import ButtonPrimary from '../../components/buttons/ButtonPrimary';
import FullLoader from '../../components/FullLoader';
import { userContact, getUserInfoProfile } from '../../services/user';
import { sendMessage, getMessages, setAsRead } from '../../services/chat';

class Chat extends Component {
  state = {
    textSuggestion: '',
    isLoadingAdd: false,
    isLoadingMessage: false,
    messages: [],
    user: null,
  }

  navigate;

  refInputSuggestion;

  constructor(props) {
    super(props);

    this.navigate = this.props.navigation;

    this.navigate.setParams({ userName: this.navigate.state.params.userName });
  }

  componentDidMount() {
    this.getMessages();
    // this.loadUserInfo();
  }


  formatMessagesArray = (messages) => {
    const { user } = this.state;
    for (let i = 0; i < messages.length; i++) {
      messages[i] = {
        _id: messages[i]._id,
        text: messages[i].message,
        createdAt: new Date(messages[i].createdAt),
        user: {
          _id: messages[i].user._id,
          name: messages[i].user.name,
          avatar: user.image ? user.image : require('../../assets/avatar1.jpg'),
        },
      }
    }

    this.setState({
      messages,
    })
  }

  getMessages = () => {
    const { user } = this.props;
    
    this.setState({
      isLoadingAdd: true,
    });

    getMessages(user._id, this.navigate.state.params.userContacted)
      .then((data) => { 

        getUserInfoProfile(this.navigate.state.params.userContacted, this.props.user._id)
          .then((res) => { 
            this.setState({
              user: res.user,
              isLoadingAdd: false,
            }, () => {
              this.formatMessagesArray(data.chats);
            });
    
            
    
            setAsRead(user._id, this.navigate.state.params.userContacted)
              .then((data) => { 
                
    
              })
              .catch((error) => {
              }); 
    
              })
            
         
          .catch((error) => {
            console.log(error);

            this.setState({
              isLoadingAdd: false
            });
          });
      })
      .catch((error) => {
        console.log(error);
        this.setState({
          isLoadingAdd: false
        });
        showToast(I18n.t('errorGenericMessage'));
      }); 
      
  }

  onSend(messages = []) {
    this.setState(previousState => ({
      messages: GiftedChat.append(previousState.messages, messages),
    }));

    this.sendMessage(messages[0].text)
  }

  sendMessage(message) {
    const { user } = this.props;

    this.setState({
      isLoadingMessage: true,
    });

    sendMessage(user._id, this.navigate.state.params.userContacted, message)
      .then((data) => { 
        this.setState({
          isLoadingMessage: false,
        });

        // showToast(I18n.t('messageSentSuccesful'));
      })
      .catch((error) => {
        console.log(error);
        this.setState({
          isLoadingMessage: false
        });
        showToast(I18n.t('errorGenericMessage'));
      }); 
  }


  render() {
    const { user } = this.props;

    return (
      <>
        <Wrapper>
          
        <GiftedChat
          messages={this.state.messages}
          onSend={messages => this.onSend(messages)}
          user={{
            _id: user._id,
          }}
          placeholder={'Digite uma mensagem...'}
          label={'Enviar'}
        />
    
        </Wrapper>

        {
          this.state.isLoadingAdd && (
            <FullLoader></FullLoader>
          )
        }

       </>
    )
  }
}

const mapStateToProps = state => ({
  addListRequest: state.userInfo.addListRequest,
  
  updateListsTitleRequest: state.userInfo.updateListsTitleRequest,
  lists: state.userInfo.lists,
  user: state.userInfo.user,
  language: state.userInfo.user.language,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(UserInfoActions, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Chat);