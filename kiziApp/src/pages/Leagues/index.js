import React, {Component} from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Icon from 'react-native-vector-icons/FontAwesome5';
import CountDown from 'react-native-countdown-component';

import {
  AdMobBanner,
} from 'react-native-admob';

import { Creators as UserInfoActions } from '../../store/ducks/userInfo';

import { 
  Wrapper,
  WrapperInner,
  ListContainer,
  Row,
  SmallTitle,
  LabelPoints,
  LabelPosition,
  KizoText,
  KizoTextFeatured,
  ContainerBanner,
} from './styles';
import { numberToReal, getFirstWord, showAlert, showToast, cutString, formatKTs } from '../../utils/functions';
import I18n from '../../utils/translation/i18n';
import ButtonPrimary from '../../components/buttons/ButtonPrimary';
import FullLoader from '../../components/FullLoader';
import Colors from '../../utils/colors';
import { Alert, View, TouchableOpacity, ScrollView, FlatList, Platform } from 'react-native';
import { requestPayment } from '../../services/payments';
import ButtonSecondary from '../../components/buttons/ButtonSecondary';
import { getUserRankingList, getLeagues } from '../../services/leagues';

class Leagues extends Component {
  state = {
    leagues: [],
    isLoadingAdd: false,
  }

  navigate;

  bottomBannerId = Platform.select({
    android: 'ca-app-pub-4982456847713514/5350850363',
    ios: 'ca-app-pub-4982456847713514/5542422057',
  }); 

  refInputSuggestion;

  constructor(props) {
    super(props);

    this.navigate = this.props.navigation;
  }

  componentDidMount() {
    this.loadLeagues();
  }

  loadLeagues = () => {
    this.setState({
      isLoadingAdd: true,
    });
    getLeagues()
      .then((res) => { 
        let leagues = res.leagues;
        if (this.props.user.kizoTokens >= 20000) {
          leagues = [];
        }
        this.setState({
          leagues: leagues,
          isLoadingAdd: false,
        });
      })
      .catch((error) => {
        console.log(error);
        this.setState({
          isLoadingAdd: false,
        });
        showToast(I18n.t('errorGenericMessage'));
      });
  }

  refreshFunction = () => {
    this.loadLeagues();
  }

  nextDayOfWeek = (x) => {
    let now = new Date();    
    now.setDate(now.getDate() + (x+(7-now.getDay())) % 7);
    return now;
  }

  getLabelColor = (index) => {
    if (index <= 2) {
      return Colors.primary;
    } else {
      return Colors.secondary;
    }
  }

  render() {
    const { leagues } = this.state;
    const { user } = this.props;

    return (
      <>
        <Wrapper>
          <ScrollView>
            <WrapperInner>
              {
                user.kizoTokens < 20000 && (
                <View style={{ marginBottom: 20 }}>
                  <ButtonPrimary
                    onPress={() => this.navigate.push('AddTournament', {
                      onGoBack: () => this.refreshFunction(),
                    })}
                    icon={'plus'}
                    title={'Criar novo torneio'}>
                  </ButtonPrimary>
                </View>
                )
              }
              

              <View style={{ flexDirection: 'row', justifyContent: 'flex-end' ,marginBottom: 10 }}>
                <KizoText>Você tem {formatKTs(user.kizoTokens)}</KizoText>
                <Icon name="coins" size={20} color={Colors.yellow} />
              </View>
            
              <Row>
                <LabelPosition>Nome do torneio</LabelPosition>
                <LabelPoints>Prêmio</LabelPoints>
              </Row>
              <FlatList
                data={leagues}
                renderItem={({ item, index }) => (
                  <TouchableOpacity onPress={() => this.navigate.push('LeagueDetails', {
                    idLeague: item._id,
                  })}>
                    <ListContainer>
                      <Row>
                        <Icon name="trophy" size={20} color={Colors.yellow} />
                        <LabelPosition style={{ marginLeft: 4 }}>{ cutString(item.title) }</LabelPosition>
                        <LabelPoints>{ numberToReal(item.award) }</LabelPoints>
                        <View style={{ flexDirection: 'row', position: 'absolute', left: 0, top: 30, }}>
                          <KizoText>Máximo de participantes: { item.userQtd }</KizoText>
                        </View>
                        <View style={{ flexDirection: 'row', position: 'absolute', left: 0, top: 55, }}>
                          {
                            item.priceToJoin ? (
                              <>
                                <KizoTextFeatured>KTs para entrar: { item.priceToJoin }</KizoTextFeatured>
                                <Icon name="coins" size={20} color={Colors.yellow} />
                              </>
                            ) : (
                              <KizoTextFeatured style={{ color: Colors.green }}>Entrada gratuita</KizoTextFeatured>
                            )
                          }
                        </View>
                        <View style={{ flexDirection: 'row', position: 'absolute', right: 10, top: 50, }}>
                          <LabelPoints style={{ top: -3, right: 20 }}>Detalhes</LabelPoints>
                          <Icon name="chevron-right" size={20} color={Colors.secondary} />
                        </View>
                      </Row>
                    </ListContainer>
                  </TouchableOpacity>
                )}
              />

              {
                (leagues.length === 0 && !this.state.isLoadingAdd) && (
                  <Row style={{ marginTop: 30, justifyContent: 'center' }}>
                    <LabelPosition>Nenhum torneio disponível</LabelPosition>
                  </Row>
                )
              }

              
            </WrapperInner>
            <View style={{ marginBottom: 200 }}/>
          </ScrollView>
          
        </Wrapper>

        {
          this.state.isLoadingAdd && (
            <FullLoader></FullLoader>
          )
        }

        <ContainerBanner>
          <AdMobBanner
            adSize="fullBanner"
            adUnitID={this.bottomBannerId}
            height={90}
          />
        </ContainerBanner>
       </>
    )
  }
}

const mapStateToProps = state => ({
  addListRequest: state.userInfo.addListRequest,
  updateListsTitleRequest: state.userInfo.updateListsTitleRequest,
  lists: state.userInfo.lists,
  user: state.userInfo.user,
  language: state.userInfo.user.language,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(UserInfoActions, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Leagues);