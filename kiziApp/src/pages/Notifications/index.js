import React, {Component} from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Icon from 'react-native-vector-icons/FontAwesome5';

import {
  AdMobBanner,
} from 'react-native-admob';

import { Creators as UserInfoActions } from '../../store/ducks/userInfo';

import { 
  Wrapper,
  WrapperInner,
  ListContainer,
  Row,
  SmallTitle,
  LabelPoints,
  LabelPosition,
  KizoText,
  KizoTextFeatured,
  ContainerBanner,
} from './styles';
import { numberToReal, getFirstWord, showAlert, showToast, cutString, formatKTs } from '../../utils/functions';
import I18n from '../../utils/translation/i18n';
import ButtonPrimary from '../../components/buttons/ButtonPrimary';
import FullLoader from '../../components/FullLoader';
import Colors from '../../utils/colors';
import { Alert, View, TouchableOpacity, ScrollView, FlatList, Platform } from 'react-native';
import { requestPayment } from '../../services/payments';
import ButtonSecondary from '../../components/buttons/ButtonSecondary';
import { getUserRankingList, getNotifications, setAllNotificationsRead } from '../../services/notifications';

class Notifications extends Component {
  state = {
    notifications: [],
    isLoadingAdd: false,
  }

  navigate;

  bottomBannerId = Platform.select({
    android: 'ca-app-pub-4982456847713514/5350850363',
    ios: 'ca-app-pub-4982456847713514/5542422057',
  }); 

  refInputSuggestion;

  constructor(props) {
    super(props);

    this.navigate = this.props.navigation;
  }

  componentDidMount() {
    this.loadNotifications();
  }

  loadNotifications = () => {
    const { user } = this.props;
    this.setState({
      isLoadingAdd: true,
    });
    getNotifications(user._id)
      .then((res) => { 
        this.setState({
          notifications: res.notifications,
          isLoadingAdd: false,
        });

        const unread = res.notifications.some(item => !item.read);

        if (unread) {
          setAllNotificationsRead(user._id)
            .then((res) => { 
              this.props.updateNotificationsCount(0);
            })
            .catch((error) => {
              console.log(error);
              this.setState({
                isLoadingAdd: false,
              });
              showToast(I18n.t('errorGenericMessage'));
            });
        }
      })
      .catch((error) => {
        console.log(error);
        this.setState({
          isLoadingAdd: false,
        });
        showToast(I18n.t('errorGenericMessage'));
      });
  }

  refreshFunction = () => {
    this.loadNotifications();
  }

  nextDayOfWeek = (x) => {
    let now = new Date();    
    now.setDate(now.getDate() + (x+(7-now.getDay())) % 7);
    return now;
  }

  setItemRead = (id) => {
    let index = this.state.notifications.findIndex(x => x._id === id);

    let newUser = this.state.notifications;

    newUser[index].read = true;
    
    this.setState({
      notifications: newUser,
    })
  }

  getLabelColor = (index) => {
    if (index <= 2) {
      return Colors.primary;
    } else {
      return Colors.secondary;
    }
  }

  render() {
    const { notifications } = this.state;
    const { user } = this.props;

    return (
      <>
        <Wrapper>
          <ScrollView>
            <WrapperInner>
              <FlatList
                data={notifications}
                renderItem={({ item, index }) => (
                  <TouchableOpacity onPress={() => {
                    this.setItemRead(item._id);
                    this.navigate.push('NotificationItem', {
                      title: item.title,
                      message: item.message,
                      link: item.link,
                    });
                    
                    }}>
                    <ListContainer>
                      <Row>
                        {
                          item.read ? (
                            <Icon name="envelope-open-text" size={20} color={Colors.darkGrey} />
                          ) : (
                            <Icon name="envelope-open-text" size={20} color={Colors.blue} />
                          )
                        }
                        
                        <LabelPosition style={{ marginLeft: 4, fontWeight: 'bold' }}>{ cutString(item.title, 33) }</LabelPosition>
                        <View style={{ flexDirection: 'row', position: 'absolute', left: 0, top: 30, }}>
                          <KizoText numberOfLines={1}>{ cutString(item.message, 33) }</KizoText>
                        </View>
                        <View style={{ flexDirection: 'row', position: 'absolute', right: 10, top: 50, }}>
                          <LabelPoints style={{ top: -3, right: 20 }}>Ver mais</LabelPoints>
                          <Icon name="chevron-right" size={20} color={Colors.secondary} />
                        </View>
                      </Row>
                    </ListContainer>
                  </TouchableOpacity>
                )}
              />

              {
                (notifications.length === 0 && !this.state.isLoadingAdd) && (
                  <Row style={{ marginTop: 30, justifyContent: 'center' }}>
                    <LabelPosition>Nenhuma mensagem para você</LabelPosition>
                  </Row>
                )
              }

              
            </WrapperInner>
            <View style={{ marginBottom: 200 }}/>
          </ScrollView>
          
        </Wrapper>

        {
          this.state.isLoadingAdd && (
            <FullLoader></FullLoader>
          )
        }

        <ContainerBanner>
          <AdMobBanner
            adSize="fullBanner"
            adUnitID={this.bottomBannerId}
            height={90}
          />
        </ContainerBanner>
       </>
    )
  }
}

const mapStateToProps = state => ({
  addListRequest: state.userInfo.addListRequest,
  updateListsTitleRequest: state.userInfo.updateListsTitleRequest,
  updateNotificationsCount: state.userInfo.updateNotificationsCount,
  notificationsCount: state.userInfo.notificationsCount,
  lists: state.userInfo.lists,
  user: state.userInfo.user,
  language: state.userInfo.user.language,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(UserInfoActions, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Notifications);