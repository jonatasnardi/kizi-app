import React, {Component} from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { Creators as UserInfoActions } from '../../store/ducks/userInfo';

import {fetch} from 'react-native-ssl-pinning';

import I18n from '../../utils/translation/i18n';

import { Alert, ActivityIndicator, NativeModules, Text, TouchableOpacity, View, Animated } from 'react-native';

import {
  AdMobRewarded,
} from 'react-native-admob';

import { 
  Wrapper,
  InputSuggestion,
  LabelForm,
  Fieldset,
  KizoText,
} from './styles';
import Colors from '../../utils/colors';
import { showAlert, showToast, numberToReal } from '../../utils/functions';
import ButtonPrimary from '../../components/buttons/ButtonPrimary';
import FullLoader from '../../components/FullLoader';
import { userContact } from '../../services/user';
import { addUserMoney } from '../../services/general';
import { BASE_URL_API } from '../../helpers/variables';

class AdsMoney extends Component {
  state = {
    textSuggestion: '',
    isLoadingAdd: false,
  }

  

  rewardedBanner = Platform.select({
    android: 'ca-app-pub-4982456847713514/9290095372',
    ios: 'ca-app-pub-4982456847713514/4229340388',
  });

  refInputSuggestion;
  navigate;

  constructor(props) {
    super(props);

    this.animatedValue = new Animated.Value(-90);
    this.navigate = this.props.navigation;
  }

  componentDidMount() {
    

    AdMobRewarded.addEventListener('rewarded', reward => {
      this.rewardNow();
    });
  }

  componentWillUnmount() {
    AdMobRewarded.removeAllListeners();
  }

  rewardNow = () => {
    const { user } = this.props;
  
      this.setState({
        isLoadingAdd: true, 
      });

      fetch(`${BASE_URL_API}/general/add-mny/Fli23/${this.props.user._id}`, {
        method: "PUT" ,
        timeoutInterval: 30000,
        body: JSON.stringify({
          lastAccess: new Date()
        }),
        sslPinning: {
            certs: ["mycert"],
        },
        headers:{
          Authorization: `Bearer ${this.props.auth.token}`,
        }
      })
      .then(response => {
        this.setState({
          isLoadingAdd: false,
        });

        let userInfo = {
          ...this.props.user,
          money: this.props.user.money + this.props.rewardPrize,
          ads: this.props.user.ads + 1,
          kizoTokens: this.props.user.kizoTokens + 1,
        };

        this.callToast();
        
        this.props.updateUserInfo(userInfo);

        AdMobRewarded.removeEventListener('rewarded', () => {});
          // console.tron.log(`response received ${response}`)
      })
      .catch(err => {
          // console.tron.log(err);
          this.setState({
            isLoadingAdd: false,
          });
          showToast('Ocorreu um erro ao adicionar o valor');
      })


      // addUserMoney(this.props.user._id)
      //   .then((res) => { 
      //     this.setState({
      //       isLoadingAdd: false,
      //     });

      //     let userInfo = {
      //       ...this.props.user,
      //       money: this.props.user.money + this.props.rewardPrize,
      //     };

      //     this.callToast();
          
      //     this.props.updateUserInfo(userInfo);

      //     AdMobRewarded.removeEventListener('rewarded', () => {});
      //   })
      //   .catch((error) => {
      //     console.log(error);
      //     this.setState({
      //       isLoadingAdd: false,
      //     });
      //     showToast('Ocorreu um erro ao adicionar o valor');
      //   });
  }

  openRewardedBanner = () => {
    this.setState({
      isLoadingAdd: true,
    });
    AdMobRewarded.setAdUnitID(this.rewardedBanner);
    AdMobRewarded.requestAd().then(() => {
      this.setState({
        isLoadingAdd: false,
      });
      AdMobRewarded.showAd();
    });
  }

  callToast() {
    Animated.timing(
      this.animatedValue,
      { 
        toValue: 15,
        duration: 350
      }).start(this.closeToast())
  }
  
  closeToast() {
    setTimeout(() => {
      Animated.timing(
      this.animatedValue,
      { 
        toValue: -90,
        duration: 350
      }).start()
    }, 4000)
  }

  render() {
    const { textSuggestion } = this.state;

    return (
      <>
        <Wrapper>
          
          <Fieldset>
            <LabelForm>Assista a vídeos curtos e ganhe dinheiro de verdade, para ajudar a chegar aos R$ 30,00 de saque mais rapidamente.</LabelForm>
            <LabelForm>Ganhe {numberToReal(this.props.rewardPrize)} por vídeo assistido na hora.</LabelForm>
            <LabelForm>Ganhe 1 KizoToken por vídeo assistido na hora.</LabelForm>
            <LabelForm>Tem o limite de 3 vídeos por dia.</LabelForm>

          </Fieldset>

          {
            this.props.user.ads < 3  ? (
              <Fieldset>
                <ButtonPrimary
                  onPress={() => this.openRewardedBanner()}
                  icon={'chevron-right'}
                  title={'Assistir vídeo'}>
                </ButtonPrimary>
              </Fieldset>
            ) : (
              <LabelForm>Você atingiu o limite do dia.</LabelForm>
            )
          }
          
    
        </Wrapper>

        {
          this.state.isLoadingAdd && (
            <FullLoader></FullLoader>
          )
        }

        <Animated.View 
          style={{ 
            transform: [{ translateY: this.animatedValue }], 
            height: 90, 
            marginRight: 15,
            marginLeft: 15,
            borderRadius: 8,
            backgroundColor: Colors.lightGrey, 
            position: 'absolute',
            left: 0, 
            top: 0, 
            right: 0, 
            justifyContent: 'center',
          }}>
          <KizoText style={{ color: Colors.primary, marginLeft: 15, fontSize: 20 }}>Você ganhou</KizoText>

          <View style={{ flexDirection: 'row', position: 'absolute', right: 15, top: 35  }}>
            <KizoText style={{ color: Colors.primary, fontSize: 22 }}>+{numberToReal(this.props.rewardPrize)}</KizoText>
          </View>
        </Animated.View>

       </>
    )
  }
}

const mapStateToProps = state => ({
  updateUserInfo: state.userInfo.updateUserInfo,
  addListRequest: state.userInfo.addListRequest,
  updateListsTitleRequest: state.userInfo.updateListsTitleRequest,
  showReward: state.userInfo.showReward,
  rewardPrize: state.userInfo.rewardPrize,
  lists: state.userInfo.lists,
  user: state.userInfo.user,
  auth: state.userInfo.auth,
  language: state.userInfo.user.language,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(UserInfoActions, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(AdsMoney);