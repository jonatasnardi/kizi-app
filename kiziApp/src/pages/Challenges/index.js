import React, {Component} from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Icon from 'react-native-vector-icons/FontAwesome5';

import { Creators as UserInfoActions } from '../../store/ducks/userInfo';

import { 
  Wrapper,
  WrapperInner,
  ListContainer,
  Row,
  SmallTitle,
  LabelPoints,
  LabelPosition,
  ImageIconUser,
  ContainerButtonsHome,
  WrapperButtonHome,
  ButtonHomeText,
  ContainerFollowers,
  ButtonHomeTextFollowers,
  ButtonHomeText2,
  BarLabel,
} from './styles';
import { numberToReal, getFirstWord, showAlert, showToast, isIntoMinutes } from '../../utils/functions';
import I18n from '../../utils/translation/i18n';
import ButtonPrimary from '../../components/buttons/ButtonPrimary';
import FullLoader from '../../components/FullLoader';
import Colors from '../../utils/colors';
import { Alert, View, ScrollView, FlatList, Platform, TouchableOpacity } from 'react-native';
import { requestPayment } from '../../services/payments';
import ButtonSecondary from '../../components/buttons/ButtonSecondary';
import { getUserRankingList, getGeneralRanking, getRankingWinnersWeekly } from '../../services/leagues';
import Images from '../../utils/images';
import { getFollowers, getFollowing } from '../../services/follower';
import { getWaitingYouChallenges, getSendChallenges } from '../../services/challenges';

class Challenges extends Component {
  state = {
    users: [],
    usersSend: [],
    isLoadingAdd: false,
    seeGeneralRanking: true,
    rankingSemanalData: [],
    isFollowers: true,
  }

  navigate;

  refInputSuggestion;

  constructor(props) {
    super(props);

    this.navigate = this.props.navigation;
  }

  componentDidMount() {
    this.loadItems();
  }

  loadItems = () => {
    const { isFollowers } = this.state;
    
    this.setState({
      isLoadingAdd: true,
    });

    getWaitingYouChallenges(this.props.user._id)
      .then((res) => { 
        this.setState({
          users: res.challenges,
          isLoadingAdd: false,
        });
      })
      .catch((error) => {
        console.log(error);
        showToast(I18n.t('errorGenericMessage'));
        this.setState({
          isLoadingAdd: false,
        });
      });

    getSendChallenges(this.props.user._id)
      .then((res) => { 
        this.setState({
          usersSend: res.challenges,
          isLoadingAdd: false,
        });
      })
      .catch((error) => {
        console.log(error);
        showToast(I18n.t('errorGenericMessage'));
        this.setState({
          isLoadingAdd: false,
        });
      });
  }

  render() {
    const { users, seeGeneralRanking, rankingSemanalData, usersSend } = this.state;
    const { user, followersCount, followingCount } = this.props;

    return (
      <>
        <Wrapper>
          <ScrollView>
            <WrapperInner>
              {/* <Row style={{ justifyContent: 'center', marginTop: -20, marginBottom: 15 }}>
                <LabelPosition style={{ color: Colors.primary, fontSize: 22, marginBottom: 0 }}>Buscar oponentes</LabelPosition>
              </Row> */}
              <ButtonPrimary
                onPress={() => this.navigate.push('SearchList')}
                title={'Buscar oponentes'}
                icon={'search'}>
              </ButtonPrimary>
              <ContainerButtonsHome>
                <WrapperButtonHome style={{ paddingRight: 10 }}>
                  <ContainerFollowers onPress={() => this.navigate.push('FollowerList', {
                        isFollowers: true,
                        userId: user._id
                      })}>
                    <ButtonHomeText2>Ver seguidores</ButtonHomeText2>
                    <ButtonHomeTextFollowers>{ followersCount }</ButtonHomeTextFollowers>
                  </ContainerFollowers>
                </WrapperButtonHome>

                <WrapperButtonHome style={{ paddingLeft: 10 }}>
                  <ContainerFollowers onPress={() => this.navigate.push('FollowerList', {
                        isFollowers: false,
                        userId: user._id
                      })}>
                    <ButtonHomeText2>Ver quem sigo</ButtonHomeText2>
                    <ButtonHomeTextFollowers>{ followingCount }</ButtonHomeTextFollowers>
                  </ContainerFollowers>
                </WrapperButtonHome>
              </ContainerButtonsHome>

              <Row style={{ justifyContent: 'center', marginTop: 10 }}>
                <LabelPosition style={{ color: Colors.secondary, fontSize: 22, marginBottom: 0 }}>Desafios aguardando eu jogar</LabelPosition>
              </Row>

              {
                (users.length > 0) ? (
                  <>
                    <Row>
                      <SmallTitle style={{ top: 1 }}>Usuário</SmallTitle>
                    </Row>
                    <FlatList
                      data={users}
                      renderItem={({ item, index }) => {
                        let baseItem = item.user;
                          return (
                            <TouchableOpacity onPress={() => this.navigate.push('Question', {
                              challengeId: item._id,
                              isChallenge: true,
                              isToFinish: true,
                              pointsUser: item.pointsUser,
                              image: item.user.image,
                            })}>
                              <ListContainer  style={{ backgroundColor: Colors.primary }}>
                                <BarLabel />
                                <Row>
                                  {
                                    !baseItem.image ? (
                                      <ImageIconUser source={Images['1']} />
                                    ) : (
                                      <ImageIconUser source={{uri: baseItem.image}} />
                                    )
                                  }
                                  {
                                    (baseItem.lastAccess && isIntoMinutes(30, baseItem.lastAccess)) && (
                                      <View style={{ backgroundColor: Colors.green, position: 'absolute', left: 40, bottom: 2, width: 14, height: 14, borderRadius: 14 }} />
                                    )
                                  }
                                  <SmallTitle>{ baseItem.name }</SmallTitle>
                                  <View style={{ position: 'absolute', right: 15, top: 7 }}>
                                    <Icon name="gamepad" size={30} color={Colors.secondary} />
                                  </View>
                                </Row>
                              </ListContainer>
                            </TouchableOpacity>
                          )
                      }
                      }
                      
                    />
                  </>
                ) : (
                  <SmallTitle style={{ color: Colors.primary, marginBottom: 15, textAlign: 'center' }}>Nenhum desafio enviado para você 🙅‍♂️</SmallTitle>
                )
              }

              <Row style={{ justifyContent: 'center', marginTop: 10 }}>
                <LabelPosition style={{ color: Colors.secondary, fontSize: 22, marginBottom: 0 }}>Desafios aguardando o adversário jogar</LabelPosition>
              </Row>

              {
                (usersSend.length > 0) ? (
                  <>
                    <Row>
                      <SmallTitle style={{ top: 1 }}>Usuário</SmallTitle>
                    </Row>
                    <FlatList
                      data={usersSend}
                      renderItem={({ item, index }) => {
                        let baseItem = item.challenged;
                          return (
                            <TouchableOpacity onPress={() => this.navigate.push('FriendProfile', {
                              idUser: baseItem._id,
                            })}>
                              <ListContainer  style={{ backgroundColor: Colors.primary }}>
                                <BarLabel style={{ backgroundColor: Colors.yellow }} />
                                <Row>
                                  {
                                    !baseItem.image ? (
                                      <ImageIconUser source={Images['1']} />
                                    ) : (
                                      <ImageIconUser source={{uri: baseItem.image}} />
                                    )
                                  }
                                  {
                                    (baseItem.lastAccess && isIntoMinutes(30, baseItem.lastAccess)) && (
                                      <View style={{ backgroundColor: Colors.green, position: 'absolute', left: 40, bottom: 2, width: 14, height: 14, borderRadius: 14 }} />
                                    )
                                  }
                                  <SmallTitle>{ baseItem.name }</SmallTitle>
                                </Row>
                              </ListContainer>
                            </TouchableOpacity>
                          )
                      }
                      }
                      
                    />
                  </>
                ) : (
                  <SmallTitle style={{ color: Colors.primary, marginBottom: 15, textAlign: 'center' }}>Nenhum desafio criado por você 👀{'\n'}Selecione um amigo e chame para o desafio</SmallTitle>
                )
              }
              
              <View style={{ marginBottom: 200 }} />
            </WrapperInner>
          </ScrollView>
          
        </Wrapper>

        {
          this.state.isLoadingAdd && (
            <FullLoader></FullLoader>
          )
        }
       </>
    )
  }
}

const mapStateToProps = state => ({
  addListRequest: state.userInfo.addListRequest,
  updateListsTitleRequest: state.userInfo.updateListsTitleRequest,
  lists: state.userInfo.lists,
  user: state.userInfo.user,
  followersCount: state.userInfo.followersCount,
  followingCount: state.userInfo.followingCount,
  language: state.userInfo.user.language,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(UserInfoActions, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Challenges);