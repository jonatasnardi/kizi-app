import React, {Component} from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { Creators as UserInfoActions } from '../../store/ducks/userInfo';



import { 
  Wrapper,
  LabelForm,
  Fieldset,
} from './styles';
import Colors from '../../utils/colors';
import { Linking } from 'react-native';
import ButtonPrimary from '../../components/buttons/ButtonPrimary';
class NotificationItem extends Component {

  navigate;

  constructor(props) {
    super(props);

    this.navigate = this.props.navigation;
  }

  componentDidMount() {
    
  }

  goToLink = (link) => {
    Linking.openURL(link);
  }

  render() {

    return (
      <>
        <Wrapper>
          
          <Fieldset>
            <LabelForm>{ this.navigate.state.params.title }</LabelForm>
          </Fieldset>

          <Fieldset>
            <LabelForm style={{ fontSize: 18, color: Colors.primary } }>{ this.navigate.state.params.message }</LabelForm>
          </Fieldset>

          {
            this.navigate.state.params.link && (
              <Fieldset>
                <ButtonPrimary
                  onPress={() => this.goToLink(this.navigate.state.params.link)}
                  backgroundColor={Colors.secondary}
                  textColor={Colors.white}
                  icon={'chevron-right'}
                  title={'Acessar link'}>
                </ButtonPrimary>
              </Fieldset>
            )
          }
          
    
        </Wrapper>

       </>
    )
  }
}

const mapStateToProps = state => ({
  addListRequest: state.userInfo.addListRequest,
  updateListsTitleRequest: state.userInfo.updateListsTitleRequest,
  lists: state.userInfo.lists,
  user: state.userInfo.user,
  language: state.userInfo.user.language,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(UserInfoActions, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(NotificationItem);