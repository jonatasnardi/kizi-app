import React, {Component} from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ImagePicker from 'react-native-image-crop-picker';

import { Creators as UserInfoActions } from '../../store/ducks/userInfo';

import I18n from '../../utils/translation/i18n';

import Icon from 'react-native-vector-icons/FontAwesome5';

import { 
  Wrapper,
  ImageUser,
  LabelForm,
  Fieldset,
  LabelValue,
  ModalAvatar,
  ModalWrapper,
  ModalTitle,
  ModalHeader,
  ButtonCloseModal,
  ContainerButtonsAvatar,
  WrapperButtonAvatar,
  ButtonQuickAccess,
  ContainerButtonsHome,
  WrapperButtonHome,
  ButtonHomeText,
  ContainerFollowers,
  ButtonHomeTextFollowers,
  ButtonHomeText2,
  Row,
  SmallTitle,
  ListContainer,
  LabelPosition,
  ImageIconUser,
} from './styles';
import Colors from '../../utils/colors';
import { showAlert, showToast, calculatePercentage, isIntoMinutes, cutString } from '../../utils/functions';
import ButtonPrimary from '../../components/buttons/ButtonPrimary';
import FullLoader from '../../components/FullLoader';
import { userContact, updateAvatar, getUserInfo, updateImageProfile } from '../../services/user';
import Images from '../../utils/images';
import { Alert, View, TouchableOpacity, FlatList, ScrollView, Platform, Image, Dimensions } from 'react-native';
import { getUserQuestionUser } from '../../services/userQuestion';

class ShareStories extends Component {
  state = {
    userQuestions: [],
    isLoadingAdd: false,
    modalAvatarVisible: false,
    followsCount: 0,
    followersCount: 0,
    imgWidth: null,
    imgHeight: null,
  };

  constructor(props) {
    super(props);

    this.navigate = this.props.navigation;
  }

  componentDidMount() {
    const { user } = this.props;

    showAlert('Tire um print', 'Feche esta mensagem, tire um print e faça o post no WhatsApp, no Instagram e Facebook.');

    // Image.getSize('../../assets/share.png', (width, height) => {
      const screenWidth = Dimensions.get('window').width;
      const scaleFactor = 793 / screenWidth;
      const imageHeight = 920 / scaleFactor;
      this.setState({imgWidth: screenWidth, imgHeight: imageHeight})
    // })
  }

  render() {
    const { userQuestions, followsCount, followersCount, imgWidth, imgHeight } = this.state;
    let { user } = this.props;

    return (
      <>
        <Wrapper>
          <Fieldset style={{ flexDirection: 'row', justifyContent: 'center', marginBottom: 0, marginTop: 0 }}>
              <TouchableOpacity onPress={() => {}}>
                {
                  !user.image ? (
                    <ImageUser source={Images['1']} />
                  ) : (
                    <ImageUser source={{uri: user.image}} />
                  )
                }
              </TouchableOpacity>
              <Fieldset style={{ alignItems: 'center', paddingTop: 0, paddingBottom: 25, marginLeft: 10, position: 'relative', top: 10}}>
                <LabelValue style={{ color: Colors.secondary }}>{user.name}</LabelValue>
                <LabelValue style={{ color: Colors.primary, }}>@{user.username}</LabelValue>
              </Fieldset>
          </Fieldset>
          
          

          {/* <ContainerButtonsHome>
            <WrapperButtonHome style={{ paddingRight: 10 }}>
              <ContainerFollowers style={{ backgroundColor: 'transparent', }} onPress={() => this.navigate.push('FollowerList', {
                    isFollowers: true,
                    userId: user._id
                  })}>
                <ButtonHomeText2>Seguidores</ButtonHomeText2>
                <ButtonHomeTextFollowers>{ this.props.followersCount }</ButtonHomeTextFollowers>
              </ContainerFollowers>
            </WrapperButtonHome>

            <WrapperButtonHome style={{ paddingLeft: 10 }}>
              <ContainerFollowers style={{ backgroundColor: 'transparent', }} onPress={() => this.navigate.push('FollowerList', {
                    isFollowers: false,
                    userId: user._id
                  })}>
                <ButtonHomeText2>Seguindo</ButtonHomeText2>
                <ButtonHomeTextFollowers>{ this.props.followingCount }</ButtonHomeTextFollowers>
              </ContainerFollowers>
            </WrapperButtonHome>
          </ContainerButtonsHome> */}
          <View style={{ alignItems: 'center' }}>
            <Image resizeMode={'contain'}  style={{ width: imgWidth, height: imgHeight, }} source={require('../../assets/share.png')} />
          </View>
          
          
          <View style={{ marginBottom: 200 }} />
    
        </Wrapper>
        

       </>
    )
  }
}

const mapStateToProps = state => ({
  addListRequest: state.userInfo.addListRequest,
  updateListsTitleRequest: state.userInfo.updateListsTitleRequest,
  updateFollowersCount: state.userInfo.updateFollowersCount,
  updateFollowingCount: state.userInfo.updateFollowingCount,
  followersCount: state.userInfo.followersCount,
  followingCount: state.userInfo.followingCount,
  lists: state.userInfo.lists,
  user: state.userInfo.user,
  language: state.userInfo.user.language,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(UserInfoActions, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(ShareStories);