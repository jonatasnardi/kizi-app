import styled from 'styled-components/native';
import Colors from '../../utils/colors';
import { Type } from '../../utils/layout';
import { Dimensions } from 'react-native';
import { isIphoneX } from '../../utils/functions';
const imageWidth = 60;

export const Wrapper = styled.View`
  ${Type.wrapper};
  background-color: ${Colors.primary};
  padding: 0;
  flex: 1;
`;

export const WrapperInner = styled.View`
  ${Type.wrapper};
  flex: 1;
`;

export const Fieldset = styled.View`
  flex-direction: column;
  margin-bottom: 20px;
`;

export const LabelForm = styled.Text`
  ${Type.body};
  color: ${Colors.white};
  font-size: 20px;
  margin-top: 0;
  margin-bottom: 10px;
`;

export const LabelTitleTopic = styled.Text`
  ${Type.body};
  color: ${Colors.secondary};
  font-size: 20px;
  margin-top: 10px;
  margin-bottom: 10px;
  text-align: left;
`;

export const LabelFinishItems = styled.Text`
  ${Type.body};
  color: ${Colors.primary};
  font-size: 30px;
  margin-top: -2px;
  margin-left: 5px;
  text-align: left;
`;


export const TextHelp = styled.Text`
  ${Type.body};
  color: ${Colors.primary};
  font-size: 16px;
  margin-bottom: 8px;
  margin-top: 8px;
  text-align: center;
`;

export const LabelCoins = styled.Text`
  ${Type.body};
  color: ${Colors.primary};
  font-size: 18px;
  margin-bottom: 10px;
  margin-right: 4px;
  text-align: center;
  font-weight: bold;
`;

export const LabelDescriptionTopic = styled.Text`
  ${Type.body};
  color: ${Colors.primary};
  font-size: 17px;
`;

export const LabelAwards = styled.Text`
  ${Type.body};
  color: ${Colors.primary};
  font-size: 20px;
`;

export const LabelFormMin = styled.Text`
  ${Type.body};
  color: ${Colors.white};
  font-size: 18px;
  margin-bottom: 10px;
  text-align: center;
  width: 100%;
`;

export const BigBox = styled.View`
  width: 100%;
  height: 58px;
`;

export const SubtitleText = styled.Text`
  ${Type.body};
  padding-top: 15px;
  color: ${Colors.primary};
`;

export const TextMoney = styled.Text`
  ${Type.body};
  color: ${Colors.secondary};
  font-size: 32px;
  text-align: center;
`;

export const TitleMotivate = styled.Text`
  ${Type.body};
  color: ${Colors.secondary};
  padding: 0 15px;
  font-size: 32px;
  text-align: center;
`;


export const ContainerMotivate = styled.View`
  width: ${Dimensions.get("window").width};
  height: ${Dimensions.get("window").height};
  position: absolute;
  top: 0;
  height: 100%;
  padding-top: 250px;
  background: rgba(0,0,0,0.7);
`;

export const ContainerIconAnimate = styled.View`
  width: ${Dimensions.get("window").width/2};
  height: ${Dimensions.get("window").height};
  position: absolute;
  top: 0%;
  right: 0;
`;



export const ContainerAlternative = styled.View`
  width: 100%;
  margin: 0 0 10px 0;
`;

export const ContainerButtonsAvatar = styled.View`
  width: 100%;
  flex-direction: row;
  flex-wrap: wrap;
`;

export const WrapperButtonAvatar = styled.View`
  width: 50%;
  margin-bottom: 10px;
  text-align: center;
  flex-direction: column;
  align-content: center;
`;

export const FixedProgress = styled.View`
  width: 100%;
  background: ${Colors.primary};
  position: absolute;
  height: 16px;
  top: 0;
  padding: 4px 15px 0 15px;
`;

export const ButtonQuickAccess = styled.TouchableOpacity`
  ${Type.buttonStyle};
  padding: 3px 10px;
  position: relative;
`;

export const KizoText = styled.Text`
  ${Type.body};
  font-size: 17;
  color: ${Colors.yellow};
  margin-right: 5px;
`;

export const ImageWithContent = styled.Image`
  margin-bottom: 10px;
  justify-content: center;
  align-items: center;
`;

export const ModalFinish = styled.Modal`
  ${Type.wrapper};
`;

export const ModalWrapper = styled.View`
  ${Type.wrapper};
`;

export const ModalHeader = styled.View`
  margin-top: 20px;
  flex-direction: row;
  justify-content: flex-end;
  align-items: center;
  margin-bottom: 30px;
  padding-bottom: 10px;
`;

export const ButtonCloseModal = styled.TouchableOpacity`

`;

export const ModalTitle = styled.Text`
  ${Type.body};
  color: ${Colors.primary};
  flex: 1;
  font-size: 22px;
`;

export const ImageLogo = styled.Image`
  width: ${imageWidth};
  height: ${imageWidth};
  margin-left: 10px;
`;

export const Row = styled.View`
  width: 100%;
  flex-direction: row;
`;

export const SmallTitle = styled.Text`
  ${Type.body};
  color: ${Colors.black};
  font-size: 18px;
  margin-left: 10px;
  position: relative;
  top: 11px;
`;

export const LabelPosition = styled.Text`
  ${Type.body};
  color: ${Colors.secondary};
  font-size: 18px;
  text-align: center;
  position: relative;
  top: 11px;
`;

export const LabelPoints = styled.Text`
  ${Type.body};
  color: ${Colors.primary};
  font-size: 20px;
  position: absolute;
  right: 10px;
  top: 10px;
`;

export const ImageIconUser = styled.Image`
  width: 45px;
  height: 45px;
  border-radius: 45px;
  margin-left: 4px;
  border: 1px solid ${Colors.secondary};
`;

export const ListContainer = styled.View`
  margin-bottom: 0;
  background: ${Colors.white};
  padding: 10px;
  margin: 5px 0;
  border-width: 1px;
  border-color: ${Colors.primary};
  border-radius: 5px;
  min-height: 30px;
  flex-direction: column;
`;

export const ContainerBanner = styled.View`
  width: 100%;
  height: 60px;
  position: absolute;
  bottom: ${isIphoneX() ? 22 : 0};
  left: 0;
`;


export const InputName = styled.TextInput`
  ${Type.body};
  padding-bottom: 5px;
  font-size: 26px;
  color: ${Colors.primary};
  border-bottom-color: ${Colors.primary};
  border-bottom-width: 2px;
`;