import React, {Component} from 'react';


import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Icon from 'react-native-vector-icons/FontAwesome5';
import * as Progress from 'react-native-progress';
import Rate, { AndroidMarket } from 'react-native-rate';
import DeviceInfo from 'react-native-device-info';
import {fetch} from 'react-native-ssl-pinning';
import LottieView from 'lottie-react-native';
const Sound = require('react-native-sound')
const requireAudioSuccess = require('../../assets/success.mp3');
const requireAudioFailure = require('../../assets/failure.mp3');
const requireAudioWin = require('../../assets/success-win-challenge.mp3');
// const requireAudioFailure = require('./test.mp3');




import { Creators as UserInfoActions } from '../../store/ducks/userInfo';
import CountDown from 'react-native-countdown-component';

import {
  AdMobBanner,
  AdMobInterstitial,
} from 'react-native-admob';

import { 
  Wrapper,
  BigBox,
  SubtitleText,
  TextMoney,
  TitleMotivate,
  ContainerMotivate,
  LabelForm,
  ContainerAlternative,
  LabelFormMin,
  WrapperInner,
  LabelTitleTopic,
  LabelFinishItems,
  LabelDescriptionTopic,
  Fieldset,
  ListContainer,
  Row,
  SmallTitle,
  LabelPoints,
  LabelPosition,
  ImageIconUser,
  ContainerButtonsAvatar,
  WrapperButtonAvatar,
  TextHelp,
  LabelCoins,
  KizoText,
  ImageWithContent,
  ModalFinish,
  ModalWrapper,
  ModalTitle,
  ModalHeader,
  ButtonCloseModal,
  ImageLogo,
  InputName,
  ContainerBanner,
  FixedProgress,
  ContainerIconAnimate,
} from './styles';
import { numberToReal, getFirstWord, showAlert, shuffleArray, showToast, formatKTs, getRandomNumber } from '../../utils/functions';
import ButtonPrimary from '../../components/buttons/ButtonPrimary';
import FullLoader from '../../components/FullLoader';
import Colors from '../../utils/colors';
import { REWARDS_ENUM } from '../../utils/enums';
import { Alert, View, Dimensions, Image, ScrollView, FlatList, TouchableOpacity, Platform, Animated, StatusBar } from 'react-native';
import { requestPayment } from '../../services/payments';
import ButtonSecondary from '../../components/buttons/ButtonSecondary';
import { get6RandomQuestions } from '../../services/questions';
import { removeUserKTs, updateValuesAfterWeeklyRound, updateValuesAfterDailyRound } from '../../services/general';
import Images from '../../utils/images';
import { listUsersOfLeague, updateUserPointsLeague } from '../../services/leagues';
import { getUserInfo, updateRewards } from '../../services/user';
import { getChallengeQuestions, updateValuesChallenge } from '../../services/challenges';
import { BASE_URL_API } from '../../helpers/variables';

const win = Dimensions.get('window');
const ratio = win.width/541; //541 is actual image width

class Question extends Component {
  state = {
    questions: [],
    messageToUser: null,
    currentQuestionIndex: 0,
    activeQuestion: null,
    alternativesList: [],
    wrongAlternativesList: [],
    isLoadingAdd: false,
    switchValue: true,
    helped: false,
    questionImage: '',
    progressValue: 1,
    goToProPage: false,
    adShown: false,
    imgWidth: null,
    imgHeight: null,
    payload: {},
    colorsAlternatives: [
      Colors.lightGrey,
      Colors.lightGrey,
      Colors.lightGrey,
      Colors.lightGrey,
    ],
    currentBarColor: Colors.secondary,
    showCountdown: true,
    hasSent: 0,
    disableAll: false,
    showHit: false,
    randomAnimate: 1,
    phrases: ['Parabéns!', 'Perfeito!', 'Demaaaaais!', 'Fera demais!', 'Show de bola!', 'Você é demais!', 'Continue assim :)', 'Você é muito inteligente!', 'Ótimo!', 'Boaaaa!', 'Vamos que vamos!', 'Desse jeito vai ficar difícil pros adversários', 'Não tem pra ninguém!', 'O melhor do Kizo!', 'Quem segura?', 'Quando crescer, quero ser igual a você!'],
    currentPhrase: 'Parabéns',
    hits: 0,
    fails: 0,
    points: 0,
    modalFinishVisible: false,
    hitsMathematics: 0,
    failsMathematics: 0,
    hitsPortuguese: 0,
    failsPortuguese: 0,
    hitsArts: 0,
    failsArts: 0,
    hitsScience: 0,
    failsScience: 0,
    hitsHistory: 0,
    failsHistory: 0,
    hitsEnglish: 0,
    failsEnglish: 0,
    hitsSports: 0,
    failsSports: 0,
    hitsEntertainment: 0,
    failsEntertainment: 0,
    hitsGeography: 0,
    failsGeography: 0,
    hitsGeneral: 0,
    failsGeneral: 0,
    // Leagues
    usersFromLeague: [],
    leagueId: null,
    leagueUserQtd: 10,

    // Rewards
    previousUserData: null,
    earnedKTs: 0,
    currentRewardTitle: '',

    reward: {
      playedFirstMatch: true,
      won1WeeklyTournament: true,
      won1OpenTournament: true,
      won10OpenTournament: true,
      joined1OpenTournament: true,
      joined5OpenTournament: true,
      joined50OpenTournament: true,
      joined100OpenTournament: true,
      reached500RankingPoints: true,
      reached1000RankingPoints: true,
      reached5000RankingPoints: true,
      reached10000RankingPoints: true,
      invited1Friend: true,
      invited5Friends: true,
      added1Question: true,
      hits10Mathematics: true,
      hits100Mathematics: true,
      hits500Mathematics: true,
      hits10Portuguese: true,
      hits100Portuguese: true,
      hits500Portuguese: true,
      hits10Arts: true,
      hits100Arts: true,
      hits500Arts: true,
      hits10Science: true,
      hits100Science: true,
      hits500Science: true,
      hits10History: true,
      hits100History: true,
      hits500History: true,
      hits10English: true,
      hits100English: true,
      hits500English: true,
      hits10Sports: true,
      hits100Sports: true,
      hits500Sports: true,
      hits10Entertainment: true,
      hits100Entertainment: true,
      hits500Entertainment: true,
      hits10General: true,
      hits100General: true,
      hits500General: true,
      hits10Geography: true,
      hits100Geography: true,
      hits500Geography: true,
    },
  }

  refInputMessage;

  bottomBannerId = Platform.select({
    android: 'ca-app-pub-4982456847713514/5350850363',
    ios: 'ca-app-pub-4982456847713514/5542422057',
  }); 

  interstitialBanner = Platform.select({
    android: 'ca-app-pub-4982456847713514/6663932035',
    ios: 'ca-app-pub-4982456847713514/5427065509',
  });

  price225 = Platform.select({
    android: 'R$ 3,97',
    ios: 'R$ 3,90',
  });

  refInputSuggestion;

  constructor(props) {
    super(props);

    this.navigate = this.props.navigation;
    this.animatedValue = new Animated.Value(-90);
    
  }

  async componentDidMount() {
    const { user } = this.props;
    let { activeQuestion } = this.state;

    this.successSound = new Sound(requireAudioSuccess, (error) => {
      if (error) {
        // console.tron.log('failed to load the sound', error);
        return;
      }
    });

    this.failureSound = new Sound(requireAudioFailure, (error) => {
      if (error) {
        // console.tron.log('failed to load the sound', error);
        return;
      }
    });

    this.winSound = new Sound(requireAudioWin, (error) => {
      if (error) {
        // console.tron.log('failed to load the sound', error);
        return;
      }
    });

    AdMobInterstitial.addEventListener("adClosed", () => {
      StatusBar.setHidden(false);
      // if (!this.state.adShown) {
        // this.premiate();
      // }
      
      AdMobInterstitial.removeAllListeners();
    });
    
    AdMobInterstitial.addEventListener("adFailedToLoad", () => {
      StatusBar.setHidden(false);
      // if (!this.state.adShown) {
        // this.premiate();
      // }

      AdMobInterstitial.removeAllListeners();
    });

    // StatusBar.setHidden(true);
    
    this.setState({
      leagueId: this.navigate.state.params.leagueId,
      leagueUserQtd: this.navigate.state.params.leagueUserQtd,
    });


    getUserInfo(user._id)
      .then((data) => { 
        this.setState({
          previousUserData: data.user,
          reward: data.user.reward,
        });
      })
      .catch((error) => {
        console.log(error)
      });

    if (this.navigate.state.params.isChallenge) {
      getChallengeQuestions(this.navigate.state.params.challengeId)
        .then((res) => { 
          let challengeQuestions = [res.challenge.q1, res.challenge.q2, res.challenge.q3, res.challenge.q4, res.challenge.q5, res.challenge.q6, res.challenge.q7, res.challenge.q8];
          activeQuestion = challengeQuestions[0];

          this.setState({
            questions: challengeQuestions,
            activeQuestion,
          });

          if (activeQuestion.image.length > 0) {
            Image.getSize(activeQuestion.image, (width, height) => {
              const screenWidth = Dimensions.get('window').width - 30;
              const scaleFactor = width / screenWidth;
              const imageHeight = height / scaleFactor;
              this.setState({imgWidth: screenWidth, imgHeight: imageHeight})
            })
          }
          this.sortAlternativesList(this.state.activeQuestion);
        })
        .catch((error) => {
          console.log(error);
          this.props.navigation.goBack(null);
          showToast('Ocorreu um erro inesperado.');
        });
    } else {
      get6RandomQuestions()
        .then((res) => { 
          activeQuestion = res.question[0];
          this.setState({
            questions: res.question,
            activeQuestion,
          });

          if (activeQuestion.image.length > 0) {
            Image.getSize(activeQuestion.image, (width, height) => {
              const screenWidth = Dimensions.get('window').width - 30;
              const scaleFactor = width / screenWidth;
              const imageHeight = height / scaleFactor;
              this.setState({imgWidth: screenWidth, imgHeight: imageHeight})
            })
          }
          this.sortAlternativesList(this.state.activeQuestion);
        })
        .catch((error) => {
          console.log(error);
          this.props.navigation.goBack(null);
          showToast('Ocorreu um erro inesperado.');
        });
    }
    
    
  }
  
  premiate = () => {
    if (this.navigate.state.params.tournamentType === 'weekly') {
      const { payload } = this.state;
          const { user } = this.props;

          fetch(`${BASE_URL_API}/general/finish-round/weekly-tournament/SdDs2d/${user._id}`, {
            method: "POST" ,
            timeoutInterval: 30000,
            body: JSON.stringify({
              hits: payload.hits,
              fails: payload.fails,
              points: payload.points,
              hitsMathematics: payload.hitsMathematics,
              failsMathematics: payload.failsMathematics,
              hitsPortuguese: payload.hitsPortuguese,
              failsPortuguese: payload.failsPortuguese,
              hitsArts: payload.hitsArts,
              failsArts: payload.failsArts,
              hitsScience: payload.hitsScience,
              failsScience: payload.failsScience,
              hitsHistory: payload.hitsHistory,
              failsHistory: payload.failsHistory,
              hitsEnglish: payload.hitsEnglish,
              failsEnglish: payload.failsEnglish,
              hitsSports: payload.hitsSports,
              failsSports: payload.failsSports,
              hitsEntertainment: payload.hitsEntertainment,
              failsEntertainment: payload.failsEntertainment,
              hitsGeography: payload.hitsGeography,
              failsGeography: payload.failsGeography,
              hitsGeneral: payload.hitsGeneral,
              failsGeneral: payload.failsGeneral,
              deviceId: DeviceInfo.getUniqueId(),
            }),
            sslPinning: {
                certs: ["mycert"],
            },
            headers:{
              Authorization: `Bearer ${this.props.auth.token}`,
            }
          })
          .then(response => {
            let userInfo = {
              ...user,
              weekPoints: user.weekPoints + payload.points,
              weekPointsMataMata: user.weekPointsMataMata + payload.points,
              points: user.points + payload.points,
            };

            this.props.updateUserInfo(userInfo);
          })
          .catch(err => {
            console.log(error);
            showToast('Ocorreu um erro ao salvar os seus pontos :(');
          })

    } else if (this.navigate.state.params.tournamentType === 'daily') {

      const { payload } = this.state;
      const { user } = this.props;


      fetch(`${BASE_URL_API}/general/finish-round/daily-tournament/da2DPds8d/${user._id}`, {
        method: "POST" ,
        timeoutInterval: 30000,
        body: JSON.stringify({
          hits: payload.hits,
          fails: payload.fails,
          points: payload.points,
          hitsMathematics: payload.hitsMathematics,
          failsMathematics: payload.failsMathematics,
          hitsPortuguese: payload.hitsPortuguese,
          failsPortuguese: payload.failsPortuguese,
          hitsArts: payload.hitsArts,
          failsArts: payload.failsArts,
          hitsScience: payload.hitsScience,
          failsScience: payload.failsScience,
          hitsHistory: payload.hitsHistory,
          failsHistory: payload.failsHistory,
          hitsEnglish: payload.hitsEnglish,
          failsEnglish: payload.failsEnglish,
          hitsSports: payload.hitsSports,
          failsSports: payload.failsSports,
          hitsEntertainment: payload.hitsEntertainment,
          failsEntertainment: payload.failsEntertainment,
          hitsGeography: payload.hitsGeography,
          failsGeography: payload.failsGeography,
          hitsGeneral: payload.hitsGeneral,
          failsGeneral: payload.failsGeneral,
          deviceId: DeviceInfo.getUniqueId(),
        }),
        sslPinning: {
            certs: ["mycert"],
        },
        headers:{
          Authorization: `Bearer ${this.props.auth.token}`,
        }
      })
      .then(response => {
        let userInfo = {
          ...user,
          dailyPoints: user.dailyPoints + payload.points,
          dailyCbPoints: user.dailyCbPoints + payload.points,
          points: user.points + payload.points,
        };
        
        this.props.updateUserInfo(userInfo);
      })
      .catch(err => {
        let errorMessage = 'Ocorreu um erro ao salvar os seus pontos :(';
        if (err.bodyString) {
          errorMessage = (JSON.parse(err.bodyString)).message;
        }
        showToast(errorMessage);
      })

    }
  }

  playSuccessSound = () => {
    if (this.props.activeSound) {
      this.successSound.play((success) => {
        if (!success) {
          // console.tron.log('Sound did not play')
        }
      });
    }
  }

  playFailureSound = () => {
    if (this.props.activeSound) {
      this.failureSound.play((success) => {
        if (!success) {
          // console.tron.log('Sound did not play')
        }
      })
    }
  }

  playWinSound = () => {
    if (this.props.activeSound) {
      this.winSound.play((success) => {
        if (!success) {
          // console.tron.log('Sound did not play')
        }
      })
    }
  }



  populateHitsFails = (isCorrect) => {
    let {
      hitsMathematics,
      failsMathematics,
      hitsPortuguese,
      failsPortuguese,
      hitsArts,
      failsArts,
      hitsScience,
      failsScience,
      hitsHistory,
      failsHistory,
      hitsEnglish,
      failsEnglish,
      hitsSports,
      failsSports,
      hitsEntertainment,
      failsEntertainment,
      hitsGeography,
      failsGeography,
      hitsGeneral,
      failsGeneral,
      activeQuestion,
    } = this.state;

    let value = 1;
    if (!isCorrect) {
      value = -1;
    }

    let baseSubject;

    if (this.navigate.state.params.isChallenge) {
      baseSubject = activeQuestion.subject;
    } else {
      baseSubject = activeQuestion.subject[0];
    }
    
    switch (baseSubject.name) {
      case 'Matemática':
        if (isCorrect) {
          hitsMathematics = hitsMathematics + 1;
        } else {
          failsMathematics = failsMathematics + 1;
        }
        break;
      
      case 'Português':
        if (isCorrect) {
          hitsPortuguese = hitsPortuguese + 1;
        } else {
          failsPortuguese = failsPortuguese + 1;
        }
        break;

      case 'Artes':
        if (isCorrect) {
          hitsArts = hitsArts + 1;
        } else {
          failsArts = failsArts + 1;
        }
        break;
      
      case 'Ciências':
        if (isCorrect) {
          hitsScience = hitsScience + 1;
        } else {
          failsScience = failsScience + 1;
        }
        break;

      case 'História':
        if (isCorrect) {
          hitsHistory = hitsHistory + 1;
        } else {
          failsHistory = failsHistory + 1;
        }
        break;

      case 'Esportes':
        if (isCorrect) {
          hitsSports = hitsSports + 1;
        } else {
          failsSports = failsSports + 1;
        }
        break;

      case 'Entretenimento':
        if (isCorrect) {
          hitsEntertainment = hitsEntertainment + 1;
        } else {
          failsEntertainment = failsEntertainment + 1;
        }
        break;

      case 'Geografia':
        if (isCorrect) {
          hitsGeography = hitsGeography + 1;
        } else {
          failsGeography = failsGeography + 1;
        }
        break;

      case 'Geral':
        if (isCorrect) {
          hitsGeneral = hitsGeneral + 1;
        } else {
          failsGeneral = failsGeneral + 1;
        }
        break;

      case 'Inglês':
        if (isCorrect) {
          hitsEnglish = hitsEnglish + 1;
        } else {
          failsEnglish = failsEnglish + 1;
        }
        break;
    
      default:
        break;
    }

    this.setState({
      hitsMathematics,
      failsMathematics,
      hitsPortuguese,
      failsPortuguese,
      hitsArts,
      failsArts,
      hitsScience,
      failsScience,
      hitsHistory,
      failsHistory,
      hitsEnglish,
      failsEnglish,
      hitsSports,
      failsSports,
      hitsEntertainment,
      failsEntertainment,
      hitsGeography,
      failsGeography,
      hitsGeneral,
      failsGeneral,
    });
    
  }

  sortAlternativesList = (question) => {
    let { alternativesList, wrongAlternativesList } = this.state;

    alternativesList = [{
      id: 0,
      title: question.optionA,
      isCorrect: question.correctOption === 'A',
      label: 'A',
      disabled: false,
    },
    {
      id: 1,
      title: question.optionB,
      isCorrect: question.correctOption === 'B',
      label: 'B',
      disabled: false,
    },
    {
      id: 2,
      title: question.optionC,
      isCorrect: question.correctOption === 'C',
      label: 'C',
      disabled: false,
    },
    {
      id: 3,
      title: question.optionD,
      isCorrect: question.correctOption === 'D',
      label: 'D',
      disabled: false,
    }];

    wrongAlternativesList = alternativesList.filter(item => !item.isCorrect);

    alternativesList = shuffleArray(alternativesList);

    this.setState({
      alternativesList,
      wrongAlternativesList,
    });
  }

  onChangeCountdown = () => {
    let { progressValue, currentBarColor } = this.state;

    if (progressValue <= 0.36) {
      currentBarColor = Colors.red;
    }

    this.setState({
      progressValue: progressValue - 0.04761,
      currentBarColor,
    });
  }

  disableRandAlternative = (arr, excludeNum) => {
    let { alternativesList } = this.state;
    let randNumber = Math.floor(Math.random() * arr.length);

    if (arr[randNumber].label == excludeNum.label || alternativesList[randNumber].disabled) {
      return this.disableRandAlternative(arr, excludeNum);
    } else {
      alternativesList[randNumber].disabled = true;

      this.setState({
        alternativesList,
      });

      return randNumber;
    }
  }

  timeEnded = () => {
    showToast(`Tempo esgotado para resposta`);
    this.setState({
      currentBarColor: Colors.secondary,
    })
    this.answerQuestion(false);
  }

  disableNItems = (n) => {
    let { alternativesList } = this.state;
    let { user } = this.props;

    if (user.kizoTokens < n) {
      return Alert.alert(
        'KizoTokens insuficientes',
        'Você não tem KizoTokens suficientes para jogar este torneio.',
        [
          {text: `Comprar 225 KTs por ${this.price225} após o jogo`, onPress: () => {
            this.setState({
              goToProPage: true,
            })
          }},
          {text: 'Ok', onPress: () => {
          }},
        ],
        {cancelable: false},
      );
    }

    const correctAlternativeIndex = alternativesList.findIndex( item => item.isCorrect);

    for (let i = 0; i < n; i++) {
      this.disableRandAlternative(alternativesList, alternativesList[correctAlternativeIndex]);
    }

    let userInfo = {
      ...user,
      kizoTokens: user.kizoTokens - n,
    };
    
    this.props.updateUserInfo(userInfo);

    removeUserKTs(user._id, n)
      .then((res) => { 
        
      })
      .catch((error) => {
        console.log(error);
        showToast('Ocorreu um erro ao remover os seus KTs');
      });

    this.setState({
      helped: true,
    })
  }

  showAlertPlayAgain = (type) => {
    setTimeout(() => {
      Alert.alert(
        'Jogar novamente',
        'Partiu jogar mais uma e ganhar mais pontos?',
        [
          {text: 'Não', onPress: () => {}},
          {text: 'Sim, quero', onPress: () => {
            switch (type) {
              case 'weekly':
                this.navigate.push('Question', { tournamentType: 'weekly', onGoBack: () => this.navigate.state.params.onGoBack() })
                break;
              case 'daily':
                this.navigate.push('Question', { tournamentType: 'daily' })
                break;
            
              default:
                break;
            }
            
          }},
        ],
        {cancelable: false},
      );
    }, 350);
  }

  resetQuestionInfo = () => {
    const { colorsAlternatives } = this.state;

    for (let i = 0; i < colorsAlternatives.length; i++) {
      colorsAlternatives[i] = Colors.lightGrey;
    }

    this.refs._scrollView.scrollTo(0);

    this.setState({
      showCountdown: true,
      helped: false,
      colorsAlternatives,
      disableAll: false,
      progressValue: 1,
      currentBarColor: Colors.secondary,
    });
    
  }

  setModalFinishVisible(visible) {
    this.setState({modalFinishVisible: visible});

    let currentTournament = null;

    if (!visible) {
      if (this.navigate.state.params.tournamentType === 'weekly') { // weekly tournament
        AdMobInterstitial.setAdUnitID(this.interstitialBanner);
        AdMobInterstitial.requestAd().then(() => {
          AdMobInterstitial.showAd();
          StatusBar.setHidden(true);
          this.setState({adShown: true});
          setTimeout(() => {
            this.props.navigation.goBack(null);  
            currentTournament = 'weekly';
            // this.showAlertPlayAgain('weekly');
  
            if (this.state.goToProPage) {
              setTimeout(() => {
                return this.navigate.push('Pro');  
              }, 300);
            }
          }, 300);
        }, (err) => {
          this.setState({adShown: true});
            StatusBar.setHidden(false);
            this.props.navigation.goBack(null);  
            currentTournament = 'weekly';
            // this.premiate();
            // this.showAlertPlayAgain('weekly');
  
            if (this.state.goToProPage) {
              setTimeout(() => {
                return this.navigate.push('Pro');  
              }, 300);
            }
        });

        // setTimeout(() => {
        //   if (!this.state.adShown) {
            
        //   }  
        // }, 5000);
        
        
      } else if (this.navigate.state.params.tournamentType === 'daily') { // daily tournament
        AdMobInterstitial.setAdUnitID(this.interstitialBanner);
        AdMobInterstitial.requestAd().then(() => {
          AdMobInterstitial.showAd();
          StatusBar.setHidden(true);
          this.setState({adShown: true});
          setTimeout(() => {

            this.props.navigation.goBack(null);  
            currentTournament = 'daily';
            // this.showAlertPlayAgain('daily');
  
            if (this.state.goToProPage) {
              setTimeout(() => {
                return this.navigate.push('Pro');  
              }, 300);
            }
          }, 300);
        }, (err) => {
          this.setState({adShown: true});
          // this.premiate();
          StatusBar.setHidden(false);
          this.props.navigation.goBack(null);  
          currentTournament = 'daily';
          // this.showAlertPlayAgain('daily');
          

          if (this.state.goToProPage) {
            setTimeout(() => {
              return this.navigate.push('Pro');  
            }, 300);
          }
        });
        // setTimeout(() => {
        //   if (!this.state.adShown) {
            
        //   }  
        // }, 5000);
      } else if (this.navigate.state.params.isChallenge) { // challenge
        updateValuesChallenge(this.props.user._id, this.state.payload, this.navigate.state.params.challengeId, this.state.messageToUser)
          .then((res) => {
          })
          .catch((error) => {
            console.log(error);
            showToast('Ocorreu um erro ao salvar os seus pontos :(');
          });
        AdMobInterstitial.setAdUnitID(this.interstitialBanner);
        AdMobInterstitial.requestAd().then(() => {
          AdMobInterstitial.showAd();
          this.setState({adShown: true});
          setTimeout(() => {
            this.navigate.push('Main');

            if (this.state.goToProPage) {
              setTimeout(() => {
                return this.navigate.push('Pro');  
              }, 300);
            }
          }, 400);
        });

        setTimeout(() => {
          if (!this.state.adShown) {
            StatusBar.setHidden(false);
            this.navigate.push('Main');

            if (this.state.goToProPage) {
              setTimeout(() => {
                return this.navigate.push('Pro');  
              }, 300);
            }
          }  
        }, 5000);
      } else { 
        AdMobInterstitial.setAdUnitID(this.interstitialBanner);
        AdMobInterstitial.requestAd().then(() => {
          AdMobInterstitial.showAd();
          this.setState({adShown: true});
          setTimeout(() => {
            this.navigate.push('Main');

            if (this.state.goToProPage) {
              setTimeout(() => {
                return this.navigate.push('Pro');  
              }, 300);
            }
          }, 400);
        });

        setTimeout(() => {
          if (!this.state.adShown) {
            StatusBar.setHidden(false);
            this.navigate.push('Main');

            if (this.state.goToProPage) {
              setTimeout(() => {
                return this.navigate.push('Pro');  
              }, 300);
            }
          }  
        }, 5000);
        
      }

      AdMobInterstitial.addEventListener("adClosed", () => {
        StatusBar.setHidden(false);
        // if (currentTournament) {
        //   this.showAlertPlayAgain(currentTournament);
        // }
        
      });
      
      AdMobInterstitial.addEventListener("adFailedToLoad", () => {
        StatusBar.setHidden(false);
      });
      
    }
  }

  loadUsersFromLeague = () => {
    let { usersFromLeague, leagueId, leagueUserQtd } = this.state;

    listUsersOfLeague(leagueId)
      .then((res) => { 
        usersFromLeague = res.userLeague;
        

        while (usersFromLeague.length < leagueUserQtd) {
          usersFromLeague.push({
            "userPoints": 0,
            "idUser": {
              "avatar": "1",
              "name": "Futuro oponente"
            },
          })
        }
        
        this.setState({
          isLoadingAdd: false,
          usersFromLeague,
        });
      })
      .catch((error) => {
        this.setState({
          isLoadingAdd: false
        });
        showToast('Ocorreu um erro ao exibir a classificação :(');
      }); 
  }

  getBgColor = (index) => {
    switch (index) {
      case 0:
        return '#f8bc44';
        break;

      default:
        break;
    }
  }

  getLabelColor = (index) => {
    if (index === 0) {
      return Colors.primary;
    } else {
      return Colors.secondary;
    }
  }

  answerQuestion = (isCorrect, index = null) => {
    let { 
      alternativesList,
      colorsAlternatives,
      activeQuestion,
      currentQuestionIndex,
      questions,
      hits,
      fails,
      points,
    } = this.state;
    const { user } = this.props;

    const correctAlternativeIndex = alternativesList.findIndex( item => item.isCorrect);

    this.setState({
      showCountdown: false,
      disableAll: true,
      currentBarColor: Colors.secondary,
    });

    if (index >= 0 && typeof index === 'number') {
      if (isCorrect) {
        colorsAlternatives[index] = Colors.green;
        hits += 1;
        points = points + parseInt(activeQuestion.weight);
        this.playSuccessSound();
        const randomAnimate = getRandomNumber(1, 5);
        let goUntil = 11;
        if (hits >= 4) {
          goUntil = 16;
        }
        let currentPhraseIndex = getRandomNumber(1, goUntil);
        let showOrNoNumber = getRandomNumber(1, 10);

        this.setState({
          showHit: (showOrNoNumber > 7) ? true : false,
          randomAnimate,
          currentPhrase: this.state.phrases[currentPhraseIndex - 1],
        });
        setTimeout(() => {
          this.setState({
            showHit: false,
          });
        }, 2000);
      } else {
        fails += 1;
        colorsAlternatives[index] = Colors.red;
        colorsAlternatives[correctAlternativeIndex] = Colors.green;
        this.playFailureSound();
      }

      this.populateHitsFails(isCorrect);
    } else {
      fails += 1;
    }

    let newReward = this.state.reward;

    setTimeout(() => {
      let ktsToEarnSubject = 0;
      if (isCorrect) {
        Object.keys(this.state.reward).forEach(key => {
          if (key.indexOf('hits500') <= 3 && key.indexOf('hits500') > -1 ) {
            let pureValue = key.split('500');
            pureValue = `${pureValue[0]}${pureValue[1]}`;

            if (!newReward[key] && (this.state.previousUserData[pureValue] + this.state[pureValue]) === 500) {
              ktsToEarnSubject += 8;
              newReward[key] = true;
              newReward = {
                ...newReward,
              }
    
              let userInfo = {
                ...user,
                kizoTokens: user.kizoTokens + ktsToEarnSubject,
              };

              this.setState({
                currentRewardTitle: REWARDS_ENUM[key],
              });
              
              this.props.updateUserInfo(userInfo);
    
              this.callToast();
    
              this.updateRewardByQuestion(newReward, ktsToEarnSubject);
            }
          } else if (key.indexOf('hits100') <= 3 && key.indexOf('hits100') > -1 ) {
            let pureValue = key.split('100');
            pureValue = `${pureValue[0]}${pureValue[1]}`;
            
            if (!newReward[key] && (this.state.previousUserData[pureValue] + this.state[pureValue]) === 100) {
              ktsToEarnSubject += 4;
              newReward[key] = true;
              newReward = {
                ...newReward,
              }
    
              let userInfo = {
                ...user,
                kizoTokens: user.kizoTokens + ktsToEarnSubject,
              };

              this.setState({
                currentRewardTitle: REWARDS_ENUM[key],
              });
              
              this.props.updateUserInfo(userInfo);
    
              this.callToast();
    
              this.updateRewardByQuestion(newReward, ktsToEarnSubject);
            }
          } else if (key.indexOf('hits10') <= 3 && key.indexOf('hits10') > -1 ) {
            let pureValue = key.split('10');
            pureValue = `${pureValue[0]}${pureValue[1]}`;
            
            
            if (!newReward[key] && (this.state.previousUserData[pureValue] + this.state[pureValue]) === 10) {
              ktsToEarnSubject += 1.5;
              newReward[key] = true;
              newReward = {
                ...newReward,
              }
    
              let userInfo = {
                ...user,
                kizoTokens: user.kizoTokens + ktsToEarnSubject,
              };

              this.setState({
                currentRewardTitle: REWARDS_ENUM[key],
              });
              
              this.props.updateUserInfo(userInfo);
    
              this.callToast();
    
              this.updateRewardByQuestion(newReward, ktsToEarnSubject);
            }
          }
        });

        
      }

      this.setState({
        earnedKTs: ktsToEarnSubject,
        reward: newReward,
      });
    }, 600);
    

    this.setState({
      colorsAlternatives,
      hits,
      fails,
      points,
    }, () => {
      if (currentQuestionIndex === questions.length - 1) { // last question
        setTimeout(() => {
          const {
            hitsMathematics,
            failsMathematics,
            hitsPortuguese,
            failsPortuguese,
            hitsArts,
            failsArts,
            hitsScience,
            failsScience,
            hitsHistory,
            failsHistory,
            hitsEnglish,
            failsEnglish,
            hitsSports,
            failsSports,
            hitsEntertainment,
            failsEntertainment,
            hitsGeography,
            failsGeography,
            hitsGeneral,
            failsGeneral,
            leagueId,
          } = this.state;
          const payload = {
            hits,
            fails,
            points,
            hitsMathematics,
            failsMathematics,
            hitsPortuguese,
            failsPortuguese,
            hitsArts,
            failsArts,
            hitsScience,
            failsScience,
            hitsHistory,
            failsHistory,
            hitsEnglish,
            failsEnglish,
            hitsSports,
            failsSports,
            hitsEntertainment,
            failsEntertainment,
            hitsGeography,
            failsGeography,
            hitsGeneral,
            failsGeneral,
          };

          this.setState({
            payload,
            hasSent: this.state.hasSent + 1,
          }, () => {
            if (this.state.hasSent <= 1) {
              this.premiate();
            }
            
          });

          
    
          if (this.navigate.state.params.tournamentType === 'weekly') { // weekly tournament
            // console.tron.log(1)
              

              // console.tron.log(2)
            

            // updateValuesAfterWeeklyRound(user._id, payload)
            //   .then((res) => {
            //     let userInfo = {
            //       ...user,
            //       weekPoints: user.weekPoints + points,
            //       weekPointsMataMata: user.weekPointsMataMata + points,
            //       points: user.points + points,
            //     };

            //     this.props.updateUserInfo(userInfo);
            //   })
            //   .catch((error) => {
            //     console.log(error);
            //     showToast('Ocorreu um erro ao salvar os seus pontos :(');
            //   });
          } else if (this.navigate.state.params.tournamentType === 'daily') { // daily tournament
            
            // updateValuesAfterDailyRound(user._id, payload)
            //   .then((res) => {
            //     let userInfo = {
            //       ...user,
            //       dailyPoints: user.dailyPoints + points,
            //       points: user.points + points,
            //     };
                
            //     this.props.updateUserInfo(userInfo);
            //   })
            //   .catch((error) => {
            //     console.log(error);
            //     showToast('Ocorreu um erro ao salvar os seus pontos :(');
            //   });
          } else if (this.navigate.state.params.isChallenge) { // challenge
            if (this.navigate.state.params.isToFinish) {
              if (this.state.points > this.navigate.state.params.pointsUser) {
                this.playWinSound();
              }
            }
            
          } else if (this.navigate.state.params.tournamentType === 'openLeague') {
            this.setState({
              isLoadingAdd: true,
            });
            updateUserPointsLeague(leagueId, user._id, payload)
              .then((res) => { 
                this.loadUsersFromLeague();
              })
              .catch((error) => {
                this.setState({
                  isLoadingAdd: false
                });
                showToast('Ocorreu um erro ao exibir a classificação :(');
              }); 
            
          }

          let copyReward = this.state.reward;
          let ktsToEarn = 0;

          if (this.state.previousUserData.points === 0 && points > 0) {
            ktsToEarn += 4;
            copyReward = {
              ...copyReward,
              playedFirstMatch: true,
            }
          }

          updateRewards(user._id, copyReward, ktsToEarn)
            .then((data) => { 
            })
            .catch((error) => {
              console.log(error)
            });
          
          let timeToOpenModal = 0;

          if (hits === questions.length && this.props.user.points > 20) {
            this.playWinSound();

            fetch(`${BASE_URL_API}/general/add-kts/I1d013cs/${user._id}`, {
              method: "POST" ,
              timeoutInterval: 30000,
              body: JSON.stringify({
                qtd: 0.1,
                hp: 'OIQJ091MIOAm01m0DAKMMD01JAKOMDAOPJ1doakodks90120d1iaspdMOKDMASJDASKOMdj0doam',
                isPro: 'no',
              }),
              sslPinning: {
                  certs: ["mycert"],
              },
              headers:{
                Authorization: `Bearer ${this.props.auth.token}`,
              }
            })
            .then(response => {
              let userInfo = {
                ...user,
                kizoTokens: this.props.user.kizoTokens + 0.1,
              };
              
              this.props.updateUserInfo(userInfo);
        
              this.setState({
                isLoading: false,
              });
            })
            .catch(err => {
              console.log(error);
              this.setState({
                isLoading: false,
              });
              // showToast('Ocorreu um erro ao adicionar os seus KTs');
            })

            if (this.props.showRatingModal && !this.props.alreadyRated) {
              const rateOptions = {
                AppleAppID:"1509070095",
                GooglePackageName:"br.com.kizoappqea",
                AmazonPackageName:"br.com.kizoappqea",
                preferredAndroidMarket: AndroidMarket.Google,
                preferInApp:true,
                openAppStoreIfInAppFails:false,
                fallbackPlatformURL:"https://kizoapp.blogspot.com",
              }

              if (Platform.OS === 'ios') {
                timeToOpenModal = 700;
                Rate.rate(rateOptions, success => {
                  if (success) {
                    this.props.updateAlreadyRated(true);
                  }
                })
              } else {
                Alert.alert(
                  'Avalie nosso app',
                  'Gostaria de avaliar nosso app? São só 10 segundos :)',
                  [
                    {text: 'Não', onPress: () => {
                      this.props.updateAlreadyRated(true);
                    }},
                    {text: 'Sim, quero', onPress: () => {
                      Rate.rate(rateOptions, success => {
                        if (success) {
                          this.props.updateAlreadyRated(true);
                        }
                      })
                    }},
                  ],
                  {cancelable: false},
                );
              }

             
            }
          }
    
          this.setState({
            helped: true,
            isLoadingAdd: false,
            reward: copyReward,
          });
        
          setTimeout(() => {
            this.setModalFinishVisible(true);  
          }, timeToOpenModal);
          
        }, 1500);
      } else {
        setTimeout(() => {
          currentQuestionIndex += 1;
          activeQuestion = questions[currentQuestionIndex];
          this.resetQuestionInfo();
          
          this.setState({
            currentQuestionIndex,
            activeQuestion,
          });
    
          if (activeQuestion.image.length > 0) {
            Image.getSize(activeQuestion.image, (width, height) => {
              const screenWidth = Dimensions.get('window').width - 30;
              const scaleFactor = width / screenWidth;
              const imageHeight = height / scaleFactor;
              this.setState({imgWidth: screenWidth, imgHeight: imageHeight})
            })
          }
          this.sortAlternativesList(this.state.activeQuestion);
        }, 1800);
      }
      
    });

    
  }

  callToast() {
    Animated.timing(
      this.animatedValue,
      { 
        toValue: 15,
        duration: 350
      }).start(this.closeToast())
  }
  
  closeToast() {
    setTimeout(() => {
      Animated.timing(
      this.animatedValue,
      { 
        toValue: -90,
        duration: 350
      }).start()
    }, 2000)
  }

  updateRewardByQuestion = (copyReward, ktsToEarnSubject) => {
    const { user } = this.props;

    updateRewards(user._id, copyReward, ktsToEarnSubject)
      .then((data) => {
      })
      .catch((error) => {
        console.log(error)
      });
  }

  render() {
    const { 
      questions,
      activeQuestion,
      currentQuestionIndex,
      alternativesList,
      helped,
      colorsAlternatives,
      imgWidth,
      imgHeight,
      showCountdown,
      disableAll,
      modalFinishVisible,
      hits,
      fails,
      points,
      usersFromLeague,
      earnedKTs,
      progressValue,
      currentBarColor,
    } = this.state;
    const { user } = this.props;

    let baseSubject;

    

    if (activeQuestion) {
      if (this.navigate.state.params.isChallenge) {
        baseSubject = activeQuestion.subject;
      } else {
        baseSubject = activeQuestion.subject[0];
      }
    }
    

    return (
      <>
      <Wrapper>
        <ScrollView ref='_scrollView'>
          <WrapperInner>
          
            {
              (this.state.isLoadingAdd || this.state.questions.length === 0 || this.state.alternativesList.length === 0 || !activeQuestion) ? (
                <View style={{ marginTop: 30, marginBottom: 30 }}>
                  <FullLoader></FullLoader>
                </View>
              ) :
              (
                  <View>
                    <View style={{ flexDirection: 'row', position: 'absolute', top: 4, left: 0, }}>
                      <Icon name="clock" size={20} color={Colors.secondary} />
                      { showCountdown && (
                        <CountDown
                          until={21}
                          onFinish={() => this.timeEnded()}
                          onPress={() => {}}
                          onChange={() => this.onChangeCountdown()}
                          size={17}
                          style={{ marginTop: -11, }}
                          timeLabelStyle={Colors.secondary}
                          digitStyle={{backgroundColor: Colors.primary}}
                          digitTxtStyle={{color: Colors.secondary}}
                          timeToShow={['S']}
                          timeLabels={{s: ''}}
                        />
                      )
                      }
                      
                    </View>
                    <View style={{ flexDirection: 'row', position: 'absolute', top: 4, right: 0, }}>
                      <KizoText>{formatKTs(user.kizoTokens)}</KizoText>
                      <Icon name="coins" size={20} color={Colors.yellow} />
                    </View>
                    <LabelForm style={{ textAlign: 'center' }}>{ currentQuestionIndex + 1 }/{ questions.length }</LabelForm>
                    <LabelForm style={{ textAlign: 'center', color: Colors.secondary }}>{ baseSubject.name.toUpperCase() }</LabelForm>
                    <LabelForm style={{ fontSize: 22 }}>{ currentQuestionIndex + 1 }) {activeQuestion.enunciation}</LabelForm>
                    {
                      activeQuestion.image.length > 0 && (
                        <ImageWithContent 
                          resizeMode={'contain'} 
                          style={{ width: imgWidth, height: imgHeight,  }} 
                          source={{uri: activeQuestion.image}}/>
                      )
                    }
                    

                    <ContainerAlternative>
                      <ButtonPrimary
                        onPress={() => this.answerQuestion(alternativesList[0].isCorrect, 0)}
                        backgroundColor={(alternativesList[0].disabled) ? '#71839b' : colorsAlternatives[0]}
                        disabled={(alternativesList[0].disabled || disableAll)}
                        textColor={Colors.primary}
                        title={alternativesList[0].title}>
                      </ButtonPrimary>
                    </ContainerAlternative>

                    <ContainerAlternative>
                      <ButtonPrimary
                        onPress={() => this.answerQuestion(alternativesList[1].isCorrect, 1)}
                        backgroundColor={(alternativesList[1].disabled) ? '#71839b' : colorsAlternatives[1]}
                        disabled={(alternativesList[1].disabled || disableAll)}
                        textColor={Colors.primary}
                        title={alternativesList[1].title}>
                      </ButtonPrimary>
                    </ContainerAlternative>

                    <ContainerAlternative>
                      <ButtonPrimary
                        onPress={() => this.answerQuestion(alternativesList[2].isCorrect, 2)}
                        backgroundColor={(alternativesList[2].disabled) ? '#71839b' : colorsAlternatives[2]}
                        disabled={(alternativesList[2].disabled || disableAll)}
                        textColor={Colors.primary}
                        title={alternativesList[2].title}>
                      </ButtonPrimary>
                    </ContainerAlternative>

                    <ContainerAlternative>
                      <ButtonPrimary
                        onPress={() => this.answerQuestion(alternativesList[3].isCorrect, 3)}
                        backgroundColor={(alternativesList[3].disabled) ? '#71839b' : colorsAlternatives[3]}
                        disabled={(alternativesList[3].disabled || disableAll)}
                        textColor={Colors.primary}
                        title={alternativesList[3].title}>
                      </ButtonPrimary>
                    </ContainerAlternative>

                    <View style={{ marginTop: 10 }} />

                    <View style={{ flexDirection: 'row' }}>
                      <LabelTitleTopic>Ajuda</LabelTitleTopic>
                      <View style={{ position: 'relative', top: 10, left: 4 }}>
                        <Icon name="hands-helping" size={20} color={Colors.secondary} />
                      </View>
                    </View>
                    

                    <ContainerButtonsAvatar>
                      <WrapperButtonAvatar style={{ paddingRight: 10 }}>
                        <TouchableOpacity onPress={() => this.disableNItems(1)} disabled={helped || disableAll} style={{ backgroundColor: helped ? '#71839b' : Colors.lightGrey, borderRadius: 8, flexDirection: 'column', justifyContent: 'center', alignItems: 'center', height: 70 }}>
                          <TextHelp>Eliminar 1 opção</TextHelp>
                          <View style={{ flexDirection: 'row' }}>
                            <LabelCoins>Por 1</LabelCoins>
                            <Icon name="coins" size={20} color={Colors.yellow} />
                          </View>
                          
                        </TouchableOpacity>
                      </WrapperButtonAvatar>

                      <WrapperButtonAvatar style={{ paddingLeft: 10 }}>
                        <TouchableOpacity onPress={() => this.disableNItems(2)} disabled={helped || disableAll} style={{ backgroundColor: helped ? '#71839b' : Colors.lightGrey, borderRadius: 8, flexDirection: 'column', justifyContent: 'center', alignItems: 'center', height: 70 }}>
                          <TextHelp>Eliminar 2 opções</TextHelp>
                          <View style={{ flexDirection: 'row' }}>
                            <LabelCoins>Por 2</LabelCoins>
                            <Icon name="coins" size={20} color={Colors.yellow} />
                          </View>
                          
                        </TouchableOpacity>
                      </WrapperButtonAvatar>
                    </ContainerButtonsAvatar>

                    <ButtonPrimary
                      onPress={() => {
                        this.setState({
                          isLoadingAdd: true,
                          currentQuestionIndex: questions.length - 1,
                        }, () => {
                          this.answerQuestion(false);
                        });
                      }}
                      backgroundColor={Colors.red}
                      disabled={(disableAll)}
                      textColor={Colors.white}
                      title={'Desistir'}>
                    </ButtonPrimary>

                    <View style={{ marginBottom: 100 }} />
                    
                  </View>
              )
            }
            {
              this.state.showHit && (
                <ContainerMotivate>
                  <TitleMotivate>{this.state.currentPhrase}</TitleMotivate>
                  {
                    this.state.randomAnimate == 1 && (
                      <ContainerIconAnimate>
                        <LottieView source={require('../../assets/congrats1.json')} autoPlay loop />
                      </ContainerIconAnimate>
                     )
                   }
                  {
                    this.state.randomAnimate == 2 && (
                      <ContainerIconAnimate>
                        <LottieView source={require('../../assets/congrats2.json')} autoPlay loop />
                      </ContainerIconAnimate>
                    )
                  }
                  {
                    this.state.randomAnimate == 3 && (
                      
                        <LottieView source={require('../../assets/congrats3.json')} autoPlay loop />
                    )
                  }
                  {
                    this.state.randomAnimate == 4 && (
                      <ContainerIconAnimate>
                        <LottieView source={require('../../assets/congrats4.json')} autoPlay loop />
                      </ContainerIconAnimate>
                    )
                  }
                  {
                    this.state.randomAnimate == 5 && (
                      <ContainerIconAnimate>
                        <LottieView source={require('../../assets/congrats5.json')} autoPlay loop />
                      </ContainerIconAnimate>
                    )
                  }

                  
                </ContainerMotivate>
              )
            }
            
            </WrapperInner>
          </ScrollView>
          
          <FixedProgress>
            <Progress.Bar progress={progressValue} color={currentBarColor} width={null} />
          </FixedProgress>
        </Wrapper>

        <ModalFinish
          animationType="slide"
          transparent={false}
          visible={modalFinishVisible}>
          <ModalWrapper>
            <ModalHeader style={{borderBottomColor: Colors.black50, borderBottomWidth: 1}}>
              <ModalTitle>Resultado</ModalTitle>
              <ButtonCloseModal onPress={() => {
                this.setModalFinishVisible(!this.state.modalFinishVisible);
              }}>
                <Icon name="times" size={40} color={Colors.primary} />
              </ButtonCloseModal>
            </ModalHeader>

            <ScrollView>
              { 
                (!this.navigate.state.params.isChallenge) && (
                  <>
                    <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                      <Icon name="award" size={50} color={Colors.yellow} />
                      <ImageLogo source={require('../../assets/logo.png')}  />
                    </View>
                    <View style={{ flexDirection: 'row', marginTop: 30, marginBottom: 10 }}>
                      <Icon name="check" size={30} color={Colors.green} />
                      <LabelFinishItems>{hits} { (hits !== 1) ? 'acertos' : 'acerto' }</LabelFinishItems>
                    </View>
                    <View style={{ flexDirection: 'row', marginBottom: 10 }}>
                      <Icon name="times" size={43} color={Colors.red} />
                      <LabelFinishItems style={{ marginTop: 0}}>{fails} { (fails !== 1) ? 'erros' : 'erro' }</LabelFinishItems>
                    </View>
                    <View style={{ flexDirection: 'row', marginBottom: 10 }}>
                      <Icon name="chart-line" size={30} color={Colors.secondary} />
                      <LabelFinishItems>+ {points} { (points !== 1) ? 'pontos' : 'ponto' }</LabelFinishItems>
                    </View>
                    

                    {
                      hits === questions.length && (
                        <View style={{ flexDirection: 'row', marginBottom: 10 }}>
                          <Icon name="coins" size={30} color={Colors.yellow} />
                          <LabelFinishItems>+ 0.1 KizoTokens</LabelFinishItems>
                        </View>
                      )
                      
                    }

                    <View style={{ flexDirection: 'row', marginBottom: 10 }}>
                      <LabelFinishItems style={{ fontSize: 14 }}>Para evitar trapaças, seus pontos serão computados apenas após assistir o anúncio a seguir.</LabelFinishItems>
                    </View>

                    
                  </>
                )
              }
              
              {
                (this.navigate.state.params.isChallenge && this.navigate.state.params.isToFinish) && (
                  <>
                    <View style={{ flexDirection: 'row', marginBottom: 10 }}>
                      {
                        !this.props.user.image ? (
                          <ImageIconUser source={Images['1']} />
                        ) : (
                          <ImageIconUser source={{uri: this.props.user.image}} />
                        )
                      }
                      <LabelFinishItems>Você fez {points} { (points !== 1) ? 'pontos' : 'ponto' }</LabelFinishItems>
                    </View>
                    <View style={{ flexDirection: 'row', marginBottom: 10 }}>
                      {
                        !this.navigate.state.params.image ? (
                          <ImageIconUser source={Images['1']} />
                        ) : (
                          <ImageIconUser source={{uri: this.navigate.state.params.image}} />
                        )
                      }
                      <LabelFinishItems>Oponente fez {this.navigate.state.params.pointsUser} { (this.navigate.state.params.pointsUser !== 1) ? 'pontos' : 'ponto' }</LabelFinishItems>
                    </View>
                    <View style={{ justifyContent: 'center', alignItems: 'center', flexDirection: 'row', marginBottom: 10 }}>
                      <LabelFinishItems style={{ textAlign: 'center'}}>{ (points > this.navigate.state.params.pointsUser) ? 'Você ganhou!': '' }</LabelFinishItems>
                      <LabelFinishItems style={{ textAlign: 'center'}}>{ (points < this.navigate.state.params.pointsUser) ? 'Você perdeu :(': '' }</LabelFinishItems>
                      <LabelFinishItems style={{ textAlign: 'center'}}>{ (points == this.navigate.state.params.pointsUser) ? 'Vocês empataram!': '' }</LabelFinishItems>
                    </View>

                    {
                      (points > this.navigate.state.params.pointsUser) && (
                        <Fieldset>
                          <LabelForm style={{color: Colors.primary}}>Escreva uma mensagem para o perdedor:</LabelForm>
                          <InputName
                            ref={ref => (this.refInputMessage = ref)}
                            value={this.state.messageToUser}
                            onChangeText={(text) => this.setState({messageToUser: text})}
                            placeholder={'Hahahaha...'}
                            placeholderTextColor={'#566b89'}
                            maxLength={60}>
                          </InputName>
                        </Fieldset>
                      )
                    }
                    
                  </>
                )
              }

{
                (this.navigate.state.params.isChallenge && !this.navigate.state.params.isToFinish) && (
                  <>
                    <View style={{ flexDirection: 'row', marginBottom: 10 }}>
                      {
                        !this.props.user.image ? (
                          <ImageIconUser source={Images['1']} />
                        ) : (
                          <ImageIconUser source={{uri: this.props.user.image}} />
                        )
                      }
                      <LabelFinishItems>Você fez {points} { (points !== 1) ? 'pontos' : 'ponto' }</LabelFinishItems>
                    </View>
                    <View style={{ flexDirection: 'row', marginBottom: 10 }}>
                      <LabelFinishItems>Aguarde o oponente jogar</LabelFinishItems>
                    </View>
                  </>
                )
              }
              

              {
                this.navigate.state.params.tournamentType === 'openLeague' && (
                  <>
                    <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                      <LabelFinishItems style={{ marginTop: 15, fontSize: 20 }}>Classificação parcial</LabelFinishItems>
                    </View>
                    <Row>
                      <LabelPosition style={{ top: 1 }}>Pos.</LabelPosition>
                      <SmallTitle style={{ top: 1 }}>Usuário</SmallTitle>
                      <LabelPoints style={{ top: 1 }}>Pontos</LabelPoints>
                    </Row>
                    <FlatList
                      data={usersFromLeague}
                      renderItem={({ item, index }) => (
                        <ListContainer style={{ backgroundColor: this.getBgColor(index) }}>
                          <Row>
                            <LabelPosition style={{ color: this.getLabelColor(index) }}>{ index + 1 }º</LabelPosition>
                            {
                              !item.idUser.image ? (
                                <ImageIconUser source={Images['1']} />
                              ) : (
                                <ImageIconUser source={{uri: item.idUser.image}} />
                              )
                            }
                            <SmallTitle>{ item.idUser.name }</SmallTitle>
                            <LabelPoints>{ item.userPoints }</LabelPoints>
                          </Row>
                        </ListContainer>
                      )}
                    />

                    <View style={{ marginBottom: 200 }} />
                  </>
                )
              }
              
            </ScrollView>
            
          
          </ModalWrapper>
        </ModalFinish>

        <ContainerBanner>
          <AdMobBanner
            adSize="fullBanner"
            adUnitID={this.bottomBannerId}
            height={90}
          />
        </ContainerBanner>

        <Animated.View 
          style={{ 
            transform: [{ translateY: this.animatedValue }], 
            height: 60, 
            marginRight: 15,
            marginLeft: 15,
            borderRadius: 8,
            backgroundColor: Colors.lightGrey, 
            position: 'absolute',
            left: 0, 
            top: 0, 
            right: 0, 
            justifyContent: 'center',
          }}>
          <KizoText style={{ color: Colors.primary, marginLeft: 15, fontSize: 20 }}>Você ganhou</KizoText>
          <KizoText style={{ color: Colors.primary, marginLeft: 15, fontSize: 18 }}>{ this.state.currentRewardTitle }</KizoText>

          <View style={{ flexDirection: 'row', position: 'absolute', right: 15, top: 18  }}>
            <KizoText style={{ color: Colors.primary, fontSize: 22 }}>+{formatKTs(earnedKTs)}</KizoText>
            <Icon name="coins" size={20} color={Colors.yellow} />
          </View>
        </Animated.View>
       </>
    )
  }
}

const mapStateToProps = state => ({
  addListRequest: state.userInfo.addListRequest,
  updateListsTitleRequest: state.userInfo.updateListsTitleRequest,
  updatePlayCount: state.userInfo.updatePlayCount,
  playCount: state.userInfo.playCount,
  lists: state.userInfo.lists,
  user: state.userInfo.user,
  auth: state.userInfo.auth,
  activeSound: state.userInfo.activeSound,
  showRatingModal: state.userInfo.showRatingModal,
  updateAlreadyRated: state.userInfo.updateAlreadyRated,
  alreadyRated: state.userInfo.alreadyRated,
  language: state.userInfo.user.language,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(UserInfoActions, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Question);