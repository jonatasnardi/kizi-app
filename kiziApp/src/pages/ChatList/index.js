import React, {Component} from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Icon from 'react-native-vector-icons/FontAwesome5';

import { Creators as UserInfoActions } from '../../store/ducks/userInfo';

import {
  AdMobInterstitial,
} from 'react-native-admob';

import { 
  Wrapper,
  WrapperInner,
  ListContainer,
  Row,
  SmallTitle,
  LabelPoints,
  LabelPosition,
  ImageIconUser,
  ContainerBanner,
} from './styles';
import { numberToReal, getFirstWord, showAlert, showToast, cutString, getRandomNumber } from '../../utils/functions';
import I18n from '../../utils/translation/i18n';
import ButtonPrimary from '../../components/buttons/ButtonPrimary';
import FullLoader from '../../components/FullLoader';
import Colors from '../../utils/colors';
import { Alert, View, ScrollView, FlatList, Platform, TouchableOpacity, StatusBar, BackHandler } from 'react-native';
import { requestPayment } from '../../services/payments';
import ButtonSecondary from '../../components/buttons/ButtonSecondary';
import { getUserRankingList, getGeneralRanking, getRankingWinnersWeekly } from '../../services/leagues';
import Images from '../../utils/images';
import { getFollowers, getFollowing } from '../../services/follower';
import { getChats } from '../../services/chat';

class ChatList extends Component {
  state = {
    users: [],
    isLoadingAdd: false,
    seeGeneralRanking: true,
    rankingSemanalData: [],
    isFollowers: true,
    isFirst: true,
  }

  interstitialBanner = Platform.select({
    android: 'ca-app-pub-4982456847713514/6663932035',
    ios: 'ca-app-pub-4982456847713514/5427065509',
  });

  navigate;

  refInputSuggestion;

  constructor(props) {
    super(props);

    this.navigate = this.props.navigation;


  }

  componentDidMount() {
    this.loadItems();

    BackHandler.addEventListener('hardwareBackPress', function() {
      return false;
    });

    this.setState({
      isFirst: false,
    })

    AdMobInterstitial.addEventListener("adClosed", () => {
      StatusBar.setHidden(false);
    });
    
    AdMobInterstitial.addEventListener("adFailedToLoad", () => {
      StatusBar.setHidden(false);
    });

    this.willFocusSubscription = this.props.navigation.addListener(
      'willFocus',
      () => {
        const randomNumber = getRandomNumber(1, 10);

        if (!this.state.isFirst && randomNumber >= 4) {
          this.setState({
            isLoadingAdd: true,
          })
          
          // StatusBar.setHidden(true);
          AdMobInterstitial.setAdUnitID(this.interstitialBanner);
          AdMobInterstitial.requestAd().then(() => {
            AdMobInterstitial.showAd();
            BackHandler.addEventListener('hardwareBackPress', function() {
              this.goBack();
              return true;
            });
            this.setState({
              isLoadingAdd: false,
            })
          });
        } else {
          StatusBar.setHidden(false);
        }
        
      }
    );

    AdMobInterstitial.addEventListener("adClosed", () => {
      StatusBar.setHidden(false);
    });
    
    AdMobInterstitial.addEventListener("adFailedToLoad", () => {
      StatusBar.setHidden(false);
    });
    
  }

  componentWillUnmount() {
    this.willFocusSubscription.remove();
  }

  loadItems = () => {
    const { user } = this.props;
    
    this.setState({
      isLoadingAdd: true,
    });

    getChats(user._id)
      .then((res) => { 
        this.setState({
          users: res.chatLists,
          isLoadingAdd: false,
        });
      })
      .catch((error) => {
        console.log(error);
        showToast(I18n.t('errorGenericMessage'));
        this.setState({
          isLoadingAdd: false,
        });
      });
  }

  setItemRead = (id) => {
    let index = this.state.users.findIndex(x => x._id === id);

    let newUser = this.state.users;

    newUser[index].read = true;
    
    this.setState({
      users: newUser,
    })
  }

  render() {
    const { users, seeGeneralRanking, rankingSemanalData } = this.state;

    const listToLoad = users;

    return (
      <>
        <Wrapper>
          <ScrollView>
            <WrapperInner>
{/* 
              <Row style={{ justifyContent: 'center', marginTop: 0 }}>
                <LabelPosition style={{ color: Colors.secondary, fontSize: 22, marginBottom: 15 }}>{ 'Lista de conversas' }</LabelPosition>
              </Row> */}
              <ButtonPrimary
                onPress={() => this.navigate.replace('Main')}
                title={'Voltar'}
                backgroundColor={Colors.secondary}
                color={Colors.primary}>
              </ButtonPrimary>

              {
                (users.length > 0) ? (
                  <>
                    <Row style={{ marginTop: 10,}}>
                      <SmallTitle style={{ top: 1 }}>Usuário</SmallTitle>
                    </Row>
                    <FlatList
                      data={listToLoad}
                      renderItem={({ item, index }) => {
                      return (
                        <TouchableOpacity onPress={() => {
                          if (!this.read) {
                            this.props.updateChatsCount(this.props.chatsCount - 1);
                          }
                          this.setItemRead(item._id);
                          this.navigate.push('Chat', {
                            userContacted: item.with._id,
                            userName: item.with.name,
                          })
                        }}>
                          <ListContainer>
                            <Row>
                              {
                                !item.with.image ? (
                                  <ImageIconUser source={Images['1']} />
                                ) : (
                                  <ImageIconUser source={{uri: item.with.image}} />
                                )
                              }
                              <SmallTitle style={{ fontWeight: item.read ? 'normal' : 'bold'}}>{ cutString(item.with.name) }</SmallTitle>
                              <View style={{ position: 'absolute', right: 15, top: 12 }}>
                                <Icon name="comment" size={20} color={Colors.secondary} />
                              </View>
                            </Row>
                          </ListContainer>
                        </TouchableOpacity>
                      )}}
                    />
                  </>
                ) : (
                  <SmallTitle style={{ textAlign: 'center' }}>Nenhuma conversa</SmallTitle>
                )
              }
              
              <View style={{ marginBottom: 200 }} />
            </WrapperInner>
          </ScrollView>
          
        </Wrapper>

        {
          this.state.isLoadingAdd && (
            <FullLoader></FullLoader>
          )
        }
       </>
    )
  }
}

const mapStateToProps = state => ({
  addListRequest: state.userInfo.addListRequest,
  updateListsTitleRequest: state.userInfo.updateListsTitleRequest,
  updateChatsCount: state.userInfo.updateChatsCount,
  chatsCount: state.userInfo.chatsCount,
  lists: state.userInfo.lists,
  user: state.userInfo.user,
  language: state.userInfo.user.language,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(UserInfoActions, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(ChatList);