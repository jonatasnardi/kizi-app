import React, {Component} from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { Creators as UserInfoActions } from '../../store/ducks/userInfo';
import I18n from '../../utils/translation/i18n';


import { 
  Wrapper,
  LabelForm,
  Fieldset,
} from './styles';
import Colors from '../../utils/colors';
import { Platform } from 'react-native';
class Terms extends Component {
  companyName = Platform.select({
    android: 'Google',
    ios: 'Apple',
  }); 

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    
  }

  render() {

    return (
      <>
        <Wrapper>
          
          <Fieldset>
            <LabelForm>Política de privacidade e termos de uso:</LabelForm>
          </Fieldset>

          <Fieldset>
          <LabelForm>{I18n.t('lineTerms1')}</LabelForm>
          <LabelForm>{I18n.t('lineTerms2')}</LabelForm>
          <LabelForm>{I18n.t('lineTerms3')}</LabelForm>
          <LabelForm>{I18n.t('lineTerms4')}</LabelForm>
          <LabelForm>{I18n.t('lineTerms5')}</LabelForm>
          <LabelForm>{I18n.t('lineTerms6')}</LabelForm>
          <LabelForm>{I18n.t('lineTerms7')}</LabelForm>
          <LabelForm>{I18n.t('lineTerms8')}</LabelForm>
          <LabelForm>{I18n.t('lineTerms9')}</LabelForm>
          <LabelForm>{I18n.t('lineTerms10')}</LabelForm>
          <LabelForm>{I18n.t('lineTerms11')}</LabelForm>
          <LabelForm>{I18n.t('lineTerms12')}</LabelForm>
          <LabelForm>{I18n.t('lineTerms13')}</LabelForm>
          <LabelForm style={{marginBottom: 200}}>A {this.companyName} não está envolvida em qualquer tipo de pagamento ou premiação, todo pagamento será realizado por conta da empresa Atomik e Kizo.</LabelForm>
          </Fieldset>
    
        </Wrapper>

       </>
    )
  }
}

const mapStateToProps = state => ({
  addListRequest: state.userInfo.addListRequest,
  updateListsTitleRequest: state.userInfo.updateListsTitleRequest,
  lists: state.userInfo.lists,
  user: state.userInfo.user,
  language: state.userInfo.user.language,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(UserInfoActions, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Terms);