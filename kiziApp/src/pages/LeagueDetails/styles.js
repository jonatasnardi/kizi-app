import styled from 'styled-components/native';
import Colors from '../../utils/colors';
import { Type } from '../../utils/layout';
import { isIphoneX } from '../../utils/functions';


export const Wrapper = styled.View`
  ${Type.wrapper};
  padding: 0;
  flex: 1;
  background-color: ${Colors.primary};
`;

export const WrapperInner = styled.View`
  ${Type.wrapper};
  flex: 1;
`;

export const Fieldset = styled.View`
  flex-direction: column;
  margin-bottom: 20px;
`;

export const LabelForm = styled.Text`
  ${Type.body};
  color: ${Colors.white};
  font-size: 20px;
  margin-top: 10px;
  margin-bottom: 10px;
  text-align: center;
`;

export const Row = styled.View`
  width: 100%;
  flex-direction: row;
`;

export const KizoText = styled.Text`
  ${Type.body};
  font-size: 17;
  color: ${Colors.white};
  margin-right: 5px;
`;

export const KizoTextFeatured = styled.Text`
  ${Type.body};
  font-size: 15;
  color: ${Colors.yellow};
  margin-right: 5px;
  font-weight: bold;
`;

export const LabelPosition = styled.Text`
  ${Type.body};
  color: ${Colors.secondary};
  font-size: 20px;
  text-align: center;
  position: relative;
  top: 11px;
`;

export const LabelFinishItems = styled.Text`
  ${Type.body};
  color: ${Colors.primary};
  font-size: 30px;
  margin-top: -2px;
  margin-left: 5px;
  text-align: left;
`;

export const SmallTitle = styled.Text`
  ${Type.body};
  color: ${Colors.black};
  font-size: 18px;
  margin-left: 10px;
  position: relative;
  top: 11px;
`;

export const LabelPoints = styled.Text`
  ${Type.body};
  color: ${Colors.primary};
  font-size: 20px;
  position: absolute;
  right: 10px;
  top: 10px;
`;

export const ImageIconUser = styled.Image`
  width: 45px;
  height: 45px;
  border-radius: 45px;
  margin-left: 4px;
  border: 1px solid ${Colors.secondary};
`;

export const ListContainer = styled.View`
  margin-bottom: 0;
  background: ${Colors.white};
  padding: 10px;
  margin: 5px 0;
  border-width: 1px;
  border-color: ${Colors.primary};
  border-radius: 5px;
  min-height: 30px;
  flex-direction: column;
`;

export const ContainerBanner = styled.View`
  width: 100%;
  height: 60px;
  position: absolute;
  bottom: ${isIphoneX() ? 22 : 0};
  left: 0;
`;