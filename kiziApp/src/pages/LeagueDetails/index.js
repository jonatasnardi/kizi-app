import React, {Component} from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Icon from 'react-native-vector-icons/FontAwesome5';
import DeviceInfo from 'react-native-device-info';

import {
  AdMobBanner,
} from 'react-native-admob';

import { Creators as UserInfoActions } from '../../store/ducks/userInfo';

import { 
  Wrapper,
  WrapperInner,
  Row,
  LabelPosition,
  LabelForm,
  LabelFinishItems,
  SmallTitle,
  LabelPoints,
  ImageIconUser,
  ListContainer,
  KizoText,
  KizoTextFeatured,
  ContainerBanner,
} from './styles';
import { numberToReal, getFirstWord, showAlert, showToast, cutString } from '../../utils/functions';
import I18n from '../../utils/translation/i18n';
import ButtonPrimary from '../../components/buttons/ButtonPrimary';
import FullLoader from '../../components/FullLoader';
import Colors from '../../utils/colors';
import { Alert, View, ScrollView, FlatList, TouchableOpacity, Platform } from 'react-native';
import { getLeague, listUsersOfLeague, vinculeUserLeague } from '../../services/leagues';
import Images from '../../utils/images';

class LeagueDetails extends Component {
  state = {
    currentLeague: {
      _id: '',
    },
    isLoadingAdd: false,
    leagueUsers: [],
    userLeaguesWithMock: [],
    currentUserAlreadyPlayed: true,
  }

  price225 = Platform.select({
    android: 'R$ 3,97',
    ios: 'R$ 3,90',
  });

  navigate;

  bottomBannerId = Platform.select({
    android: 'ca-app-pub-4982456847713514/5350850363',
    ios: 'ca-app-pub-4982456847713514/5542422057',
  }); 

  refInputSuggestion;

  constructor(props) {
    super(props);

    this.navigate = this.props.navigation;
  }

  componentDidMount() {
    this.loadLeagueDetails(this.navigate.state.params.idLeague);
  }

  loadLeagueDetails = (id) => {
    this.setState({
      isLoadingAdd: true,
    });
    getLeague(id)
      .then((res) => { 
        this.setState({
          currentLeague: res.league,
        }, () => {
          listUsersOfLeague(id)
            .then((data) => { 
              let { currentUserAlreadyPlayed, currentLeague } = this.state;

              let userLeaguesWithMock = [];
              
              const raw = data.userLeague;
              this.setState({
                leagueUsers: raw,
              });

              const { user } = this.props;

              const loggedUserInLeague = data.userLeague.find(item => {
                return item.idUser._id === user._id;
              });

              if (!loggedUserInLeague) {
                currentUserAlreadyPlayed = false;
              }

              for (let i = 0; i < data.userLeague.length; i++) {
                userLeaguesWithMock.push(data.userLeague[i]);
              }
              

              while (userLeaguesWithMock.length < currentLeague.userQtd) {
                userLeaguesWithMock.push({
                  "userPoints": 0,
                  "idUser": {
                    "avatar": "1",
                    "name": "Futuro oponente"
                  },
                })
              }

              

              this.setState({
                isLoadingAdd: false,
                userLeaguesWithMock: userLeaguesWithMock,
                currentUserAlreadyPlayed,
              });
            })
            .catch((error) => {
              showToast(I18n.t('errorGenericMessage'));
            }); 
        });

        
      })
      .catch((error) => {
        console.log(error);
        this.setState({
          isLoadingAdd: false,
        });
        showToast(I18n.t('errorGenericMessage'));
      });    
  }

  getBgColor = (index) => {
    switch (index) {
      case 0:
        return '#f8bc44';
        break;

      default:
        break;
    }
  }

  getLabelColor = (index) => {
    if (index === 0) {
      return Colors.primary;
    } else {
      return Colors.secondary;
    }
  }

  vinculeAndPlay = () => {
    const { currentLeague } = this.state;
    const { user } = this.props;
    const deviceId = DeviceInfo.getUniqueId();

    if (user.kizoTokens < currentLeague.priceToJoin && currentLeague.priceToJoin != 0) {
      return Alert.alert(
        'KizoTokens insuficientes',
        'Você não tem KizoTokens suficientes para jogar este torneio.',
        [
          {text: `Comprar mais por ${this.price225}`, onPress: () => {
            this.navigate.push('Pro');
          }},
          {text: 'Ok', onPress: () => {
          }},
        ],
        {cancelable: false},
      );
    }

    this.setState({
      isLoadingAdd: true,
    });
    vinculeUserLeague(this.navigate.state.params.idLeague, user._id, deviceId)
      .then((data) => { 
        this.navigate.push('Question', { 
          leagueId: currentLeague._id, 
          leagueUserQtd: currentLeague.userQtd, 
          tournamentType: 'openLeague' 
        })

        let userInfo = {
          ...user,
          kizoTokens: user.kizoTokens - currentLeague.priceToJoin,
        };
        
        this.props.updateUserInfo(userInfo);

        this.setState({
          isLoadingAdd: false,
        });
      })
      .catch((error) => {
        this.setState({
          isLoadingAdd: false,
        });
        showToast(error.data.message);
      }); 
  }

  render() {
    const { currentLeague, leagueUsers, currentUserAlreadyPlayed, userLeaguesWithMock } = this.state;

    return (
      <>
        <Wrapper>
          <ScrollView>
            <WrapperInner>
              <Row style={{ justifyContent: 'center' }}>
                <Icon name="trophy" size={40} color={'#f8bc44'} />
              </Row>

              <Row style={{ justifyContent: 'center', marginTop: 10 }}>
                <LabelPosition>{currentLeague.title}</LabelPosition>
              </Row>
              <Row style={{ marginTop: 10 }}>
                {
                  currentLeague.priceToJoin ? (
                    <>
                      <KizoTextFeatured>KTs para entrar: { currentLeague.priceToJoin }</KizoTextFeatured>
                      <Icon name="coins" size={20} color={Colors.yellow} />
                    </>
                  ) : (
                    <KizoTextFeatured style={{ color: Colors.green }}>Entrada gratuita</KizoTextFeatured>
                  )
                }
              </Row>
              <Row style={{ marginTop: 10 }}>
                <LabelForm style={{ color: Colors.secondary }}>Prêmio: { numberToReal(currentLeague.award)}</LabelForm>
              </Row>

              <Row>
                <LabelForm>Participantes: { leagueUsers.length }/{ currentLeague.userQtd }</LabelForm>
              </Row>
              {
                (!currentUserAlreadyPlayed && currentLeague.active) ? (
                  <View style={{ marginBottom: 20 }}>
                    <ButtonPrimary
                      onPress={() => this.vinculeAndPlay()}
                      icon={'gamepad'}
                      title={'Jogar agora'}>
                    </ButtonPrimary>
                  </View>
                ) : (
                  <Row style={{ justifyContent: 'center', marginTop: 10 }}>
                    <LabelPosition>Você já disputou este torneio</LabelPosition>
                  </Row>
                )
              }

              <Row style={{ justifyContent: 'center', marginTop: 10 }}>
                <LabelPosition style={{ color: Colors.yellow, fontSize: 17 }}>Caso haja empate, o vencedor será aquele que possui mais pontos no torneio do dia</LabelPosition>
              </Row>

              {
                (currentUserAlreadyPlayed || !currentLeague.active) && (
                  <>
                    <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                      <LabelFinishItems style={{ marginTop: 15, fontSize: 20, color: Colors.white }}>Classificação</LabelFinishItems>
                    </View>
                    <Row>
                      <LabelPosition style={{ top: 1 }}>Pos.</LabelPosition>
                      <SmallTitle style={{ color: Colors.white, top: 1 }}>Usuário</SmallTitle>
                      <LabelPoints style={{ color: Colors.white, top: 1 }}>Pontos</LabelPoints>
                    </Row>
                    
                    <FlatList
                      data={userLeaguesWithMock}
                      renderItem={({ item, index }) => (
                        <TouchableOpacity onPress={() => this.navigate.push('FriendProfile', {
                          idUser: item.idUser._id,
                        })}>
                          <ListContainer style={{ backgroundColor: this.getBgColor(index) }}>
                            <Row>
                              <LabelPosition style={{ color: this.getLabelColor(index) }}>{ index + 1 }º</LabelPosition>
                              {
                                !item.idUser.image ? (
                                  <ImageIconUser source={Images['1']} />
                                ) : (
                                  <ImageIconUser source={{uri: item.idUser.image}} />
                                )
                              }
                              
                              <SmallTitle style={{ color: (index === 0) ? Colors.primary : Colors.white }}>{ item.idUser.name }</SmallTitle>
                              <LabelPoints style={{ color: (index === 0) ? Colors.primary : Colors.white }}>{ item.userPoints }</LabelPoints>
                            </Row>
                          </ListContainer>
                        </TouchableOpacity>
                      )}
                    />

                    <View style={{ marginBottom: 200 }} />
                  </>
                )
              }
              
            </WrapperInner>
          </ScrollView>
          
        </Wrapper>

        {
          this.state.isLoadingAdd && (
            <FullLoader></FullLoader>
          )
        }

        <ContainerBanner>
          <AdMobBanner
            adSize="fullBanner"
            adUnitID={this.bottomBannerId}
            height={90}
          />
        </ContainerBanner>
       </>
    )
  }
}

const mapStateToProps = state => ({
  addListRequest: state.userInfo.addListRequest,
  updateListsTitleRequest: state.userInfo.updateListsTitleRequest,
  lists: state.userInfo.lists,
  user: state.userInfo.user,
  language: state.userInfo.user.language,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(UserInfoActions, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(LeagueDetails);