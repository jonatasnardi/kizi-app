import React, {Component} from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Icon from 'react-native-vector-icons/FontAwesome5';

import { Creators as UserInfoActions } from '../../store/ducks/userInfo';

import { 
  Wrapper,
  WrapperInner,
  ListContainer,
  Row,
  SmallTitle,
  LabelPoints,
  LabelPosition,
  ImageIconUser,
  ContainerBanner,
  Fieldset,
  InputName,
} from './styles';
import { numberToReal, getFirstWord, showAlert, showToast, isIntoMinutes, normalizarString, cutString } from '../../utils/functions';
import I18n from '../../utils/translation/i18n';
import ButtonPrimary from '../../components/buttons/ButtonPrimary';
import FullLoader from '../../components/FullLoader';
import Colors from '../../utils/colors';
import { Alert, View, ScrollView, FlatList, Platform, TouchableOpacity } from 'react-native';
import { requestPayment } from '../../services/payments';
import ButtonSecondary from '../../components/buttons/ButtonSecondary';
import { getUserRankingList, getGeneralRanking, getRankingWinnersWeekly } from '../../services/leagues';
import Images from '../../utils/images';
import { getFollowers, getFollowing } from '../../services/follower';
import { getUserMyQuestionNotAnswered, getUserQuestionBeMeNotAnswered, getUserQuestionBeMeAnswered } from '../../services/userQuestion';

class UserQuestionWaitingTheir extends Component {
  state = {
    userQuestions: [],
    isLoadingAdd: false,
  }

  navigate;

  refInputSuggestion;
  refInputSearch

  constructor(props) {
    super(props);

    this.navigate = this.props.navigation;
  }

  componentDidMount() {
    this.loadItems();
  }

  loadItems = () => {
    
    this.setState({
      isLoadingAdd: true,
    });

    getUserQuestionBeMeAnswered(this.props.user._id)
      .then((res) => { 
        this.setState({
          userQuestions: res.userQuestions,
          isLoadingAdd: false,
        });
      })
      .catch((error) => {
        console.log(error);
        showToast(I18n.t('errorGenericMessage'));
        this.setState({
          isLoadingAdd: false,
        });
        });
  }
  render() {
    const { userQuestions } = this.state;
    const { user } = this.props;

    let listToLoad = userQuestions;

    return (
      <>
        <Wrapper>
          <ScrollView>
            <WrapperInner>

              <Row style={{ justifyContent: 'center', marginTop: -15 }}>
                <LabelPosition style={{ color: Colors.secondary, fontSize: 22, marginBottom: 15 }}>Perguntas respondidas</LabelPosition>
              </Row>

              {
                (userQuestions.length > 0) ? (
                  <>
                    <FlatList
                      data={listToLoad}
                      renderItem={({ item, index }) => {
                        let baseItem = item.to;
                      
                        if (baseItem) {
                          return (
                            <TouchableOpacity onPress={() => this.navigate.push('QuestionOpen', {
                              idQuestion: item._id,
                              answered: item.answered,
                            })}>
                              <ListContainer>
                                <Row>
                                  {
                                    (!baseItem.image || item.anonymous) ? (
                                      <ImageIconUser source={Images['1']} />
                                    ) : (
                                      <ImageIconUser source={{uri: baseItem.image}} />
                                    )
                                  }
                                  <SmallTitle style={{ position: 'absolute', top: -6, left: 49, color: Colors.secondary, fontSize: 14 }}>{ baseItem.name }</SmallTitle>
                                  <SmallTitle>{ cutString(item.question, 18) }</SmallTitle>
                                  <View style={{ position: 'absolute', right: 15, top: 12 }}>
                                    <Icon name="chevron-right" size={20} color={Colors.secondary} />
                                  </View>
                                </Row>
                              </ListContainer>
                            </TouchableOpacity>
                          )
                        } else {
                          return null
                        }
                      }
                      }
                      
                    />
                  </>
                ) : (
                  <>
                    <SmallTitle style={{ textAlign: 'center', marginBottom: 10 }}>Nenhuma pergunta respondida</SmallTitle>
                  </>
                )
              }
              
              <View style={{ marginBottom: 200 }} />
            </WrapperInner>
          </ScrollView>
          
        </Wrapper>

        {
          this.state.isLoadingAdd && (
            <FullLoader></FullLoader>
          )
        }
       </>
    )
  }
}

const mapStateToProps = state => ({
  addListRequest: state.userInfo.addListRequest,
  updateListsTitleRequest: state.userInfo.updateListsTitleRequest,
  lists: state.userInfo.lists,
  user: state.userInfo.user,
  language: state.userInfo.user.language,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(UserInfoActions, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(UserQuestionWaitingTheir);