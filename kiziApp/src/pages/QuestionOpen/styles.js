import styled from 'styled-components/native';
import Colors from '../../utils/colors';
import { Type } from '../../utils/layout';


export const Wrapper = styled.ScrollView`
  ${Type.wrapper};
  flex: 1;
`;

export const Fieldset = styled.View`
  flex-direction: column;
  margin-bottom: 10px;
`;

export const LabelForm = styled.Text`
  ${Type.body};
  color: ${Colors.secondary};
  font-size: 18px;
`;

export const LabelValue = styled.Text`
  ${Type.body};
  color: ${Colors.primary};
  font-size: 20px;
`;

export const ImageUser = styled.Image`
  width: 75px;
  height: 75px;
  border-radius: 75px;
  border: 2px solid ${Colors.secondary};
`;

export const ModalAvatar = styled.Modal`
  ${Type.wrapper};
`;

export const ModalWrapper = styled.View`
  ${Type.wrapper};
`;

export const ModalHeader = styled.View`
  margin-top: 20px;
  flex-direction: row;
  justify-content: flex-end;
  align-items: center;
  margin-bottom: 30px;
  padding-bottom: 10px;
`;

export const ButtonCloseModal = styled.TouchableOpacity`

`;

export const ModalTitle = styled.Text`
  ${Type.body};
  color: ${Colors.primary};
  flex: 1;
  font-size: 22px;
`;

export const ContainerButtonsAvatar = styled.View`
  width: 100%;
  flex-direction: row;
  flex-wrap: wrap;
`;

export const WrapperButtonAvatar = styled.View`
  width: 50%;
  margin-bottom: 10px;
  text-align: center;
  flex-direction: column;
  align-content: center;
`;

export const ButtonQuickAccess = styled.TouchableOpacity`
  ${Type.buttonStyle};
  padding: 12px 10px;
  position: relative;
`;

export const ContainerButtonsHome = styled.View`
  flex-direction: row;
  flex-wrap: wrap;
`;

export const WrapperButtonHome = styled.View`
  width: 50%;
  margin-bottom: 10px;
`;

export const ButtonHomeText = styled.Text`
  ${Type.body};
  font-family: 'Futura-Medium';
  font-weight: bold;
  color: ${Colors.primary};
  font-size: 14px;
  text-align: center;
  margin-top: 10px;
`;

export const ButtonHomeText2 = styled.Text`
  ${Type.body};
  color: ${Colors.secondary};
  font-size: 20px;
  text-align: center;
  margin-top: 10px;
`;

export const ContainerFollowers = styled.TouchableOpacity`
  ${Type.buttonStyle};
  padding: 1px 10px;
  position: relative;
  margin-top: -30px;
`;

export const ButtonHomeTextFollowers = styled.Text`
  ${Type.body};
  font-weight: bold;
  color: ${Colors.secondary};
  font-size: 24px;
  text-align: center;
  margin-top: 0;
  margin-bottom: 0;
`;

export const Row = styled.View`
  width: 100%;
  flex-direction: row;
`;

export const SmallTitle = styled.Text`
  ${Type.body};
  color: ${Colors.black};
  font-size: 18px;
  margin-left: 0;
  position: relative;
  top: 11px;
`;

export const ListContainer = styled.View`
  margin-bottom: 0;
  background: ${Colors.white};
  padding: 10px 0 ;
  margin: 5px 0;
  border-radius: 5px;
  min-height: 30px;
  flex-direction: column;
`;

export const LabelPosition = styled.Text`
  ${Type.body};
  color: ${Colors.secondary};
  font-size: 18px;
  text-align: center;
  position: relative;
  top: 11px;
`;

export const ImageIconUser = styled.Image`
  width: 45px;
  height: 45px;
  border-radius: 45px;
  margin-left: 4px;
  border: 1px solid ${Colors.secondary};
`;