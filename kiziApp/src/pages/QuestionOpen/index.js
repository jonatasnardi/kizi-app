import React, {Component} from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ImagePicker from 'react-native-image-crop-picker';

import { Creators as UserInfoActions } from '../../store/ducks/userInfo';

import I18n from '../../utils/translation/i18n';

import Icon from 'react-native-vector-icons/FontAwesome5';

import { 
  Wrapper,
  ImageUser,
  LabelForm,
  Fieldset,
  LabelValue,
  ModalAvatar,
  ModalWrapper,
  ModalTitle,
  ModalHeader,
  ButtonCloseModal,
  ContainerButtonsAvatar,
  WrapperButtonAvatar,
  ButtonQuickAccess,
  ContainerButtonsHome,
  WrapperButtonHome,
  ButtonHomeText,
  ContainerFollowers,
  ButtonHomeTextFollowers,
  ButtonHomeText2,
  Row,
  SmallTitle,
  ListContainer,
  LabelPosition,
  ImageIconUser,
} from './styles';
import Colors from '../../utils/colors';
import { showAlert, showToast, calculatePercentage, isIntoMinutes, cutString } from '../../utils/functions';
import ButtonPrimary from '../../components/buttons/ButtonPrimary';
import FullLoader from '../../components/FullLoader';
import { userContact, updateAvatar, getUserInfo, updateImageProfile } from '../../services/user';
import Images from '../../utils/images';
import { Alert, View, TouchableOpacity, FlatList, ScrollView, Platform } from 'react-native';
import { getUserQuestionUser, getUserQuestionById, deleteUserQuestion } from '../../services/userQuestion';

class QuestionOpen extends Component {
  state = {
    question: {},
    isLoadingAdd: false,
    modalAvatarVisible: false,
    followsCount: 0,
    followersCount: 0,
  };

  constructor(props) {
    super(props);

    this.navigate = this.props.navigation;
  }

  componentDidMount() {
    const { user } = this.props;

    this.setState({
      isLoadingAdd: true,
    });
    getUserQuestionById(this.navigate.state.params.idQuestion)
      .then((data) => {
        this.setState({
          question: data.userQuestion,
          isLoadingAdd: false,
        });
      })
      .catch((error) => {
        console.log(error);
        this.setState({
          isLoadingAdd: false,
        });
      });
  }

  deleteQuestion = () => {
    Alert.alert(
      'Tem certeza?',
      'Tem certeza que deseja apagar esta pergunta?',
      [
        {text: `Cancelar`, onPress: () => {}},
        {text: 'Sim, apagar', onPress: () => {
          deleteUserQuestion(this.navigate.state.params.idQuestion)
            .then((data) => {
              this.setState({
                isLoadingAdd: false,
              });

              showAlert('Pergunta deletada', 'Pergunta deletada com sucesso!');

              this.props.navigation.goBack(null);
              setTimeout(() => {
                this.props.navigation.goBack(null);  
              }, 400);
              
            })
            .catch((error) => {
              console.log(error);
              this.setState({
                isLoadingAdd: false,
              });
            });
        }},
      ],
      {cancelable: false},
    );


    
  }

  render() {
    const { question } = this.state;
    const { user } = this.props;

    let baseItem = question.from;

    return (
      <>
        <Wrapper>
         
          <Row style={{ justifyContent: 'center', marginTop: 0 }}>
            <LabelPosition style={{ color: Colors.secondary, fontSize: 22, marginBottom: 15 }}>Pergunta</LabelPosition>
          </Row>

          {
            baseItem && (
              <>
                <ListContainer style={{ marginBottom: 15 }}>
                  <Row style={{ flexDirection: 'column' }}>
                    <SmallTitle style={{ position: 'absolute', top: -6, left: 0, color: Colors.secondary, fontSize: 14 }}>{ cutString((question.anonymous ? 'Anônimo' : baseItem.name), 58) }</SmallTitle>
                    <SmallTitle style={{ fontWeight: 'bold' }}>Pergunta:</SmallTitle>
                    <SmallTitle>{ question.question }</SmallTitle>
                    <SmallTitle style={{ color: Colors.secondary, fontWeight: 'bold', marginTop: 20 }}>Resposta:</SmallTitle>
                    <SmallTitle style={{ marginBottom: 10 }}>{ question.answer ? question.answer : 'Ainda não foi respondido' }</SmallTitle>
                    
                    <View style={{ position: 'absolute', right: 0, top: 0 }}>
                      <TouchableOpacity onPress={() => question.anonymous ? null : this.navigate.push('FriendProfile', {
                        idUser: baseItem._id,
                      })}>
                        {
                          (!baseItem.image || question.anonymous) ? (
                            <ImageIconUser source={Images['1']} />
                          ) : (
                            <ImageIconUser source={{uri: baseItem.image}} />
                          )
                        }
                      </TouchableOpacity>
                    </View>
                    
                  </Row>
                </ListContainer>

                <ButtonPrimary
                  onPress={() => this.deleteQuestion()}
                  title={'Apagar pergunta'}
                  backgroundColor={Colors.red}
                  icon={'trash'}>
                </ButtonPrimary>
              </>
            )
          }
          
           
          
          <View style={{ marginBottom: 200 }} />
    
        </Wrapper>

        {
          this.state.isLoadingAdd && (
            <FullLoader></FullLoader>
          )
        }
        

       </>
    )
  }
}

const mapStateToProps = state => ({
  addListRequest: state.userInfo.addListRequest,
  updateListsTitleRequest: state.userInfo.updateListsTitleRequest,
  updateFollowersCount: state.userInfo.updateFollowersCount,
  updateFollowingCount: state.userInfo.updateFollowingCount,
  followersCount: state.userInfo.followersCount,
  followingCount: state.userInfo.followingCount,
  lists: state.userInfo.lists,
  user: state.userInfo.user,
  language: state.userInfo.user.language,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(UserInfoActions, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(QuestionOpen);