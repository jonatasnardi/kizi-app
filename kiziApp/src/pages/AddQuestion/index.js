import React, {Component} from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Icon from 'react-native-vector-icons/FontAwesome5';
import RadioGroup from 'react-native-radio-buttons-group';
import ImagePicker from 'react-native-image-picker';

import { Creators as UserInfoActions } from '../../store/ducks/userInfo';

import { 
  Wrapper,
  WrapperInner,
  LabelTitleTopic,
  Row,
  SmallTitle,
  LabelPoints,
  LabelPosition,
  Fieldset,
  LabelForm,
  InputName,
  ImageWithContent,
  ImageContainer,
  ImageOverlay,
  ImageAction,
  ImageBlock,
} from './styles';
import { numberToReal, getFirstWord, showAlert, showToast } from '../../utils/functions';
import I18n from '../../utils/translation/i18n';
import ButtonPrimary from '../../components/buttons/ButtonPrimary';
import FullLoader from '../../components/FullLoader';
import Colors from '../../utils/colors';
import { ScrollView, Alert, View, Switch } from 'react-native';
import { requestPayment } from '../../services/payments';
import ButtonSecondary from '../../components/buttons/ButtonSecondary';
import { getUserRankingList } from '../../services/leagues';
import Images from '../../utils/images';
import { addSuggestionQuestion } from '../../services/questions';

class AddQuestion extends Component {
  state = {
    question: {
      enunciation: '',
      optionA: '',
      optionB: '',
      optionC: '',
      optionD: '',
      correctOption: 'A',
      weight: 1,
      image: '',
    },
    switchValueHasImage: false,
    isLoadingAdd: false,
    isLoadingImage: false,
    alternatives: [
      {
          label: 'A',
          value: 0,
          color: Colors.secondary,
      },
      {
          label: 'B',
          value: 1,
          color: Colors.secondary,
      },
      {
          label: 'C',
          value: 2,
          color: Colors.secondary,
      },
      {
          label: 'D',
          value: 3,
          color: Colors.secondary,
      },
    ],
    weights: [
      {
          label: 'Fácil',
          value: 0,
          color: Colors.secondary,
      },
      {
          label: 'Médio',
          value: 1,
          color: Colors.secondary,
      },
      {
          label: 'Difícil',
          value: 2,
          color: Colors.secondary,
      },
    ],
  }

  refInputName;

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const { alternatives, weights } = this.state;
    
    alternatives[1].selected = false;
    alternatives[2].selected = false;
    alternatives[3].selected = false;
    alternatives[0].selected = true;

    weights[1].selected = false;
    weights[2].selected = false;
    weights[0].selected = true;
  }

  confirmAdd = () => {
    const { question } = this.state;
    const { user } = this.props;

    this.setState({
      isLoadingAdd: true,
    });

    addSuggestionQuestion(user._id, question)
      .then((res) => { 
        this.setState({
          isLoadingAdd: false,
        });

        this.toggleSwitchHasImage(false);

        showAlert('Pergunta adicionada', 'Pergunta adicionada para avaliação com sucesso! Aguarde a nossa avaliação :)');

        this.props.navigation.goBack(null);
      })
      .catch((error) => {
        console.log(error);
        this.setState({
          isLoadingAdd: false
        });
        showToast(I18n.t('errorGenericMessage'));
      }); 
  }

  selectPhoto() {
    const options = {
      title: I18n.t('selectImage'),
      customButtons: [],
      quality: 0.3,
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    this.setState({
      isLoadingImage: true,
    })
    
    /**
     * The first arg is the options object for customization (it can also be null or omitted for default options),
     * The second arg is the callback which sends object: response (more info in the API Reference)
     */
    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);
    
      if (response.didCancel) {
        console.log('User cancelled image picker');
        this.setState({
          isLoadingImage: false,
        })
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
        this.setState({
          isLoadingImage: false,
        })
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
        this.setState({
          isLoadingImage: false,
        })
      } else {
        // const source = { uri: response.uri };
    
        // You can also display the image using data:
        const source = 'data:image/jpeg;base64,' + response.data;

        console.log('b64', source);

        this.setState({
          question: {
            ...this.state.question,
            image: source,
          },
          isLoadingImage: false,
        });
      }
    });
  }

  toggleSwitchHasImage = (value) => {
    this.setState({
      switchValueHasImage: value,
      question: {
        ...this.state.question,
        image: '',
      },
    });
  }

  onPressRadio = data => {
    this.setState({ alternatives: data });

    setTimeout(() => {
      let selectedButton = this.state.alternatives.find(e => e.selected == true);
        selectedButton = selectedButton ? selectedButton.label : this.state.alternatives[0].label;

      this.setState({
        question: {
          ...this.state.question,
          correctOption: selectedButton,
        }
      });
    }, 800);
  };

  onPressRadioWeight = data => {
    this.setState({ weights: data });

    setTimeout(() => {
      let selectedButton = this.state.weights.find(e => e.selected == true);
        selectedButton = selectedButton ? selectedButton.value + 1 : this.state.weights[0].value + 1;

      this.setState({
        question: {
          ...this.state.question,
          weight: selectedButton,
        }
      });
    }, 800);
  };

  isFormValid = () => {
    const { question } = this.state;
    let valid = false;

    if (
      question.enunciation.length >= 10 &&
      question.optionA.length >= 1 &&
      question.optionB.length >= 1 &&
      question.optionC.length >= 1 &&
      question.optionD.length >= 1 &&
      question.correctOption.length >= 1
      ) {
        valid = true;
      }
    
    return valid;
  }

  render() {
    const { switchValueHasImage } = this.state;

    return (
      <>
        <Wrapper>
          <ScrollView>
            <WrapperInner>
              <View>
                <LabelTitleTopic>Colabore e tenha um dedo seu aqui no jogo! Adicione uma sugestão de pergunta abaixo! A sua sugestão passará por aprovação, e, caso aprovada, você ganhará 2 KTs (KizoTokens).</LabelTitleTopic>
              </View>

              <Fieldset>
                <LabelForm>Pergunta:</LabelForm>

                <InputName
                  ref={ref => (this.refInputName = ref)}
                  value={this.state.question.enunciation}
                  onChangeText={(text) => this.setState({question: { ...this.state.question, enunciation: text}})}
                  placeholder={'Ex: Qual a cor da grama?'}
                  placeholderTextColor={'#778aa5'}
                  multiline={true}>
                </InputName>
              </Fieldset>

              <Fieldset>
                <LabelForm>Precisa adicionar imagem?</LabelForm>
                <View style={{ justifyContent: 'center', alignItems: 'flex-start' }}>
                  <Switch
                    onValueChange={this.toggleSwitchHasImage}
                    value={switchValueHasImage}/>
                </View>
                
                { switchValueHasImage && (
                  <View style={{ flexDirection: 'row', marginTop: 10 }}>
                    <ImageAction onPress={() => this.selectPhoto()} style={{paddingRight: 7.5}}>
                      {
                        this.state.question.image.length > 0 && (
                          <ImageContainer>
                            <ImageWithContent source={{uri: this.state.question.image}}/>
                            <ImageOverlay />
                            <Icon name="camera" size={45} color={Colors.white} />
                          </ImageContainer>
                        )
                      }

                      {
                        this.state.question.image.length == 0 && (
                          <ImageBlock>
                            <Icon name="camera" size={45} color={Colors.white} />
                          </ImageBlock>
                        )
                      }
                    </ImageAction>
                  </View>
                )}
                
              </Fieldset>

              <Fieldset>
                <LabelForm>Alternativa A:</LabelForm>

                <InputName
                  ref={ref => (this.refInputName = ref)}
                  value={this.state.question.optionA}
                  onChangeText={(text) => this.setState({question: { ...this.state.question, optionA: text}})}
                  placeholder={'Ex: Azul'}
                  placeholderTextColor={'#778aa5'}>
                </InputName>
              </Fieldset>

              <Fieldset>
                <LabelForm>Alternativa B:</LabelForm>

                <InputName
                  ref={ref => (this.refInputName = ref)}
                  value={this.state.question.optionB}
                  onChangeText={(text) => this.setState({question: { ...this.state.question, optionB: text}})}
                  placeholder={'Ex: Verde'}
                  placeholderTextColor={'#778aa5'}>
                </InputName>
              </Fieldset>

              <Fieldset>
                <LabelForm>Alternativa C:</LabelForm>

                <InputName
                  ref={ref => (this.refInputName = ref)}
                  value={this.state.question.optionC}
                  onChangeText={(text) => this.setState({question: { ...this.state.question, optionC: text}})}
                  placeholder={'Ex: Vermelha'}
                  placeholderTextColor={'#778aa5'}>
                </InputName>
              </Fieldset>

              <Fieldset>
                <LabelForm>Alternativa D:</LabelForm>

                <InputName
                  ref={ref => (this.refInputName = ref)}
                  value={this.state.question.optionD}
                  onChangeText={(text) => this.setState({question: { ...this.state.question, optionD: text}})}
                  placeholder={'Ex: Amarela'}
                  placeholderTextColor={'#778aa5'}>
                </InputName>
              </Fieldset>

              <Fieldset>
                <LabelForm>Qual a alternativa correta?</LabelForm>
                <View style={{ alignItems: 'flex-start' }}>
                  <RadioGroup 
                    radioButtons={this.state.alternatives} 
                    onPress={this.onPressRadio} />
                </View>
                
              </Fieldset>

              <Fieldset>
                <LabelForm>Qual o nível de dificuldade?</LabelForm>
                <View style={{ alignItems: 'flex-start' }}>
                  <RadioGroup 
                    radioButtons={this.state.weights} 
                    onPress={this.onPressRadioWeight} />
                </View>
                
              </Fieldset>

              <ButtonPrimary
                onPress={() => this.confirmAdd()}
                title={'Finalizar'}
                disabled={!this.isFormValid()}
                backgroundColor={!(this.isFormValid()) ? Colors.lightSecondary : Colors.secondary}
                icon={'chevron-right'}>
              </ButtonPrimary>

              <View style={{ marginBottom: 200 }}/>
            </WrapperInner>
          </ScrollView>
          
        </Wrapper>

        {
          (this.state.isLoadingImage || this.state.isLoadingAdd) && (
            <FullLoader></FullLoader>
          )
        }
       </>
    )
  }
}

const mapStateToProps = state => ({
  addListRequest: state.userInfo.addListRequest,
  updateListsTitleRequest: state.userInfo.updateListsTitleRequest,
  lists: state.userInfo.lists,
  user: state.userInfo.user,
  language: state.userInfo.user.language,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(UserInfoActions, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(AddQuestion);