import React, {Component} from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Icon from 'react-native-vector-icons/FontAwesome5';
import RadioGroup from 'react-native-radio-buttons-group';
import ImagePicker from 'react-native-image-picker';
import {
  AdMobRewarded,
} from 'react-native-admob';

import { Creators as UserInfoActions } from '../../store/ducks/userInfo';

import { 
  Wrapper,
  WrapperInner,
  LabelTitleTopic,
  Row,
  SmallTitle,
  LabelPoints,
  LabelPosition,
  Fieldset,
  LabelForm,
  InputName,
  ImageWithContent,
  ImageContainer,
  ImageOverlay,
  ImageAction,
  ImageBlock,
} from './styles';
import { numberToReal, getFirstWord, showAlert, showToast, formatKTs, getRandomNumber } from '../../utils/functions';
import I18n from '../../utils/translation/i18n';
import ButtonPrimary from '../../components/buttons/ButtonPrimary';
import FullLoader from '../../components/FullLoader';
import Colors from '../../utils/colors';
import { ScrollView, FlatList, Alert, View, Switch, Platform } from 'react-native';
import { requestPayment } from '../../services/payments';
import ButtonSecondary from '../../components/buttons/ButtonSecondary';
import { getUserRankingList, createLeague } from '../../services/leagues';
import Images from '../../utils/images';
import { addSuggestionQuestion } from '../../services/questions';

class AddTournament extends Component {
  state = {
    league: {
      title: '',
      award: 0.0017,
      userQtd: 5,
      createdBy: '',
      priceToJoin: 0,
    },
    switchValueHasImage: false,
    isLoadingAdd: false,
    isLoadingImage: false,
    alternatives: [
      {
          label: '5 participantes',
          value: 5,
          color: Colors.secondary,
      },
      {
          label: '10 participantes',
          value: 10,
          color: Colors.secondary,
      },
    ],
    awards5: [
      {
        label: 'R$ 0,0017 (Entrada gratuita)',
        value: 0.0017,
        priceToJoin: 0,
        color: Colors.secondary,
      },
      {
        label: 'R$ 0,10 (Entrada 10 KizoTokens)',
        value: 0.10,
        priceToJoin: 10,
        color: Colors.secondary,
      },
      {
        label: 'R$ 0,18 (Entrada 18 KizoTokens)',
        value: 0.18,
        priceToJoin: 18,
        color: Colors.secondary,
      },
      // {
      //   label: 'R$ 1,00 (Entrada 38 KizoTokens)',
      //   value: 1.00,
      //   color: Colors.secondary,
      // },
      // {
      //   label: 'R$ 2,00 (Entrada 75 KizoTokens)',
      //   value: 2.00,
      //   color: Colors.secondary,
      // },
    ],
    awards10: [
      {
        label: 'R$ 0,003 (Entrada gratuita)',
        value: 0.003,
        priceToJoin: 0,
        color: Colors.secondary,
      },
      {
        label: 'R$ 0,10 (Entrada 5 KizoTokens)',
        value: 0.10,
        priceToJoin: 5,
        color: Colors.secondary,
      },
      {
        label: 'R$ 0,18 (Entrada 9 KizoTokens)',
        value: 0.18,
        priceToJoin: 9,
        color: Colors.secondary,
      },
      // {
      //   label: 'R$ 2,00',
      //   value: 2.00,
      //   color: Colors.secondary,
      // },
      // {
      //   label: 'R$ 1,00 (Entrada 38 KizoTokens)',
      //   value: 1.00,
      //   color: Colors.secondary,
      // },
    ],
    selectedNumberOfParticipants: 5,
  }

  price225 = Platform.select({
    android: 'R$ 3,97',
    ios: 'R$ 3,90',
  });


  rewardedBanner = Platform.select({
    android: 'ca-app-pub-4982456847713514/9290095372',
    ios: 'ca-app-pub-4982456847713514/4229340388',
  });

  navigate;

  refInputName;

  constructor(props) {
    super(props);

    this.navigate = this.props.navigation;
  }

  componentDidMount() {
    const { alternatives, awards5 } = this.state;
    const { user } = this.props;
    
    alternatives[1].selected = false;
    alternatives[0].selected = true;

    awards5[1].selected = false;
    awards5[2].selected = false;
    awards5[0].selected = true;

    this.setState({
      league: {
        ...this.state.league,
        createdBy: user._id,
      },
    });
  }

  openRewardedBanner = () => {
    
  }

  confirmAdd = () => {
    const { league } = this.state;
    const { user } = this.props;

    this.setState({
      isLoadingAdd: true,
    });

    if (user.kizoTokens >= league.priceToJoin) {
      const r = getRandomNumber(1, 10);
      if (league.priceToJoin <= 0.003 && r >= 7) {
        AdMobRewarded.setAdUnitID(this.rewardedBanner);
        AdMobRewarded.requestAd().then(() => AdMobRewarded.showAd()).catch(error => {
          createLeague(league)
            .then((res) => { 
              this.setState({
                isLoadingAdd: false,
              });

              let userInfo = {
                ...user,
                kizoTokens: user.kizoTokens - league.priceToJoin,
              };
              
              this.props.updateUserInfo(userInfo);

              Alert.alert(
                'Torneio criado',
                'Torneio criado com sucesso. A sua partida começará agora.',
                [
                  {text: 'Começar agora', onPress: () => {
                    this.navigate.push('Question', { 
                      leagueId: res.league._id, 
                      leagueUserQtd: res.league.userQtd, 
                      tournamentType: 'openLeague' 
                    });
                  }},
                ],
                {cancelable: false},
              );

              // this.props.navigation.goBack();
              // this.props.navigation.state.params.onGoBack()
            })
            .catch((error) => {
              this.setState({
                isLoadingAdd: false
              });
              showToast(I18n.t('errorGenericMessage'));
            }); 
        });

        AdMobRewarded.addEventListener('rewarded', reward => {
          createLeague(league)
            .then((res) => { 
              this.setState({
                isLoadingAdd: false,
              });

              AdMobRewarded.removeAllListeners();

              let userInfo = {
                ...user,
                kizoTokens: user.kizoTokens - league.priceToJoin,
              };
              
              this.props.updateUserInfo(userInfo);

              Alert.alert(
                'Torneio criado',
                'Torneio criado com sucesso. A sua partida começará agora.',
                [
                  {text: 'Começar agora', onPress: () => {
                    this.navigate.push('Question', { 
                      leagueId: res.league._id, 
                      leagueUserQtd: res.league.userQtd, 
                      tournamentType: 'openLeague' 
                    });
                  }},
                ],
                {cancelable: false},
              );

              // this.props.navigation.goBack();
              // this.props.navigation.state.params.onGoBack()
            })
            .catch((error) => {
              this.setState({
                isLoadingAdd: false
              });
              showToast(I18n.t('errorGenericMessage'));
            }); 
        });
      } else {
        createLeague(league)
            .then((res) => { 
              this.setState({
                isLoadingAdd: false,
              });

              let userInfo = {
                ...user,
                kizoTokens: user.kizoTokens - league.priceToJoin,
              };
              
              this.props.updateUserInfo(userInfo);

              Alert.alert(
                'Torneio criado',
                'Torneio criado com sucesso. A sua partida começará agora.',
                [
                  {text: 'Começar agora', onPress: () => {
                    this.navigate.push('Question', { 
                      leagueId: res.league._id, 
                      leagueUserQtd: res.league.userQtd, 
                      tournamentType: 'openLeague' 
                    });
                  }},
                ],
                {cancelable: false},
              );

              // this.props.navigation.goBack();
              // this.props.navigation.state.params.onGoBack()
            })
            .catch((error) => {
              this.setState({
                isLoadingAdd: false
              });
              showToast(I18n.t('errorGenericMessage'));
            }); 
      }
      
    } else {
      Alert.alert(
        'Não foi possível criar',
        'Você não possui KizoTokens suficientes para criar o torneio. Ou você cria um torneio gratuito ou então compre/ganhe mais KizoTokens.',
        [
          {text: `Comprar mais por ${this.price225}`, onPress: () => {
            this.navigate.push('Pro');
          }},
          {text: 'Ok', onPress: () => {
          }},
        ],
        {cancelable: false},
      );

      this.setState({
        isLoadingAdd: false
      });
    }

    
  }

  onPressRadio = data => {
    let { selectedNumberOfParticipants } = this.state;
    this.setState({ alternatives: data });

    setTimeout(() => {
      let selectedButton = this.state.alternatives.find(e => e.selected == true);
        selectedButton = selectedButton ? selectedButton.value : this.state.alternatives[0].value;

      selectedNumberOfParticipants = selectedButton;

      let selectedButtonKts, selectedButtonValue;
      if (selectedNumberOfParticipants === 5) {
        selectedButtonKts = this.state.awards5.find(e => e.selected == true);
        selectedButtonKts = selectedButtonKts ? selectedButtonKts.priceToJoin : this.state.awards5[0].priceToJoin;
        selectedButtonValue = this.state.awards5.find(e => e.selected == true);
        selectedButtonValue = selectedButtonValue ? selectedButtonValue.value : this.state.awards5[0].value;
      } else {
        selectedButtonKts = this.state.awards10.find(e => e.selected == true);
        selectedButtonKts = selectedButtonKts ? selectedButtonKts.priceToJoin : this.state.awards10[0].priceToJoin;
        selectedButtonValue = this.state.awards10.find(e => e.selected == true);
        selectedButtonValue = selectedButtonValue ? selectedButtonValue.value : this.state.awards10[0].value;
      }

      this.setState({
        league: {
          ...this.state.league,
          userQtd: selectedButton,
          award: selectedButtonValue,
          priceToJoin: selectedButtonKts,
        },
        selectedNumberOfParticipants,
      });
    }, 100);
  };

  onPressRadioWeight5 = data => {
    this.setState({ awards5: data });

    setTimeout(() => {
      let selectedButton = this.state.awards5.find(e => e.selected == true);
        selectedButton = selectedButton ? selectedButton.value : this.state.awards5[0].value;

      let selectedButtonKts = this.state.awards5.find(e => e.selected == true);
        selectedButtonKts = selectedButtonKts ? selectedButtonKts.priceToJoin : this.state.awards5[0].priceToJoin;

      this.setState({
        league: {
          ...this.state.league,
          award: selectedButton,
          priceToJoin: selectedButtonKts,
        },
      });
    }, 300);
  };

  onPressRadioWeight10 = data => {
    this.setState({ awards10: data });

    setTimeout(() => {
      let selectedButton = this.state.awards10.find(e => e.selected == true);
        selectedButton = selectedButton ? selectedButton.value : this.state.awards10[0].value;

      let selectedButtonKts = this.state.awards10.find(e => e.selected == true);
        selectedButtonKts = selectedButtonKts ? selectedButtonKts.priceToJoin : this.state.awards10[0].priceToJoin;

      this.setState({
        league: {
          ...this.state.league,
          award: selectedButton,
          priceToJoin: selectedButtonKts,
        },
      });
    }, 300);
  };

  isFormValid = () => {
    const { league } = this.state;
    let valid = false;

    if (
      league.title.length >= 2
      ) {
        valid = true;
      }
    
    return valid;
  }

  render() {
    const { switchValueHasImage, selectedNumberOfParticipants } = this.state;
    const { user } = this.props;

    return (
      <>
        <Wrapper>
          <ScrollView>
            <WrapperInner>
              <View>
                <LabelTitleTopic>Preencha os campos abaixo para criar o seu torneio.</LabelTitleTopic>
              </View>

              <Fieldset>
                <LabelForm>Nome do torneio:</LabelForm>

                <InputName
                  ref={ref => (this.refInputName = ref)}
                  value={this.state.league.title}
                  onChangeText={(text) => this.setState({league: { ...this.state.league, title: text}})}
                  placeholder={'Ex: Os gênios'}
                  maxLength={35}
                  placeholderTextColor={'#778aa5'}>
                </InputName>
              </Fieldset>

              

              <Fieldset>
                <LabelForm>Qual a quantidade de participantes?</LabelForm>
                <View style={{ alignItems: 'flex-start' }}>
                  <RadioGroup 
                    radioButtons={this.state.alternatives} 
                    onPress={this.onPressRadio} />
                </View>
                
              </Fieldset>

              <Fieldset>
                <View style={{ flexDirection: 'row' }}>
                  <LabelForm>Qual a premiação?</LabelForm>
                  <LabelForm style={{ marginLeft: 5, marginTop: 12, color: Colors.primary, fontSize: 17 }}>Você tem {formatKTs(user.kizoTokens)}</LabelForm>
                  <Icon style={{ marginLeft: 5, marginTop: 12 }} name="coins" size={20} color={Colors.yellow} />
                </View>
                {
                  (selectedNumberOfParticipants === 5) && (
                    <View style={{ alignItems: 'flex-start' }}>
                      <RadioGroup 
                        radioButtons={this.state.awards5} 
                        onPress={this.onPressRadioWeight5} />
                    </View>
                  )
                }

                {
                  (selectedNumberOfParticipants === 10) && (
                    <View style={{ alignItems: 'flex-start' }}>
                      <RadioGroup 
                        radioButtons={this.state.awards10} 
                        onPress={this.onPressRadioWeight10} />
                    </View>
                  )
                }
                
              </Fieldset>

              <ButtonPrimary
                onPress={() => this.confirmAdd()}
                title={'Finalizar'}
                disabled={!this.isFormValid()}
                backgroundColor={!(this.isFormValid()) ? Colors.lightSecondary : Colors.secondary}
                icon={'chevron-right'}>
              </ButtonPrimary>

              <View style={{ marginBottom: 200 }}/>
            </WrapperInner>
          </ScrollView>
          
        </Wrapper>

        {
          this.state.isLoadingAdd && (
            <FullLoader></FullLoader>
          )
        }
       </>
    )
  }
}

const mapStateToProps = state => ({
  addListRequest: state.userInfo.addListRequest,
  updateListsTitleRequest: state.userInfo.updateListsTitleRequest,
  lists: state.userInfo.lists,
  user: state.userInfo.user,
  language: state.userInfo.user.language,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(UserInfoActions, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(AddTournament);