import React, {Component} from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Icon from 'react-native-vector-icons/FontAwesome5';

import { Creators as UserInfoActions } from '../../store/ducks/userInfo';

import { 
  Wrapper,
  WrapperInner,
  ListContainer,
  Row,
  SmallTitle,
  LabelPoints,
  LabelPosition,
  ImageIconUser,
  ContainerBanner,
  Fieldset,
  InputName,
  NotificationCount,
  NotificationValue,
} from './styles';
import { numberToReal, getFirstWord, showAlert, showToast, isIntoMinutes, normalizarString, cutString } from '../../utils/functions';
import I18n from '../../utils/translation/i18n';
import ButtonPrimary from '../../components/buttons/ButtonPrimary';
import FullLoader from '../../components/FullLoader';
import Colors from '../../utils/colors';
import { Alert, View, ScrollView, FlatList, Platform, TouchableOpacity } from 'react-native';
import { requestPayment } from '../../services/payments';
import ButtonSecondary from '../../components/buttons/ButtonSecondary';
import { getUserRankingList, getGeneralRanking, getRankingWinnersWeekly } from '../../services/leagues';
import Images from '../../utils/images';
import { getFollowers, getFollowing } from '../../services/follower';
import { getUserMyQuestionNotAnswered } from '../../services/userQuestion';

class UserQuestionDashboard extends Component {
  state = {
    userQuestions: [],
    isLoadingAdd: false,
  }

  navigate;

  refInputSuggestion;
  refInputSearch

  constructor(props) {
    super(props);

    this.navigate = this.props.navigation;
  }

  componentDidMount() {
    this.loadItems();
  }

  loadItems = () => {
    
    this.setState({
      isLoadingAdd: true,
    });

    getUserMyQuestionNotAnswered(this.props.user._id)
      .then((res) => { 
        this.setState({
          userQuestions: res.userQuestions,
          isLoadingAdd: false,
        });
      })
      .catch((error) => {
        console.log(error);
        showToast(I18n.t('errorGenericMessage'));
        this.setState({
          isLoadingAdd: false,
        });
        });
  }
  render() {
    const { userQuestions } = this.state;
    const { user } = this.props;

    let listToLoad = userQuestions;

    return (
      <>
        <Wrapper>
          <ScrollView>
            <WrapperInner>

              <Row style={{ justifyContent: 'center', marginTop: -15 }}>
                <LabelPosition style={{ color: Colors.secondary, fontSize: 22, marginBottom: 15 }}>Recebidas</LabelPosition>
              </Row>
              <Row style={{ flexDirection: 'column' }}>

                <ButtonPrimary
                  onPress={() => this.navigate.push('UserQuestionWaiting')}
                  title={'Aguardando minha resposta'}
                  backgroundColor={Colors.secondary}
                  textColor={Colors.primary}
                  icon={'chevron-right'}>
                </ButtonPrimary>

              

                {
                  this.props.notAnsweredQuestionCount > 0 && (
                      <View style={{ position: 'absolute', zIndex: 999999, top: -7, right: 45 }}>
                        <NotificationCount style={{ backgroundColor: Colors.red, zIndex: 999999}}>
                          <NotificationValue style={{ color: Colors.white }}>{this.props.notAnsweredQuestionCount}</NotificationValue>
                        </NotificationCount>
                      </View>
                  )
                }

                <View style={{ marginBottom: 10 }} />

                <ButtonPrimary
                  onPress={() => this.navigate.push('UserQuestionProfile', {
                    userObj: this.props.user,
                  })}
                  title={'Respondidas'}
                  backgroundColor={Colors.blue}
                  icon={'fire'}>
                </ButtonPrimary>

                <Row style={{ justifyContent: 'center', marginTop: 10 }}>
                  <LabelPosition style={{ color: Colors.secondary, fontSize: 22, marginBottom: 15 }}>Enviadas</LabelPosition>
                </Row>

                <ButtonPrimary
                  onPress={() => this.navigate.push('UserQuestionWaitingTheir')}
                  title={'Aguardando resposta'}
                  backgroundColor={Colors.secondary}
                  textColor={Colors.primary}
                  icon={'chevron-right'}>
                </ButtonPrimary>

                <View style={{ marginBottom: 10 }} />
                <ButtonPrimary
                  onPress={() => this.navigate.push('UserQuestionAnsweredTheir')}
                  title={'Respondidas'}
                  backgroundColor={Colors.blue}
                  icon={'fire'}>
                </ButtonPrimary>
                
                <View style={{ marginBottom: 200 }} />
              </Row>
            </WrapperInner>
          </ScrollView>
          
        </Wrapper>

        {
          this.state.isLoadingAdd && (
            <FullLoader></FullLoader>
          )
        }
       </>
    )
  }
}

const mapStateToProps = state => ({
  addListRequest: state.userInfo.addListRequest,
  updateListsTitleRequest: state.userInfo.updateListsTitleRequest,
  updateNotAnsweredQuestionCount: state.userInfo.updateNotAnsweredQuestionCount,
  notAnsweredQuestionCount: state.userInfo.notAnsweredQuestionCount,
  lists: state.userInfo.lists,
  user: state.userInfo.user,
  language: state.userInfo.user.language,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(UserInfoActions, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(UserQuestionDashboard);