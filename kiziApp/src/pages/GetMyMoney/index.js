import React, {Component} from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { Creators as UserInfoActions } from '../../store/ducks/userInfo';

import { 
  Wrapper,
  BigBox,
  SubtitleText,
  TextMoney,
  LabelForm,
  ContainerMainBanner,
  LabelFormMin,
  WrapperInner,
  LabelTitleTopic,
  LabelDescriptionTopic,
} from './styles';
import { numberToReal, getFirstWord, showAlert } from '../../utils/functions';
import ButtonPrimary from '../../components/buttons/ButtonPrimary';
import FullLoader from '../../components/FullLoader';
import Colors from '../../utils/colors';
import { Alert, ScrollView, View } from 'react-native';
import { requestPayment } from '../../services/payments';

class GetMyMoney extends Component {
  state = {
    textSuggestion: '',
    isLoadingAdd: false,
    switchValue: true,
  }

  refInputSuggestion;
  navigate;

  constructor(props) {
    super(props);

    this.navigate = this.props.navigation;
  }

  async componentDidMount() {
    
  }

  requestPayment = () => {
    const { user } = this.props;

    // if (user.money < 30) {
    //   return showAlert('Dinheiro insuficiente', 'É necessário ter pelo menos R$ 30,00 acumulados para poder solicitar o pagamento');
    // }

    Alert.alert(
      'Solicitar pagamento',
      `Tem certeza que deseja solicitar o pagamento de ${numberToReal(user.money)}?`,
      [
        { text: "Cancelar", onPress: () => {}},
        {text: 'Sim', onPress: () => {
          this.navigate.push('AddPayment');
          // this.setState({
          //   isLoadingAdd: true,
          // });

          // requestPayment(user._id)
          //   .then((res) => { 
          //     this.setState({
          //       isLoadingAdd: false,
          //     });
      
          //     showAlert('Pagamento solicitado', 'Pagamento solicitado com sucesso! Aguarde contato para finalizarmos o pagamento direto na sua conta :)');
          //   })
          //   .catch((error) => {
          //     console.log(error);
          //     this.setState({
          //       isLoadingAdd: false
          //     });
          //     showToast('Ocorreu um erro inesperado');
          //   }); 
        }},
      ],
      {cancelable: true},
    );
  }

  render() {
    const { user } = this.props;

    return (
      <>
        <Wrapper>
          <ContainerMainBanner>
            <LabelForm>Você possui:</LabelForm>
            <BigBox>
              <TextMoney>{ numberToReal(user.money) }</TextMoney>
            </BigBox>
            <LabelFormMin>É necessário ter pelo menos R$ 30,00{"\n"}para poder solicitar o pagamento</LabelFormMin>
          </ContainerMainBanner>

          <ScrollView>
            <WrapperInner>
              <ButtonPrimary
                onPress={() => this.requestPayment()}
                title={'Solicitar agora'}
                // disabled={user.money < 30}
                // backgroundColor={(user.money < 30) ? Colors.lightSecondary : Colors.secondary}
                backgroundColor={Colors.secondary}
                icon={'dollar-sign'}>
              </ButtonPrimary>

              <View style={{ marginTop: 10 }}/>

              <ButtonPrimary
                onPress={() => this.navigate.push('PaymentRules')}
                title={'Regras oficiais'}
                // disabled={user.money < 30}
                backgroundColor={Colors.primary}
                icon={'align-right'}>
              </ButtonPrimary>

              

              <LabelTitleTopic>Como ganhar mais dinheiro?</LabelTitleTopic>
              <LabelDescriptionTopic>
                - Participe de torneios abertos;{"\n"}
                - Participe do torneio semanal;{"\n"}
                - Ganhe moedas no menu "Ganhar moedas";{"\n"}
                - Desbloqueie as conquistas e vá ganhando KizoTokens;{"\n"}
                - Adicione perguntas, se for aprovada, você ganha;{"\n"}
                {/* - Convide amigos, se eles entrarem com seu código, você ganha;{"\n"} */}
                - Acompanhe nossas redes sociais, por lá temos diversas surpresas.{"\n"}
              </LabelDescriptionTopic>

              <LabelTitleTopic style={{ marginTop: 0 }}>Como funciona o pagamento?</LabelTitleTopic>
              <View style={{ marginTop: 10 }}/>

              <ButtonPrimary
                onPress={() => this.navigate.push('PaymentRules')}
                title={'Regras oficiais'}
                // disabled={user.money < 30}
                backgroundColor={Colors.primary}
                icon={'align-right'}>
              </ButtonPrimary>

              <View style={{ marginBottom: 100 }}/>
            </WrapperInner>
          </ScrollView>
          
        </Wrapper>

        {
          this.state.isLoadingAdd && (
            <FullLoader></FullLoader>
          )
        }
       </>
    )
  }
}

const mapStateToProps = state => ({
  addListRequest: state.userInfo.addListRequest,
  updateListsTitleRequest: state.userInfo.updateListsTitleRequest,
  lists: state.userInfo.lists,
  user: state.userInfo.user,
  language: state.userInfo.user.language,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(UserInfoActions, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(GetMyMoney);