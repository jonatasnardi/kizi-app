import React, {Component} from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { Creators as UserInfoActions } from '../../store/ducks/userInfo';

import I18n from '../../utils/translation/i18n';

import Icon from 'react-native-vector-icons/FontAwesome5';

import {fetch} from 'react-native-ssl-pinning';

import RNIap, {
  Product,
  ProductPurchase,
  acknowledgePurchaseAndroid,
  purchaseUpdatedListener,
  purchaseErrorListener,
  PurchaseError,
} from 'react-native-iap';

import { Alert, ActivityIndicator, NativeModules, Text, Platform, TouchableOpacity, View, ScrollView } from 'react-native';

import { 
  Wrapper,
  InputSuggestion,
  LabelForm,
  Fieldset,
  ButtonQuickAccess,
  ContainerButtonsHome,
  WrapperButtonHome,
  ButtonHomeText,
  ButtonHomeTextPrice,
  ContainerKts,
} from './styles';
import Colors from '../../utils/colors';
import { showAlert, showToast } from '../../utils/functions';
import ButtonPrimary from '../../components/buttons/ButtonPrimary';
import FullLoader from '../../components/FullLoader';
import { userContact, toPro } from '../../services/user';
import { addUserKTs } from '../../services/general';
import { BASE_URL_API } from '../../helpers/variables';

let purchaseUpdateSubscription;
let purchaseErrorSubscription;

const itemSkus = Platform.select({
  ios: [
    'br.com.kizoapp.kts225',
    'br.com.kizoapp.kts500',
    'br.com.kizoapp.kts1000',
    'br.com.kizoapp.kts1500',
    'br.com.kizoapp.kts2000',
    'br.com.kizoapp.kts3000',
  ],
  android: [
    'br.com.kizoapp.kts225',
    'br.com.kizoapp.kts500',
    'br.com.kizoapp.kts1000',
    'br.com.kizoapp.kts1500',
    'br.com.kizoapp.kts2000',
    'br.com.kizoapp.kts3000',
  ],
});

class Pro extends Component {
  state = {
    textSuggestion: '',
    isLoading: false,
    productList: [],
    receipt: '',
    availableItemsMessage: '',
  }

  purchaseUpdateSubscription = null;
  purchaseErrorSubscription = null;

  price225 = Platform.select({
    android: 'R$ 3,97',
    ios: 'R$ 3,90',
  });

  price500 = Platform.select({
    android: 'R$ 7,97',
    ios: 'R$ 7,90',
  });

  price1000 = Platform.select({
    android: 'R$ 14,97',
    ios: 'R$ 14,90',
  });

  price1500 = Platform.select({
    android: 'R$ 22,97',
    ios: 'R$ 22,90',
  });

  price2000 = Platform.select({
    android: 'R$ 24,97',
    ios: 'R$ 24,90',
  });

  price3000 = Platform.select({
    android: 'R$ 34,97',
    ios: 'R$ 34,90',
  });

  refInputSuggestion;

  constructor(props) {
    super(props);
  }

  async componentDidMount() {
    const { user } = this.props;

    

    // fetch(`${BASE_URL_API}/general/add-kts/I1d013cs/${user._id}`, {
    //   method: "POST" ,
    //   timeoutInterval: 30000,
    //   body: JSON.stringify({
    //     qtd: 3000,
    //     hp: 'OIQJ091MIOAm01m0DAKMMD01JAKOMDAOPJ1doakodks90120d1iaspdMOKDMASJDASKOMdj0doam'
    //   }),
    //   sslPinning: {
    //       certs: ["mycert"],
    //   },
    //   headers:{
    //     Authorization: `Bearer ${this.props.auth.token}`,
    //   }
    // })
    // .then(response => {
    //   let userInfo = {
    //     ...user,
    //     kizoTokens: user.kizoTokens + 3000,
    //     buyCount: user.buyCount + 1,
    //   };
      
    //   this.props.updateUserInfo(userInfo);

    //   this.setState({
    //     isLoading: false,
    //   });

    //   showAlert('Sucesso', `Os ${3000} KizoTokens foram adquiridos e adicionados com sucesso!`);
    // })
    // .catch(err => {
    //   console.log(error);
    //   this.setState({
    //     isLoading: false,
    //   });
    //   showToast('Ocorreu um erro ao adicionar os seus KTs');
    // })
    
    
    try {
      const result = await RNIap.initConnection();
      await this.getAvailablePurchases();
      await this.getItems();
      await RNIap.consumeAllItemsAndroid();
      console.log('result', result);
    } catch (err) {
      console.warn(err.code, err.message);
    }

    purchaseUpdateSubscription = purchaseUpdatedListener(async(purchase) => {
      console.log('purchaseUpdatedListener', purchase);
      if (purchase.purchaseStateAndroid === 1 && !purchase.isAcknowledgedAndroid) {
        try {
          const ackResult = await acknowledgePurchaseAndroid(purchase.purchaseToken);
          console.log('ackResult', ackResult);
        } catch (ackErr) {
          console.warn('ackErr', ackErr);
        }
      }

      const toJson = JSON.parse(purchase.transactionReceipt);
      if ((toJson.orderId.indexOf('GPA') > -1) || (Platform.OS == 'ios')) {
        this.setState({ receipt: purchase.transactionReceipt }, () => this.goNext());
      } else {
        Alert.alert('Error', 'Error');
        return false;
      }

      
      
      
    });

    purchaseErrorSubscription = purchaseErrorListener((error) => {
      console.log('purchaseErrorListener', error);
      // this.setState({
      //   isLoading: true,
      // });
      // toPro(this.props.user._id)
      //   .then((data) => { 
      //     this.props.updateIsPro(true);
      //     this.setState({
      //       isLoading: false
      //     });
      //     showAlert('Pro', I18n.t('proSuccess'));
      //   })
      //   .catch((error) => {
      //     console.log(error);
      //     this.setState({
      //       isLoading: false
      //     });
      //     showToast(I18n.t('errorGenericMessage'));
      //   }); 
      showAlert(I18n.t('errorTitle'), `${I18n.t('errorGenericMessage')} - ${JSON.stringify(error)}`);
      this.setState({
        isLoading: false,
      });
      // Alert.alert('purchase error', JSON.stringify(error));
    });
  }

  componentWillMount() {
    if (purchaseUpdateSubscription) {
      purchaseUpdateSubscription.remove();
      purchaseUpdateSubscription = null;
    }
    if (purchaseErrorSubscription) {
      purchaseErrorSubscription.remove();
      purchaseErrorSubscription = null;
    }
  }

  // async init() {
  //   // value = await AsyncStorage.getItem('@kizo:notificationDateScheduled').then((result) => {
  //   //   alert(result);
  //   // });
  //  }

  defineCurrentBuy = (value) => {
    if (this.props.user.buyCount <= 0) {
      this.setState({
        selectedValue: value,
      }, () => {
        this.requestPurchase(`br.com.kizoapp.kts${value}`)
      });
    } else {
      showAlert('Somente amanhã', 'Não é possível mais comprar hoje');
    }
  }

  goNext = () => {
    const { selectedValue } = this.state;
    const { user } = this.props;
    // Alert.alert('Receipt', this.state.receipt);
    // final function here, update isPro
    this.setState({
      isLoading: true,
    });

    fetch(`${BASE_URL_API}/general/add-kts/I1d013cs/${user._id}`, {
      method: "POST" ,
      timeoutInterval: 30000,
      body: JSON.stringify({
        qtd: selectedValue,
        hp: 'OIQJ091MIOAm01m0DAKMMD01JAKOMDAOPJ1doakodks90120d1iaspdMOKDMASJDASKOMdj0doam'
      }),
      sslPinning: {
          certs: ["mycert"],
      },
      headers:{
        Authorization: `Bearer ${this.props.auth.token}`,
      }
    })
    .then(response => {
      let userInfo = {
        ...user,
        kizoTokens: user.kizoTokens + selectedValue,
        buyCount: user.buyCount + 1,
      };


      this.props.navigation.goBack(null);
      
      this.props.updateUserInfo(userInfo);

      this.setState({
        isLoading: false,
      });

      showAlert('Sucesso', `Os ${selectedValue} KizoTokens foram adquiridos e adicionados com sucesso!`);
    })
    .catch(err => {
      console.log(err);
      this.setState({
        isLoading: false,
      });
      showToast('Ocorreu um erro ao adicionar os seus KTs');
    })

    // addUserKTs(user._id, selectedValue)
    //   .then((res) => { 
    //     let userInfo = {
    //       ...user,
    //       kizoTokens: user.kizoTokens + selectedValue,
    //     };
        
    //     this.props.updateUserInfo(userInfo);

    //     this.setState({
    //       isLoading: false,
    //     });

    //     showAlert('Sucesso', `Os ${selectedValue} KizoTokens foram adquiridos e adicionados com sucesso!`);

    //   })
    //   .catch((error) => {
    //     console.log(error);
    //     this.setState({
    //       isLoading: false,
    //     });
    //     showToast('Ocorreu um erro ao adicionar os seus KTs');
    //   });
  }

  getItems = async() => {
    try {
      const products = await RNIap.getProducts(itemSkus);
      // const products = await RNIap.getSubscriptions(itemSkus);
      console.log('Products', products);
      this.setState({ productList: products });
    } catch (err) {
      console.log(err.code, err.message);
    }
  }

  getAvailablePurchases = async() => {
    try {
      console.info('Get available purchases (non-consumable or unconsumed consumable)');
      const purchases = await RNIap.getAvailablePurchases();
      console.info('Available purchases :: ', purchases);
      if (purchases && purchases.length > 0) {
        this.setState({
          availableItemsMessage: `Got ${purchases.length} items.`,
          receipt: purchases[0].transactionReceipt,
        });
      }
    } catch (err) {
      console.warn(err.code, err.message);
    }
  }

  // Version 3 apis
  requestPurchase = async(sku) => {
    this.setState({
      isLoading: true,
    });
    // alert(sku);
    try {
      // alert('2b');
      RNIap.requestPurchase(sku);
      // alert('4b')
    } catch (err) {
      // alert('3b');
      this.setState({
        isLoading: false,
      });
      console.warn(err.code, err.message);
    }
  }

  // Deprecated apis
  buyItem = async(sku) => {
    console.info('buyItem', sku);
    // const purchase = await RNIap.buyProduct(sku);
    // const products = await RNIap.buySubscription(sku);
    // const purchase = await RNIap.buyProductWithoutFinishTransaction(sku);
    try {
      const purchase = await RNIap.buyProduct(sku);
      // console.log('purchase', purchase);
      // await RNIap.consumePurchaseAndroid(purchase.purchaseToken);
      this.setState({ receipt: purchase.transactionReceipt }, () => this.goNext());
    } catch (err) {
      console.warn(err.code, err.message);
      const subscription = RNIap.addAdditionalSuccessPurchaseListenerIOS(async(purchase) => {
        this.setState({ receipt: purchase.transactionReceipt }, () => this.goNext());
        subscription.remove();
      });
    }
  }

  render() {
    const { textSuggestion } = this.state;
    const { user } = this.props;

    return (
      <>
        <Wrapper>
          <ScrollView>
          
            <Fieldset>
              <LabelForm>Adquira já mais KizoTokens. {'\n'}Com KizoTokens, você poderá dobrar o seus ganhos em premiações e ainda:</LabelForm>
              <LabelForm>
                - Participar de torneios com premiação maior;{'\n'}
                - Poder utilizar mais ajudas durante a resposta das perguntas;{'\n'}
                - Criar mais torneios com premiação maior.
              </LabelForm>
            </Fieldset>

            <ContainerButtonsHome>
              <WrapperButtonHome style={{ paddingRight: 10 }}>
                <ContainerKts style={{ backgroundColor: 'transparent', }}>
                  <Icon name="coins" size={40} color={Colors.yellow} />
                  <ButtonHomeText>Comprar 225 KTs</ButtonHomeText>
                  <ButtonHomeTextPrice>{ this.price225 }</ButtonHomeTextPrice>
                  <ButtonPrimary
                    onPress={() => this.defineCurrentBuy(225)}
                    backgroundColor={Colors.yellow}
                    textColor={Colors.primary}
                    title={'Comprar agora'}>
                  </ButtonPrimary>
                </ContainerKts>
              </WrapperButtonHome>

              <WrapperButtonHome style={{ paddingLeft: 10 }}>
                <ContainerKts style={{ backgroundColor: 'transparent', }}>
                  <Icon name="coins" size={40} color={Colors.yellow} />
                  <ButtonHomeText>Comprar 500 KTs</ButtonHomeText>
                  <ButtonHomeTextPrice>{ this.price500 }</ButtonHomeTextPrice>
                  <ButtonPrimary
                    onPress={() => this.defineCurrentBuy(500)}
                    backgroundColor={Colors.yellow}
                    textColor={Colors.primary}
                    title={'Comprar agora'}>
                  </ButtonPrimary>
                </ContainerKts>
              </WrapperButtonHome>
            </ContainerButtonsHome>

            <ContainerButtonsHome>
              <WrapperButtonHome style={{ paddingRight: 10 }}>
                <ContainerKts style={{ backgroundColor: 'transparent', }}>
                  <Icon name="coins" size={40} color={Colors.yellow} />
                  <ButtonHomeText>Comprar 1000 KTs</ButtonHomeText>
                  <ButtonHomeTextPrice>{ this.price1000 }</ButtonHomeTextPrice>
                  <ButtonPrimary
                    onPress={() => this.defineCurrentBuy(1000)}
                    backgroundColor={Colors.yellow}
                    textColor={Colors.primary}
                    title={'Comprar agora'}>
                  </ButtonPrimary>
                </ContainerKts>
              </WrapperButtonHome>

              <WrapperButtonHome style={{ paddingLeft: 10 }}>
                <ContainerKts style={{ backgroundColor: 'transparent', }}>
                  <Icon name="coins" size={40} color={Colors.yellow} />
                  <ButtonHomeText>Comprar 1500 KTs</ButtonHomeText>
                  <ButtonHomeTextPrice>{ this.price1500 }</ButtonHomeTextPrice>
                  <ButtonPrimary
                    onPress={() => this.defineCurrentBuy(1500)}
                    backgroundColor={Colors.yellow}
                    textColor={Colors.primary}
                    title={'Comprar agora'}>
                  </ButtonPrimary>
                </ContainerKts>
              </WrapperButtonHome>
            </ContainerButtonsHome>

            <ContainerButtonsHome>
              <WrapperButtonHome style={{ paddingRight: 10 }}>
                <ContainerKts style={{ backgroundColor: 'transparent', }}>
                  <Icon name="coins" size={40} color={Colors.yellow} />
                  <ButtonHomeText>Comprar 2000 KTs</ButtonHomeText>
                  <ButtonHomeTextPrice>{ this.price2000 }</ButtonHomeTextPrice>
                  <ButtonPrimary
                    onPress={() => this.defineCurrentBuy(2000)}
                    backgroundColor={Colors.yellow}
                    textColor={Colors.primary}
                    title={'Comprar agora'}>
                  </ButtonPrimary>
                </ContainerKts>
              </WrapperButtonHome>

              <WrapperButtonHome style={{ paddingLeft: 10 }}>
                <ContainerKts style={{ backgroundColor: 'transparent', }}>
                  <Icon name="coins" size={40} color={Colors.yellow} />
                  <ButtonHomeText>Comprar 3000 KTs</ButtonHomeText>
                  <ButtonHomeTextPrice>{ this.price3000 }</ButtonHomeTextPrice>
                  <ButtonPrimary
                    onPress={() => this.defineCurrentBuy(3000)}
                    backgroundColor={Colors.yellow}
                    textColor={Colors.primary}
                    title={'Comprar agora'}>
                  </ButtonPrimary>
                </ContainerKts>
              </WrapperButtonHome>
            </ContainerButtonsHome>
            <View style={{ marginBottom: 200 }}/>
          </ScrollView>
        </Wrapper>

        {
          this.state.isLoading && (
            <FullLoader></FullLoader>
          )
        }

       </>
    )
  }
}

const mapStateToProps = state => ({
  addListRequest: state.userInfo.addListRequest,
  updateListsTitleRequest: state.userInfo.updateListsTitleRequest,
  updateUserInfo: state.userInfo.updateUserInfo,
  lists: state.userInfo.lists,
  user: state.userInfo.user,
  auth: state.userInfo.auth,
  updateIsPro: state.userInfo.updateIsPro,
  language: state.userInfo.user.language,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(UserInfoActions, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Pro);