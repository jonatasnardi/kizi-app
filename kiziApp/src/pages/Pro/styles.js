import styled from 'styled-components/native';
import Colors from '../../utils/colors';
import { Type } from '../../utils/layout';


export const Wrapper = styled.View`
  ${Type.wrapper};
  flex: 1;
`;

export const Fieldset = styled.View`
  flex-direction: column;
  margin-bottom: 20px;
`;

export const LabelForm = styled.Text`
  ${Type.body};
  color: ${Colors.black};
  font-size: 18px;
  margin-bottom: 20px;
`;

export const InputSuggestion = styled.TextInput`
  ${Type.body};
  font-size: 18px;
  padding-bottom: 5px;
  border-bottom-color: ${Colors.primary};
  border-bottom-width: 2px;
`;


export const ContainerButtonsHome = styled.View`
  flex-direction: row;
  flex-wrap: wrap;
`;

export const WrapperButtonHome = styled.View`
  width: 50%;
  margin-bottom: 10px;
`;

export const ButtonHomeText = styled.Text`
  ${Type.body};
  font-weight: bold;
  color: ${Colors.primary};
  font-size: 20px;
  text-align: center;
  margin-top: 10px;
`;

export const ButtonHomeTextPrice = styled.Text`
  ${Type.body};
  font-weight: bold;
  color: ${Colors.primary};
  font-size: 20px;
  text-align: center;
  margin-top: 10px;
  margin-bottom: 10px;
`;

export const ButtonQuickAccess = styled.TouchableOpacity`
  ${Type.buttonStyle};
  padding: 12px 10px;
  position: relative;
`;

export const ContainerKts = styled.View`
  ${Type.buttonStyle};
  padding: 12px 10px;
  position: relative;
  border: 1px solid ${Colors.yellow};
  border-radius: 10px;
`;

