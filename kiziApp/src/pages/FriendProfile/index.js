import React, {Component} from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ImagePicker from 'react-native-image-crop-picker';

import { Creators as UserInfoActions } from '../../store/ducks/userInfo';

import I18n from '../../utils/translation/i18n';


import {
  AdMobBanner,
  AdMobRewarded,
} from 'react-native-admob';

import Icon from 'react-native-vector-icons/FontAwesome5';

import { 
  Wrapper,
  ImageUser,
  LabelForm,
  Fieldset,
  LabelValue,
  ModalAvatar,
  ModalWrapper,
  ModalTitle,
  ModalHeader,
  LabelValue2,
  ButtonCloseModal,
  ContainerButtonsAvatar,
  WrapperButtonAvatar,
  ButtonQuickAccess,
  ContainerButtonsHome,
  WrapperButtonHome2,
  WrapperButtonHome,
  ButtonHomeText,
  ContainerFollowers,
  ButtonHomeTextFollowers,
  ButtonHomeText2,
  ContainerBanner,
} from './styles';
import Colors from '../../utils/colors';
import { showAlert, showToast, calculatePercentage, isIntoMinutes, getRandomNumber } from '../../utils/functions';
import ButtonPrimary from '../../components/buttons/ButtonPrimary';
import FullLoader from '../../components/FullLoader';
import { userContact, updateAvatar, getUserInfo, getUserInfoProfile } from '../../services/user';
import Images from '../../utils/images';
import { Alert, View, TouchableOpacity, ScrollView, Platform, Image } from 'react-native';
import { followUser, unfollowUser } from '../../services/follower';
import { createChallenge, getOpponentInfo } from '../../services/challenges';

class FriendProfile extends Component {
  state = {
    user: null,
    textSuggestion: '',
    isLoadingAdd: true,
    modalAvatarVisible: false,
    isFollowing: false,
    followsCount: 0,
    followersCount: 0,
    winsCount: 0,
    drawsCount: 0,
    losesCount: 0,
  };

  bottomBannerId = Platform.select({
    android: 'ca-app-pub-4982456847713514/5350850363',
    ios: 'ca-app-pub-4982456847713514/5542422057',
  }); 

  rewardedBanner = Platform.select({
    android: 'ca-app-pub-4982456847713514/9290095372',
    ios: 'ca-app-pub-4982456847713514/4229340388',
  });

  constructor(props) {
    super(props);

    this.navigate = this.props.navigation;
  }

  componentDidMount() {
    const { user } = this.state;
    this.loadUserInfo();
  }

  loadUserInfo = () => {
    getUserInfoProfile(this.navigate.state.params.idUser, this.props.user._id)
      .then((data) => { 
        this.setState({
          user: data.user,
          isLoadingAdd: false,
          followsCount: data.followsCount,
          followersCount: data.followersCount,
          isFollowing: data.isFollowedByMe,
        })
        
      })
      .catch((error) => {
        console.log(error);
        this.setState({
          isLoadingAdd: false,
        })
      });

    getOpponentInfo(this.navigate.state.params.idUser, this.props.user._id)
      .then((data) => { 
        this.setState({
          winsCount: data.countWinnerMy,
          drawsCount: data.countDraws,
          losesCount: data.countWinnerUser,
        })
        
      })
      .catch((error) => {
        console.log(error);
        this.setState({
          isLoadingAdd: false,
        })
      });
  }

  followUser = () => {
    const { user } = this.props;

    this.setState({
      isLoadingAdd: true,
    })

    followUser(user._id ,this.navigate.state.params.idUser)
      .then((data) => { 
        this.setState({
          isLoadingAdd: false,
          isFollowing: true,
          followersCount: this.state.followersCount + 1,
        })
        
      })
      .catch((error) => {
        console.log(error);
        this.setState({
          isLoadingAdd: false,
          isFollowing: false,
        })
      });
  }

  unfollowUser = () => {
    const { user } = this.props;

    this.setState({
      isLoadingAdd: true,
    })

    unfollowUser(user._id , this.navigate.state.params.idUser)
      .then((data) => { 
        this.setState({
          isLoadingAdd: false,
          isFollowing: false,
          followersCount: this.state.followersCount - 1,
        });
        
      })
      .catch((error) => {
        console.log(error);
        this.setState({
          isLoadingAdd: false,
        })
      });
  }

  challenge = () => {
    
    this.setState({
      isLoadingAdd: true,
    });

    const randomNumber = getRandomNumber(1, 10);

    if (randomNumber >= 5) {
      AdMobRewarded.setAdUnitID(this.rewardedBanner);
      AdMobRewarded.requestAd().then(() => AdMobRewarded.showAd()).catch(error => {
        createChallenge(this.props.user._id , this.navigate.state.params.idUser)
          .then((data) => { 
            this.setState({
              isLoadingAdd: false,
            });
  
            this.navigate.push('Question', {
              challengeId: data.challenge._id,
              isChallenge: true,
              isToFinish: false,
              pointsUser: data.challenge.pointsUser,
              image: null,
            })
            
          })
          .catch((error) => {
            console.log(error);
            this.setState({
              isLoadingAdd: false,
            })
          });
      });
  
      AdMobRewarded.addEventListener('rewarded', reward => {
  
        createChallenge(this.props.user._id , this.navigate.state.params.idUser)
          .then((data) => { 
            this.setState({
              isLoadingAdd: false,
            });
  
            this.navigate.push('Question', {
              challengeId: data.challenge._id,
              isChallenge: true,
              isToFinish: false,
              pointsUser: data.challenge.pointsUser,
              image: null,
            })
            
          })
          .catch((error) => {
            console.log(error);
            this.setState({
              isLoadingAdd: false,
            })
          });
      });
    } else {
      createChallenge(this.props.user._id , this.navigate.state.params.idUser)
        .then((data) => { 
          this.setState({
            isLoadingAdd: false,
          });

          this.navigate.push('Question', {
            challengeId: data.challenge._id,
            isChallenge: true,
            isToFinish: false,
            pointsUser: data.challenge.pointsUser,
            image: null,
          })
          
        })
        .catch((error) => {
          console.log(error);
          this.setState({
            isLoadingAdd: false,
          })
        });
    }

    
  }

  render() {
    const { user, isFollowing, followsCount, followersCount } = this.state;

    return (
      <>
      {
        user && (
          <Wrapper>
          <Fieldset style={{ flexDirection: 'column', alignItems: 'center' }}>
            {
              !user.image ? (
                <ImageUser source={Images['1']} />
              ) : (
                <ImageUser source={{uri: user.image}} />
              )
            }

            {
              (user.lastAccess && isIntoMinutes(30, user.lastAccess)) && (
                <View style={{ width: 18, height: 18, borderRadius: 18, backgroundColor: Colors.green, position: 'relative', top: -23, left: 31 }} />
              )
            }
          </Fieldset>
          
          <Fieldset style={{ alignItems: 'center', paddingTop: 0, paddingBottom: 25, }}>
            <View style={{ flexDirection: 'row' }}>
              <LabelValue>{user.name}</LabelValue>
              {
                user.verified && (
                  <Image style={{ width: 18, height: 18, position: 'relative', top: 3 }} source={require('../../assets/verified.png')} />
                )
              }
            </View>
            <LabelValue2 style={{ color: Colors.secondary,  }}>@{user.username}</LabelValue2>
            <LabelValue style={{ fontSize: 22, color: Colors.primary, fontWeight: 'normal' }}>{user.points} pontos</LabelValue>
            {
              user.division >= 1 && (
                <LabelValue style={{ fontSize: 18, color: Colors.secondary, fontWeight: 'bold' }}>{user.division}ª divisão</LabelValue>
              )
            }
          </Fieldset>

          <ContainerButtonsHome>
            <WrapperButtonHome style={{ paddingRight: 10 }}>
              <ContainerFollowers style={{ backgroundColor: 'transparent', }} onPress={() => this.navigate.push('FollowerList', {
                    isFollowers: true,
                    userId: user._id
                  })}>
                <ButtonHomeText2>Seguidores</ButtonHomeText2>
                <ButtonHomeTextFollowers>{ followersCount }</ButtonHomeTextFollowers>
              </ContainerFollowers>
            </WrapperButtonHome>

            <WrapperButtonHome style={{ paddingLeft: 10 }}>
              <ContainerFollowers style={{ backgroundColor: 'transparent', }} onPress={() => this.navigate.push('FollowerList', {
                    isFollowers: false,
                    userId: user._id
                  })}>
                <ButtonHomeText2>Seguindo</ButtonHomeText2>
                <ButtonHomeTextFollowers>{ followsCount }</ButtonHomeTextFollowers>
              </ContainerFollowers>
            </WrapperButtonHome>
          </ContainerButtonsHome>
          <Fieldset>
            <ButtonPrimary
              onPress={() => this.navigate.push('AddUserQuestion', {
                to: user._id,
              })}
              icon={'comments'}
              backgroundColor={Colors.primary}
              title={ 'Fazer pergunta\n anonimamente'}>
            </ButtonPrimary>
          </Fieldset>
          <Fieldset>
            <ButtonPrimary
              onPress={() => this.navigate.push('UserQuestionProfile', {
                userObj: user,
                followersCount: this.state.followersCount,
                followsCount: this.state.followsCount,
              })}
              icon={'fire'}
              backgroundColor={Colors.primary}
              title={ 'Ver perguntas respondidas'}>
            </ButtonPrimary>
          </Fieldset>
          <Fieldset>
            <ButtonPrimary
              onPress={() => isFollowing ? this.unfollowUser() : this.followUser()}
              icon={isFollowing ? 'minus' : 'plus'}
              backgroundColor={isFollowing ? Colors.red : Colors.secondary}
              title={ isFollowing ? `Deixar de seguir` : `Seguir`}>
            </ButtonPrimary>
          </Fieldset>
          <Fieldset>
            <ButtonPrimary
              onPress={() => this.challenge()}
              icon={'gamepad'}
              backgroundColor={Colors.secondary}
              title={ `Desafiar pro X1`}>
            </ButtonPrimary>
          </Fieldset>
          <Fieldset>
            <ButtonPrimary
              onPress={() => this.navigate.push('Chat', {
                userContacted: user._id,
                userName: user.name,
              })}
              icon={'comment'}
              backgroundColor={Colors.secondary}
              title={ `Chat`}>
            </ButtonPrimary>
          </Fieldset>

          <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
            <LabelForm style={{ fontSize: 20, color: Colors.primary }}>Seu retrospecto contra este amigo:</LabelForm>
          </View>

          <ContainerButtonsHome>
            <WrapperButtonHome2 style={{ paddingRight: 10 }}>
              <ButtonQuickAccess style={{ backgroundColor: 'transparent' }} onPress={() => {}}>
                <Icon name="thumbs-up" size={40} color={Colors.green} />
                <ButtonHomeText>Vitórias</ButtonHomeText>
                <View style={{ flexDirection: 'row' }}>
                  <ButtonHomeText style={{ fontSize: 26 }}>{ this.state.winsCount }</ButtonHomeText>
                </View>
              </ButtonQuickAccess>
            </WrapperButtonHome2>

          <WrapperButtonHome2 style={{ paddingLeft: 10 }}>
            <ButtonQuickAccess style={{ backgroundColor: 'transparent' }} onPress={() => {}}>
              <Icon name="handshake" size={40} color={Colors.primary} />
              <ButtonHomeText>Empates</ButtonHomeText>
              <View style={{ flexDirection: 'row' }}>
                <ButtonHomeText style={{ fontSize: 26 }}>{ this.state.drawsCount }</ButtonHomeText>
              </View>
            </ButtonQuickAccess>
          </WrapperButtonHome2>

            <WrapperButtonHome2 style={{ paddingRight: 10 }}>
              <ButtonQuickAccess style={{ backgroundColor: 'transparent' }} onPress={() => {}}>
                <Icon name="thumbs-down" size={40} color={Colors.red} />
                <ButtonHomeText>Derrotas</ButtonHomeText>
                <View style={{ flexDirection: 'row' }}>
                  <ButtonHomeText style={{ fontSize: 26 }}>{ this.state.losesCount }</ButtonHomeText>
                </View>
              </ButtonQuickAccess>
            </WrapperButtonHome2>
          </ContainerButtonsHome>

          {/* <Fieldset>
            <ButtonPrimary
              onPress={() => {}}
              icon={'letter'}
              title={`Enviar mensagem`}>
            </ButtonPrimary>
          </Fieldset> */}

          <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
            <LabelForm style={{ fontSize: 20, color: Colors.primary }}>Títulos:</LabelForm>
          </View>

          <ContainerButtonsHome>
            <WrapperButtonHome style={{ paddingRight: 10 }}>
              <ButtonQuickAccess style={{ backgroundColor: 'transparent' }} onPress={() => {}}>
                <Icon name="trophy" size={40} color={Colors.yellow} />
                <ButtonHomeText>TORNEIOS{'\n'}SEMANAIS</ButtonHomeText>
                <View style={{ flexDirection: 'row' }}>
                  <ButtonHomeText style={{ fontSize: 26 }}>{ user.weekLeagueTitles }</ButtonHomeText>
                </View>
              </ButtonQuickAccess>
            </WrapperButtonHome>

          <WrapperButtonHome style={{ paddingLeft: 10 }}>
            <ButtonQuickAccess style={{ backgroundColor: 'transparent' }} onPress={() => {}}>
              <Icon name="trophy" size={40} color={Colors.blue} />
              <ButtonHomeText>TORNEIOS{'\n'}DO DIA</ButtonHomeText>
              <View style={{ flexDirection: 'row' }}>
                <ButtonHomeText style={{ fontSize: 26 }}>{ user.dailyLeagueTitles }</ButtonHomeText>
              </View>
            </ButtonQuickAccess>
          </WrapperButtonHome>

            <WrapperButtonHome style={{ paddingRight: 10 }}>
              <ButtonQuickAccess style={{ backgroundColor: 'transparent' }} onPress={() => {}}>
                <Icon name="medal" size={40} color={Colors.yellow} />
                <ButtonHomeText>TORNEIOS{'\n'}ABERTOS</ButtonHomeText>
                <View style={{ flexDirection: 'row' }}>
                  <ButtonHomeText style={{ fontSize: 26 }}>{ user.leagueTitles }</ButtonHomeText>
                </View>
              </ButtonQuickAccess>
            </WrapperButtonHome>

            <WrapperButtonHome style={{ paddingLeft: 10 }}>
              <ButtonQuickAccess style={{ backgroundColor: 'transparent' }} onPress={() => {}}>
                <Icon name="medal" size={40} color={Colors.red} />
                <ButtonHomeText>DEFAFIOS X1{'\n'}GANHOS</ButtonHomeText>
                <View style={{ flexDirection: 'row' }}>
                  <ButtonHomeText style={{ fontSize: 26 }}>{ user.challengesWon }</ButtonHomeText>
                </View>
              </ButtonQuickAccess>
            </WrapperButtonHome>
          </ContainerButtonsHome>

          <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
            <LabelForm style={{ fontSize: 20, color: Colors.primary }}>Desempenho:</LabelForm>
          </View>

          <ContainerButtonsHome>
            <WrapperButtonHome style={{ paddingRight: 10 }}>
              <ButtonQuickAccess style={{ backgroundColor: 'transparent' }} onPress={() => {}}>
                <Icon name="square-root-alt" size={40} color={Colors.blue} />
                <ButtonHomeText>Matemática</ButtonHomeText>
                <View style={{ flexDirection: 'row' }}>
                  <Icon name="check" size={18} color={Colors.green} />
                  <ButtonHomeText style={{ marginTop: 0, marginLeft: 4 }}>{ user.hitsMathematics }</ButtonHomeText>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Icon name="times" size={18} color={Colors.red} />
                  <ButtonHomeText style={{ marginTop: 0, marginLeft: 4 }}>{ user.failsMathematics }</ButtonHomeText>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <ButtonHomeText style={{ marginTop: 0, marginLeft: 4, fontSize: 26 }}>{ calculatePercentage(user.hitsMathematics, user.hitsMathematics + user.failsMathematics) }</ButtonHomeText>
                  <Icon style={{ position: 'relative', top: 5 }} name="percentage" size={26} color={Colors.black} />
                </View>
              </ButtonQuickAccess>
            </WrapperButtonHome>

            <WrapperButtonHome style={{ paddingLeft: 10 }}>
              <ButtonQuickAccess style={{ backgroundColor: 'transparent' }} onPress={() => {}}>
                <Icon name="book" size={40} color={Colors.yellow} />
                <ButtonHomeText>Português</ButtonHomeText>
                <View style={{ flexDirection: 'row' }}>
                  <Icon name="check" size={18} color={Colors.green} />
                  <ButtonHomeText style={{ marginTop: 0, marginLeft: 4 }}>{ user.hitsPortuguese }</ButtonHomeText>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Icon name="times" size={18} color={Colors.red} />
                  <ButtonHomeText style={{ marginTop: 0, marginLeft: 4 }}>{ user.failsPortuguese }</ButtonHomeText>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <ButtonHomeText style={{ marginTop: 0, marginLeft: 4, fontSize: 26 }}>{ calculatePercentage(user.hitsPortuguese, user.hitsPortuguese + user.failsPortuguese) }</ButtonHomeText>
                  <Icon style={{ position: 'relative', top: 5 }} name="percentage" size={26} color={Colors.black} />
                </View>
              </ButtonQuickAccess>
            </WrapperButtonHome>

            <WrapperButtonHome style={{ paddingRight: 10 }}>
              <ButtonQuickAccess style={{ backgroundColor: 'transparent' }} onPress={() => {}}>
                <Icon name="palette" size={40} color={Colors.pink} />
                <ButtonHomeText>Artes</ButtonHomeText>
                <View style={{ flexDirection: 'row' }}>
                  <Icon name="check" size={18} color={Colors.green} />
                  <ButtonHomeText style={{ marginTop: 0, marginLeft: 4 }}>{ user.hitsArts }</ButtonHomeText>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Icon name="times" size={18} color={Colors.red} />
                  <ButtonHomeText style={{ marginTop: 0, marginLeft: 4 }}>{ user.failsArts }</ButtonHomeText>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <ButtonHomeText style={{ marginTop: 0, marginLeft: 4, fontSize: 26 }}>{ calculatePercentage(user.hitsArts, user.hitsArts + user.failsArts) }</ButtonHomeText>
                  <Icon style={{ position: 'relative', top: 5 }} name="percentage" size={26} color={Colors.black} />
                </View>
              </ButtonQuickAccess>
            </WrapperButtonHome>

            <WrapperButtonHome style={{ paddingLeft: 10 }}>
              <ButtonQuickAccess style={{ backgroundColor: 'transparent' }} onPress={() => {}}>
                <Icon name="flask" size={40} color={Colors.green} />
                <ButtonHomeText>Ciências</ButtonHomeText>
                <View style={{ flexDirection: 'row' }}>
                  <Icon name="check" size={18} color={Colors.green} />
                  <ButtonHomeText style={{ marginTop: 0, marginLeft: 4 }}>{ user.hitsScience }</ButtonHomeText>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Icon name="times" size={18} color={Colors.red} />
                  <ButtonHomeText style={{ marginTop: 0, marginLeft: 4 }}>{ user.failsScience }</ButtonHomeText>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <ButtonHomeText style={{ marginTop: 0, marginLeft: 4, fontSize: 26 }}>{ calculatePercentage(user.hitsScience, user.hitsScience + user.failsScience) }</ButtonHomeText>
                  <Icon style={{ position: 'relative', top: 5 }} name="percentage" size={26} color={Colors.black} />
                </View>
              </ButtonQuickAccess>
            </WrapperButtonHome>

            <WrapperButtonHome style={{ paddingRight: 10 }}>
              <ButtonQuickAccess style={{ backgroundColor: 'transparent' }} onPress={() => {}}>
                <Icon name="chess-rook" size={40} color={Colors.brown} />
                <ButtonHomeText>História</ButtonHomeText>
                <View style={{ flexDirection: 'row' }}>
                  <Icon name="check" size={18} color={Colors.green} />
                  <ButtonHomeText style={{ marginTop: 0, marginLeft: 4 }}>{ user.hitsHistory }</ButtonHomeText>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Icon name="times" size={18} color={Colors.red} />
                  <ButtonHomeText style={{ marginTop: 0, marginLeft: 4 }}>{ user.failsHistory }</ButtonHomeText>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <ButtonHomeText style={{ marginTop: 0, marginLeft: 4, fontSize: 26 }}>{ calculatePercentage(user.hitsHistory, user.hitsHistory + user.failsHistory) }</ButtonHomeText>
                  <Icon style={{ position: 'relative', top: 5 }} name="percentage" size={26} color={Colors.black} />
                </View>
              </ButtonQuickAccess>
            </WrapperButtonHome>

            <WrapperButtonHome style={{ paddingLeft: 10 }}>
              <ButtonQuickAccess style={{ backgroundColor: 'transparent' }} onPress={() => {}}>
                <Icon name="globe-americas" size={40} color={Colors.purple} />
                <ButtonHomeText>Geografia</ButtonHomeText>
                <View style={{ flexDirection: 'row' }}>
                  <Icon name="check" size={18} color={Colors.green} />
                  <ButtonHomeText style={{ marginTop: 0, marginLeft: 4 }}>{ user.hitsGeography }</ButtonHomeText>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Icon name="times" size={18} color={Colors.red} />
                  <ButtonHomeText style={{ marginTop: 0, marginLeft: 4 }}>{ user.failsGeography }</ButtonHomeText>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <ButtonHomeText style={{ marginTop: 0, marginLeft: 4, fontSize: 26 }}>{ calculatePercentage(user.hitsGeography, user.hitsGeography + user.failsGeography) }</ButtonHomeText>
                  <Icon style={{ position: 'relative', top: 5 }} name="percentage" size={26} color={Colors.black} />
                </View>
              </ButtonQuickAccess>
            </WrapperButtonHome>

            <WrapperButtonHome style={{ paddingRight: 10 }}>
              <ButtonQuickAccess style={{ backgroundColor: 'transparent' }} onPress={() => {}}>
                <Icon name="futbol" size={40} color={Colors.orange} />
                <ButtonHomeText>Esportes</ButtonHomeText>
                <View style={{ flexDirection: 'row' }}>
                  <Icon name="check" size={18} color={Colors.green} />
                  <ButtonHomeText style={{ marginTop: 0, marginLeft: 4 }}>{ user.hitsSports }</ButtonHomeText>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Icon name="times" size={18} color={Colors.red} />
                  <ButtonHomeText style={{ marginTop: 0, marginLeft: 4 }}>{ user.failsSports }</ButtonHomeText>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <ButtonHomeText style={{ marginTop: 0, marginLeft: 4, fontSize: 26 }}>{ calculatePercentage(user.hitsSports, user.hitsSports + user.failsSports) }</ButtonHomeText>
                  <Icon style={{ position: 'relative', top: 5 }} name="percentage" size={26} color={Colors.black} />
                </View>
              </ButtonQuickAccess>
            </WrapperButtonHome>

            <WrapperButtonHome style={{ paddingLeft: 10 }}>
              <ButtonQuickAccess style={{ backgroundColor: 'transparent' }} onPress={() => {}}>
                <Icon name="tv" size={40} color={Colors.black} />
                <ButtonHomeText>Entretenimento</ButtonHomeText>
                <View style={{ flexDirection: 'row' }}>
                  <Icon name="check" size={18} color={Colors.green} />
                  <ButtonHomeText style={{ marginTop: 0, marginLeft: 4 }}>{ user.hitsEntertainment }</ButtonHomeText>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Icon name="times" size={18} color={Colors.red} />
                  <ButtonHomeText style={{ marginTop: 0, marginLeft: 4 }}>{ user.failsEntertainment }</ButtonHomeText>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <ButtonHomeText style={{ marginTop: 0, marginLeft: 4, fontSize: 26 }}>{ calculatePercentage(user.hitsEntertainment, user.hitsEntertainment + user.failsEntertainment) }</ButtonHomeText>
                  <Icon style={{ position: 'relative', top: 5 }} name="percentage" size={26} color={Colors.black} />
                </View>
              </ButtonQuickAccess>
            </WrapperButtonHome>

            <WrapperButtonHome style={{ paddingRight: 10 }}>
              <ButtonQuickAccess style={{ backgroundColor: 'transparent' }} onPress={() => {}}>
                <Icon name="flag-usa" size={40} color={Colors.red} />
                <ButtonHomeText>Inglês</ButtonHomeText>
                <View style={{ flexDirection: 'row' }}>
                  <Icon name="check" size={18} color={Colors.green} />
                  <ButtonHomeText style={{ marginTop: 0, marginLeft: 4 }}>{ user.hitsEnglish }</ButtonHomeText>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Icon name="times" size={18} color={Colors.red} />
                  <ButtonHomeText style={{ marginTop: 0, marginLeft: 4 }}>{ user.failsEnglish }</ButtonHomeText>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <ButtonHomeText style={{ marginTop: 0, marginLeft: 4, fontSize: 26 }}>{ calculatePercentage(user.hitsEnglish, user.hitsEnglish + user.failsEnglish) }</ButtonHomeText>
                  <Icon style={{ position: 'relative', top: 5 }} name="percentage" size={26} color={Colors.black} />
                </View>
              </ButtonQuickAccess>
            </WrapperButtonHome>

            <WrapperButtonHome style={{ paddingLeft: 10 }}>
              <ButtonQuickAccess style={{ backgroundColor: 'transparent' }} onPress={() => {}}>
                <Icon name="table" size={40} color={Colors.wine} />
                <ButtonHomeText>Geral</ButtonHomeText>
                <View style={{ flexDirection: 'row' }}>
                  <Icon name="check" size={18} color={Colors.green} />
                  <ButtonHomeText style={{ marginTop: 0, marginLeft: 4 }}>{ user.hitsGeneral }</ButtonHomeText>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Icon name="times" size={18} color={Colors.red} />
                  <ButtonHomeText style={{ marginTop: 0, marginLeft: 4 }}>{ user.failsGeneral }</ButtonHomeText>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <ButtonHomeText style={{ marginTop: 0, marginLeft: 4, fontSize: 26 }}>{ calculatePercentage(user.hitsGeneral, user.hitsGeneral + user.failsGeneral) }</ButtonHomeText>
                  <Icon style={{ position: 'relative', top: 5 }} name="percentage" size={26} color={Colors.black} />
                </View>
              </ButtonQuickAccess>
            </WrapperButtonHome>

          </ContainerButtonsHome>

          {/* {
            !user.isPro && (
              <Fieldset>
                <ButtonPrimary
                  onPress={() => this.navigate.push('Pro')}
                  title={I18n.t('goToProVersion')}
                  backgroundColor={'#FFCC33'}
                  textColor={Colors.darkGrey}>
                </ButtonPrimary>
              </Fieldset>
            )
          } */}
          <View style={{ marginBottom: 200 }} />
    
        </Wrapper>
        )
      }
        

        {
          this.state.isLoadingAdd && (
            <FullLoader></FullLoader>
          )
        }
        
        <ContainerBanner>
          <AdMobBanner
            adSize="fullBanner"
            adUnitID={this.bottomBannerId}
            height={90}
          />
        </ContainerBanner>

       </>
    )
  }
}

const mapStateToProps = state => ({
  addListRequest: state.userInfo.addListRequest,
  updateListsTitleRequest: state.userInfo.updateListsTitleRequest,
  lists: state.userInfo.lists,
  user: state.userInfo.user,
  language: state.userInfo.user.language,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(UserInfoActions, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(FriendProfile);