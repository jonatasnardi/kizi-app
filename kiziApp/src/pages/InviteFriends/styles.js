import styled from 'styled-components/native';
import Colors from '../../utils/colors';
import { Type } from '../../utils/layout';


export const Wrapper = styled.View`
  ${Type.wrapper};
  flex: 1;
`;

export const Fieldset = styled.View`
  flex-direction: column;
  margin-bottom: 20px;
`;

export const LabelForm = styled.Text`
  ${Type.body};
  color: ${Colors.black};
  font-size: 20px;
  margin-bottom: 10px;
`;

export const InputSuggestion = styled.TextInput`
  ${Type.body};
  font-size: 18px;
  padding-bottom: 5px;
  border-bottom-color: ${Colors.primary};
  border-bottom-width: 2px;
`;