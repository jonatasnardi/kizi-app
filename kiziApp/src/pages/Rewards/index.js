import React, {Component} from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Icon from 'react-native-vector-icons/FontAwesome5';
import REWARDS_ENUM from '../../utils/enums';


import { Creators as UserInfoActions } from '../../store/ducks/userInfo';

import { 
  Wrapper,
  WrapperInner,
  ListContainer,
  Row,
  SmallTitle,
  LabelPoints,
  LabelPosition,
  ImageIconUser,
  ContainerBanner,
} from './styles';
import { numberToReal, getFirstWord, showAlert, showToast } from '../../utils/functions';
import I18n from '../../utils/translation/i18n';
import ButtonPrimary from '../../components/buttons/ButtonPrimary';
import FullLoader from '../../components/FullLoader';
import Colors from '../../utils/colors';
import { Alert, View, ScrollView, FlatList, Platform } from 'react-native';
import { requestPayment } from '../../services/payments';
import ButtonSecondary from '../../components/buttons/ButtonSecondary';
import { getUserRewards } from '../../services/user';
import Images from '../../utils/images';

class Rewards extends Component {
  state = {
    users: [],
    isLoadingAdd: false,
    rewards: null,
  }

  refInputSuggestion;

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const { user } = this.props;
    this.setState({
      isLoadingAdd: true,
    });

    getUserRewards(user._id)
      .then((res) => { 
        this.setState({
          rewards: res.reward,
          isLoadingAdd: false,
        });
      })
      .catch((error) => {
        console.log(error);
        showToast(I18n.t('errorGenericMessage'));
        this.setState({
          isLoadingAdd: false,
        });
      });
  }

  render() {

    return (
      <>
        <Wrapper>
          <ScrollView>
            <WrapperInner>
              
              <Row style={{ justifyContent: 'flex-start', marginTop: 10, marginBottom: 20 }}>
                <LabelPosition style={{ color: Colors.primary, textAlign: 'left', fontSize: 18 }}>Desbloqueie as conquistas para conseguir as recompensas</LabelPosition>
              </Row>

              <Row>
                <LabelPosition>Ok?</LabelPosition>
                <SmallTitle>Descrição</SmallTitle>
                <LabelPoints>KTs de prêmio</LabelPoints>
              </Row>
              { this.state.rewards && (
                <>
                <ListContainer>
                  <Row>
                    {
                      this.state.rewards.playedFirstMatch ? (
                        <Icon name="check" size={20} color={Colors.green} />
                      ) : (
                        <Icon name="times" size={28} color={Colors.red} />
                      )
                    }
                    <SmallTitle>{ REWARDS_ENUM.playedFirstMatch }</SmallTitle>
                    <LabelPoints>+4</LabelPoints>
                  </Row>
                </ListContainer>
                <ListContainer>
                  <Row>
                    {
                      this.state.rewards.won1WeeklyTournament ? (
                        <Icon name="check" size={20} color={Colors.green} />
                      ) : (
                        <Icon name="times" size={28} color={Colors.red} />
                      )
                    }
                    <SmallTitle>{ REWARDS_ENUM.won1WeeklyTournament }</SmallTitle>
                    <LabelPoints>+15</LabelPoints>
                  </Row>
                </ListContainer>
                <ListContainer>
                  <Row>
                    {
                      this.state.rewards.won1OpenTournament ? (
                        <Icon name="check" size={20} color={Colors.green} />
                      ) : (
                        <Icon name="times" size={28} color={Colors.red} />
                      )
                    }
                    <SmallTitle>{ REWARDS_ENUM.won1OpenTournament }</SmallTitle>
                    <LabelPoints>+3</LabelPoints>
                  </Row>
                </ListContainer>
                <ListContainer>
                  <Row>
                    {
                      this.state.rewards.won10OpenTournament ? (
                        <Icon name="check" size={20} color={Colors.green} />
                      ) : (
                        <Icon name="times" size={28} color={Colors.red} />
                      )
                    }
                    <SmallTitle>{ REWARDS_ENUM.won10OpenTournament }</SmallTitle>
                    <LabelPoints>+8</LabelPoints>
                  </Row>
                </ListContainer>
                <ListContainer>
                  <Row>
                    {
                      this.state.rewards.joined1OpenTournament ? (
                        <Icon name="check" size={20} color={Colors.green} />
                      ) : (
                        <Icon name="times" size={28} color={Colors.red} />
                      )
                    }
                    <SmallTitle>{ REWARDS_ENUM.joined1OpenTournament }</SmallTitle>
                    <LabelPoints>+3</LabelPoints>
                  </Row>
                </ListContainer>
                <ListContainer>
                  <Row>
                    {
                      this.state.rewards.joined5OpenTournament ? (
                        <Icon name="check" size={20} color={Colors.green} />
                      ) : (
                        <Icon name="times" size={28} color={Colors.red} />
                      )
                    }
                    <SmallTitle>{ REWARDS_ENUM.joined5OpenTournament }</SmallTitle>
                    <LabelPoints>+7</LabelPoints>
                  </Row>
                </ListContainer>
                <ListContainer>
                  <Row>
                    {
                      this.state.rewards.joined50OpenTournament ? (
                        <Icon name="check" size={20} color={Colors.green} />
                      ) : (
                        <Icon name="times" size={28} color={Colors.red} />
                      )
                    }
                    <SmallTitle>{ REWARDS_ENUM.joined50OpenTournament }</SmallTitle>
                    <LabelPoints>+9</LabelPoints>
                  </Row>
                </ListContainer>
                <ListContainer>
                  <Row>
                    {
                      this.state.rewards.joined100OpenTournament ? (
                        <Icon name="check" size={20} color={Colors.green} />
                      ) : (
                        <Icon name="times" size={28} color={Colors.red} />
                      )
                    }
                    <SmallTitle>{ REWARDS_ENUM.joined100OpenTournament }</SmallTitle>
                    <LabelPoints>+12</LabelPoints>
                  </Row>
                </ListContainer>
                <ListContainer>
                  <Row>
                    {
                      this.state.rewards.reached500RankingPoints ? (
                        <Icon name="check" size={20} color={Colors.green} />
                      ) : (
                        <Icon name="times" size={28} color={Colors.red} />
                      )
                    }
                    <SmallTitle>{ REWARDS_ENUM.reached500RankingPoints }</SmallTitle>
                    <LabelPoints>+4</LabelPoints>
                  </Row>
                </ListContainer>
                <ListContainer>
                  <Row>
                    {
                      this.state.rewards.reached1000RankingPoints ? (
                        <Icon name="check" size={20} color={Colors.green} />
                      ) : (
                        <Icon name="times" size={28} color={Colors.red} />
                      )
                    }
                    <SmallTitle>{ REWARDS_ENUM.reached1000RankingPoints }</SmallTitle>
                    <LabelPoints>+7</LabelPoints>
                  </Row>
                </ListContainer>
                <ListContainer>
                  <Row>
                    {
                      this.state.rewards.reached5000RankingPoints ? (
                        <Icon name="check" size={20} color={Colors.green} />
                      ) : (
                        <Icon name="times" size={28} color={Colors.red} />
                      )
                    }
                    <SmallTitle>{ REWARDS_ENUM.reached5000RankingPoints }</SmallTitle>
                    <LabelPoints>+12</LabelPoints>
                  </Row>
                </ListContainer>
                <ListContainer>
                  <Row>
                    {
                      this.state.rewards.reached10000RankingPoints ? (
                        <Icon name="check" size={20} color={Colors.green} />
                      ) : (
                        <Icon name="times" size={28} color={Colors.red} />
                      )
                    }
                    <SmallTitle>{ REWARDS_ENUM.reached10000RankingPoints }</SmallTitle>
                    <LabelPoints>+25</LabelPoints>
                  </Row>
                </ListContainer>

                {/* <ListContainer>
                  <Row>
                    {
                      this.state.rewards.invited1Friend ? (
                        <Icon name="check" size={20} color={Colors.green} />
                      ) : (
                        <Icon name="times" size={28} color={Colors.red} />
                      )
                    }
                    <SmallTitle>{ REWARDS_ENUM.invited1Friend }</SmallTitle>
                    <LabelPoints>+4</LabelPoints>
                  </Row>
                </ListContainer>

                <ListContainer>
                  <Row>
                    {
                      this.state.rewards.invited5Friends ? (
                        <Icon name="check" size={20} color={Colors.green} />
                      ) : (
                        <Icon name="times" size={28} color={Colors.red} />
                      )
                    }
                    <SmallTitle>{ REWARDS_ENUM.invited5Friends }</SmallTitle>
                    <LabelPoints>+4</LabelPoints>
                  </Row>
                </ListContainer> */}

                <ListContainer>
                  <Row>
                    {
                      this.state.rewards.added1Question ? (
                        <Icon name="check" size={20} color={Colors.green} />
                      ) : (
                        <Icon name="times" size={28} color={Colors.red} />
                      )
                    }
                    <SmallTitle>{ REWARDS_ENUM.added1Question }</SmallTitle>
                    <LabelPoints>+1</LabelPoints>
                  </Row>
                </ListContainer>

                <Row style={{ justifyContent: 'flex-start', marginTop: 20, marginBottom: 20 }}>
                  <LabelPosition style={{ color: Colors.primary, textAlign: 'left', fontSize: 18 }}>Conquistas de acertos</LabelPosition>
                </Row>

                <ListContainer>
                  <Row>
                    {
                      this.state.rewards.hits10Mathematics ? (
                        <Icon name="check" size={20} color={Colors.green} />
                      ) : (
                        <Icon name="times" size={28} color={Colors.red} />
                      )
                    }
                    <SmallTitle>{ REWARDS_ENUM.hits10Mathematics }</SmallTitle>
                    <LabelPoints>+1,5</LabelPoints>
                  </Row>
                </ListContainer>

                <ListContainer>
                  <Row>
                    {
                      this.state.rewards.hits100Mathematics ? (
                        <Icon name="check" size={20} color={Colors.green} />
                      ) : (
                        <Icon name="times" size={28} color={Colors.red} />
                      )
                    }
                    <SmallTitle>{ REWARDS_ENUM.hits100Mathematics }</SmallTitle>
                    <LabelPoints>+4</LabelPoints>
                  </Row>
                </ListContainer>

                <ListContainer>
                  <Row>
                    {
                      this.state.rewards.hits500Mathematics ? (
                        <Icon name="check" size={20} color={Colors.green} />
                      ) : (
                        <Icon name="times" size={28} color={Colors.red} />
                      )
                    }
                    <SmallTitle>{ REWARDS_ENUM.hits500Mathematics }</SmallTitle>
                    <LabelPoints>+8</LabelPoints>
                  </Row>
                </ListContainer>

                <ListContainer>
                  <Row>
                    {
                      this.state.rewards.hits10Portuguese ? (
                        <Icon name="check" size={20} color={Colors.green} />
                      ) : (
                        <Icon name="times" size={28} color={Colors.red} />
                      )
                    }
                    <SmallTitle>{ REWARDS_ENUM.hits10Portuguese }</SmallTitle>
                    <LabelPoints>+1,5</LabelPoints>
                  </Row>
                </ListContainer>

                <ListContainer>
                  <Row>
                    {
                      this.state.rewards.hits100Portuguese ? (
                        <Icon name="check" size={20} color={Colors.green} />
                      ) : (
                        <Icon name="times" size={28} color={Colors.red} />
                      )
                    }
                    <SmallTitle>{ REWARDS_ENUM.hits100Portuguese }</SmallTitle>
                    <LabelPoints>+4</LabelPoints>
                  </Row>
                </ListContainer>

                <ListContainer>
                  <Row>
                    {
                      this.state.rewards.hits500Portuguese ? (
                        <Icon name="check" size={20} color={Colors.green} />
                      ) : (
                        <Icon name="times" size={28} color={Colors.red} />
                      )
                    }
                    <SmallTitle>{ REWARDS_ENUM.hits500Portuguese }</SmallTitle>
                    <LabelPoints>+8</LabelPoints>
                  </Row>
                </ListContainer>

                <ListContainer>
                  <Row>
                    {
                      this.state.rewards.hits10Arts ? (
                        <Icon name="check" size={20} color={Colors.green} />
                      ) : (
                        <Icon name="times" size={28} color={Colors.red} />
                      )
                    }
                    <SmallTitle>{ REWARDS_ENUM.hits10Arts }</SmallTitle>
                    <LabelPoints>+1,5</LabelPoints>
                  </Row>
                </ListContainer>

                <ListContainer>
                  <Row>
                    {
                      this.state.rewards.hits100Arts ? (
                        <Icon name="check" size={20} color={Colors.green} />
                      ) : (
                        <Icon name="times" size={28} color={Colors.red} />
                      )
                    }
                    <SmallTitle>{ REWARDS_ENUM.hits100Arts }</SmallTitle>
                    <LabelPoints>+4</LabelPoints>
                  </Row>
                </ListContainer>

                <ListContainer>
                  <Row>
                    {
                      this.state.rewards.hits500Arts ? (
                        <Icon name="check" size={20} color={Colors.green} />
                      ) : (
                        <Icon name="times" size={28} color={Colors.red} />
                      )
                    }
                    <SmallTitle>{ REWARDS_ENUM.hits500Arts }</SmallTitle>
                    <LabelPoints>+8</LabelPoints>
                  </Row>
                </ListContainer>

                <ListContainer>
                  <Row>
                    {
                      this.state.rewards.hits10Science ? (
                        <Icon name="check" size={20} color={Colors.green} />
                      ) : (
                        <Icon name="times" size={28} color={Colors.red} />
                      )
                    }
                    <SmallTitle>{ REWARDS_ENUM.hits10Science }</SmallTitle>
                    <LabelPoints>+1,5</LabelPoints>
                  </Row>
                </ListContainer>

                <ListContainer>
                  <Row>
                    {
                      this.state.rewards.hits100Science ? (
                        <Icon name="check" size={20} color={Colors.green} />
                      ) : (
                        <Icon name="times" size={28} color={Colors.red} />
                      )
                    }
                    <SmallTitle>{ REWARDS_ENUM.hits100Science }</SmallTitle>
                    <LabelPoints>+4</LabelPoints>
                  </Row>
                </ListContainer>

                <ListContainer>
                  <Row>
                    {
                      this.state.rewards.hits500Science ? (
                        <Icon name="check" size={20} color={Colors.green} />
                      ) : (
                        <Icon name="times" size={28} color={Colors.red} />
                      )
                    }
                    <SmallTitle>{ REWARDS_ENUM.hits500Science }</SmallTitle>
                    <LabelPoints>+8</LabelPoints>
                  </Row>
                </ListContainer>

                <ListContainer>
                  <Row>
                    {
                      this.state.rewards.hits10History ? (
                        <Icon name="check" size={20} color={Colors.green} />
                      ) : (
                        <Icon name="times" size={28} color={Colors.red} />
                      )
                    }
                    <SmallTitle>{ REWARDS_ENUM.hits10History }</SmallTitle>
                    <LabelPoints>+1,5</LabelPoints>
                  </Row>
                </ListContainer>

                <ListContainer>
                  <Row>
                    {
                      this.state.rewards.hits100History ? (
                        <Icon name="check" size={20} color={Colors.green} />
                      ) : (
                        <Icon name="times" size={28} color={Colors.red} />
                      )
                    }
                    <SmallTitle>{ REWARDS_ENUM.hits100History }</SmallTitle>
                    <LabelPoints>+4</LabelPoints>
                  </Row>
                </ListContainer>

                <ListContainer>
                  <Row>
                    {
                      this.state.rewards.hits500History ? (
                        <Icon name="check" size={20} color={Colors.green} />
                      ) : (
                        <Icon name="times" size={28} color={Colors.red} />
                      )
                    }
                    <SmallTitle>{ REWARDS_ENUM.hits500History }</SmallTitle>
                    <LabelPoints>+8</LabelPoints>
                  </Row>
                </ListContainer>

                <ListContainer>
                  <Row>
                    {
                      this.state.rewards.hits10English ? (
                        <Icon name="check" size={20} color={Colors.green} />
                      ) : (
                        <Icon name="times" size={28} color={Colors.red} />
                      )
                    }
                    <SmallTitle>{ REWARDS_ENUM.hits10English }</SmallTitle>
                    <LabelPoints>+1,5</LabelPoints>
                  </Row>
                </ListContainer>

                <ListContainer>
                  <Row>
                    {
                      this.state.rewards.hits100English ? (
                        <Icon name="check" size={20} color={Colors.green} />
                      ) : (
                        <Icon name="times" size={28} color={Colors.red} />
                      )
                    }
                    <SmallTitle>{ REWARDS_ENUM.hits100English }</SmallTitle>
                    <LabelPoints>+4</LabelPoints>
                  </Row>
                </ListContainer>

                <ListContainer>
                  <Row>
                    {
                      this.state.rewards.hits500English ? (
                        <Icon name="check" size={20} color={Colors.green} />
                      ) : (
                        <Icon name="times" size={28} color={Colors.red} />
                      )
                    }
                    <SmallTitle>{ REWARDS_ENUM.hits500English }</SmallTitle>
                    <LabelPoints>+8</LabelPoints>
                  </Row>
                </ListContainer>

                <ListContainer>
                  <Row>
                    {
                      this.state.rewards.hits10Sports ? (
                        <Icon name="check" size={20} color={Colors.green} />
                      ) : (
                        <Icon name="times" size={28} color={Colors.red} />
                      )
                    }
                    <SmallTitle>{ REWARDS_ENUM.hits10Sports }</SmallTitle>
                    <LabelPoints>+1,5</LabelPoints>
                  </Row>
                </ListContainer>

                <ListContainer>
                  <Row>
                    {
                      this.state.rewards.hits100Sports ? (
                        <Icon name="check" size={20} color={Colors.green} />
                      ) : (
                        <Icon name="times" size={28} color={Colors.red} />
                      )
                    }
                    <SmallTitle>{ REWARDS_ENUM.hits100Sports }</SmallTitle>
                    <LabelPoints>+4</LabelPoints>
                  </Row>
                </ListContainer>

                <ListContainer>
                  <Row>
                    {
                      this.state.rewards.hits500Sports ? (
                        <Icon name="check" size={20} color={Colors.green} />
                      ) : (
                        <Icon name="times" size={28} color={Colors.red} />
                      )
                    }
                    <SmallTitle>{ REWARDS_ENUM.hits500Sports }</SmallTitle>
                    <LabelPoints>+8</LabelPoints>
                  </Row>
                </ListContainer>

                <ListContainer>
                  <Row>
                    {
                      this.state.rewards.hits10Entertainment ? (
                        <Icon name="check" size={20} color={Colors.green} />
                      ) : (
                        <Icon name="times" size={28} color={Colors.red} />
                      )
                    }
                    <SmallTitle>{ REWARDS_ENUM.hits10Entertainment }</SmallTitle>
                    <LabelPoints>+1,5</LabelPoints>
                  </Row>
                </ListContainer>

                <ListContainer>
                  <Row>
                    {
                      this.state.rewards.hits100Entertainment ? (
                        <Icon name="check" size={20} color={Colors.green} />
                      ) : (
                        <Icon name="times" size={28} color={Colors.red} />
                      )
                    }
                    <SmallTitle>{ REWARDS_ENUM.hits100Entertainment }</SmallTitle>
                    <LabelPoints>+4</LabelPoints>
                  </Row>
                </ListContainer>

                <ListContainer>
                  <Row>
                    {
                      this.state.rewards.hits500Entertainment ? (
                        <Icon name="check" size={20} color={Colors.green} />
                      ) : (
                        <Icon name="times" size={28} color={Colors.red} />
                      )
                    }
                    <SmallTitle>{ REWARDS_ENUM.hits500Entertainment }</SmallTitle>
                    <LabelPoints>+8</LabelPoints>
                  </Row>
                </ListContainer>

                <ListContainer>
                  <Row>
                    {
                      this.state.rewards.hits10General ? (
                        <Icon name="check" size={20} color={Colors.green} />
                      ) : (
                        <Icon name="times" size={28} color={Colors.red} />
                      )
                    }
                    <SmallTitle>{ REWARDS_ENUM.hits10General }</SmallTitle>
                    <LabelPoints>+1,5</LabelPoints>
                  </Row>
                </ListContainer>

                <ListContainer>
                  <Row>
                    {
                      this.state.rewards.hits100General ? (
                        <Icon name="check" size={20} color={Colors.green} />
                      ) : (
                        <Icon name="times" size={28} color={Colors.red} />
                      )
                    }
                    <SmallTitle>{ REWARDS_ENUM.hits100General }</SmallTitle>
                    <LabelPoints>+4</LabelPoints>
                  </Row>
                </ListContainer>

                <ListContainer>
                  <Row>
                    {
                      this.state.rewards.hits500General ? (
                        <Icon name="check" size={20} color={Colors.green} />
                      ) : (
                        <Icon name="times" size={28} color={Colors.red} />
                      )
                    }
                    <SmallTitle>{ REWARDS_ENUM.hits500General }</SmallTitle>
                    <LabelPoints>+8</LabelPoints>
                  </Row>
                </ListContainer>

                <ListContainer>
                  <Row>
                    {
                      this.state.rewards.hits10Geography ? (
                        <Icon name="check" size={20} color={Colors.green} />
                      ) : (
                        <Icon name="times" size={28} color={Colors.red} />
                      )
                    }
                    <SmallTitle>{ REWARDS_ENUM.hits10Geography }</SmallTitle>
                    <LabelPoints>+1,5</LabelPoints>
                  </Row>
                </ListContainer>

                <ListContainer>
                  <Row>
                    {
                      this.state.rewards.hits100Geography ? (
                        <Icon name="check" size={20} color={Colors.green} />
                      ) : (
                        <Icon name="times" size={28} color={Colors.red} />
                      )
                    }
                    <SmallTitle>{ REWARDS_ENUM.hits100Geography }</SmallTitle>
                    <LabelPoints>+4</LabelPoints>
                  </Row>
                </ListContainer>

                <ListContainer>
                  <Row>
                    {
                      this.state.rewards.hits500Geography ? (
                        <Icon name="check" size={20} color={Colors.green} />
                      ) : (
                        <Icon name="times" size={28} color={Colors.red} />
                      )
                    }
                    <SmallTitle>{ REWARDS_ENUM.hits500Geography }</SmallTitle>
                    <LabelPoints>+8</LabelPoints>
                  </Row>
                </ListContainer>

                
                </>
              )}

              
              <View style={{ marginBottom: 200 }} />
            </WrapperInner>
          </ScrollView>
          
        </Wrapper>

        {
          this.state.isLoadingAdd && (
            <FullLoader></FullLoader>
          )
        }
       </>
    )
  }
}

const mapStateToProps = state => ({
  addListRequest: state.userInfo.addListRequest,
  updateListsTitleRequest: state.userInfo.updateListsTitleRequest,
  lists: state.userInfo.lists,
  user: state.userInfo.user,
  language: state.userInfo.user.language,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(UserInfoActions, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Rewards);