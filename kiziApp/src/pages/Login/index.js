import React, {Component} from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { NativeModules, Text, ScrollView, Image, View, Alert } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import TextInputMask from 'react-native-text-input-mask';
import { LoginButton, AccessToken, LoginManager, GraphRequest, GraphRequestManager } from 'react-native-fbsdk';
import DeviceInfo from 'react-native-device-info';
const CryptoJS = require("crypto-js");


import I18n from '../../utils/translation/i18n';

import { Creators as UserInfoActions } from '../../store/ducks/userInfo';

import { 
  Wrapper,
  LoaderOverlay,
  LabelForm,
  LabelFormDark,
  Fieldset,
  InputName,
  InputEmail,
  BoxAdditionalInfo,
  ModalEmail,
  ModalPassword,
  ModalLogin,
  ModalWrapper,
  ModalTitle,
  ModalHeader,
  ButtonCloseModal,
  InputPassword,
  InputConfirmPassword,
  InputLoginEmail,
  InputLoginPassword,
  ButtonGeneric,
  LabelFormBlack,
  ModalTerms,
  ContainerLogo,
  ImageLogo,
  ContainerBtnFacebook,
} from './styles';
import ButtonSecondary from '../../components/buttons/ButtonSecondary';
import MinText from '../../components/texts/MinText';
import Colors from '../../utils/colors';
import ButtonPrimary from '../../components/buttons/ButtonPrimary';
import { isValidEmail, showAlert, replaceLettersWithNumbers, showToast } from '../../utils/functions';
import { registerUser, doLogin, createRewards, updateUserRewardId, forgotPassword } from '../../services/user';
import FullLoader from '../../components/FullLoader';
import { Platform } from 'react-native';

class Login extends Component {
  state = {
    modalActive: 0,
    confirmPassword: '',
    loginEmail: '',
    loginPassword: '',
    loginInfo: {
      email: '',
      phone: '',
      name: '',
      password: '',
      image: null,
      code: null,
    },
    isLoading: false,
    modalEmailVisible: false,
    modalPasswordVisible: false,
    modalLoginVisible: false,
    modalTermsVisible: false,
    modalForgotVisible: false,
  }

  companyName = Platform.select({
    android: 'Google',
    ios: 'Apple',
  }); 

  navigate;
  refInputName;
  refInputEmail;
  refInputPhone;
  refInputPassword;
  refInputConfirmPassword;
  refInputLoginEmail;
  refInputLoginPassword;
  refInputCode;

  constructor(props) {
    super(props);

    this.navigate = this.props.navigation;
  }

  componentDidMount() {
    let deviceLanguage =
      Platform.OS === 'ios'
        ? NativeModules.SettingsManager.settings.AppleLocale
        : NativeModules.I18nManager.localeIdentifier;

    if (deviceLanguage) {
      this.props.updateDeviceLanguage(deviceLanguage.substring(0, 2));
    } else {
      deviceLanguage = 'pt';
    }

    this.props.updateIsLogged(false);
    this.props.updateToken(null);
    AsyncStorage.removeItem('@kizo:userId', (err, response) => {});

    
  }

  setModalEmailVisible(visible) {
    this.setState({modalEmailVisible: visible});
  }

  setModalPasswordVisible(visible) {
    this.setState({modalPasswordVisible: visible});
  }

  setModalLoginVisible(visible) {
    this.setState({modalLoginVisible: visible});
  }

  setModalForgotVisible(visible) {
    this.setState({modalForgotVisible: visible});
  }

  setModalTermsVisible(visible) {
    if (visible) {
      this.setState({modalPasswordVisible: false});

      setTimeout(() => {
        this.setState({modalTermsVisible: visible});
      }, 1000);
    } else {
      this.setState({modalTermsVisible: visible});
      setTimeout(() => {
        this.setState({modalPasswordVisible: true});
      }, 1000);
    }
    

    
  }

  _responseInfoCallback = (error, result) => {
    if (error) {
      console.log('Error fetching data: ' + error.toString());
    } else {
      console.log('Success fetching data: ',  result);
      this.setState({
        loginInfo: { 
          ...this.state.loginInfo, 
          name: result.name,
          email: result.email,
          image: result.picture.data.url,
        }
      }, () => {
        this.setModalPasswordVisible(true);
      });
    }
  }

  nextStepName = () => {
    if (this.state.loginInfo.name.length >= 2) {
      this.setModalEmailVisible(true);
    } else {
      showAlert(I18n.t('warningTitle'), I18n.t('validateNameMessage'));
    }
  }

  nextStepEmail = () => {
    if (!isValidEmail(this.state.loginInfo.email)) {
      showAlert(I18n.t('labelRegister'), I18n.t('validateEmailMessage'));
    } else {
      this.setModalEmailVisible(false);
      this.setModalPasswordVisible(true);
    }
  }

  nextStepPassword = () => {
    if (this.state.loginInfo.password.length < 4) {
      showAlert(I18n.t('labelRegister'), I18n.t('validatePasswordLengthMessage'));
    } else if (this.state.loginInfo.password != this.state.confirmPassword) {
      showAlert(I18n.t('labelRegister'), I18n.t('validatePasswordDivergentMessage'));
    } else {
      this.setState({
        isLoading: true,
      });

      const deviceId = DeviceInfo.getUniqueId();

      registerUser(this.state.loginInfo, deviceId)
        .then((data) => { 

          createRewards()
            .then((res) => { 
              this.setState({
                isLoading: false,
              });

              updateUserRewardId(data.user._id, res.reward._id)
                .then((resUpdate) => { 
                  this.props.updateIsLogged(true);
                  this.props.updateToken(data.token);
                  AsyncStorage.setItem('@kizo:userId', data.user._id);
                  this.props.updateUserInfo(data.user);

                  const ciphertext = CryptoJS.AES.encrypt(this.state.loginInfo.password, 'db7b7213e691acfc27fd11a2a4e4c5b6');

                  // const bytes = CryptoJS.AES.decrypt(ciphertext.toString(), 'db7b7213e691acfc27fd11a2a4e4c5b6');
                  // const plaintext = bytes.toString(CryptoJS.enc.Utf8);

                  this.props.updatePs(ciphertext.toString());

                  this.props.updateToken(data.token);
      
                  setTimeout(() => {
                    this.props.navigation.replace('Main')
                    this.navigate.push('Main');
                  }, 400); 
                })
                .catch((error) => {
                  showToast('Ocorreu um erro inesperado.');
                });
            })
            .catch((error) => {
              console.log(error);
              showToast('Ocorreu um erro inesperado.');
            });

           
        })
        .catch((error) => {
          this.setState({
            isLoading: false,
          });
          if (error && error.data && error.data.message) {
            if (error.data.message.indexOf('Este usuário já existe') > -1) {
              this.setModalPasswordVisible(false);
              this.setModalEmailVisible(true);

              setTimeout(() => {
                return showAlert(I18n.t('warningTitle'), error.data.message);  
              }, 1500);
            } else {
              showAlert(I18n.t('errorTitle'), error.data.message);
            }
          } else {
            showAlert(I18n.t('errorTitle'), I18n.t('errorGenericMessage'));
          }
        }); 
    }
  }

  doLogin = () => {

    if (isValidEmail(this.state.loginEmail)) {
      this.setState({
        isLoading: true,
      });
      doLogin(this.state.loginEmail, this.state.loginPassword)
        .then((data) => { 
          this.setState({
            isLoading: false,
          });

          const ciphertext = CryptoJS.AES.encrypt(this.state.loginPassword, 'db7b7213e691acfc27fd11a2a4e4c5b6');

          const bytes = CryptoJS.AES.decrypt(ciphertext.toString(), 'db7b7213e691acfc27fd11a2a4e4c5b6');
          const plaintext = bytes.toString(CryptoJS.enc.Utf8);

          this.props.updatePs(ciphertext.toString());

          // if (data.status === 1) { // login successful
          this.props.updateUserInfo(data.user);
            
          this.props.updateIsLogged(true);
          AsyncStorage.setItem('@kizo:userId', data.user._id);
          this.props.updateToken(data.token);

          this.props.updateUserInfo(data.user);

          setTimeout(() => {
            this.props.navigation.replace('Main')
            this.navigate.push('Main');
          }, 400); 
          // } else {
            
          // }
        })
        .catch((error) => {
          console.log(error);

          this.setState({
            isLoading: false,
          });

          if (error && error.data && error.data.message) {
            showAlert(I18n.t('errorTitle'), error.data.message);
          } else {
            showAlert(I18n.t('errorTitle'), I18n.t('errorGenericMessage'));
          }
        }); 
    } else {
      showAlert(`Login`, I18n.t('validateLoginEmailInvalidMessage'));  
    }
  }

  forgotPassword = () => {
    this.setState({
      isLoading: true,
    });
    forgotPassword(this.state.loginEmail)
      .then((data) => { 
        this.setState({
          isLoading: false,
        });
        showAlert('Senha alterada', 'Sua nova senha foi enviada para o seu e-mail.');
      })
      .catch((error) => {
        this.setState({
          isLoading: false,
        });
        showAlert('Erro', 'Não foi possível enviar.');
      }); 
  }

  render() {
    return (
      <>
        <Wrapper>
          <ScrollView>
          <ContainerLogo>
            <ImageLogo source={require('../../assets/logo.png')}  />
          </ContainerLogo>

          



          <Fieldset>
            {/* <LabelForm>{I18n.t('labelLoginRegister')}</LabelForm> */}
            <InputName
              ref={ref => (this.refInputName = ref)}
              value={this.state.loginInfo.name}
              onChangeText={(text) => this.setState({loginInfo: { ...this.state.loginInfo, name: text}})}
              placeholder={'Nome e sobrenome...'}
              placeholderTextColor={'#566b89'}
              maxLength={60}>
            </InputName>
          </Fieldset>

          <ButtonSecondary
            onPress={this.nextStepName}
            title={I18n.t('nextStep')}>
          </ButtonSecondary>

          
          <BoxAdditionalInfo style={{ marginTop: 5, marginBottom: 5}}>
            <MinText
              text={'ou mais rápido ainda com o Facebook'}
              color={Colors.white}>
            </MinText>
          </BoxAdditionalInfo>


          <ContainerBtnFacebook>
            {/* <LabelForm>Cria sua conta com o:</LabelForm> */}
            <LoginButton
              style={{width: "100%", height: (Platform.OS === 'ios') ? 56 : 35}}
              permissions={["email", "public_profile"]}
              onLoginFinished={
                (error, result) => {
                  console.log('bla')
                  if (error) {
                    console.log("login has error: " + result.error);
                  } else if (result.isCancelled) {
                    console.log("login is cancelled.");
                  } else {
                    AccessToken.getCurrentAccessToken().then(
                      (data) => {
                        console.log(data)
                        const param = {
                          fields: {
                            string: 'email, name, picture.type(large)',
                          },
                        };
                        const infoRequest = new GraphRequest(
                          '/me',
                          {
                            httpMethod: 'GET',
                            version: 'v2.5',
                            parameters: param,
                          },
                          this._responseInfoCallback,
                        );
                        // Start the graph request.
                        const res = new GraphRequestManager().addRequest(infoRequest).start();
                      }
                    )

                    
                  }
                }
              }
            onLogoutFinished={() => console.log("logout.")}/>
          </ContainerBtnFacebook>

          <BoxAdditionalInfo>
            <MinText
              text={I18n.t('or')}
              color={Colors.white}>
            </MinText>
          </BoxAdditionalInfo>

          <ButtonPrimary
            onPress={() => this.setModalLoginVisible(true)}
            title={I18n.t('alreadyHasAccount')}
            backgroundColor={Colors.secondary}
            color={Colors.primary}
            icon={'user'}>
          </ButtonPrimary>

          <ButtonPrimary
            onPress={() => this.setModalForgotVisible(true)}
            title={'Esqueci a senha'}
            backgroundColor={Colors.primary}
            >
          </ButtonPrimary>

          </ScrollView>
        </Wrapper>

        <ModalEmail
          animationType="slide"
          transparent={false}
          visible={this.state.modalEmailVisible}>
          <ModalWrapper>
            <ModalHeader>
              <ButtonCloseModal onPress={() => {
                this.setModalEmailVisible(!this.state.modalEmailVisible);
              }}>
                <Icon name="times" size={40} color={Colors.primary} />
              </ButtonCloseModal>
            </ModalHeader>

            <Fieldset>
              <LabelFormDark>{I18n.t('labelEmailRegister')}</LabelFormDark>
              <InputEmail
                ref={ref => (this.refInputEmail = ref)}
                value={this.state.loginInfo.email}
                onChangeText={(text) => this.setState({loginInfo: { ...this.state.loginInfo, email: text.toLowerCase().trim()}})}
                placeholder="email@email.com"
                placeholderTextColor={'#17242A'}
                autoCapitalize="none"
                maxLength={70}>
              </InputEmail>
            </Fieldset>

            {
              Platform.OS !== 'ios' && (
                <Fieldset>
                  <LabelFormDark>Qual o seu número de telefone?</LabelFormDark>
                  <TextInputMask
                    ref={ref => (this.refInputPhone = ref)}
                    style={{
                      paddingBottom: 5,
                      fontSize: 20,
                      color: Colors.primary,
                      borderBottomColor: Colors.primary,
                      borderBottomWidth: 2,
                    }}
                    value={this.state.loginInfo.phone}
                    onChangeText={(text) => this.setState({loginInfo: { ...this.state.loginInfo, phone: text}})}
                    placeholder="(  ) _____-____"
                    placeholderTextColor={'#17242A'}
                    keyboardType={'numeric'}
                    mask={"([00]) [00000]-[0000]"}
                    maxLength={15}>
                  </TextInputMask>
                </Fieldset>
              )
            }

            <ButtonPrimary
              onPress={this.nextStepEmail}
              title={I18n.t('nextStep')}
              icon={'chevron-right'}>
            </ButtonPrimary>
            
            <Fieldset style={{ marginTop: 10 }}>
              <LabelFormDark>ATENÇÃO: É importante que os dados fornecidos sejam reais, pois será através deles que entraremos em contato para realizar as premiações :)</LabelFormDark>
            </Fieldset>
           
          </ModalWrapper>
        </ModalEmail>

        <ModalPassword
          animationType="slide"
          transparent={false}
          visible={this.state.modalPasswordVisible}>

          {
            this.state.isLoading && (
              <FullLoader></FullLoader>
            )
          }

          <ModalWrapper>
            <ModalHeader>
              <ButtonCloseModal onPress={() => {
                this.setModalPasswordVisible(!this.state.modalPasswordVisible);
              }}>
                <Icon name="times" size={40} color={Colors.primary} />
              </ButtonCloseModal>
            </ModalHeader>

            <Fieldset>
              <LabelFormDark>{I18n.t('labelPasswordRegister')}</LabelFormDark>
              <InputPassword
                ref={ref => (this.refInputPassword = ref)}
                value={this.state.loginInfo.password}
                onChangeText={(text) => this.setState({loginInfo: { ...this.state.loginInfo, password: text}})}
                placeholder={I18n.t('labelPassword')}
                placeholderTextColor={'#17242A'}
                secureTextEntry={true}
                autoCapitalize="none">
              </InputPassword>
            </Fieldset>

            <Fieldset>
              <InputConfirmPassword
                ref={ref => (this.refInputConfirmPassword = ref)}
                value={this.state.confirmPassword}
                onChangeText={(text) => this.setState({confirmPassword: text})}
                placeholder={I18n.t('labelConfirmPassword')}
                placeholderTextColor={'#17242A'}
                secureTextEntry={true}
                autoCapitalize="none">
              </InputConfirmPassword>
            </Fieldset>

            <Fieldset>
              <LabelForm style={{color: Colors.primary}}>Você foi convidado por alguém? Se sim, informe o código abaixo:</LabelForm>
              <InputConfirmPassword
                ref={ref => (this.refInputCode = ref)}
                value={this.state.loginInfo.code}
                onChangeText={(text) => this.setState({loginInfo: { ...this.state.loginInfo, code: text}})}
                placeholder={'Código de convite (opcional)'}
                placeholderTextColor={'#17242A'}>
              </InputConfirmPassword>
            </Fieldset>

            <ButtonPrimary
              onPress={this.nextStepPassword}
              title={I18n.t('finishRegister')}
              icon={'check'}>
            </ButtonPrimary>

            <Fieldset style={{marginTop: 20}}>
              <LabelFormDark style={{color: Colors.black, fontSize: 16}}>{I18n.t('agreeTerms')}</LabelFormDark>
              
              <ButtonGeneric onPress={() => this.setModalTermsVisible(true)}>
                <Text style={{fontWeight: "bold", color: Colors.secondary}}>{I18n.t('readTerms')}</Text>
              </ButtonGeneric>

              
            </Fieldset>
           
          </ModalWrapper>
        </ModalPassword>

        <ModalLogin
          animationType="slide"
          transparent={false}
          visible={this.state.modalLoginVisible}>
            
            {
              this.state.isLoading && (
                <FullLoader></FullLoader>
              )
            }
          

          <ModalWrapper>
            <ModalHeader style={{borderBottomColor: Colors.black50, borderBottomWidth: 1}}>
              <ModalTitle>Login</ModalTitle>
              <ButtonCloseModal onPress={() => {
                this.setModalLoginVisible(!this.state.modalLoginVisible);
              }}>
                <Icon name="times" size={40} color={Colors.primary} />
              </ButtonCloseModal>
            </ModalHeader>

            <Fieldset>
              <LabelFormDark>E-mail:</LabelFormDark>
              <InputLoginEmail
                ref={ref => (this.refInputLoginEmail = ref)}
                value={this.state.loginEmail}
                onChangeText={(text) => this.setState({loginEmail: text.toLowerCase().trim()})}
                placeholder="E-mail"
                placeholderTextColor={'#17242A'}
                autoCapitalize="none">
              </InputLoginEmail>
            </Fieldset>

            <Fieldset>
              <LabelFormDark>{I18n.t('labelPassword')}:</LabelFormDark>
              <InputLoginPassword
                ref={ref => (this.refInputLoginPassword = ref)}
                value={this.state.loginPassword}
                onChangeText={(text) => this.setState({loginPassword: text})}
                placeholder={I18n.t('labelPassword')}
                placeholderTextColor={'#17242A'}
                secureTextEntry={true}
                autoCapitalize="none">
              </InputLoginPassword>
            </Fieldset>

            <ButtonPrimary
              onPress={this.doLogin}
              title={I18n.t('accessAccount')}
              icon={'chevron-right'}>
            </ButtonPrimary>
           
          </ModalWrapper>
        </ModalLogin>

        <ModalLogin
          animationType="slide"
          transparent={false}
          visible={this.state.modalForgotVisible}>
            
            {
              this.state.isLoading && (
                <FullLoader></FullLoader>
              )
            }
          

          <ModalWrapper>
            <ModalHeader style={{borderBottomColor: Colors.black50, borderBottomWidth: 1}}>
              <ModalTitle>Esqueci a senha</ModalTitle>
              <ButtonCloseModal onPress={() => {
                this.setModalForgotVisible(!this.state.modalForgotVisible);
              }}>
                <Icon name="times" size={40} color={Colors.primary} />
              </ButtonCloseModal>
            </ModalHeader>

            <Fieldset>
              <LabelFormDark>Informe o seu e-mail de cadastro:</LabelFormDark>
              <InputLoginEmail
                ref={ref => (this.refInputLoginEmail = ref)}
                value={this.state.loginEmail}
                onChangeText={(text) => this.setState({loginEmail: text.toLowerCase().trim()})}
                placeholder="exemplo@exemplo.com"
                placeholderTextColor={'#17242A'}
                autoCapitalize="none">
              </InputLoginEmail>
            </Fieldset>

            <ButtonPrimary
              onPress={() => this.forgotPassword()}
              title={'Enviar nova senha por e-mail'}
              icon={'chevron-right'}>
            </ButtonPrimary>
           
          </ModalWrapper>
        </ModalLogin>

        <ModalTerms
          animationType="slide"
          transparent={false}
          visible={this.state.modalTermsVisible}>
          

          <ModalWrapper>
            <ModalHeader style={{borderBottomColor: Colors.black50, borderBottomWidth: 1}}>
              <ModalTitle>{I18n.t('titleTerms')}</ModalTitle>
              <ButtonCloseModal onPress={() => {
                this.setModalTermsVisible(!this.state.modalTermsVisible);
              }}>
                <Icon name="times" size={40} color={Colors.primary} />
              </ButtonCloseModal>
            </ModalHeader>

            <ScrollView>
              <LabelFormBlack>{I18n.t('lineTerms1')}</LabelFormBlack>
              <LabelFormBlack>{I18n.t('lineTerms2')}</LabelFormBlack>
              <LabelFormBlack>{I18n.t('lineTerms3')}</LabelFormBlack>
              <LabelFormBlack>{I18n.t('lineTerms4')}</LabelFormBlack>
              <LabelFormBlack>{I18n.t('lineTerms5')}</LabelFormBlack>
              <LabelFormBlack>{I18n.t('lineTerms6')}</LabelFormBlack>
              <LabelFormBlack>{I18n.t('lineTerms7')}</LabelFormBlack>
              <LabelFormBlack>{I18n.t('lineTerms8')}</LabelFormBlack>
              <LabelFormBlack>{I18n.t('lineTerms9')}</LabelFormBlack>
              <LabelFormBlack>{I18n.t('lineTerms10')}</LabelFormBlack>
              <LabelFormBlack>{I18n.t('lineTerms11')}</LabelFormBlack>
              <LabelFormBlack>{I18n.t('lineTerms12')}</LabelFormBlack>
              <LabelFormBlack>{I18n.t('lineTerms13')}</LabelFormBlack>
              <LabelFormBlack style={{marginBottom: 200}}>A {this.companyName} não está envolvida em qualquer tipo de pagamento ou premiação, todo pagamento será realizado por conta da empresa Atomik e Kizo.</LabelFormBlack>
            </ScrollView>
            
           
          </ModalWrapper>
        </ModalTerms>
      </>
    )
  }

  
}

const mapStateToProps = state => ({
  updateUserInfo: state.userInfo.updateUserInfo,
  addListRequest: state.userInfo.addListRequest,
  updateListsTitleRequest: state.userInfo.updateListsTitleRequest,
  updatePs: state.userInfo.updatePs,
  lists: state.userInfo.lists,
  user: state.userInfo.user,
  language: state.userInfo.user.language,
  updateIsLogged: state.userInfo.updateIsLogged,
  updateToken: state.userInfo.updateToken,
  updateDeviceLanguage: state.userInfo.updateDeviceLanguage,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(UserInfoActions, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Login);