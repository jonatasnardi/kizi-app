import styled from 'styled-components/native';
import Colors from '../../utils/colors';
import { Type } from '../../utils/layout';
import { Dimensions } from 'react-native';
const imageWidth = Dimensions.get('window').width / 2.5;

export const Wrapper = styled.View`
  ${Type.wrapper};
  flex: 1;
  background: ${Colors.primary};
  justify-content: center;
`;

export const Fieldset = styled.View`
  flex-direction: column;
  margin-bottom: 20px;
`;

export const ContainerBtnFacebook = styled.View`
  flex-direction: column;
  align-items: center;
  width: 100%;
`;



export const LabelForm = styled.Text`
  ${Type.body};
  color: ${Colors.white};
  font-size: 18px;
  margin-bottom: 10px;
`;

export const InputName = styled.TextInput`
  ${Type.body};
  padding-bottom: 5px;
  font-size: 26px;
  color: ${Colors.white};
  border-bottom-color: ${Colors.white};
  border-bottom-width: 2px;
`;

export const LabelFormDark = styled.Text`
  ${Type.body};
  color: ${Colors.primary};
  font-size: 18px;
  margin-bottom: 10px;
`;

export const LabelFormBlack = styled.Text`
  ${Type.body};
  color: ${Colors.primary};
  font-size: 18px;
  margin-bottom: 10px;
`;

export const InputEmail = styled.TextInput`
  ${Type.body};
  padding-bottom: 5px;
  font-size: 20px;
  color: ${Colors.primary};
  border-bottom-color: ${Colors.primary};
  border-bottom-width: 2px;
`;

export const InputPassword = styled.TextInput`
  ${Type.body};
  padding-bottom: 5px;
  font-size: 20px;
  color: ${Colors.primary};
  border-bottom-color: ${Colors.primary};
  border-bottom-width: 2px;
`;

export const InputConfirmPassword = styled.TextInput`
  ${Type.body};
  padding-bottom: 5px;
  font-size: 20px;
  color: ${Colors.primary};
  border-bottom-color: ${Colors.primary};
  border-bottom-width: 2px;
`;

export const BoxAdditionalInfo = styled.View`
  align-items: center;
  margin: 15px 0;
`;


export const ModalEmail = styled.Modal`
  ${Type.wrapper};
`;

export const ModalWrapper = styled.View`
  ${Type.wrapper};
`;

export const ContainerLogo = styled.View`
  align-items: center;
  width: 100%;
  margin-top: 50px;
`;

export const ImageLogo = styled.Image`
  width: ${imageWidth};
  height: ${imageWidth};
`;

export const ModalHeader = styled.View`
  margin-top: 20px;
  flex-direction: row;
  justify-content: flex-end;
  align-items: center;
  margin-bottom: 30px;
  padding-bottom: 10px;
`;

export const ButtonCloseModal = styled.TouchableOpacity`

`;

export const ButtonGeneric = styled.TouchableOpacity`

`;

export const ModalTerms = styled.Modal`
  ${Type.wrapper};
`;

export const ModalPassword = styled.Modal`
  ${Type.wrapper};
`;

export const ModalLogin = styled.Modal`
  ${Type.wrapper};
`;

export const InputLoginEmail = styled.TextInput`
  ${Type.body};
  padding-bottom: 5px;
  font-size: 20px;
  color: ${Colors.primary};
  border-bottom-color: ${Colors.primary};
  border-bottom-width: 2px;
`;

export const InputLoginPassword = styled.TextInput`
  ${Type.body};
  padding-bottom: 5px;
  font-size: 20px;
  color: ${Colors.primary};
  border-bottom-color: ${Colors.primary};
  border-bottom-width: 2px;
`;

export const ModalTitle = styled.Text`
  ${Type.body};
  color: ${Colors.primary};
  flex: 1;
  font-size: 22px;
`;