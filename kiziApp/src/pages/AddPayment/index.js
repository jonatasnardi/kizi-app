import React, {Component} from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Icon from 'react-native-vector-icons/FontAwesome5';
import RadioGroup from 'react-native-radio-buttons-group';
import ImagePicker from 'react-native-image-picker';

import { Creators as UserInfoActions } from '../../store/ducks/userInfo';

import { 
  Wrapper,
  WrapperInner,
  LabelTitleTopic,
  Row,
  SmallTitle,
  LabelPoints,
  LabelPosition,
  Fieldset,
  LabelForm,
  InputName,
  ImageWithContent,
  ImageContainer,
  ImageOverlay,
  ImageAction,
  ImageBlock,
} from './styles';
import { numberToReal, getFirstWord, showAlert, showToast } from '../../utils/functions';
import I18n from '../../utils/translation/i18n';
import ButtonPrimary from '../../components/buttons/ButtonPrimary';
import FullLoader from '../../components/FullLoader';
import Colors from '../../utils/colors';
import { ScrollView, FlatList, Alert, View, Switch } from 'react-native';
import { requestPayment } from '../../services/payments';
import ButtonSecondary from '../../components/buttons/ButtonSecondary';
import { getUserRankingList, createLeague } from '../../services/leagues';
import Images from '../../utils/images';
import { addSuggestionQuestion } from '../../services/questions';

class AddPayment extends Component {
  state = {
    league: {
      title: '',
      award: 0.0015,
      userQtd: 5,
      createdBy: '',
      priceToJoin: 0,
    },
    payload: {
      user: '',
      fullName: '',
      bank: '',
      agency: '',
      account: '',
      cpf: '',
    },
    switchValueHasImage: false,
    isLoadingAdd: false,
    isLoadingImage: false,
  }

  navigate;

  refInputName;
  refInputCpf;
  refInputAccount;
  refInputBank;
  refInputAgency;
  refInputType;

  constructor(props) {
    super(props);

    this.navigate = this.props.navigation;
  }

  componentDidMount() {
    const { user, bankInfo } = this.props;

    this.setState({
      payload: {
        ...this.state.payload,
        user: user._id,
        fullName: bankInfo.fullName,
        bank: bankInfo.bank,
        agency: bankInfo.agency,
        account: bankInfo.account,
        cpf: bankInfo.cpf,
        type: bankInfo.type,
      },
    });  
    
  }

  confirmAdd = () => {
    const { league, payload } = this.state;
    const { user } = this.props;

    this.setState({
      isLoadingAdd: true,
    });
    
    if (user.money < 30) {
      this.setState({
        isLoadingAdd: false,
      });
      return showAlert('Dinheiro insuficiente', 'Conforme informado, é necessário ter pelo menos R$ 30,00 acumulados para poder solicitar o pagamento');
    }

    this.setState({
      isLoadingAdd: true,
    });

    this.props.updateBankInfo(payload);

    requestPayment(user._id, payload)
      .then((res) => { 
        this.setState({
          isLoadingAdd: false,
        });

        showAlert('Pagamento solicitado', 'Pagamento solicitado com sucesso! O pagamento será realizado dentro de 5 dias úteis :)');
      })
      .catch((error) => {
        this.setState({
          isLoadingAdd: false
        });
        showToast(error.data.message);
      }); 

    
  }

  isFormValid = () => {
    const { payload } = this.state;
    let valid = false;

    if (
      payload.cpf.length >= 10 &&
      payload.bank.length >= 2 &&
      payload.agency.length >= 3 &&
      payload.account.length >= 5 &&
      payload.fullName.length >= 6
      ) {
        valid = true;
      }
    
    return valid;
  }

  render() {
    const { user } = this.props;

    return (
      <>
        <Wrapper>
          <ScrollView>
            <WrapperInner>
              <View>
                <LabelTitleTopic>Preencha os campos abaixo receber o seu pagamento.</LabelTitleTopic>
              </View>

              <Fieldset>
                <LabelForm>Seu nome completo:</LabelForm>

                <InputName
                  ref={ref => (this.refInputName = ref)}
                  value={this.state.payload.fullName}
                  onChangeText={(text) => this.setState({payload: { ...this.state.payload, fullName: text}})}
                  placeholder={'Informe aqui o seu nome completo...'}
                  placeholderTextColor={'#778aa5'}
                  multiline={false}>
                </InputName>
              </Fieldset>

              <Fieldset>
                <LabelForm>Banco:</LabelForm>

                <InputName
                  ref={ref => (this.refInputBank = ref)}
                  value={this.state.payload.bank}
                  onChangeText={(text) => this.setState({payload: { ...this.state.payload, bank: text}})}
                  placeholder={'Ex: Banco Santander'}
                  placeholderTextColor={'#778aa5'}
                  multiline={false}>
                </InputName>
              </Fieldset>

              <Fieldset>
                <LabelForm>Tipo de conta:</LabelForm>
                <InputName
                  ref={ref => (this.refInputType = ref)}
                  value={this.state.payload.type}
                  onChangeText={(text) => this.setState({payload: { ...this.state.payload, type: text}})}
                  placeholder={'Corrente ou poupança?'}
                  placeholderTextColor={'#778aa5'}
                  multiline={false}>
                </InputName>
              </Fieldset>

              <Fieldset>
                <LabelForm>Agência:</LabelForm>
                <InputName
                  ref={ref => (this.refInputAgency = ref)}
                  value={this.state.payload.agency}
                  onChangeText={(text) => this.setState({payload: { ...this.state.payload, agency: text}})}
                  placeholder={'Ex: 0001'}
                  placeholderTextColor={'#778aa5'}
                  multiline={false}>
                </InputName>
              </Fieldset>

              <Fieldset>
                <LabelForm>Conta (com o dígito):</LabelForm>
                <InputName
                  ref={ref => (this.refInputAccount = ref)}
                  value={this.state.payload.account}
                  onChangeText={(text) => this.setState({payload: { ...this.state.payload, account: text}})}
                  placeholder={'Ex: 0123456789'}
                  placeholderTextColor={'#778aa5'}
                  multiline={false}>
                </InputName>
              </Fieldset>

              <Fieldset>
                <LabelForm>CPF (solicitado por alguns bancos):</LabelForm>
                <InputName
                  ref={ref => (this.refInputCpf = ref)}
                  value={this.state.payload.cpf}
                  onChangeText={(text) => this.setState({payload: { ...this.state.payload, cpf: text}})}
                  placeholder={'Ex: 0123456789'}
                  placeholderTextColor={'#778aa5'}
                  multiline={false}>
                </InputName>
              </Fieldset>

              
              <ButtonPrimary
                onPress={() => this.confirmAdd()}
                title={'Finalizar pedido'}
                disabled={!this.isFormValid()}
                backgroundColor={!(this.isFormValid()) ? Colors.lightSecondary : Colors.secondary}
                icon={'chevron-right'}>
              </ButtonPrimary>

              <View style={{ marginBottom: 200 }}/>
            </WrapperInner>
          </ScrollView>
          
        </Wrapper>

        {
          this.state.isLoadingAdd && (
            <FullLoader></FullLoader>
          )
        }
       </>
    )
  }
}

const mapStateToProps = state => ({
  addListRequest: state.userInfo.addListRequest,
  updateBankInfo: state.userInfo.updateBankInfo,
  lists: state.userInfo.lists,
  user: state.userInfo.user,
  bankInfo: state.userInfo.bankInfo,
  language: state.userInfo.user.language,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(UserInfoActions, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(AddPayment);