import React, {Component} from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Icon from 'react-native-vector-icons/FontAwesome5';

import { Creators as UserInfoActions } from '../../store/ducks/userInfo';

import { 
  Wrapper,
  WrapperInner,
  ListContainer,
  Row,
  SmallTitle,
  LabelPoints,
  LabelPosition,
  ImageIconUser,
  ContainerBanner,
  Fieldset,
  InputName,
} from './styles';
import { numberToReal, getFirstWord, showAlert, showToast, isIntoMinutes, normalizarString } from '../../utils/functions';
import I18n from '../../utils/translation/i18n';
import ButtonPrimary from '../../components/buttons/ButtonPrimary';
import FullLoader from '../../components/FullLoader';
import Colors from '../../utils/colors';
import { Alert, View, ScrollView, FlatList, Platform, TouchableOpacity, Image } from 'react-native';
import { requestPayment } from '../../services/payments';
import ButtonSecondary from '../../components/buttons/ButtonSecondary';
import { getUserRankingList, getGeneralRanking, getRankingWinnersWeekly } from '../../services/leagues';
import Images from '../../utils/images';
import { getFollowers, getFollowing } from '../../services/follower';
import { searchUsers } from '../../services/user';
import {fetch} from 'react-native-ssl-pinning';
import { BASE_URL_API } from '../../helpers/variables';

class SearchList extends Component {
  state = {
    bkpUsers: [],
    users: [],
    isLoadingAdd: false,
    seeGeneralRanking: true,
    rankingSemanalData: [],
    isFollowers: true,
    searchText: '',
    typing: false,
    typingTimeout: 0,
    name: '',
  }

  navigate;

  refInputSuggestion;
  refInputSearch

  constructor(props) {
    super(props);

    this.navigate = this.props.navigation;
    this.filterUsers = this.filterUsers.bind(this);
  }

  componentDidMount() {
  }

  searchUsers = (text) => {


    if (text.length > 2) {    
      this.setState({
        isLoadingAdd: true,
      });
      
      fetch(`${BASE_URL_API}/user/search-users/F9dlP1s0124-/${text}`, {
        method: "GET" ,
        timeoutInterval: 30000,
        sslPinning: {
            certs: ["mycert"],
        },
        headers:{
          Authorization: `Bearer ${this.props.auth.token}`,
        }
      })
      .then(res => {
        // console.tron.log(res)
        res = JSON.parse(res.bodyString);

        this.setState({
          users: res.users,
          isLoadingAdd: false,
        });
      })
      .catch(err => {
        // console.tron.log(error);
        showToast(I18n.t('errorGenericMessage'));
        this.setState({
          isLoadingAdd: false,
        });
      })


      // searchUsers(text)
      //   .then((res) => { 
      //     this.setState({
      //       users: res.users,
      //       isLoadingAdd: false,
      //     });
      //   })
      //   .catch((error) => {
      //     console.log(error);
      //     showToast(I18n.t('errorGenericMessage'));
      //     this.setState({
      //       isLoadingAdd: false,
      //     });
      //   });
    } else {
      this.setState({
        users: [],
      });
    }

   
  }

  filterUsers = (text) => {
    const self = this;

    if (self.state.typingTimeout) {
       clearTimeout(self.state.typingTimeout);
    }

    self.setState({
       name: text,
       searchText: text,
       typing: false,
       typingTimeout: setTimeout(function () {
          self.searchUsers(self.state.name);
         }, 1200)
    });
   
  }

  render() {
    const { users, seeGeneralRanking, rankingSemanalData, isFollowers } = this.state;
    const { user } = this.props;

    let listToLoad = users;

    return (
      <>
        <Wrapper>
          <ScrollView>
            <WrapperInner>

              <Fieldset>
                <InputName
                  ref={ref => (this.refInputSearch = ref)}
                  value={this.state.searchText}
                  onChangeText={(text) => this.filterUsers(text)}
                  placeholder={'Pesquisar...'}
                  placeholderTextColor={'#566b89'}
                  maxLength={60}>
                </InputName>
              </Fieldset>
              {
                (users.length > 0) && (
                  <Row style={{ justifyContent: 'center', marginTop: 0 }}>
                    <LabelPosition style={{ color: Colors.secondary, fontSize: 22, marginBottom: 15 }}>Resultado</LabelPosition>
                  </Row>
                )
              }
             

              {
                (users.length > 0) ? (
                  <>
                    <Row>
                      <SmallTitle style={{ top: 1 }}>Usuário</SmallTitle>
                    </Row>
                    <FlatList
                      data={listToLoad}
                      renderItem={({ item, index }) => {
                        let baseItem = item;
                      
                        if (baseItem) {
                          return (
                            <TouchableOpacity onPress={() => this.navigate.push('FriendProfile', {
                              idUser: baseItem._id,
                            })}>
                              <ListContainer>
                                <Row>
                                  {
                                    !baseItem.image ? (
                                      <ImageIconUser source={Images['1']} />
                                    ) : (
                                      <ImageIconUser source={{uri: baseItem.image}} />
                                    )
                                  }
                                  {
                                    (baseItem.lastAccess && isIntoMinutes(30, baseItem.lastAccess)) && (
                                      <View style={{ backgroundColor: Colors.green, position: 'absolute', left: 40, bottom: 2, width: 14, height: 14, borderRadius: 14 }} />
                                    )
                                  }
                                  <View style={{ flexDirection: 'column', marginTop: -13}}>
                                    <View style={{ flexDirection: 'row' }}>
                                      <SmallTitle>{ baseItem.name }</SmallTitle>
                                      {
                                        baseItem.verified && (
                                          <Image style={{ width: 18, height: 18, position: 'relative', top: 8 }} source={require('../../assets/verified.png')} />
                                        )
                                      }
                                      
                                    </View>
                                    <SmallTitle>@{ baseItem.username }</SmallTitle>
                                  </View>
                                  
                                  <View style={{ position: 'absolute', right: 15, top: 12 }}>
                                    <Icon name="chevron-right" size={20} color={Colors.secondary} />
                                  </View>
                                </Row>
                              </ListContainer>
                            </TouchableOpacity>
                          )
                        } else {
                          return null
                        }
                      }
                      }
                      
                    />
                  </>
                ) : (
                  <SmallTitle style={{ textAlign: 'center' }}>Nenhum resultado encontrado</SmallTitle>
                )
              }
              
              <View style={{ marginBottom: 200 }} />
            </WrapperInner>
          </ScrollView>
          
        </Wrapper>

        {
          this.state.isLoadingAdd && (
            <FullLoader></FullLoader>
          )
        }
       </>
    )
  }
}

const mapStateToProps = state => ({
  addListRequest: state.userInfo.addListRequest,
  updateListsTitleRequest: state.userInfo.updateListsTitleRequest,
  lists: state.userInfo.lists,
  user: state.userInfo.user,
  auth: state.userInfo.auth,
  language: state.userInfo.user.language,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(UserInfoActions, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(SearchList);