import React, {Component} from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { Creators as UserInfoActions } from '../../store/ducks/userInfo';



import { 
  Wrapper,
  LabelForm,
  Fieldset,
} from './styles';
import Colors from '../../utils/colors';
import { Platform } from 'react-native';
class PaymentRules extends Component {
  companyName = Platform.select({
    android: 'Google',
    ios: 'Apple',
  }); 

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    
  }

  render() {

    return (
      <>
        <Wrapper>
          
          <Fieldset>
            <LabelForm>Regras para receber a premiação e pagamento:</LabelForm>
          </Fieldset>

          <Fieldset>
            <LabelForm style={{ fontSize: 20, color: Colors.primary } }>O pagamento só poderá ser solicitado caso você tenha R$ 30,00 ou mais na sua conta do jogo. Caso contrário, terá que juntar até que possa ter o pagamento liberado.</LabelForm>
            <LabelForm style={{ fontSize: 20, color: Colors.primary } }>O pagamento das premiações será realizado através das informações do banco informadas ao solicitar o pagamento e será realizado dentro de um prazo de até 10 dias úteis para o pagamento após a solicitação.</LabelForm>
            <LabelForm style={{ fontSize: 20, color: Colors.primary } }>Somente serão aceitos bancos que possuem funcionamento no Brasil.</LabelForm>
            <LabelForm style={{ fontSize: 20, color: Colors.primary } }>A {this.companyName} não está envolvida em qualquer tipo de pagamento ou premiação, todo pagamento será realizado por conta da empresa Atomik e Kizo.</LabelForm>
          </Fieldset>
    
        </Wrapper>

       </>
    )
  }
}

const mapStateToProps = state => ({
  addListRequest: state.userInfo.addListRequest,
  updateListsTitleRequest: state.userInfo.updateListsTitleRequest,
  lists: state.userInfo.lists,
  user: state.userInfo.user,
  language: state.userInfo.user.language,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(UserInfoActions, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(PaymentRules);