import React, {Component} from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Icon from 'react-native-vector-icons/FontAwesome5';
import CountDown from 'react-native-countdown-component';

import {
  AdMobBanner,
} from 'react-native-admob';

import { Creators as UserInfoActions } from '../../store/ducks/userInfo';

import { 
  Wrapper,
  WrapperInner,
  ListContainer,
  Row,
  SmallTitle,
  LabelPoints,
  LabelPosition,
  ImageIconUser,
  ContainerBanner,
} from './styles';
import { numberToReal, getFirstWord, showAlert, showToast, cutString, isIntoMinutes } from '../../utils/functions';
import I18n from '../../utils/translation/i18n';
import ButtonPrimary from '../../components/buttons/ButtonPrimary';
import FullLoader from '../../components/FullLoader';
import Colors from '../../utils/colors';
import { Alert, View, ScrollView, FlatList, Platform, TouchableOpacity } from 'react-native';
import { requestPayment } from '../../services/payments';
import ButtonSecondary from '../../components/buttons/ButtonSecondary';
import { getUserRankingList, getGeneralRanking, getRankingWinnersWeekly } from '../../services/leagues';
import Images from '../../utils/images';

class GeneralRanking extends Component {
  state = {
    users: [],
    isLoadingAdd: false,
    seeGeneralRanking: true,
    rankingSemanalData: [],
  }

  navigate

  refInputSuggestion;

  bottomBannerId = Platform.select({
    android: 'ca-app-pub-4982456847713514/5350850363',
    ios: 'ca-app-pub-4982456847713514/5542422057',
  }); 

  constructor(props) {
    super(props);

    this.navigate = this.props.navigation;
  }

  componentDidMount() {
    this.setState({
      isLoadingAdd: true,
    });

    getGeneralRanking()
      .then((res) => { 
        this.setState({
          users: res.users,
          isLoadingAdd: false,
        });
      })
      .catch((error) => {
        console.log(error);
        showToast(I18n.t('errorGenericMessage'));
        this.setState({
          isLoadingAdd: false,
        });
      });

    
  }

  loadRankingByTitle = () => {
    this.setState({
      isLoadingAdd: true,
    });
    getRankingWinnersWeekly()
      .then((res) => { 
        this.setState({
          rankingSemanalData: res.users,
          isLoadingAdd: false,
        });
      })
      .catch((error) => {
        console.log(error);
        showToast(I18n.t('errorGenericMessage'));
        this.setState({
          isLoadingAdd: false,
        });
      });
  }

  switchSeeGeneralRanking = (value) => {
    if (!value && this.state.rankingSemanalData.length === 0) {
      this.loadRankingByTitle();
    }

    this.setState({
      seeGeneralRanking: value,
    })
  }

  render() {
    const { users, seeGeneralRanking, rankingSemanalData } = this.state;
    const { user } = this.props;

    const listToLoad = seeGeneralRanking ? users : rankingSemanalData;

    return (
      <>
        <Wrapper>
          <ScrollView>
            <WrapperInner>
              <View style={{ marginBottom: 20 }}>
                <ButtonPrimary
                  onPress={() => this.switchSeeGeneralRanking(true)}
                  icon={'medal'}
                  title={'Ver ranking geral'}>
                </ButtonPrimary>
              </View>
              <View style={{ marginBottom: 20 }}>
                <ButtonPrimary
                  onPress={() => this.switchSeeGeneralRanking(false)}
                  icon={'trophy'}
                  title={'Ranking de campeões do semanal'}>
                </ButtonPrimary>
              </View>

              <Row style={{ justifyContent: 'center', marginTop: 10 }}>
                <LabelPosition style={{ color: Colors.primary, fontWeight: 'bold' }}>Você tem { user.points } pontos</LabelPosition>
              </Row>

              <Row style={{ justifyContent: 'center', marginTop: 10 }}>
                <LabelPosition style={{ color: Colors.primary }}>TOP 100 GERAL</LabelPosition>
              </Row>

              <Row>
                <LabelPosition style={{ top: 1 }}>Pos.</LabelPosition>
                <SmallTitle style={{ top: 1 }}>Usuário</SmallTitle>
                <LabelPoints style={{ top: 1 }}>{ seeGeneralRanking ? 'Pontos' : 'Títulos' }</LabelPoints>
              </Row>
              <FlatList
                data={listToLoad}
                renderItem={({ item, index }) => (
                  <TouchableOpacity onPress={() => this.navigate.push('FriendProfile', {
                    idUser: item._id,
                  })}>
                    <ListContainer>
                      <Row>
                        <LabelPosition>{ index + 1 }º</LabelPosition>
                        {
                          !item.image ? (
                            <ImageIconUser source={Images['1']} />
                          ) : (
                            <ImageIconUser source={{uri: item.image}} />
                          )
                        }
                        {
                          (item.lastAccess && isIntoMinutes(30, item.lastAccess)) && (
                            <View style={{ backgroundColor: Colors.green, position: 'absolute', left: (index > 9) ? 60 : 55, bottom: 2, width: 14, height: 14, borderRadius: 14 }} />
                          )
                        }
                        <SmallTitle>{ cutString(item.name) }</SmallTitle>
                        <LabelPoints>{ seeGeneralRanking ? item.points : item.weekLeagueTitles }</LabelPoints>
                      </Row>
                    </ListContainer>
                  </TouchableOpacity>
                )}
              />
              <View style={{ marginBottom: 200 }} />
            </WrapperInner>
          </ScrollView>
          
        </Wrapper>

        {
          this.state.isLoadingAdd && (
            <FullLoader></FullLoader>
          )
        }

        <ContainerBanner>
          <AdMobBanner
            adSize="fullBanner"
            adUnitID={this.bottomBannerId}
            height={90}
          />
        </ContainerBanner>
       </>
    )
  }
}

const mapStateToProps = state => ({
  addListRequest: state.userInfo.addListRequest,
  updateListsTitleRequest: state.userInfo.updateListsTitleRequest,
  lists: state.userInfo.lists,
  user: state.userInfo.user,
  language: state.userInfo.user.language,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(UserInfoActions, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(GeneralRanking);