import React, {Component} from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Icon from 'react-native-vector-icons/FontAwesome5';
import debounce from 'lodash.debounce';

import { Creators as UserInfoActions } from '../../store/ducks/userInfo';

import {
  AdMobRewarded,
} from 'react-native-admob';

import { 
  Wrapper,
  BigBox,
  SubtitleText,
  TextMoney,
  LabelForm,
  ContainerMainBanner,
  LabelFormMin,
  WrapperInner,
  LabelTitleTopic,
  LabelDescriptionTopic,
  LabelAwards,
} from './styles';
import { numberToReal, getFirstWord, showAlert, getRandomNumber } from '../../utils/functions';
import ButtonPrimary from '../../components/buttons/ButtonPrimary';
import FullLoader from '../../components/FullLoader';
import Colors from '../../utils/colors';
import { Alert, View, ScrollView } from 'react-native';
import { requestPayment } from '../../services/payments';
import ButtonSecondary from '../../components/buttons/ButtonSecondary';
import { CURRENT_DIVISION_NAME } from '../../utils/enums';

const withPreventDoubleClick = (WrappedComponent) => {

  class PreventDoubleClick extends React.PureComponent {

    debouncedOnPress = () => {
      this.props.onPress && this.props.onPress();
    }

    onPress = debounce(this.debouncedOnPress, 300, { leading: true, trailing: false });

    render() {
      return <WrappedComponent {...this.props} onPress={this.onPress} />;
    }
  }

  PreventDoubleClick.displayName = `withPreventDoubleClick(${WrappedComponent.displayName ||WrappedComponent.name})`
  return PreventDoubleClick;
}

const ButtonPrimaryEx = withPreventDoubleClick(ButtonPrimary);


class DailyTournament extends Component {
  state = {
    textSuggestion: '',
    isLoadingAdd: false,
    switchValue: true,
    currentDivision: '1',
  }

  disabled = false;

  rewardedBanner = Platform.select({
    android: 'ca-app-pub-4982456847713514/9290095372',
    ios: 'ca-app-pub-4982456847713514/4229340388',
  });

  refInputSuggestion;

  constructor(props) {
    super(props);

    this.navigate = this.props.navigation;
  }

  async componentDidMount() {
    AdMobRewarded.addEventListener('rewarded', reward => {
      this.setState({
        isLoadingAdd: false,
      });
      this.props.updatePlayCount(0);
      this.navigate.push('Question', { tournamentType: 'daily' });
      AdMobRewarded.removeAllListeners();
    });
  }

  playnow = () => {
    this.disabled = true;

    setTimeout(()=>{
      this.disabled = false;
    }, 9000);
    if (this.props.playCount >= 30) {
      this.setState({
        isLoadingAdd: true,
      });
      
      AdMobRewarded.setAdUnitID(this.rewardedBanner);
      AdMobRewarded.requestAd().then(() => {
        this.setState({
          isLoadingAdd: false,
        });
        AdMobRewarded.showAd();
      }).catch(error => {
        this.setState({
          isLoadingAdd: false,
        });
        this.props.updatePlayCount(5);
        // this.navigate.push('Question', { tournamentType: 'daily' });
      });
  
      
    } else {
      this.props.updatePlayCount((this.props.playCount || 0) + 1);
      this.navigate.push('Question', { tournamentType: 'daily' });
    }
    
  }


  render() {
    const { user } = this.props;

    return (
      <>
        <Wrapper>
          <ContainerMainBanner>
            <LabelForm>Só depende de você :){'\n'}Todo dia tem um vencedor!</LabelForm>
            <BigBox>
              <TextMoney>VALENDO R$ { numberToReal(this.props.awardDaily)}</TextMoney>
            </BigBox>
            <LabelFormMin>Prêmio válido apenas {'\n'}para a 1ª divisão</LabelFormMin>
          </ContainerMainBanner>

          <ScrollView>
            {
              this.props.showDaily ? (
              <WrapperInner>
              
                <ButtonPrimaryEx
                  onPress={() => this.playnow()}
                  title={'JOGAR AGORA'}
                  icon={'gamepad'}
                  backgroundColor={Colors.blue}>
                </ButtonPrimaryEx>

                <View style={{ marginTop: 10 }} />

                <ButtonPrimary
                  onPress={() => this.navigate.push('DailyRanking')}
                  backgroundColor={Colors.primary}
                  title={`Ver ${CURRENT_DIVISION_NAME[this.props.user.division]}`}
                  icon={'trophy'}>
                </ButtonPrimary>

                <LabelTitleTopic>Premiação, promoções e rebaixamentos</LabelTitleTopic>
                <LabelTitleTopic style={{ color: Colors.primary }}>Você está na {CURRENT_DIVISION_NAME[this.props.user.division]}</LabelTitleTopic>
                {
                  this.props.user.division == '0' && (
                    <>
                      <View style={{ flexDirection: 'row', marginTop: 10 }}>
                        <Icon style={{marginRight: 4}} name="trophy" size={24} color={Colors.secondary} />
                        <LabelAwards>1º ao 100º lugar - vaga na 1ª divisão</LabelAwards>
                      </View>
                      <View style={{ flexDirection: 'row', marginTop: 10 }}>
                        <Icon style={{marginRight: 4}} name="trophy" size={24} color={Colors.secondary} />
                        <LabelAwards>101º ao 400º lugar - vaga na 2ª divisão</LabelAwards>
                      </View>
                      <View style={{ flexDirection: 'row', marginTop: 10 }}>
                        <Icon style={{marginRight: 4}} name="trophy" size={24} color={Colors.secondary} />
                        <LabelAwards>401º ao 1000º lugar - vaga na 3ª divisão</LabelAwards>
                      </View>
                      <View style={{ flexDirection: 'row', marginTop: 10 }}>
                        <Icon style={{marginRight: 4}} name="trophy" size={24} color={Colors.secondary} />
                        <LabelAwards>1001º pra trás - vaga na 4ª divisão</LabelAwards>
                      </View>
                    </>
                  )
                }

                {
                  this.props.user.division == '1' && (
                    <>
                      <View style={{ flexDirection: 'row', marginTop: 10 }}>
                        <Icon style={{marginRight: 4}} name="trophy" size={24} color={'#f8bc44'} />
                        <LabelAwards>1º Lugar - { numberToReal(this.props.awardDaily)} + 25</LabelAwards>
                        <Icon style={{marginLeft: 4}} name="coins" size={20} color={Colors.yellow} />
                      </View>
                      <View style={{ flexDirection: 'row', marginTop: 10 }}>
                        <Icon style={{marginRight: 4}} name="trophy" size={24} color={'#d6d6d6'} />
                        <LabelAwards>2º Lugar - R$ 2,00 + 20</LabelAwards>
                        <Icon style={{marginLeft: 4}} name="coins" size={20} color={Colors.yellow} />
                      </View>
                      <View style={{ flexDirection: 'row', marginTop: 10 }}>
                        <Icon style={{marginRight: 4}} name="trophy" size={24} color={'#e9a051'} />
                        <LabelAwards>3º Lugar - R$ 1,00 + 20</LabelAwards>
                        <Icon style={{marginLeft: 4}} name="coins" size={20} color={Colors.yellow} />
                      </View>
                      <View style={{ flexDirection: 'row', marginTop: 10 }}>
                        <Icon style={{marginRight: 4}} name="trophy" size={24} color={Colors.secondary} />
                        <LabelAwards>4º ao 10º Lugar - 15</LabelAwards>
                        <Icon style={{marginLeft: 4}} name="coins" size={20} color={Colors.yellow} />
                      </View>
                      <View style={{ flexDirection: 'row', marginTop: 10 }}>
                        <Icon style={{marginRight: 4}} name="arrow-down" size={24} color={Colors.red} />
                        <LabelAwards>Os 20 últimos caem para a 2ª divisão</LabelAwards>
                      </View>
                    </>
                  )
                }

                {
                  this.props.user.division == '2' && (
                    <>
                      <View style={{ flexDirection: 'row', marginTop: 10 }}>
                        <Icon style={{marginRight: 4}} name="trophy" size={24} color={'#f8bc44'} />
                        <LabelAwards>1º Lugar - R$ 1,00 + 25</LabelAwards>
                        <Icon style={{marginLeft: 4}} name="coins" size={20} color={Colors.yellow} />
                      </View>
                      <View style={{ flexDirection: 'row', marginTop: 10 }}>
                        <Icon style={{marginRight: 4}} name="trophy" size={24} color={Colors.secondary} />
                        <LabelAwards>2º ao 10º Lugar - 15</LabelAwards>
                        <Icon style={{marginLeft: 4}} name="coins" size={20} color={Colors.yellow} />
                      </View>
                      <View style={{ flexDirection: 'row', marginTop: 10 }}>
                        <Icon style={{marginRight: 4}} name="arrow-up" size={24} color={Colors.green} />
                        <LabelAwards>Os 20 primeiros sobem para a 1ª divisão</LabelAwards>
                      </View>
                      <View style={{ flexDirection: 'row', marginTop: 10 }}>
                        <Icon style={{marginRight: 4}} name="arrow-down" size={24} color={Colors.red} />
                        <LabelAwards>Os 50 últimos caem para a 3ª divisão</LabelAwards>
                      </View>
                    </>
                  )
                }

                {
                  this.props.user.division == '3' && (
                    <>
                      <View style={{ flexDirection: 'row', marginTop: 10 }}>
                        <Icon style={{marginRight: 4}} name="trophy" size={24} color={'#f8bc44'} />
                        <LabelAwards>1º Lugar - R$ 1,00 + 25</LabelAwards>
                        <Icon style={{marginLeft: 4}} name="coins" size={20} color={Colors.yellow} />
                      </View>
                      <View style={{ flexDirection: 'row', marginTop: 10 }}>
                        <Icon style={{marginRight: 4}} name="trophy" size={24} color={Colors.secondary} />
                        <LabelAwards>2º ao 10º Lugar - 15</LabelAwards>
                        <Icon style={{marginLeft: 4}} name="coins" size={20} color={Colors.yellow} />
                      </View>
                      <View style={{ flexDirection: 'row', marginTop: 10 }}>
                        <Icon style={{marginRight: 4}} name="arrow-up" size={24} color={Colors.green} />
                        <LabelAwards>Os 50 primeiros sobem para a 2ª divisão</LabelAwards>
                      </View>
                      <View style={{ flexDirection: 'row', marginTop: 10 }}>
                        <Icon style={{marginRight: 4}} name="arrow-down" size={24} color={Colors.red} />
                        <LabelAwards>Os 300 últimos caem para a 4ª divisão</LabelAwards>
                      </View>
                    </>
                  )
                }

                {
                  this.props.user.division == '4' && (
                    <>
                      <View style={{ flexDirection: 'row', marginTop: 10 }}>
                        <Icon style={{marginRight: 4}} name="trophy" size={24} color={'#f8bc44'} />
                        <LabelAwards>1º Lugar - R$ 1,00 + 25</LabelAwards>
                        <Icon style={{marginLeft: 4}} name="coins" size={20} color={Colors.yellow} />
                      </View>
                      <View style={{ flexDirection: 'row', marginTop: 10 }}>
                        <Icon style={{marginRight: 4}} name="trophy" size={24} color={Colors.secondary} />
                        <LabelAwards>2º ao 10º Lugar - 15</LabelAwards>
                        <Icon style={{marginLeft: 4}} name="coins" size={20} color={Colors.yellow} />
                      </View>
                      <View style={{ flexDirection: 'row', marginTop: 10 }}>
                        <Icon style={{marginRight: 4}} name="arrow-up" size={24} color={Colors.green} />
                        <LabelAwards>Os 300 primeiros sobem para a 3ª divisão</LabelAwards>
                      </View>
                    </>
                  )
                }

                
                <LabelTitleTopic>Como funciona?</LabelTitleTopic>
                
                <LabelDescriptionTopic>
                  Cada pergunta vale de 1 a 3 pontos, de acordo com sua respectiva dificuldade.{'\n'}O torneio se inicia todo dia após as 23:00h e finaliza no dia seguinte às 23:00h. A pontuação é acumulada durante todo o dia jogando o torneio. Acompanhe o ganhador todo dia nas redes sociais do Kizo.{'\n'}{'\n'}
                  O valor da premiação será adicionado no jogo e você poderá soliticar o pagamento quando quiser, dentro das regras.
                </LabelDescriptionTopic>

                <LabelTitleTopic>Como será o pagamento?</LabelTitleTopic>
                <View style={{ marginTop: 10 }}/>

                <ButtonPrimary
                  onPress={() => this.navigate.push('PaymentRules')}
                  title={'Regras oficiais'}
                  // disabled={user.money < 30}
                  backgroundColor={Colors.primary}
                  icon={'align-right'}>
                </ButtonPrimary>
                <View style={{ marginBottom: 100 }}/>
              </WrapperInner>
              ) : (
                <LabelDescriptionTopic style={{ textAlign: 'center'}}>
                  Torneio inativo no momento.
                </LabelDescriptionTopic>
              )

            }
            
          </ScrollView>
          
        </Wrapper>

        {
          this.state.isLoadingAdd && (
            <FullLoader></FullLoader>
          )
        }
       </>
    )
  }
}

const mapStateToProps = state => ({
  addListRequest: state.userInfo.addListRequest,
  updateListsTitleRequest: state.userInfo.updateListsTitleRequest,
  lists: state.userInfo.lists,
  awardDaily: state.userInfo.awardDaily,
  updatePlayCount: state.userInfo.updatePlayCount,
  playCount: state.userInfo.playCount,
  user: state.userInfo.user,
  showDaily: state.userInfo.showDaily,
  language: state.userInfo.user.language,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(UserInfoActions, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(DailyTournament);