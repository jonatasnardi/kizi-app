import React, {Component} from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { Creators as UserInfoActions } from '../../store/ducks/userInfo';

import I18n from '../../utils/translation/i18n';

import { Alert, ActivityIndicator, NativeModules, Text, TouchableOpacity, View } from 'react-native';

import { 
  Wrapper,
  InputSuggestion,
  LabelForm,
  Fieldset,
} from './styles';
import Colors from '../../utils/colors';
import { showAlert, showToast } from '../../utils/functions';
import ButtonPrimary from '../../components/buttons/ButtonPrimary';
import FullLoader from '../../components/FullLoader';
import { userContact } from '../../services/user';

class Contact extends Component {
  state = {
    textSuggestion: '',
    isLoadingAdd: false,
  }

  refInputSuggestion;

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    
  }

  addSuggestion = () => {
    const { textSuggestion } = this.state;

    this.setState({
      isLoadingAdd: true
    });

    userContact(textSuggestion, this.props.user._id)
      .then((data) => { 
        this.setState({
          isLoadingAdd: false,
          textSuggestion: '',
        });

        showToast(I18n.t('messageSentSuccesful'));
      })
      .catch((error) => {
        console.log(error);
        this.setState({
          isLoadingAdd: false
        });
        showToast(I18n.t('errorGenericMessage'));
      }); 
    
  }

  render() {
    const { textSuggestion } = this.state;

    return (
      <>
        <Wrapper>
          
          <Fieldset>
            <LabelForm>{I18n.t('suggestionDescription')}</LabelForm>

            <InputSuggestion
              ref={ref => (this.refInputSuggestion = ref)}
              value={textSuggestion}
              onChangeText={(text) => this.setState({textSuggestion:  text})}
              placeholder={I18n.t('typeHere')}
              placeholderTextColor={Colors.primary}
              multiline={true}>
            </InputSuggestion>
          </Fieldset>

          <Fieldset>
            <ButtonPrimary
              disabled={textSuggestion.length === 0}
              onPress={() => this.addSuggestion()}
              title={I18n.t('send')}>
            </ButtonPrimary>
          </Fieldset>
    
        </Wrapper>

        {
          this.state.isLoadingAdd && (
            <FullLoader></FullLoader>
          )
        }

       </>
    )
  }
}

const mapStateToProps = state => ({
  addListRequest: state.userInfo.addListRequest,
  updateListsTitleRequest: state.userInfo.updateListsTitleRequest,
  lists: state.userInfo.lists,
  user: state.userInfo.user,
  language: state.userInfo.user.language,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(UserInfoActions, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Contact);