import styled from 'styled-components/native';
import Colors from '../../utils/colors';
import { Type } from '../../utils/layout';
import { isIphoneX } from '../../utils/functions';


export const Wrapper = styled.View`
  ${Type.wrapper};
  padding: 0;
  flex: 1;
`;

export const WrapperInner = styled.View`
  ${Type.wrapper};
  flex: 1;
`;

export const Fieldset = styled.View`
  flex-direction: column;
  margin-bottom: 20px;
`;

export const LabelForm = styled.Text`
  ${Type.body};
  color: ${Colors.white};
  font-size: 20px;
  margin-top: 10px;
  margin-bottom: 10px;
  text-align: center;
`;

export const LabelTitleTopic = styled.Text`
  ${Type.body};
  color: ${Colors.secondary};
  font-size: 20px;
  margin-top: 20px;
  text-align: left;
`;

export const LabelDescriptionTopic = styled.Text`
  ${Type.body};
  color: ${Colors.primary};
  font-size: 17px;
`;

export const LabelAwards = styled.Text`
  ${Type.body};
  color: ${Colors.primary};
  font-size: 20px;
`;

export const LabelFormMin = styled.Text`
  ${Type.body};
  color: ${Colors.white};
  font-size: 18px;
  margin-bottom: 10px;
  text-align: center;
  width: 100%;
`;

export const BigBox = styled.View`
  width: 100%;
  height: 58px;
`;

export const SubtitleText = styled.Text`
  ${Type.body};
  padding-top: 15px;
  color: ${Colors.primary};
`;

export const TextMoney = styled.Text`
  ${Type.body};
  color: ${Colors.secondary};
  font-size: 32px;
  text-align: center;
`;

export const ContainerMainBanner = styled.View`
  padding: 0px 15px 20px 15px;
  width: 100%;
  height: 180px;
  background: ${Colors.primary};
`;

export const ListContainer = styled.View`
  margin-bottom: 0;
  background: ${Colors.white};
  padding: 10px;
  margin: 5px 0;
  border-width: 1px;
  border-color: ${Colors.primary};
  border-radius: 5px;
  min-height: 30px;
  flex-direction: column;
`;

export const Row = styled.View`
  width: 100%;
  flex-direction: row;
`;

export const SmallTitle = styled.Text`
  ${Type.body};
  color: ${Colors.black};
  font-size: 18px;
  margin-left: 10px;
  position: relative;
  top: 11px;
`;

export const CenteredText = styled.Text`
  ${Type.body};
  color: ${Colors.primary};
  font-size: 18px;
  text-align: center;
`;

export const LabelPosition = styled.Text`
  ${Type.body};
  color: ${Colors.secondary};
  font-size: 18px;
  text-align: center;
  position: relative;
  top: 11px;
`;

export const LabelPoints = styled.Text`
  ${Type.body};
  color: ${Colors.primary};
  font-size: 20px;
  position: absolute;
  right: 10px;
  top: 11px;
`;

export const ImageIconUser = styled.Image`
  width: 45px;
  height: 45px;
  border-radius: 45px;
  margin-left: 4px;
  border: 1px solid ${Colors.primary};
`;

export const ContainerBanner = styled.View`
  width: 100%;
  height: 60px;
  position: absolute;
  bottom: ${isIphoneX() ? 22 : 0};
  left: 0;
`;