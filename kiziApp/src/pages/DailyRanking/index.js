import React, {Component} from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Icon from 'react-native-vector-icons/FontAwesome5';
import CountDown from 'react-native-countdown-component';
import {fetch} from 'react-native-ssl-pinning';
import LottieView from 'lottie-react-native';

import {
  AdMobBanner,
} from 'react-native-admob';

import { Creators as UserInfoActions } from '../../store/ducks/userInfo';

import { 
  Wrapper,
  WrapperInner,
  ListContainer,
  Row,
  SmallTitle,
  LabelPoints,
  LabelPosition,
  ImageIconUser,
  ContainerBanner,
} from './styles';
import { numberToReal, getFirstWord, showAlert, showToast, cutString, isIntoMinutes } from '../../utils/functions';
import I18n from '../../utils/translation/i18n';
import ButtonPrimary from '../../components/buttons/ButtonPrimary';
import FullLoader from '../../components/FullLoader';
import Colors from '../../utils/colors';
import { Alert, View, ScrollView, FlatList, Platform, TouchableOpacity, Image } from 'react-native';
import { requestPayment } from '../../services/payments';
import ButtonSecondary from '../../components/buttons/ButtonSecondary';
import { getUserDailyRankingList } from '../../services/leagues';
import Images from '../../utils/images';
import { BASE_URL_API } from '../../helpers/variables';
import { CURRENT_DIVISION_NAME } from '../../utils/enums';

class DailyRanking extends Component {
  state = {
    users: [],
    isLoadingAdd: false,
    currentDivision: '1',
    position: 0,
  }

  refInputSuggestion;

  bottomBannerId = Platform.select({
    android: 'ca-app-pub-4982456847713514/5350850363',
    ios: 'ca-app-pub-4982456847713514/5542422057',
  }); 

  navigate;

  constructor(props) {
    super(props);

    this.navigate = this.props.navigation;
  }

  componentDidMount() {
    this.setState({
      isLoadingAdd: true,
    });

    fetch(`${BASE_URL_API}/league/list/daily-ranking/DDIOV/${this.props.user.division}/${this.props.user._id}`, {
      method: "GET" ,
      timeoutInterval: 30000,
      sslPinning: {
          certs: ["mycert"],
      },
      headers:{
        Authorization: `Bearer ${this.props.auth.token}`,
      }
    })
    .then(res => {
      console.tron.log(res)
      
      res = JSON.parse(res.bodyString);
      
      this.setState({
        users: res.users,
        position: res.position,
        isLoadingAdd: false,
      }, () => {
        this.showAlertRelegation();
      });
    })
    .catch(err => {
      console.tron.log(err)
        showToast(I18n.t('errorGenericMessage'));
        this.setState({
          isLoadingAdd: false,
        });
    })

    // getUserDailyRankingList()
    //   .then((res) => { 
    //     this.setState({
    //       users: res.users,
    //       isLoadingAdd: false,
    //     });
    //   })
    //   .catch((error) => {
    //     console.log(error);
    //     showToast(I18n.t('errorGenericMessage'));
    //     this.setState({
    //       isLoadingAdd: false,
    //     });
    //   });
  }

  getSecondsToFinish = () => {
    let t1 = new Date();
    let t2 = this.nextDayOfWeek(1);
    t2.setHours(23, 0, 0, 0);
    
    let dif = t1.getTime() - t2.getTime();

    let Seconds_from_T1_to_T2 = dif / 1000;
    let Seconds_Between_Dates = Math.abs(Seconds_from_T1_to_T2);

    if (Math.sign(Seconds_from_T1_to_T2) === 1) {
      return 0;
    } else {
      return Seconds_Between_Dates;
    }
  }

  nextDayOfWeek = (x) => {
    let now = new Date();    
    now.setDate(now.getDate() + (x+(7-now.getDay())) % 7);
    return now;
  }

  getBgColor = (index) => {
    switch (this.props.user.division) {
      case 0:
        
        if (index == 0) {
          return '#f8bc44';
        } else if (index == 1) {
          return '#d6d6d6';
        }  else if (index == 2) {
          return '#e9a051';
        } 
        
        break;
      case 1:
        if (index == 0) {
          return '#f8bc44';
        } else if (index == 1) {
          return '#d6d6d6';
        }  else if (index == 2) {
          return '#e9a051';
        } 
        break;

      case 2:
        
        if (index < 20) {
          return Colors.green;
        } 
        break;

      case 3:
        
        if (index < 50) {
          return Colors.green;
        } 
        break;

      case 4:
        
        if (index < 300) {
          return Colors.green;
        } 
        break;
    
      default:
        break;
    }
  }

  showAlertRelegation = () => {
    let showAlertRel = false;

    if (this.props.user.division == 1) {
      if (this.state.position > 80) {
        showAlertRel = true;
      }
    }

    if (this.props.user.division == 2) {
      if (this.state.position > 250) {
        showAlertRel = true;
      }
    }

    if (this.props.user.division == 3) {
      if (this.state.position > 300) {
        showAlertRel = true;
      }
    }

    if (showAlertRel) {
      showAlert('Zona de rebaixamento', 'Você está na zona de rebaixamento! Pontue para não ser rebaixado!')
    }


  }

  getLabelColor = (index) => {
      return Colors.primary;
  }

  render() {
    const { users } = this.state;
    const { user } = this.props;

    return (
      <>
        <Wrapper>
          <ScrollView>
            <WrapperInner>

              
              <Row style={{  height: 300 }}>
                <LottieView source={require('../../assets/champion.json')} autoPlay loop />
                {/* <Icon name="trophy" size={60} color={Colors.secondary} /> */}
              </Row>


              <Row style={{ justifyContent: 'center', marginTop: 10 }}>
                <LabelPosition style={{ color: Colors.secondary, fontSize: 20, fontWeight: 'bold' }}>{CURRENT_DIVISION_NAME[this.props.user.division]}</LabelPosition>
              </Row>

              <Row style={{ justifyContent: 'center', marginTop: 10 }}>
                <LabelPosition style={{ color: Colors.primary }}>Se encerra em:</LabelPosition>
              </Row>
              <Row style={{ justifyContent: 'center', marginTop: 10 }}>
                <CountDown
                  until={this.getSecondsToFinish()}
                  onFinish={() => {}}
                  onPress={() => {}}
                  size={20}
                  digitStyle={{backgroundColor: Colors.primary}}
                  digitTxtStyle={{color: Colors.secondary}}
                  timeToShow={['H', 'M', 'S']}
                  timeLabels={{h: 'Hora(s)', m: 'Min.', s: 'Seg.'}}
                />
              </Row>

              <Row style={{ justifyContent: 'center', marginTop: 10 }}>
                <LabelPosition style={{ color: Colors.primary }}>Você tem { user.dailyCbPoints } pontos</LabelPosition>
              </Row>

              {
                this.state.position != 0 && (
                  <Row style={{ justifyContent: 'center', marginTop: 3 }}>
                    <LabelPosition style={{ color: Colors.primary }}>Está em { this.state.position }º lugar</LabelPosition>
                  </Row>
                )
              }

              

              {
                this.props.user.division >= 2 && (
                  <Row style={{ flexDirection: 'row',  marginTop: 10, marginBottom: 10 }}>
                    <View style={{ width: 45, height: 25, backgroundColor: Colors.green }} />
                    <SmallTitle style={{ top: 1 }}>Zona de promoção</SmallTitle>
                  </Row>
                )
                
              }

              


              <Row>
                <LabelPosition style={{ top: 1 }}>Pos.</LabelPosition>
                <SmallTitle style={{ top: 1 }}>Usuário</SmallTitle>
                <LabelPoints style={{ top: 1 }}>Pontos</LabelPoints>
              </Row>
              <FlatList
                data={users}
                renderItem={({ item, index }) => (
                  <TouchableOpacity onPress={() => this.navigate.push('FriendProfile', {
                    idUser: item._id,
                  })}>
                    <ListContainer style={{ backgroundColor: this.getBgColor(index) }}>
                      <Row>
                        <LabelPosition style={{ color: this.getLabelColor(index) }}>{ index + 1 }º</LabelPosition>
                        {
                          !item.image ? (
                            <ImageIconUser source={Images['1']} />
                          ) : (
                            <ImageIconUser source={{uri: item.image}} />
                          )
                        }
                        {
                          (item.lastAccess && isIntoMinutes(30, item.lastAccess)) && (
                            <View style={{ backgroundColor: Colors.green, position: 'absolute', left: (index > 9) ? 60 : 55, bottom: 2, width: 14, height: 14, borderRadius: 14 }} />
                          )
                        }
                        
                        <View style={{ flexDirection: 'row' }}>
                          <SmallTitle style={{ fontWeight: this.props.user._id === item._id ? 'bold' : 'normal' }}>{ cutString(item.name) }</SmallTitle>
                          {
                            item.verified && (
                              <Image style={{ width: 18, height: 18, position: 'relative', top: 8 }} source={require('../../assets/verified.png')} />
                            )
                          }
                        </View>
                        
                        <LabelPoints>{ item.dailyCbPoints }</LabelPoints>
                      </Row>
                    </ListContainer>
                  </TouchableOpacity>
                )}
              />
              <View style={{ marginBottom: 200 }} />
            </WrapperInner>
          </ScrollView>
          
        </Wrapper>

        {
          this.state.isLoadingAdd && (
            <FullLoader></FullLoader>
          )
        }

        <ContainerBanner>
          <AdMobBanner
            adSize="fullBanner"
            adUnitID={this.bottomBannerId}
            height={90}
          />
        </ContainerBanner>
       </>
    )
  }
}

const mapStateToProps = state => ({
  addListRequest: state.userInfo.addListRequest,
  updateListsTitleRequest: state.userInfo.updateListsTitleRequest,
  lists: state.userInfo.lists,
  user: state.userInfo.user,
  auth: state.userInfo.auth,
  language: state.userInfo.user.language,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(UserInfoActions, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(DailyRanking);