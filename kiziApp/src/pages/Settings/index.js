import React, {Component} from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { Creators as UserInfoActions } from '../../store/ducks/userInfo';


import I18n from '../../utils/translation/i18n';


import { Alert, ActivityIndicator, NativeModules, Text, TouchableOpacity, View, Switch } from 'react-native';

import { 
  Wrapper,
  InputSuggestion,
  LabelForm,
  Fieldset,
} from './styles';

class Settings extends Component {
  state = {
    textSuggestion: '',
    isLoadingAdd: false,
    switchValue: true,
  }

  refInputSuggestion;

  constructor(props) {
    super(props);
  }

  async componentDidMount() {
    if (this.props.activeSound === undefined) {
      this.props.updateActiveSound(true);
    }
    
    if (this.props.activeSound) {
      this.setState({switchValue: true})
    } else {
      this.setState({switchValue: false})
    }
    
  }

  toggleSwitch = (value) => {
    this.setState({switchValue: value});

    if (value) {
      this.props.updateActiveSound(true);
    } else {
      this.props.updateActiveSound(false);
    }
  }

  render() {
    const { textSuggestion } = this.state;

    return (
      <>
        <Wrapper>
          <Fieldset>
            <LabelForm>Ativar sons?</LabelForm>
            <View style={{flexDirection: 'column', alignItems: 'flex-start', justifyContent: 'flex-start' }}>
              <Switch
                onValueChange = {this.toggleSwitch}
                value = {this.state.switchValue}/>
            </View>
          </Fieldset>
        </Wrapper>
       </>
    )
  }
}

const mapStateToProps = state => ({
  addListRequest: state.userInfo.addListRequest,
  updateListsTitleRequest: state.userInfo.updateListsTitleRequest,
  updateActiveSound: state.userInfo.updateActiveSound,
  activeSound: state.userInfo.activeSound,
  lists: state.userInfo.lists,
  user: state.userInfo.user,
  language: state.userInfo.user.language,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(UserInfoActions, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Settings);